<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr">
<context>
    <name>AboutDlg</name>
    <message>
        <location filename="../gui/ui/aboutDlg.ui" line="17"/>
        <source>massXpert: About</source>
        <translation>massXpert: À propos</translation>
    </message>
    <message>
        <location filename="../gui/ui/aboutDlg.ui" line="31"/>
        <source>About massXpert</source>
        <translation>À propos de massXpert</translation>
    </message>
    <message>
        <location filename="../gui/ui/aboutDlg.ui" line="56"/>
        <location filename="../gui/ui/aboutDlg.ui" line="99"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="../gui/ui/aboutDlg.ui" line="124"/>
        <source>How to cite</source>
        <translation>Citation</translation>
    </message>
    <message>
        <location filename="../gui/ui/aboutDlg.ui" line="130"/>
        <source>Please cite the massXpert program in your articles</source>
        <translation>Merci de citer le programme massXpert dans vos articles</translation>
    </message>
    <message utf8="true">
        <location filename="../gui/ui/aboutDlg.ui" line="136"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:7pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt;Dear User of the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt; font-style:italic; color:#ae0002;&quot;&gt;massXpert&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt; software,&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt; font-style:italic; color:#ae0002;&quot;&gt;massXpert&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt; was developed as part of my work as a staff scientist at the Centre national de la Recherche scientifique (CNRS, France). &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt;If &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt; font-style:italic; color:#ae0002;&quot;&gt;massXpert&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt; is of some use to you, please, do not forget to cite &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt; font-style:italic; color:#ae0002;&quot;&gt;massXpert&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt; in your publications, using the following citation: &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:600; color:#117e56;&quot;&gt;Rusconi, F. massXpert 2: a cross-platform software environment for polymer chemistry modelling and simulation/analysis of mass spectrometric data, &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:600; font-style:italic; color:#117e56;&quot;&gt;Bioinformatics&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:600; color:#117e56;&quot;&gt;, 2009, &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:600; text-decoration: underline; color:#117e56;&quot;&gt;25&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:600; color:#117e56;&quot;&gt;:2741-2742, doi:10.1093/bioinformatics/btp504.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt;To tell me your appreciation, either positive or negative, please, send an email to massxpert-maintainer@massxpert.org.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt;Thank you,&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt; font-weight:600;&quot;&gt;				Filippo Rusconi&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt; font-weight:600;&quot;&gt;				Author of &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt; font-weight:600; font-style:italic;&quot;&gt;massXpert&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;DejaVu Sans&apos;; font-size:7pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt;Cher Utilisateur du logiciel &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt; font-style:italic; color:#ae0002;&quot;&gt;massXpert&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt;,&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt; font-style:italic; color:#ae0002;&quot;&gt;massXpert&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt; a été développé dans le cadre de mon travail comme chercheur au Centre national de la Recherche scientifique (CNRS, France). &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt;Si &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt; font-style:italic; color:#ae0002;&quot;&gt;massXpert&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt; vous est d&apos;une quelconque utilité, merci de ne pas oublier de citer &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt; font-style:italic; color:#ae0002;&quot;&gt;massXpert&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt; dans vos publications en utilisant la référence ci-dessous : &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:600; color:#117e56;&quot;&gt;Rusconi, F. massXpert 2: a cross-platform software environment for polymer chemistry modelling and simulation/analysis of mass spectrometric data, &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:600; font-style:italic; color:#117e56;&quot;&gt;Bioinformatics&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:600; color:#117e56;&quot;&gt;, 2009, &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:600; text-decoration: underline; color:#117e56;&quot;&gt;25&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:10pt; font-weight:600; color:#117e56;&quot;&gt;:2741-2742, doi:10.1093/bioinformatics/btp504.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt;Pour me donner un avis, positif ou négatif, merci d&apos;envoyer un message à l&apos;adresse massxpert-maintainer@massxpert.org.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt;&quot;&gt;Merci,&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt; font-weight:600;&quot;&gt;			Filippo Rusconi&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt; font-weight:600;&quot;&gt;			Auteur de &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:11pt; font-weight:600; font-style:italic;&quot;&gt;massXpert&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/ui/aboutDlg.ui" line="163"/>
        <source>General Public License</source>
        <translation>General Public License</translation>
    </message>
    <message>
        <location filename="../gui/ui/aboutDlg.ui" line="179"/>
        <source>Features</source>
        <translation>Fonctionnalités</translation>
    </message>
    <message>
        <location filename="../gui/ui/aboutDlg.ui" line="195"/>
        <source>History</source>
        <translation>Histoire</translation>
    </message>
    <message>
        <location filename="../gui/ui/aboutDlg.ui" line="229"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
</context>
<context>
    <name>AtomDefDlg</name>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="31"/>
        <source>Atoms</source>
        <translation>Atomes</translation>
    </message>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="40"/>
        <location filename="../gui/ui/atomDefDlg.ui" line="89"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="56"/>
        <location filename="../gui/ui/atomDefDlg.ui" line="96"/>
        <source>Remove</source>
        <translation>Enlever</translation>
    </message>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="63"/>
        <location filename="../gui/ui/atomDefDlg.ui" line="103"/>
        <source>Move up</source>
        <translation>Vers le haut</translation>
    </message>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="70"/>
        <location filename="../gui/ui/atomDefDlg.ui" line="110"/>
        <source>Move down</source>
        <translation>Vers le bas</translation>
    </message>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="80"/>
        <source>Isotopes</source>
        <translation>Isotopes</translation>
    </message>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="124"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="130"/>
        <source>Identity</source>
        <translation>Identité</translation>
    </message>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="136"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="149"/>
        <source>Symbol:</source>
        <translation>Symbole:</translation>
    </message>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="162"/>
        <location filename="../gui/ui/atomDefDlg.ui" line="210"/>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="172"/>
        <source>Isotope/Atom</source>
        <translation>Isotope/Atome</translation>
    </message>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="178"/>
        <source>Isotope</source>
        <translation>Isotope</translation>
    </message>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="184"/>
        <source>Mass:</source>
        <translation>Masse:</translation>
    </message>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="197"/>
        <source>Abund.:</source>
        <translation>Abond.:</translation>
    </message>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="220"/>
        <source>Atom</source>
        <translation>Atome</translation>
    </message>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="226"/>
        <source>Mono:</source>
        <translation>Mono:</translation>
    </message>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="249"/>
        <source>Avg:</source>
        <translation>Moy:</translation>
    </message>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="278"/>
        <source>&amp;Validate</source>
        <translation>&amp;Valider</translation>
    </message>
    <message>
        <location filename="../gui/ui/atomDefDlg.ui" line="14"/>
        <source>massXpert: Atom definitions</source>
        <translation>massXpert: Définitions d&apos;atomes</translation>
    </message>
</context>
<context>
    <name>CalculatorChemPadDlg</name>
    <message>
        <location filename="../gui/ui/calculatorChemPadDlg.ui" line="13"/>
        <source>massXpert: Calculator&apos; s chemical pad</source>
        <translation>massXpert: Pavé numérique de calculatrice</translation>
    </message>
</context>
<context>
    <name>CalculatorRecorderDlg</name>
    <message>
        <location filename="../gui/ui/calculatorRecorderDlg.ui" line="13"/>
        <source>massXpert: Calculator&apos; s recorder</source>
        <translation>massXpert: Enregistreur de calculatrice</translation>
    </message>
</context>
<context>
    <name>CalculatorWnd</name>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="14"/>
        <source>massXpert: Calculator</source>
        <translation>massXpert: Calculatrice</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="39"/>
        <source>Seed masses</source>
        <translation>Masses semis</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="67"/>
        <location filename="../gui/ui/calculatorWnd.ui" line="177"/>
        <source>Mono mass</source>
        <translation>Masse mono</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="74"/>
        <location filename="../gui/ui/calculatorWnd.ui" line="190"/>
        <source>Avg mass</source>
        <translation>Masse moy</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="99"/>
        <source>Add to result</source>
        <translation>Ajouter au résultat</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="106"/>
        <source>Send to result</source>
        <translation>Envoyer au résultat</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="123"/>
        <source>Remove from result</source>
        <translation>Enlever de résultat</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="130"/>
        <location filename="../gui/ui/calculatorWnd.ui" line="249"/>
        <source>Clear</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="146"/>
        <source>Result masses</source>
        <translation>Masses résultat</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="218"/>
        <source>Add to seed</source>
        <translation>Ajouter au semis</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="225"/>
        <source>Send to seed</source>
        <translation>Envoyer au semis</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="242"/>
        <source>Remove from seed</source>
        <translation>Enlever de semis</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="395"/>
        <source>&amp;Formula</source>
        <oldsource>Formula</oldsource>
        <translation>&amp;Formule</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="415"/>
        <source>List of entities defined in the loaded polymer chemistry definition. Select any item from the monomer or modification combo boxes and set a count for it (positive or negative).</source>
        <translation>Liste des entités définies dans la définition de chimie de polymère en cours. Sélectionner tout élément dans les boîtes combo monomer ou modification et établir un décompte pour celui-ci (positif ou négatif).</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="418"/>
        <source>Polymer chemistry definition entities</source>
        <translation>Entités de définition de chimie de polymère</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="448"/>
        <source>List of available monomers</source>
        <translation>Liste des monomères disponibles</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="454"/>
        <source>List of monomers defined in the loaded polymer chemistry definition</source>
        <translation>Liste des monomères définis dans la définition de chimie de polymère en cours</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="461"/>
        <location filename="../gui/ui/calculatorWnd.ui" line="494"/>
        <location filename="../gui/ui/calculatorWnd.ui" line="526"/>
        <source>Times (&gt; 0, 0 or &lt;0) to account entity for.</source>
        <translation>Nombre de fois (&gt; 0, 0 ou &lt;0) à prendre en compte.</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="467"/>
        <location filename="../gui/ui/calculatorWnd.ui" line="497"/>
        <location filename="../gui/ui/calculatorWnd.ui" line="529"/>
        <source>How many times this entity has to be accounted for in the current calculation (added if count is positive or removed if count is negative).</source>
        <translation>Nombre de fois qu&apos;il faut prendre en compte cette entité pour le calcul courant (ajout si le décompte est positif, retranchement si le décompte est négatif).</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="484"/>
        <source>List of available modifications</source>
        <translation>Liste des modications disponibles</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="487"/>
        <source>List of modifications defined in the loaded polymer chemistry definition</source>
        <translation>Liste des modifications définies dans la définition de chimie de polymère chargée</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="516"/>
        <source>Polymer sequence</source>
        <translation>Séquence de polymère</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="519"/>
        <source>Write a sequence in the form of monomer codes, without spaces</source>
        <translation>Écrire une séquence sous forme de codes de monomères, sans espace</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="269"/>
        <source>Current definition:</source>
        <translation>Définition en cours:</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="292"/>
        <source>Show recorder</source>
        <translation>Afficher l&apos;enregistreur</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="299"/>
        <source>Show chemical pad</source>
        <translation>Afficher le pavé chimique</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="329"/>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="349"/>
        <source>m/z &amp;calculation</source>
        <translation>&amp;Calcul de m/z</translation>
    </message>
    <message>
        <location filename="../gui/ui/calculatorWnd.ui" line="369"/>
        <source>&amp;Isotopic pattern calculation</source>
        <translation>Calcul de patron &amp;isotopique</translation>
    </message>
</context>
<context>
    <name>CleavageDlg</name>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="41"/>
        <source>Configuration of the cleavage</source>
        <translation>Configuration du clivage</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="193"/>
        <source>Ionization level &amp;range</source>
        <translation>Gamme de &amp;niveaux d&apos;ionisation</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="199"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:7pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Initial ionization level for the oligomers to bear&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:7pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Niveau d&apos;ionisation initial pour les oligomères&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="215"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:7pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Last ionization level for the oligomers to bear&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:7pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Niveau d&apos;ionisation final pour les oligomères&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="237"/>
        <source>Ac&amp;tions</source>
        <translation>Ac&amp;tions</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="245"/>
        <source>With &amp;sequence</source>
        <translation>Avec &amp;séquence</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="252"/>
        <source>Stack &amp;oligomers</source>
        <translation>Empiler &amp;oligomères</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="261"/>
        <source>Export selected results</source>
        <translation>Exporter résulats sélectionnés</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="273"/>
        <source>Export the results either to the clipboard or to a file</source>
        <translation>Exporter les résultats vers le presse-papiers ou vers un fichier</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="280"/>
        <source>For &amp;XpertMiner</source>
        <translation>Vers &amp;XpertMiner</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="289"/>
        <source>Mono</source>
        <translation>Mono</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="299"/>
        <source>Avg</source>
        <translation>Moy</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="310"/>
        <source>Delimiter: </source>
        <translation>Délimiteur:</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="317"/>
        <source>When exporting the results, separate the data elements for
any given oligomer with this character or string. By default, &apos;$&apos; is used.</source>
        <translation>Lors de l&apos;exportation des résultats, séparer les différents éléments de données
pour tout oligomère par ce caractère ou cette chaîne. Par défault, &apos;$&apos; est utilisé.</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="348"/>
        <source>&amp;Cleave</source>
        <translation>&amp;Cliver</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="364"/>
        <source>Oligomers</source>
        <translation>Oligomères</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="370"/>
        <source>Filtering options (Ctrl+F)</source>
        <translation>Options de filtrage (Ctrl+F)</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="402"/>
        <source>Partial:</source>
        <translation>Partiel:</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="416"/>
        <source>Mono:</source>
        <translation>Mono:</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="430"/>
        <source>Avg:</source>
        <translation>Moy:</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="447"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="458"/>
        <source>Charge:</source>
        <translation>Charge:</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="486"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="502"/>
        <source>Sequence</source>
        <translation>Séquence</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="518"/>
        <source>Cleavage details</source>
        <translation>Détails de clivage</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="543"/>
        <source>Monomers</source>
        <translation>Monomères</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="552"/>
        <source>Modifications</source>
        <translation>Modifications</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="565"/>
        <source>Cross-links</source>
        <translation>Pontages</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="578"/>
        <source>Polymer</source>
        <translation>Polymère</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="593"/>
        <source>Right cap</source>
        <translation>Coiffe droite</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="606"/>
        <source>Right modif</source>
        <translation>Modif droite</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="632"/>
        <source>Left cap</source>
        <translation>Coiffe gauche</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="619"/>
        <source>Left modif</source>
        <translation>Modif gauche</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="17"/>
        <source>massXpert: Polymer cleavage</source>
        <translation>massXpert: Clivage de polymère</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="62"/>
        <source>&amp;Available cleavage agents</source>
        <translation>&amp;Agents de clivage disponibles</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="115"/>
        <source>Oligomer coordinates</source>
        <translation>Coordonnées d&apos;oligomère</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="123"/>
        <source>Start:</source>
        <translation>Début:</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="136"/>
        <source>s</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="147"/>
        <source>End:</source>
        <translation>Fin:</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="160"/>
        <source>e</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="169"/>
        <source>&amp;Whole sequence</source>
        <translation>Séquence &amp;entière</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleavageDlg.ui" line="181"/>
        <source>Partial cleavages:</source>
        <translation>Clivages partiels:</translation>
    </message>
</context>
<context>
    <name>CleaveSpecDefDlg</name>
    <message>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="26"/>
        <source>Cleavage specifications</source>
        <translation>Spécifications de clivage</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="35"/>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="84"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="51"/>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="91"/>
        <source>Remove</source>
        <translation>Enlever</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="58"/>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="98"/>
        <source>Move up</source>
        <translation>Vers le haut</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="65"/>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="105"/>
        <source>Move down</source>
        <translation>Vers le bas</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="75"/>
        <source>Cleavage rules</source>
        <translation>Règles de clivage</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="119"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="125"/>
        <source>Identity</source>
        <translation>Identité</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="131"/>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="188"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="144"/>
        <source>Pattern:</source>
        <translation>Patron:</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="157"/>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="289"/>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="167"/>
        <source>Cleavage rule</source>
        <translation>Règle de clivage</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="216"/>
        <source>Left end</source>
        <translation>Extrémité gauche</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="222"/>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="251"/>
        <source>Code:</source>
        <translation>Code:</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="232"/>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="261"/>
        <source>Formula:</source>
        <translation>Formule:</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="245"/>
        <source>Right end</source>
        <translation>Extrémité droite</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="317"/>
        <source>&amp;Validate</source>
        <translation>&amp;Valider</translation>
    </message>
    <message>
        <location filename="../gui/ui/cleaveSpecDefDlg.ui" line="13"/>
        <source>massXpert: Cleavage definitions</source>
        <translation>massXpert: Définitions de clivages</translation>
    </message>
</context>
<context>
    <name>CompositionsDlg</name>
    <message>
        <location filename="../gui/ui/compositionsDlg.ui" line="13"/>
        <source>massXpert: Compositions</source>
        <translation>massXpert: Compositions</translation>
    </message>
    <message>
        <location filename="../gui/ui/compositionsDlg.ui" line="36"/>
        <source>Configuration</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <location filename="../gui/ui/compositionsDlg.ui" line="42"/>
        <source>&amp;Whole sequence</source>
        <translation>Séquence &amp;entière</translation>
    </message>
    <message>
        <location filename="../gui/ui/compositionsDlg.ui" line="93"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../gui/ui/compositionsDlg.ui" line="121"/>
        <source>&amp;Monomeric</source>
        <translation>&amp;Monomérique</translation>
    </message>
    <message>
        <location filename="../gui/ui/compositionsDlg.ui" line="131"/>
        <source>&amp;Elemental</source>
        <translation>&amp;Élementale</translation>
    </message>
    <message>
        <location filename="../gui/ui/compositionsDlg.ui" line="146"/>
        <source>Export the results either to the clipboard or to a file</source>
        <translation>Exporter les résultats vers le presse-papiers ou vers un fichier</translation>
    </message>
    <message>
        <location filename="../gui/ui/compositionsDlg.ui" line="164"/>
        <source>Elemental composition</source>
        <translation>Composition élémentale</translation>
    </message>
    <message>
        <location filename="../gui/ui/compositionsDlg.ui" line="206"/>
        <source>Monomeric composition</source>
        <translation>Composition monomérique</translation>
    </message>
    <message>
        <location filename="../gui/ui/compositionsDlg.ui" line="54"/>
        <source>&amp;Selected sequence:</source>
        <translation>&amp;Séquence sélectionnée:</translation>
    </message>
    <message>
        <location filename="../gui/ui/compositionsDlg.ui" line="73"/>
        <source>If the selection has changed in the sequence,
update its data here.</source>
        <translation>Si la sélection a changé dans la séquence, mettre à jour ses données ici.</translation>
    </message>
    <message>
        <location filename="../gui/ui/compositionsDlg.ui" line="77"/>
        <source>Update</source>
        <translation>Actualiser</translation>
    </message>
</context>
<context>
    <name>ConfigurationSettingsDlg</name>
    <message>
        <location filename="../gui/ui/configurationSettingsDlg.ui" line="22"/>
        <source>General concepts</source>
        <translation>Concepts généraux</translation>
    </message>
    <message>
        <location filename="../gui/ui/configurationSettingsDlg.ui" line="34"/>
        <source>Data directory</source>
        <translation>Répertoire de données</translation>
    </message>
    <message>
        <location filename="../gui/ui/configurationSettingsDlg.ui" line="43"/>
        <location filename="../gui/ui/configurationSettingsDlg.ui" line="62"/>
        <location filename="../gui/ui/configurationSettingsDlg.ui" line="81"/>
        <source>Browse...</source>
        <translation>Parcourir...</translation>
    </message>
    <message>
        <location filename="../gui/ui/configurationSettingsDlg.ui" line="53"/>
        <source>Plugin directory</source>
        <translation>Répertoire des plugins</translation>
    </message>
    <message>
        <location filename="../gui/ui/configurationSettingsDlg.ui" line="72"/>
        <source>Localization directory</source>
        <translation>Répertoire de localisation</translation>
    </message>
    <message>
        <location filename="../gui/ui/configurationSettingsDlg.ui" line="108"/>
        <source>Save the settings stored in the configuration settings file</source>
        <translation>Sauver les paramètres dans le fichier de paramétrage de configuration</translation>
    </message>
    <message>
        <location filename="../gui/ui/configurationSettingsDlg.ui" line="111"/>
        <source>&amp;Save settings</source>
        <translation>&amp;Sauver les paramètres</translation>
    </message>
    <message>
        <location filename="../gui/ui/configurationSettingsDlg.ui" line="118"/>
        <source>Erase the settings stored in the configuration settings file</source>
        <translation>Effacer les paramètres enregistrés dans le fichier de paramétrage de configuration</translation>
    </message>
    <message>
        <location filename="../gui/ui/configurationSettingsDlg.ui" line="121"/>
        <source>&amp;Erase</source>
        <translation>&amp;Effacer</translation>
    </message>
    <message>
        <location filename="../gui/ui/configurationSettingsDlg.ui" line="128"/>
        <source>Close with no modification</source>
        <translation>Fermer sans modification</translation>
    </message>
    <message>
        <location filename="../gui/ui/configurationSettingsDlg.ui" line="131"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../gui/ui/configurationSettingsDlg.ui" line="13"/>
        <source>massXpert: Configuration settings</source>
        <translation>massXpert: Paramètres de configuration</translation>
    </message>
</context>
<context>
    <name>CrossLinkerDefDlg</name>
    <message>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="39"/>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="88"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="55"/>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="95"/>
        <source>Remove</source>
        <translation>Enlever</translation>
    </message>
    <message>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="62"/>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="102"/>
        <source>Move up</source>
        <translation>Vers le haut</translation>
    </message>
    <message>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="69"/>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="109"/>
        <source>Move down</source>
        <translation>Vers le bas</translation>
    </message>
    <message>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="79"/>
        <source>Modifications</source>
        <translation>Modifications</translation>
    </message>
    <message>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="123"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="129"/>
        <source>Identity</source>
        <translation>Identité</translation>
    </message>
    <message>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="135"/>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="192"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="148"/>
        <source>Formula:</source>
        <translation>Formule:</translation>
    </message>
    <message>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="161"/>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="232"/>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="171"/>
        <source>Modification</source>
        <translation>Modification</translation>
    </message>
    <message>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="260"/>
        <source>&amp;Validate</source>
        <translation>&amp;Valider</translation>
    </message>
    <message>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="13"/>
        <source>massXpert: Cross-linker definitions</source>
        <translation>massXpert: Définitions d&apos;agents pontants</translation>
    </message>
    <message>
        <location filename="../gui/ui/crossLinkerDefDlg.ui" line="30"/>
        <source>Cross-linkers</source>
        <translation>Agents pontants</translation>
    </message>
</context>
<context>
    <name>DecimalPlacesOptionsDlg</name>
    <message>
        <location filename="../gui/ui/decimalPlacesOptionsDlg.ui" line="19"/>
        <source>massXpert: Decimal Places</source>
        <translation>massXpert: Décimales</translation>
    </message>
    <message>
        <location filename="../gui/ui/decimalPlacesOptionsDlg.ui" line="31"/>
        <source>Configure the number of
decimal places used
to display numerical values
for classes of chemical entities</source>
        <translation>Configurer les décimales
utilisées pour l&apos;affichage
des valeurs numériques
pour les classes d&apos;entités
chimiques </translation>
    </message>
    <message>
        <location filename="../gui/ui/decimalPlacesOptionsDlg.ui" line="46"/>
        <source>Atoms: </source>
        <translation>Atomes:</translation>
    </message>
    <message>
        <location filename="../gui/ui/decimalPlacesOptionsDlg.ui" line="63"/>
        <source>pKa-pH-pI: </source>
        <translation>pKa-pH-pI:</translation>
    </message>
    <message>
        <location filename="../gui/ui/decimalPlacesOptionsDlg.ui" line="73"/>
        <source>Oligomers: </source>
        <translation>Oligomères:</translation>
    </message>
    <message>
        <location filename="../gui/ui/decimalPlacesOptionsDlg.ui" line="83"/>
        <source>Polymers: </source>
        <translation>Polymères:</translation>
    </message>
    <message>
        <location filename="../gui/ui/decimalPlacesOptionsDlg.ui" line="112"/>
        <source>&amp;Validate</source>
        <translation>&amp;Valider</translation>
    </message>
</context>
<context>
    <name>FragSpecDefDlg</name>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="31"/>
        <source>Fragmentation specifications</source>
        <translation>Spécifications de fragmentation</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="40"/>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="89"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="56"/>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="96"/>
        <source>Remove</source>
        <translation>Enlever</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="63"/>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="103"/>
        <source>Move up</source>
        <translation>Vers le haut</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="70"/>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="110"/>
        <source>Move down</source>
        <translation>Vers le bas</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="80"/>
        <source>Fragmentation rules</source>
        <translation>Règles de fragmentation</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="124"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="130"/>
        <source>Identity</source>
        <translation>Identité</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="136"/>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="218"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="149"/>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="231"/>
        <source>Formula:</source>
        <translation>Formule:</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="162"/>
        <source>End:</source>
        <translation>Extrémité:</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="172"/>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="283"/>
        <source>Comment:</source>
        <translation>Commentaire:</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="185"/>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="311"/>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="192"/>
        <source>Monomer:</source>
        <translation>Monomère:</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="212"/>
        <source>FragRule</source>
        <translation>Règle de fragmentation</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="244"/>
        <source>Local logic</source>
        <translation>Logique locale</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="250"/>
        <source>Prev code:</source>
        <translation>Code précéd:</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="260"/>
        <source>Curr code:</source>
        <translation>Code courant:</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="270"/>
        <source>Next code:</source>
        <translation>Code suivant:</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="339"/>
        <source>&amp;Validate</source>
        <translation>&amp;Valider</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragSpecDefDlg.ui" line="14"/>
        <source>massXpert: Fragmentation definitions</source>
        <translation>massXpert: Définitions de fragmentations</translation>
    </message>
</context>
<context>
    <name>FragmentationDlg</name>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="41"/>
        <source>Configuration of the fragmentation</source>
        <translation>Configuration de la  fragmentation</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="76"/>
        <source>&amp;Available fragmentation patterns</source>
        <translation>&amp;Patrons de fragmentation disponibles</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="108"/>
        <source>Decompositions</source>
        <translation>Décompositions</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="114"/>
        <source>-H20</source>
        <translation>-H20</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="121"/>
        <source>-NH3</source>
        <translation>-NH3</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="131"/>
        <source>Formula:</source>
        <translation>Formule:</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="187"/>
        <source>Oligomer coordinates</source>
        <translation>Coordonnées d&apos;oligomère</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="193"/>
        <source>Start:</source>
        <translation>Début:</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="206"/>
        <location filename="../gui/ui/fragmentationDlg.ui" line="226"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="213"/>
        <source>End:</source>
        <translation>Fin:</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="236"/>
        <source>Ionization level &amp;range</source>
        <translation>Gamme de &amp;niveaux d&apos;ionisation</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="242"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:7pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Initial ionization level for the oligomers to bear&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:7pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Niveau d&apos;ionisation initial pour les oligomères&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="261"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:7pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Last ionization level for the oligomers to bear&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:7pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Niveau d&apos;ionisation final pour les oligomères&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="379"/>
        <source>When exporting the results, separate the data elements for
any given oligomer with this character or string. By default, &apos;$&apos; is used.</source>
        <translation>Lors de l&apos;exportation des résultats, séparer les différents éléments de données
pour tout oligomère par ce caractère ou cette chaîne. Par défault, &apos;$&apos; est utilisé.</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="407"/>
        <source>&amp;Fragment</source>
        <translation>&amp;Fragmenter</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="335"/>
        <source>Export the results either to the clipboard or to a file</source>
        <translation>Exporter les résultats vers le presse-papiers ou vers un fichier</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="423"/>
        <source>Oligomers</source>
        <translation>Oligomères</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="665"/>
        <source>Cross-links</source>
        <translation>Pontages</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="429"/>
        <source>Filtering options (Ctrl+F)</source>
        <translation>Options de filtrage (Ctrl+F)</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="299"/>
        <source>Ac&amp;tions</source>
        <translation>Ac&amp;tions</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="307"/>
        <source>With &amp;sequence</source>
        <translation>Avec &amp;séquence</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="314"/>
        <source>Stack &amp;oligomers</source>
        <translation>Empiler &amp;oligomères</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="323"/>
        <source>Export selected results</source>
        <translation>Exporter résulats sélectionnés</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="342"/>
        <source>For &amp;XpertMiner</source>
        <translation>Vers &amp;XpertMiner</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="351"/>
        <source>Mono</source>
        <translation>Mono</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="361"/>
        <source>Avg</source>
        <translation>Moy</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="372"/>
        <source>Delimiter: </source>
        <translation>Délimiteur:</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="461"/>
        <source>Pattern:</source>
        <translation>Patron:</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="475"/>
        <source>Mono:</source>
        <translation>Mono:</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="489"/>
        <source>Avg:</source>
        <translation>Moy:</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="506"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="517"/>
        <source>Charge:</source>
        <translation>Charge:</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="545"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="561"/>
        <source>&amp;Sequence</source>
        <translation>&amp;Séquence</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="577"/>
        <source>Fragmentation &amp;details</source>
        <translation>&amp;Détails de fragmentation</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="602"/>
        <source>Polymer</source>
        <translation>Polymère</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="617"/>
        <source>Right modif</source>
        <translation>Modif droite</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="630"/>
        <source>Left modif</source>
        <translation>Modif gauche</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="643"/>
        <source>Monomers</source>
        <translation>Monomères</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="652"/>
        <source>Modifications</source>
        <translation>Modifications</translation>
    </message>
    <message>
        <location filename="../gui/ui/fragmentationDlg.ui" line="17"/>
        <source>massXpert: Polymer fragmentation</source>
        <translation>massXpert: Fragmentation de polymère</translation>
    </message>
</context>
<context>
    <name>MassSearchDlg</name>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="28"/>
        <source>Configuration of the mass search</source>
        <translation>Configuration de la recherche de masses</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="36"/>
        <source>Mono masses</source>
        <translation>Masses mono</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="47"/>
        <location filename="../gui/ui/massSearchDlg.ui" line="89"/>
        <location filename="../gui/ui/massSearchDlg.ui" line="454"/>
        <location filename="../gui/ui/massSearchDlg.ui" line="573"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="75"/>
        <source>Avg masses</source>
        <translation>Masses moy</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="116"/>
        <source>Target sequence</source>
        <translation>Séquence cible</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="122"/>
        <source>Wh&amp;ole</source>
        <translation>&amp;Entière</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="179"/>
        <source>Ionization</source>
        <translation>Ionisation</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="187"/>
        <source>S&amp;tart:</source>
        <translation>&amp;Début:</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="204"/>
        <source>E&amp;nd:</source>
        <translation>&amp;Fin:</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="231"/>
        <source>&amp;Search</source>
        <translation>&amp;Rechercher</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="241"/>
        <source>&amp;Abort</source>
        <translation>&amp;Abandonner</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="307"/>
        <source>When exporting the results, separate the data elements for
any given oligomer with this character or string. By default, &apos;$&apos; is used.</source>
        <translation>Lors de l&apos;exportation des résultats, séparer les différents éléments de données
pour tout oligomère par ce caractère ou cette chaîne. Par défault, &apos;$&apos; est utilisé.</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="326"/>
        <source>Export the results either to the clipboard or to a file</source>
        <translation>Exporter les résultats vers le presse-papiers ou vers un fichier</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="225"/>
        <source>Ac&amp;tions</source>
        <translation>Ac&amp;tions</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="248"/>
        <source>With &amp;sequence</source>
        <translation>Avec &amp;séquence</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="255"/>
        <source>Export selected results</source>
        <translation>Exporter résulats sélectionnés</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="263"/>
        <source>For &amp;XpertMiner</source>
        <translation>Vers &amp;XpertMiner</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="270"/>
        <source>Mono</source>
        <translation>Mono</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="280"/>
        <source>Avg</source>
        <translation>Moy</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="300"/>
        <source>Delimiter: </source>
        <translation>Délimiteur:</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="344"/>
        <source>Found oligomers</source>
        <translation>Oligomères trouvés</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="366"/>
        <source>&amp;Filtering options (Ctrl+M, F)</source>
        <translation>Options de &amp;filtrage (Ctrl+M, F)</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="395"/>
        <location filename="../gui/ui/massSearchDlg.ui" line="514"/>
        <source>Searched:</source>
        <translation>Rerchechée:</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="409"/>
        <location filename="../gui/ui/massSearchDlg.ui" line="528"/>
        <source>Error:</source>
        <translation>Erreur:</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="423"/>
        <location filename="../gui/ui/massSearchDlg.ui" line="542"/>
        <location filename="../gui/ui/massSearchDlg.ui" line="794"/>
        <source>Mono:</source>
        <translation>Mono:</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="437"/>
        <location filename="../gui/ui/massSearchDlg.ui" line="556"/>
        <location filename="../gui/ui/massSearchDlg.ui" line="787"/>
        <source>Avg:</source>
        <translation>Moy:</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="482"/>
        <source>&amp;Filtering options (Ctrl+A, F)</source>
        <translation>Options de &amp;filtrage (Ctrl+1, F)</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="599"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="609"/>
        <source>&amp;Progress details</source>
        <translation>Détails de &amp;Progression</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="621"/>
        <source>Overall progression</source>
        <translation>Progression d&apos;ensemble</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="636"/>
        <source>Current mass:</source>
        <translation>Masse courante:</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="653"/>
        <source>Mass searches:</source>
        <translation>Recherches de masse:</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="660"/>
        <source>Oligomers found:</source>
        <translation>Oligomères trouvés:</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="667"/>
        <source>Oligomers tested:</source>
        <translation>Oligomères testés:</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="680"/>
        <location filename="../gui/ui/massSearchDlg.ui" line="687"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="697"/>
        <source>Last oligomer data</source>
        <translation>Données sur le dernier oligomère</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="717"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="747"/>
        <source>Mass type:</source>
        <translation>Type de masse:</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="777"/>
        <source>Coordinates:</source>
        <translation>Coordonnées:</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="827"/>
        <source>Mass search &amp;details</source>
        <translation>&amp;Détails de recherche de masse</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="860"/>
        <source>Polymer</source>
        <translation>Polymère</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="875"/>
        <source>Right modif</source>
        <translation>Modif droite</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="888"/>
        <source>Left modif</source>
        <translation>Modif gauche</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="914"/>
        <source>Monomers</source>
        <translation>Monomères</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="923"/>
        <source>Modifications</source>
        <translation>Modifications</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="936"/>
        <source>Cross-links</source>
        <translation>Pontages</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="965"/>
        <source>Se&amp;quence</source>
        <translation>Sé&amp;quence</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="14"/>
        <source>massXpert: Mass search</source>
        <translation>massXpert: Recherche de masses</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="141"/>
        <source>Current selection</source>
        <translation>Sélection courante</translation>
    </message>
    <message>
        <location filename="../gui/ui/massSearchDlg.ui" line="154"/>
        <source>Update</source>
        <translation>Actualiser</translation>
    </message>
</context>
<context>
    <name>ModifDefDlg</name>
    <message>
        <location filename="../gui/ui/modifDefDlg.ui" line="23"/>
        <source>Modifications</source>
        <translation>Modifications</translation>
    </message>
    <message>
        <location filename="../gui/ui/modifDefDlg.ui" line="32"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../gui/ui/modifDefDlg.ui" line="48"/>
        <source>Remove</source>
        <translation>Enlever</translation>
    </message>
    <message>
        <location filename="../gui/ui/modifDefDlg.ui" line="55"/>
        <source>Move up</source>
        <translation>Vers le haut</translation>
    </message>
    <message>
        <location filename="../gui/ui/modifDefDlg.ui" line="62"/>
        <source>Move down</source>
        <translation>Vers le bas</translation>
    </message>
    <message>
        <location filename="../gui/ui/modifDefDlg.ui" line="73"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../gui/ui/modifDefDlg.ui" line="79"/>
        <source>Identity</source>
        <translation>Identité</translation>
    </message>
    <message>
        <location filename="../gui/ui/modifDefDlg.ui" line="85"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../gui/ui/modifDefDlg.ui" line="98"/>
        <source>Formula:</source>
        <translation>Formule:</translation>
    </message>
    <message>
        <location filename="../gui/ui/modifDefDlg.ui" line="111"/>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <location filename="../gui/ui/modifDefDlg.ui" line="148"/>
        <source>&amp;Validate</source>
        <translation>&amp;Valider</translation>
    </message>
    <message>
        <location filename="../gui/ui/modifDefDlg.ui" line="13"/>
        <source>massXpert: Modification definitions</source>
        <translation>massXpert: Définitions de modifications</translation>
    </message>
    <message>
        <location filename="../gui/ui/modifDefDlg.ui" line="118"/>
        <source>Target(s):</source>
        <translation>Cible(s):</translation>
    </message>
    <message>
        <location filename="../gui/ui/modifDefDlg.ui" line="128"/>
        <source>Max. count</source>
        <translation>Nombre max</translation>
    </message>
</context>
<context>
    <name>MonomerCrossLinkDlg</name>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="39"/>
        <source>Creation of cross-links</source>
        <translation>Création de pontages</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="47"/>
        <source>Cross-linkers</source>
        <translation>Agents pontants</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="54"/>
        <source>Cross-linkers defined in the 
active polymer chemistry definition</source>
        <translation>Agents pontants définis dans la définition de chimie de polymère active</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="64"/>
        <source>&amp;Cross-link</source>
        <translation>&amp;Ponter</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="72"/>
        <source>Cross-link details</source>
        <translation>Détails du pontage</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="83"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="101"/>
        <source>Comment:</source>
        <translation>Commentaire:</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="119"/>
        <source>Modifications</source>
        <translation>Modifications</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="138"/>
        <source>Targets&apos; positions</source>
        <translation>Positions des cibles</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="144"/>
        <source>Enter a semicolumn-separated list of
monomer positions in the same order
as the modifications (if any).
Example:  &quot;3;13&quot;.</source>
        <translation>Entrer une liste de positions de monomères
séparées par un point-virgule, dans le même
ordre que celui des modifications (s&apos;il y en a).
Exemple:  &quot;3;13&quot;.</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="154"/>
        <source>List of monomers engaged in the selected cross-link</source>
        <translation>Liste des monomères engagés dans le pontage sélectionné</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="177"/>
        <source>Cross-linked monomers</source>
        <translation>Monomères pontés</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="184"/>
        <source>List of all the monomers engaged in cross-links</source>
        <translation>Liste de tous les monomères engagés dans des pontages</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="193"/>
        <source>Cross-links</source>
        <translation>Pontages</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="201"/>
        <source>List of the cross-links for the selected 
monomer (or all the cross-links in the 
polymer sequence)</source>
        <translation>Liste des pontages pour le monomère 
sélectionné (out tous les pontages dans 
la séquence de polymère)</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="210"/>
        <source>If checked, triggers the display of all the cross-links in the polymer sequence</source>
        <translation>Si coché, déclenche l&apos;affichage de tous les pontages dans la séquence de polymère</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="213"/>
        <source>&amp;All cross-links</source>
        <translation>&amp;Tous les pontages</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="225"/>
        <source>Uncross-link the selected cross-link</source>
        <translation>Déponter le pontage sélectionné</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="228"/>
        <source>&amp;Uncross-link</source>
        <translation>&amp;Déponter</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="16"/>
        <source>massXpert: Monomer cross-link</source>
        <translation>massXpert: Pontage de monomères</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerCrossLinkDlg.ui" line="166"/>
        <source>View/remove cross-links</source>
        <translation>Voir/Enlever pontages</translation>
    </message>
</context>
<context>
    <name>MonomerDefDlg</name>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="28"/>
        <source>Monomers</source>
        <translation>Monomères</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="37"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="53"/>
        <source>Remove</source>
        <translation>Enlever</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="60"/>
        <source>Move up</source>
        <translation>Vers le haut</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="67"/>
        <source>Move down</source>
        <translation>Vers le bas</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="78"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="84"/>
        <source>Identity</source>
        <translation>Identité</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="90"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="113"/>
        <source>Formula:</source>
        <translation>Formule:</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="126"/>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="136"/>
        <source>Masses</source>
        <translation>Masses</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="166"/>
        <source>Code length:</source>
        <translation>Longueur de code:</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="178"/>
        <source>&amp;Validate</source>
        <translation>&amp;Valider</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="185"/>
        <source>Calculate mass differences</source>
        <translation>Calculer différences de masse</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="191"/>
        <source>Threshold:</source>
        <translation>Seuil:</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="201"/>
        <source>&amp;Mono mass</source>
        <translation>Masse &amp;mono</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="211"/>
        <source>&amp;Avg mass</source>
        <translation>Masse m&amp;oy</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="218"/>
        <source>Calculate</source>
        <translation>Calculer</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="14"/>
        <source>massXpert: Monomer definitions</source>
        <translation>massXpert: Définitions de monomères</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerDefDlg.ui" line="103"/>
        <source>Code:</source>
        <translation>Code:</translation>
    </message>
</context>
<context>
    <name>MonomerModificationDlg</name>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="40"/>
        <source>Creation of modifications</source>
        <translation>Création de modifications</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="48"/>
        <source>List of all the modifications available in the polymer chemistry definition. Only one item is selectable at a time.</source>
        <translation>Liste de toutes les modifications disponibles dans la définition de chimie de polymère. Un élément unique est sélectionnable à la fois.</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="51"/>
        <source>Available modifications</source>
        <translation>Modifications disponibles</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="57"/>
        <source>Modifications defined in the 
polymer chemistry definition</source>
        <translation>Modifications définies dans 
la définition de chimie de polymère</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="68"/>
        <source>Define a modification manually</source>
        <translation>Définir manuellement une modification</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="71"/>
        <source>Define modification</source>
        <translation>Définir modification</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="83"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="96"/>
        <source>Formula:</source>
        <translation>Formule:</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="109"/>
        <source>Targets:</source>
        <translation>Cibles:</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="127"/>
        <source>The controls in this group box permit the selection of the monomers in the sequence that will be modified (or unmodified).</source>
        <translation>Les contrôles dans cette boîte groupe permettent la sélection des monomères dans la séquence devant être modifiés (ou démodifiés).</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="130"/>
        <source>Target</source>
        <translation>Cible</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="163"/>
        <source>Current selection</source>
        <translation>Sélection courante</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="188"/>
        <source>All the monomers of the same code as the one selected will be modified.</source>
        <translation>Tous les monomères du même code que celui sélectionné seront modifiés.</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="191"/>
        <source>Monomers of same code</source>
        <translation>Monomères de même code</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="198"/>
        <source>All the monomers having the same code(s) as the item(s) selected in the list will be modified.</source>
        <translation>Tous les monomères de même code que les éléments sélectionnés dans la liste seront modifiés.</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="201"/>
        <source>Monomers from the list</source>
        <translation>Monomères de la liste</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="208"/>
        <source>All the monomers will be modified.</source>
        <translation>Tous les monomères seront modifiés.</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="211"/>
        <source>All monomers</source>
        <translation>Tous les monomères</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="138"/>
        <source>Available monomers</source>
        <translation>Monomères disponibles</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="247"/>
        <source>&amp;Modify</source>
        <translation>&amp;Modifier</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="266"/>
        <source>Modified monomers</source>
        <translation>Monomères modifiés</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="273"/>
        <source>List of all the monomers modified</source>
        <translation>Liste de tous les monomères modifiés</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="285"/>
        <source>Modifications</source>
        <translation>Modifications</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="293"/>
        <source>List of all the modifications beared by the 
selected monomer (or all the modifications 
in the polymer sequence)</source>
        <translation>Liste de toutes les modifications portées 
par le monomère sélectionné (ou toutes les modifications 
dans le séquence de polymère)</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="305"/>
        <source>If checked, triggers the display of all the cross-links in the polymer sequence</source>
        <translation>Si coché, déclenche l&apos;affichage de tous les pontages dans la séquence de polymère</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="308"/>
        <source>&amp;All modifications</source>
        <translation>&amp;Toutes modifications</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="320"/>
        <source>Uncross-link the selected cross-link</source>
        <translation>Déponter le pontage sélectionné</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="323"/>
        <source>&amp;Unmodify</source>
        <translation>&amp;Démodifier</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="338"/>
        <source>Messages</source>
        <translation>Messages</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="344"/>
        <source>Text widget used to display all the messages 
for the modification/unmodification tasks</source>
        <translation>Widget texte utilisé pour afficher tous les messages 
pour les tâches de modification/démodification</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="16"/>
        <source>massXpert: Monomer modification</source>
        <translation>massXpert: Modification de monomère</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="255"/>
        <source>View/remove modifications</source>
        <translation>Voir/Ôter modifications</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="176"/>
        <source>Update</source>
        <translation>Actualiser</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="221"/>
        <source>Override target and max. count limitations.</source>
        <translation>Ignorer les limitations de cible et de nombre max.</translation>
    </message>
    <message>
        <location filename="../gui/ui/monomerModificationDlg.ui" line="224"/>
        <source>Override limitations</source>
        <translation>Ignorer limitations</translation>
    </message>
</context>
<context>
    <name>MzCalculationDlg</name>
    <message>
        <location filename="../gui/ui/mzCalculationDlg.ui" line="40"/>
        <source>Initial status</source>
        <translation>Status initial</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzCalculationDlg.ui" line="116"/>
        <source>Mono m/z:</source>
        <translation>m/z mono:</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzCalculationDlg.ui" line="129"/>
        <source>Avg m/z:</source>
        <translation>m/z moy:</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzCalculationDlg.ui" line="66"/>
        <source>Ionization rule</source>
        <translation>Règle d&apos;ionisation</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzCalculationDlg.ui" line="54"/>
        <location filename="../gui/ui/mzCalculationDlg.ui" line="72"/>
        <location filename="../gui/ui/mzCalculationDlg.ui" line="171"/>
        <source>Formula:</source>
        <translation>Formule:</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzCalculationDlg.ui" line="82"/>
        <location filename="../gui/ui/mzCalculationDlg.ui" line="191"/>
        <source>Charge:</source>
        <translation>Charge:</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzCalculationDlg.ui" line="99"/>
        <source>Level:</source>
        <translation>Niveau:</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzCalculationDlg.ui" line="151"/>
        <source>Target ionization status</source>
        <translation>Status d&apos;ionisation cible</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzCalculationDlg.ui" line="215"/>
        <source>Ending level:</source>
        <translation>Niveau de fin:</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzCalculationDlg.ui" line="248"/>
        <source>Starting level:</source>
        <translation>Niveau de début:</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzCalculationDlg.ui" line="308"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzCalculationDlg.ui" line="326"/>
        <source>Export the results either to the clipboard or to a file</source>
        <translation>Exporter les résultats vers le presse-papiers ou vers un fichier</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzCalculationDlg.ui" line="339"/>
        <source>Calculate</source>
        <translation>Calculer</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzCalculationDlg.ui" line="356"/>
        <source>Ion charge family</source>
        <translation>Famille de charges d&apos;ions</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzCalculationDlg.ui" line="17"/>
        <source>massXpert: m/z ratio calculator</source>
        <translation>massXpert: Calculateur de ratios m/z</translation>
    </message>
</context>
<context>
    <name>MzLabFormulaBasedActionsDlg</name>
    <message>
        <location filename="../gui/ui/mzLabFormulaBasedActionsDlg.ui" line="14"/>
        <source>MzLab: Formula-based Actions</source>
        <translation>MzLab: Actions sur formule</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabFormulaBasedActionsDlg.ui" line="22"/>
        <source>Apply &amp;formula</source>
        <translation>Appliquer &amp;Formule</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabFormulaBasedActionsDlg.ui" line="32"/>
        <source>Increment charge by</source>
        <translation>Incrémenter charge par</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabFormulaBasedActionsDlg.ui" line="44"/>
        <source>Reionization</source>
        <translation>Réionisation</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabFormulaBasedActionsDlg.ui" line="52"/>
        <source>Unitary formula:</source>
        <translation>Formule unitaire:</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabFormulaBasedActionsDlg.ui" line="59"/>
        <source>Ionization formula</source>
        <translation>Formule d&apos;ionisation</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabFormulaBasedActionsDlg.ui" line="70"/>
        <source>Unitary charge:</source>
        <translation>Charge unitaire:</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabFormulaBasedActionsDlg.ui" line="77"/>
        <source>Ionization unitary charge</source>
        <translation>Charge unitaire d&apos;ionisation</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabFormulaBasedActionsDlg.ui" line="94"/>
        <source>Ionization level:</source>
        <translation>Niveau d&apos;ionisation:</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabFormulaBasedActionsDlg.ui" line="101"/>
        <source>Ionization level</source>
        <translation>Niveau d&apos;ionisation</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabFormulaBasedActionsDlg.ui" line="110"/>
        <source>Reionize</source>
        <translation>Réioniser</translation>
    </message>
</context>
<context>
    <name>MzLabInputOligomerTableViewDlg</name>
    <message>
        <location filename="../gui/ui/mzLabInputOligomerTableViewDlg.ui" line="14"/>
        <source>massXpert: mzLab - list</source>
        <translation>massXpert: mzLab - liste</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabInputOligomerTableViewDlg.ui" line="48"/>
        <source>Mass type</source>
        <translation>Type de masse</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabInputOligomerTableViewDlg.ui" line="54"/>
        <source>&amp;Avg</source>
        <translation>&amp;Moy</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabInputOligomerTableViewDlg.ui" line="61"/>
        <source>&amp;Mono</source>
        <translation>&amp;Mono</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabInputOligomerTableViewDlg.ui" line="78"/>
        <source>Helper data</source>
        <translation>Données accessoires</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabInputOligomerTableViewDlg.ui" line="84"/>
        <source>Sequence editor
identifier: </source>
        <translation>Identifiant de l&apos;éditeur
de séquence:</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabInputOligomerTableViewDlg.ui" line="95"/>
        <source>Field delimiter: </source>
        <translation>Délimiteur de champ:</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabInputOligomerTableViewDlg.ui" line="105"/>
        <source>When exporting the results, separate the data elements for
any given oligomer with this character or string. By default, &apos;$&apos; is used.</source>
        <translation>Lors de l&apos;exportation des résultats, séparer les différents éléments de données
pour tout oligomère par ce caractère ou cette chaîne. Par défault, &apos;$&apos; est utilisé.</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabInputOligomerTableViewDlg.ui" line="133"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabInputOligomerTableViewDlg.ui" line="139"/>
        <source>&amp;Export selected
to clipboard</source>
        <translation>&amp;Exporter sélectionnés
vers le presse-papiers</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabInputOligomerTableViewDlg.ui" line="147"/>
        <source>&amp;Get data
from clipboard</source>
        <translation>&amp;Récuperer données</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabInputOligomerTableViewDlg.ui" line="68"/>
        <source>&amp;Fragments</source>
        <translation>&amp;Fragments</translation>
    </message>
</context>
<context>
    <name>MzLabMassBasedActionsDlg</name>
    <message>
        <location filename="../gui/ui/mzLabMassBasedActionsDlg.ui" line="14"/>
        <source>MzLab: Mass-based Actions</source>
        <translation>MzLab: Actions sur masse</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabMassBasedActionsDlg.ui" line="20"/>
        <source>Mass-based</source>
        <translation>Sur masse</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabMassBasedActionsDlg.ui" line="29"/>
        <source>Threshold</source>
        <translation>Seuil</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabMassBasedActionsDlg.ui" line="35"/>
        <source>On m/z value</source>
        <translation>Sur valeur m/z</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabMassBasedActionsDlg.ui" line="45"/>
        <source>On M value</source>
        <translation>Sur valeur M</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabMassBasedActionsDlg.ui" line="52"/>
        <source>Apply &amp;threshold</source>
        <translation>Appliquer &amp;seuil</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabMassBasedActionsDlg.ui" line="65"/>
        <source>Apply &amp;mass</source>
        <translation>Appliquer &amp;masse</translation>
    </message>
</context>
<context>
    <name>MzLabMatchBasedActionsDlg</name>
    <message>
        <location filename="../gui/ui/mzLabMatchBasedActionsDlg.ui" line="14"/>
        <source>MzLab: Match-based Actions</source>
        <translation>MzLab: Actions sur correspondance</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabMatchBasedActionsDlg.ui" line="20"/>
        <source>Input List 1: measured masses vs Input List 2: theoretical masses</source>
        <translation>Liste entrée 1: masses mesurées vs liste entrée 2: masses théoriques</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabMatchBasedActionsDlg.ui" line="23"/>
        <source>Will try to perform matches between
the masses in the selected Input 1 list and the selected Input 2 list</source>
        <comment>Input 1 list and Input 2 list are widget names, translate accordingly.</comment>
        <translation>Recherchera des paires entre les
masses dans la liste entrée 1 sélectionnée
et la liste entrée 2 sélectionnée</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabMatchBasedActionsDlg.ui" line="27"/>
        <source>Perform matches between two input lists</source>
        <translation>Rechercher paires entre les deux listes entrée</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabMatchBasedActionsDlg.ui" line="33"/>
        <source>&amp;Tolerance</source>
        <translation>&amp;Tolérance</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabMatchBasedActionsDlg.ui" line="48"/>
        <source>Match strategy</source>
        <translation>Stratégie de correspondance</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabMatchBasedActionsDlg.ui" line="54"/>
        <source>&amp;Matches</source>
        <translation>&amp;Correspondances</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabMatchBasedActionsDlg.ui" line="61"/>
        <source>&amp;Non-matches</source>
        <translation>&amp;Non-correspond</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabMatchBasedActionsDlg.ui" line="68"/>
        <source>&amp;Perform match work</source>
        <translation>&amp;Rechercher les correspondances</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabMatchBasedActionsDlg.ui" line="91"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
</context>
<context>
    <name>MzLabOutputOligomerTableViewDlg</name>
    <message>
        <location filename="../gui/ui/mzLabOutputOligomerTableViewDlg.ui" line="14"/>
        <source>massXpert: mzLab - list</source>
        <translation>massXpert: mzLab - liste</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabOutputOligomerTableViewDlg.ui" line="27"/>
        <source>Mass type</source>
        <translation>Type de masse</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabOutputOligomerTableViewDlg.ui" line="36"/>
        <source>Mono</source>
        <translation>Mono</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabOutputOligomerTableViewDlg.ui" line="49"/>
        <source>Avg</source>
        <translation>Moy</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabOutputOligomerTableViewDlg.ui" line="65"/>
        <source>&amp;Export selected to clipboard</source>
        <translation>&amp;Exporter vers le presse-papiers</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabOutputOligomerTableViewDlg.ui" line="72"/>
        <source>Export selected as &amp;new list</source>
        <translation>Exporter la sélection comme &amp;nouvelle liste</translation>
    </message>
</context>
<context>
    <name>MzLabWnd</name>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="14"/>
        <source>massXpert: m/z lab</source>
        <translation>massXpert: Labo m/z</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="39"/>
        <location filename="../gui/ui/mzLabWnd.ui" line="75"/>
        <source>Catalogue of all (m/z,z) lists available</source>
        <oldsource>Catalogue of all working lists currently available</oldsource>
        <translation>Catalogue de toutes les listes (m/z,z) disponibles</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="180"/>
        <source>Actions on a single list</source>
        <translation>Actions sur liste unique</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="27"/>
        <source>Working lists</source>
        <translation>Listes de travail</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="88"/>
        <source>Create a new list</source>
        <translation>Créer nouvelle liste</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="91"/>
        <source>&amp;New list (Ctrl+N)</source>
        <translation>&amp;Nouvelle liste (Ctrl+N)</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="94"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="49"/>
        <source>Remove the list selected in Catalogue 1</source>
        <oldsource>Remove the selected list from Catalogue 1</oldsource>
        <translation>Supprimer la liste sélectionnée en Catalogue 1</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="33"/>
        <source>Catalogue &amp;1</source>
        <translation>Catalogue &amp;1</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="52"/>
        <source>&amp;Remove list selected
in Catalogue 1 (Ctrl+D)</source>
        <oldsource>&amp;Delete list selected
in Catalogue 1 (Ctrl+D)</oldsource>
        <translation>&amp;Supprimer liste sélectionnée
en Catalogue 1 (Ctrl+S)</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="56"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="69"/>
        <source>Catalogue &amp;2</source>
        <translation>Catalogue &amp;2</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="114"/>
        <source>Default ionization</source>
        <translation>Ionisation par défaut</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="137"/>
        <source>Unitary formula:</source>
        <translation>Formule unitaire:</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="272"/>
        <source>Mass-based actions</source>
        <translation>Actions sur masse</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="279"/>
        <source>Formula-based actions</source>
        <translation>Actions sur formule</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="291"/>
        <source>Actions on multiplelists</source>
        <translation>Actions sur listes multiples</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="297"/>
        <source>Match-based actions</source>
        <translation>Actions sur correspondance</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="120"/>
        <source>Ionization formula</source>
        <translation>Formule d&apos;ionisation</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="127"/>
        <source>Unitary charge:</source>
        <translation>Charge unitaire:</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="147"/>
        <source>Ionization unitary charge</source>
        <translation>Charge unitaire d&apos;ionisation</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="160"/>
        <source>Ionization level:</source>
        <translation>Niveau d&apos;ionisation:</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="170"/>
        <source>Ionization level</source>
        <translation>Niveau d&apos;ionisation</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="188"/>
        <source>Catalogue containing the selected target list:</source>
        <translation>Catalogue comportant la liste cible sélectionnée:</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="258"/>
        <source>Perform computation in place</source>
        <translation>Effectuer calcul sur place</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="213"/>
        <source>Cat. 1</source>
        <translation>Cat. 1</translation>
    </message>
    <message>
        <location filename="../gui/ui/mzLabWnd.ui" line="236"/>
        <source>Cat. 2</source>
        <translation>Cat. 2</translation>
    </message>
</context>
<context>
    <name>PkaPhPiDlg</name>
    <message>
        <location filename="../gui/ui/pkaPhPiDlg.ui" line="49"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../gui/ui/pkaPhPiDlg.ui" line="61"/>
        <source>Choose a file in which to store the data for the plotting of the isotopic peak. </source>
        <translation>Choisir un fichier dans lequel stocker les données pour faire le graphe du pic isotopique. </translation>
    </message>
    <message>
        <location filename="../gui/ui/pkaPhPiDlg.ui" line="64"/>
        <source>&amp;Isoelectric point</source>
        <translation>Point &amp;isoélectrique</translation>
    </message>
    <message>
        <location filename="../gui/ui/pkaPhPiDlg.ui" line="71"/>
        <source>&amp;Net charge</source>
        <translation>Charge &amp;nette</translation>
    </message>
    <message>
        <location filename="../gui/ui/pkaPhPiDlg.ui" line="78"/>
        <source>Export the results either to the clipboard or to a file</source>
        <translation>Exporter les résultats vers le presse-papiers ou vers un fichier</translation>
    </message>
    <message>
        <location filename="../gui/ui/pkaPhPiDlg.ui" line="94"/>
        <source>Input data</source>
        <translation>Données entrées</translation>
    </message>
    <message>
        <location filename="../gui/ui/pkaPhPiDlg.ui" line="132"/>
        <source>&amp;Selected sequence</source>
        <translation>&amp;Séquence sélectionnée</translation>
    </message>
    <message>
        <location filename="../gui/ui/pkaPhPiDlg.ui" line="139"/>
        <source>&amp;Whole sequence</source>
        <translation>Séquence &amp;entière</translation>
    </message>
    <message>
        <location filename="../gui/ui/pkaPhPiDlg.ui" line="170"/>
        <source>&amp;pH:</source>
        <translation>&amp;pH:</translation>
    </message>
    <message>
        <location filename="../gui/ui/pkaPhPiDlg.ui" line="208"/>
        <source>Results</source>
        <translation>Résultats</translation>
    </message>
    <message>
        <location filename="../gui/ui/pkaPhPiDlg.ui" line="228"/>
        <source>Chemical groups tested</source>
        <translation>Groupes chimiques testés</translation>
    </message>
    <message>
        <location filename="../gui/ui/pkaPhPiDlg.ui" line="256"/>
        <source>Isoelectric point (pI)</source>
        <translation>Point isoélectrique (pI)</translation>
    </message>
    <message>
        <location filename="../gui/ui/pkaPhPiDlg.ui" line="286"/>
        <source>Charges</source>
        <translation>Charges</translation>
    </message>
    <message>
        <location filename="../gui/ui/pkaPhPiDlg.ui" line="306"/>
        <source>Net:</source>
        <translation>Nette:</translation>
    </message>
    <message>
        <location filename="../gui/ui/pkaPhPiDlg.ui" line="336"/>
        <source>Negative:</source>
        <translation>Négative(s):</translation>
    </message>
    <message>
        <location filename="../gui/ui/pkaPhPiDlg.ui" line="366"/>
        <source>Positive:</source>
        <translation>Positive (s):</translation>
    </message>
    <message>
        <location filename="../gui/ui/pkaPhPiDlg.ui" line="19"/>
        <source>massXpert: Isoelectric point (pI) calculator</source>
        <translation>massXpert: Calculateur de point isoélectrique (pI)</translation>
    </message>
</context>
<context>
    <name>PolChemDefWnd</name>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="37"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="52"/>
        <source>Singular entities</source>
        <translation>Entités singulières</translation>
    </message>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="64"/>
        <source>Caps</source>
        <translation>Coiffes</translation>
    </message>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="70"/>
        <source>Left:</source>
        <translation>Gauche:</translation>
    </message>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="83"/>
        <source>Right:</source>
        <translation>Droite:</translation>
    </message>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="105"/>
        <source>Ionization rule</source>
        <translation>Règle d&apos;ionisation</translation>
    </message>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="111"/>
        <source>Formula:</source>
        <translation>Formule:</translation>
    </message>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="124"/>
        <source>Charge:</source>
        <translation>Charge:</translation>
    </message>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="144"/>
        <source>Level:</source>
        <translation>Niveau:</translation>
    </message>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="167"/>
        <source>Plural entities</source>
        <translation>Entités plurielles</translation>
    </message>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="173"/>
        <source>Atoms</source>
        <translation>Atomes</translation>
    </message>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="183"/>
        <source>Monomers</source>
        <translation>Monomères</translation>
    </message>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="193"/>
        <source>Modifications</source>
        <translation>Modifications</translation>
    </message>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="203"/>
        <source>CrossLinkers</source>
        <translation>Agents pontants</translation>
    </message>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="213"/>
        <source>Cleavages</source>
        <translation>Clivages</translation>
    </message>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="223"/>
        <source>Fragmentations</source>
        <translation>Fragmentations</translation>
    </message>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="244"/>
        <source>&amp;Validate</source>
        <translation>&amp;Valider</translation>
    </message>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="251"/>
        <source>&amp;Save</source>
        <translation>&amp;Sauvegarder</translation>
    </message>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="258"/>
        <source>Save &amp;as</source>
        <translation>Sauvegarder &amp;nouveau</translation>
    </message>
    <message>
        <location filename="../gui/ui/polChemDefWnd.ui" line="13"/>
        <source>massXpert: Polymer chemistry definition</source>
        <translation>massXpert: Définition de chimie de polymère</translation>
    </message>
</context>
<context>
    <name>PolymerModificationDlg</name>
    <message>
        <location filename="../gui/ui/polymerModificationDlg.ui" line="31"/>
        <source>List of all the modifications available in the polymer chemistry definition. Only one item is selectable at a time.</source>
        <translation>Liste de toutes les modifications disponibles dans la définition de chimie de polymère. Un élément unique est sélectionnable à la fois.</translation>
    </message>
    <message>
        <location filename="../gui/ui/polymerModificationDlg.ui" line="34"/>
        <source>Available modifications</source>
        <translation>Modifications disponibles</translation>
    </message>
    <message>
        <location filename="../gui/ui/polymerModificationDlg.ui" line="66"/>
        <source>Define modification</source>
        <translation>Définir modification</translation>
    </message>
    <message>
        <location filename="../gui/ui/polymerModificationDlg.ui" line="78"/>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <location filename="../gui/ui/polymerModificationDlg.ui" line="91"/>
        <source>Formula:</source>
        <translation>Formule:</translation>
    </message>
    <message>
        <location filename="../gui/ui/polymerModificationDlg.ui" line="107"/>
        <source>The controls in this group box permit the selection of the monomers in the sequence that will be modified (or unmodified).</source>
        <translation>Les contrôles dans cette boîte groupe permettent la sélection des monomères dans la séquence devant être modifiés (ou démodifiés).</translation>
    </message>
    <message>
        <location filename="../gui/ui/polymerModificationDlg.ui" line="110"/>
        <source>Target</source>
        <translation>Cible</translation>
    </message>
    <message>
        <location filename="../gui/ui/polymerModificationDlg.ui" line="165"/>
        <source>&amp;Left end</source>
        <translation>Extrémité &amp;gauche</translation>
    </message>
    <message>
        <location filename="../gui/ui/polymerModificationDlg.ui" line="198"/>
        <source>&amp;Right end</source>
        <translation>Extrémité &amp;droite</translation>
    </message>
    <message>
        <location filename="../gui/ui/polymerModificationDlg.ui" line="219"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../gui/ui/polymerModificationDlg.ui" line="243"/>
        <source>&amp;Unmodify</source>
        <translation>&amp;Démodifier</translation>
    </message>
    <message>
        <location filename="../gui/ui/polymerModificationDlg.ui" line="250"/>
        <source>&amp;Modify</source>
        <translation>&amp;Modifier</translation>
    </message>
    <message>
        <location filename="../gui/ui/polymerModificationDlg.ui" line="16"/>
        <source>massXpert: Polymer modification</source>
        <translation>massXpert: Modification de polymère</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="507"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="274"/>
        <source>
Ionization rule:
</source>
        <translation>
Règle d&apos;ionisation :
</translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="509"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="276"/>
        <source>Formula: %1 - </source>
        <translation>Formule : %1 - </translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="512"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="279"/>
        <source>Charge: %1 - </source>
        <translation>Charge : %1 - </translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="515"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="282"/>
        <source>Level: %1
</source>
        <translation>Niveau : %1
</translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="519"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="285"/>
        <source>
Calculation options:
</source>
        <translation>
Options de calcul :
</translation>
    </message>
    <message>
        <location filename="../gui/pkaPhPiDlg.cpp" line="378"/>
        <source>Start position: %1 - End position: %2 - Sequence: %3

</source>
        <translation>Position de début : %1 - Position de fin : %2 - Séquence : %3

</translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="500"/>
        <source># 
# -------------
# Compositions: 
# -------------
</source>
        <translation># 
# -------------
# Compositions: 
# -------------
</translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="537"/>
        <location filename="../gui/pkaPhPiDlg.cpp" line="386"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="288"/>
        <source>Account monomer modifs: yes
</source>
        <translation>Prendre en compte modifs de monomère : oui
</translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="539"/>
        <location filename="../gui/pkaPhPiDlg.cpp" line="388"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="290"/>
        <source>Account monomer modifs: no
</source>
        <translation>Prendre en compte modifs de monomère : non
</translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="549"/>
        <location filename="../gui/pkaPhPiDlg.cpp" line="398"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="309"/>
        <source>Account ends&apos; modifs: no
</source>
        <translation>Prendre en compte les modifs d&apos;extrémité : non
</translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="553"/>
        <location filename="../gui/pkaPhPiDlg.cpp" line="402"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="313"/>
        <source>Account ends&apos; modifs: yes - </source>
        <translation>Prendre en compte les modifs d&apos;extrémité : oui - </translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="560"/>
        <location filename="../gui/pkaPhPiDlg.cpp" line="409"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="320"/>
        <source>Left end modif: %1 - </source>
        <translation>Modif d&apos;extrémité gauche : %1 - </translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="569"/>
        <location filename="../gui/pkaPhPiDlg.cpp" line="418"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="329"/>
        <source>Right end modif: %1</source>
        <translation>Modif d&apos;extrémité droite : %1 - </translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="574"/>
        <source>

Elemental composition: %1</source>
        <translation>

Composition élémentale: %1</translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="598"/>
        <location filename="../gui/compositionsDlg.cpp" line="607"/>
        <source>%1 - </source>
        <translation>%1 - </translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="616"/>
        <source>Modified ?: %1 - </source>
        <translation>Modifié ?: %1 - </translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="625"/>
        <source>Count: %1.
</source>
        <translation>Décompte : %1.
</translation>
    </message>
    <message>
        <location filename="../gui/main.cpp" line="123"/>
        <source>Failed loading the translation file:</source>
        <translation>Échec de la lecture du fichier de traduction:</translation>
    </message>
    <message>
        <location filename="../gui/main.cpp" line="149"/>
        <source>The following options are available:
</source>
        <translation>Les options suivantes sont disponibles :
</translation>
    </message>
    <message>
        <location filename="../gui/main.cpp" line="150"/>
        <source>? | -h | --help : print this help
</source>
        <translation>? | -h | --help : print this help
</translation>
    </message>
    <message>
        <location filename="../gui/main.cpp" line="165"/>
        <source>massXpert, version %1

</source>
        <translation>massXpert, version %1

</translation>
    </message>
    <message>
        <location filename="../gui/main.cpp" line="168"/>
        <source>Type &apos;massXpert --help&apos; for help

</source>
        <translation>Taper &apos;massXpert --help&apos; pour de l&apos;aide

</translation>
    </message>
    <message>
        <location filename="../gui/main.cpp" line="193"/>
        <source>massXpert, version %1 -- Compiled against Qt, version %2
</source>
        <translation>massXpert, version %1 -- Compilé avec Qt, version %2
</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="1538"/>
        <source># 
# ---------------------------
# Mass Search: 
# ---------------------------
</source>
        <translation># 
# ------------------------------
# Recherche de masse: 
# -------------------------------
</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="1569"/>
        <source>
Searched mono masses:
%1
</source>
        <translation>
Masses mono recherchées :
%1
</translation>
    </message>
    <message>
        <location filename="../gui/massSearchOligomerTableView.cpp" line="260"/>
        <source>
Searched mass 	 : Name 	 :Coordinates 	 : Error 	 :Mono 	 : Avg 	 : Modif

</source>
        <translation>
Masse recherchée 	 : Nom 	 :Coordonnées 	 : Erreur 	 :Mono 	 : Moy 	 : Modif

</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="1603"/>
        <source>
Searched average masses:
%1
</source>
        <translation>
Masses moyennes recherchées :
%1
</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="664"/>
        <source># 
# ---------------------------
# m/z Calculations: 
# ---------------------------
</source>
        <translation># 
# ----------------------
# Calculs de m/z: 
# ----------------------
</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="671"/>
        <source>
Source conditions:
------------------
</source>
        <translation>
Conditions source:
------------------
</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="674"/>
        <source>Mono mass: %1</source>
        <translation>Masse mono : %1</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="677"/>
        <source> - Avg mass: %1
</source>
        <translation> - Masse moy : %1
</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="680"/>
        <location filename="../gui/mzCalculationDlg.cpp" line="692"/>
        <source>Ionization formula: %1
</source>
        <translation>Formule d&apos;ionisation : %1
</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="683"/>
        <location filename="../gui/mzCalculationDlg.cpp" line="695"/>
        <source>Ionization charge: %1 - </source>
        <translation>Charge d&apos;ionisation : %1 - </translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="686"/>
        <source>Ionization level: %1
</source>
        <translation>Niveau d&apos;ionisation : %1
</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="689"/>
        <source>
Destination conditions:
-----------------------
</source>
        <translation>
Conditions destination:
-----------------------
</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="698"/>
        <source>Start level: %1 - </source>
        <translation>Niveau de début : %1 - </translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="701"/>
        <source>End level: %1


</source>
        <translation>Niveau de fin : %1


</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="737"/>
        <source>Level: %1 -- </source>
        <translation>Niveau : %1 -- </translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="756"/>
        <source>Mono: %1 -- </source>
        <translation>Mono : %1 -- </translation>
    </message>
    <message>
        <location filename="../gui/pkaPhPiDlg.cpp" line="365"/>
        <source># 
# ----------------------------
# pKa - pH - pI Calculations: 
# ----------------------------
</source>
        <translation># 
# --------------------------------
#Calculs de pKa - pH - pI: 
# --------------------------------
</translation>
    </message>
    <message>
        <location filename="../gui/pkaPhPiDlg.cpp" line="429"/>
        <source>
pI Calculation:
---------------
pI value: %1 - Chemical groups tested: %2

</source>
        <translation>
Calcul de pI :
---------------
Valeur de pI : %1 - Groupes chimiques testés : %2

</translation>
    </message>
    <message>
        <location filename="../gui/pkaPhPiDlg.cpp" line="441"/>
        <source>
Net Charge Calculation:
-----------------------
At pH value: %1
Chemical groups tested: %2
</source>
        <translation>
Calcul de charge nette :
-----------------------
Valeur de pH : %1
Groupes chimiques testés : %2
</translation>
    </message>
    <message>
        <location filename="../gui/pkaPhPiDlg.cpp" line="451"/>
        <source>Positive: %1 - </source>
        <translation>Positive(s) : %1 - </translation>
    </message>
    <message>
        <location filename="../gui/pkaPhPiDlg.cpp" line="456"/>
        <source>Negative:  %1 - </source>
        <translation>Negative(s) :  %1 - </translation>
    </message>
    <message>
        <location filename="../gui/pkaPhPiDlg.cpp" line="462"/>
        <source>Net:  %1

</source>
        <translation>Nette:  %1

</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="260"/>
        <source>
---------------------------
Sequence Data: %1
---------------------------
Name: %1
Code : %2
File path: %3
Sequence: %4
</source>
        <translation>
---------------------------
Données de séquence : %1
---------------------------
Nom : %1
Code : %2
Chemin de fichier : %3
Séquence : %4
</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="395"/>
        <source>

Whole sequence mono mass: %1
</source>
        <translation>

Masse mono séquence entière : %1
</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="398"/>
        <source>Whole sequence avg mass: %1

</source>
        <translation>Masse moy séquence entière : %1

</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="424"/>
        <source>Selected sequence mono mass: %1
</source>
        <translation>Masse mono séquence sélectionnée : %1
</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="427"/>
        <source>Selected sequence avg mass: %1
</source>
        <translation>Masse moy séquence sélectionnée : %1</translation>
    </message>
    <message>
        <location filename="../gui/main.cpp" line="151"/>
        <source>-v | --version : print version
</source>
        <translation>-v | --version : imprime la version
</translation>
    </message>
    <message>
        <location filename="../gui/main.cpp" line="152"/>
        <source>-c | --config : print configuration
</source>
        <translation>-c | --config : print configuration
</translation>
    </message>
    <message>
        <location filename="../gui/main.cpp" line="153"/>
        <source>
</source>
        <translation>
</translation>
    </message>
    <message>
        <location filename="../gui/main.cpp" line="205"/>
        <source>massXpert: Compiled with the following configuration:
EXECUTABLE BINARY FILE: = %1
MASSXPERT_BIN_DIR = %3
MASSXPERT_PLUGIN_DIR = %4
MASSXPERT_DATA_DIR = %5
MASSXPERT_LOCALE_DIR = %6
MASSXPERT_USERMAN_DIR = %7
</source>
        <translation>massXpert: Compilé avec la configuration suivante:EXECUTABLE BINARY FILE: = %1MASSXPERT_BIN_DIR = %3MASSXPERT_PLUGIN_DIR = %4MASSXPERT_DATA_DIR = %5MASSXPERT_LOCALE_DIR = %6MASSXPERT_USERMAN_DIR = %7</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="296"/>
        <source>Account cross-links: yes
</source>
        <translation>Prendre en compte les pontages: oui
</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="298"/>
        <source>Account cross-links: no
</source>
        <translation>Prendre en compte les pontages: non
</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="375"/>
        <source>

Cross-links:
</source>
        <translation>

Pontages:
</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="420"/>
        <source>%1

</source>
        <translation>%1

</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="339"/>
        <source>
Multi-region selection enabled: yes
</source>
        <translation>
Sélection multi-région active: oui
</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="344"/>
        <source>Multi-region selections are treated as oligomers
</source>
        <translation>Les sélections multi-région sont traitées comme des oligomères
</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="350"/>
        <source>Multi-region selections are treated as residual chains
</source>
        <translation>Les sélections multi-région sont traitées comme des chaînes résiduelles
</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="357"/>
        <source>Multi-selection region enabled: yes
</source>
        <translation>Région multi-sélection active: oui
</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="362"/>
        <source>Multi-selection region enabled: no
</source>
        <translation>Région multi-sélection active: non
</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="368"/>
        <source>
Multi-region selection enabled: no
</source>
        <translation>
Sélection multi-région active: non
</translation>
    </message>
    <message>
        <location filename="../gui/main.cpp" line="170"/>
        <source>massXpert is Copyright 2000, 2001,
2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009 
by Filippo Rusconi.

massXpert comes with ABSOLUTELY NO WARRANTY.
massXpert is free software, covered by the GNU General
Public License Version 3, and you are welcome to change it
and/or distribute copies of it under certain conditions.
Check the file COPYING in the distribution and/or the
&apos;Help/About(Ctrl+H)&apos; menu item of the program.

Happy massXpert&apos;ing!

</source>
        <translation>massXpert is Copyright 2000, 2001,2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009 by Filippo Rusconi.massXpert comes with ABSOLUTELY NO WARRANTY.massXpert is free software, covered by the GNU GeneralPublic License Version 3, and you are welcome to change itand/or distribute copies of it under certain conditions.Check the file COPYING in the distribution and/or the&apos;Help/About(Ctrl+H)&apos; menu item of the program.Happy massXpert&apos;ing!</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="775"/>
        <source>Avg: %1</source>
        <translation>Moy:%1</translation>
    </message>
    <message>
        <location filename="../lib/crossLink.cpp" line="483"/>
        <source>The number of modification items does not match the number of monomers. This is an error.</source>
        <translation>Le nombre d&apos;éléments de modification ne correspond pas au nombre des monomères à modifier. C&apos;est une erreur.</translation>
    </message>
    <message>
        <location filename="../lib/crossLink.cpp" line="502"/>
        <source>The monomer &apos;%1&apos; is not target of crossLinker modif &apos;%2&apos;</source>
        <translation>Le monomère &apos;%1&apos; n&apos;est pas cible de la modification d&apos;agent pontant &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../lib/crossLink.cpp" line="542"/>
        <source>
Cross-link:
===============
Cross-linker name: %1
Cross-link comment: %2
</source>
        <translation>
Agent pontant:
===============
Nom d&apos;agent pontant: %1
Commentaire d&apos;agent pontant: %2
</translation>
    </message>
    <message>
        <location filename="../lib/crossLink.cpp" line="555"/>
        <source>Partner %1: %2 at position: %3
</source>
        <translation>Partenaire %1 : %2 à la position: %3
</translation>
    </message>
    <message>
        <location filename="../lib/monomer.cpp" line="515"/>
        <source>	%1 not target of modif %2 (no overriding allowed)</source>
        <translation>	%1 non cible de la modification %2 (pas d&apos;exception autorisée)</translation>
    </message>
    <message>
        <location filename="../lib/monomer.cpp" line="529"/>
        <source>	%1 already modified %2 times (no overriding allowed)</source>
        <translation>	%1 déjà modifié %2 fois (pas d&apos;exception autorisée)</translation>
    </message>
    <message>
        <location filename="../lib/monomerDictionary.cpp" line="231"/>
        <location filename="../lib/monomerDictionary.cpp" line="261"/>
        <source>Monomer dictionary:</source>
        <translation>Dictionnaire de monomère :</translation>
    </message>
    <message>
        <location filename="../lib/monomerDictionary.cpp" line="232"/>
        <location filename="../lib/monomerDictionary.cpp" line="262"/>
        <source>Failed to load dictionary.</source>
        <translation>Échec du chargement du dictionnaire.</translation>
    </message>
    <message>
        <location filename="../lib/monomerDictionary.cpp" line="233"/>
        <source>Monomer code lengths do not match:</source>
        <translation>Les longueurs de code de monomère ne correspondent pas :</translation>
    </message>
    <message>
        <location filename="../lib/monomerDictionary.cpp" line="234"/>
        <source>inputCode:</source>
        <translation>Code en entrée :</translation>
    </message>
    <message>
        <location filename="../lib/monomerDictionary.cpp" line="235"/>
        <source>outputCode:</source>
        <translation>Code en sortie :</translation>
    </message>
    <message>
        <location filename="../lib/prop.cpp" line="984"/>
        <source>%1-This function does not return anything interesting-%2</source>
        <translation>%1-Cette fonction ne renvoie pas de valeur utile-%2</translation>
    </message>
    <message>
        <location filename="../lib/userSpec.cpp" line="74"/>
        <source>NOT_SET</source>
        <translation>NOT_SET</translation>
    </message>
    <message>
        <location filename="../lib/fragmenter.cpp" line="549"/>
        <location filename="../lib/fragmenter.cpp" line="661"/>
        <location filename="../lib/fragmenter.cpp" line="966"/>
        <location filename="../lib/fragmenter.cpp" line="1078"/>
        <location filename="../lib/fragmenter.cpp" line="1384"/>
        <location filename="../lib/fragmenter.cpp" line="1496"/>
        <source>massxpert - Fragmentation : Failed to generate ionized fragment oligomers.</source>
        <translation>massxpert - Fragmentation : échec de la production des oligomères fragments ionisés.</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="838"/>
        <source># 
# ---------------------------
# Polymer sequence cleavage: 
# ---------------------------
</source>
        <translation># 
# ---------------------------------------------------
# Coupure de séquence de polymère: 
# ---------------------------------------------------
</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="840"/>
        <source># 
# ---------------------------
# Fragmentation: 
# ---------------------------
</source>
        <translation># 
# ---------------------------
# Fragmentation: 
# ---------------------------
</translation>
    </message>
</context>
<context>
    <name>SequenceEditorFindDlg</name>
    <message>
        <location filename="../gui/ui/sequenceEditorFindDlg.ui" line="24"/>
        <source>Sequence motif to find:</source>
        <translation>Motif de séquence à trouver:</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorFindDlg.ui" line="40"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorFindDlg.ui" line="46"/>
        <source>&amp;Clear history</source>
        <extracomment>historique en français</extracomment>
        <translation>&amp;Effacer historique</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorFindDlg.ui" line="84"/>
        <source>&amp;Find</source>
        <translation>&amp;Trouver</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorFindDlg.ui" line="91"/>
        <source>&amp;Next</source>
        <translation>&amp;Suivant</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorFindDlg.ui" line="14"/>
        <source>massXpert: Find sequence</source>
        <translation>massXpert: Recherche de séquence</translation>
    </message>
</context>
<context>
    <name>SequenceEditorWnd</name>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="43"/>
        <source>Sequence name:</source>
        <translation>Nom de la séquence:</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="53"/>
        <source>Size of the monomer vignettes (pixels)</source>
        <translation>Taille des vignettes de monomère (pixels)</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="56"/>
        <source>The monomers of the sequence are displayed graphically as graphical vignettes. The vignettes have to be squared and have a square side of a given pixel size. This widget allows one to set the size of the vignettes.</source>
        <translation>Les monomères de la séquence sont affichés sous forme de vignettes graphiques. Les vignettes doivent être carrées. Ce contrôle permet de définir la taille des vignettes.</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="150"/>
        <source>Masses</source>
        <translation>Masses</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="162"/>
        <source>Whole sequence</source>
        <translation>Séquence entière</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="198"/>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="233"/>
        <source>Mono:</source>
        <translation>Mono:</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="168"/>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="243"/>
        <source>Avg:</source>
        <translation>Moy:</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="217"/>
        <source>Selected sequence</source>
        <translation>Séquence sélectionnée</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="298"/>
        <source>Polymer modifications</source>
        <translation>Modifications de polymère</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="304"/>
        <source>Left end:</source>
        <translation>Extrémité gauche:</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="318"/>
        <source>Left end modif</source>
        <translation>Modif gauche</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="311"/>
        <source>Right end:</source>
        <translation>Extrémité droite:</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="76"/>
        <source>This window
identifier: </source>
        <translation>Identifiant de
cette fenêtre:</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="84"/>
        <source>Use this identifier to connect to this window from another window</source>
        <translation>Utiliser cet identifiant pour se connecter à cette fenêtre depuis une autre fenêtre </translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="338"/>
        <source>Right end modif</source>
        <translation>Modif droite</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="377"/>
        <source>Calculation engine</source>
        <translation>Moteur de calcul</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="383"/>
        <source>Polymer</source>
        <translation>Polymère</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="403"/>
        <source>Left cap</source>
        <translation>Coiffe gauche</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="410"/>
        <source>Right cap</source>
        <translation>Coiffe droite</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="427"/>
        <source>Left modif</source>
        <translation>Modif gauche</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="444"/>
        <source>Right modif</source>
        <translation>Modif droite</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="466"/>
        <source>Monomers</source>
        <translation>Monomères</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="472"/>
        <source>Modifications</source>
        <translation>Modifications</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="479"/>
        <source>Cross-links</source>
        <translation>Pontages</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="489"/>
        <source>Ionization</source>
        <translation>Ionisation</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="517"/>
        <source>Unitary charge</source>
        <translation>Charge unitaire</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="524"/>
        <source>Ionization unitary charge</source>
        <translation>Charge unitaire d&apos;ionisation</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="547"/>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="554"/>
        <source>Ionization level</source>
        <translation>Niveau d&apos;ionisation</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="573"/>
        <source>Unitary formula</source>
        <translation>Formule unitaire</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="580"/>
        <source>Ionization formula</source>
        <translation>Formule d&apos;ionisation</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="682"/>
        <source>Monomer list</source>
        <translation>Liste des monomères</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="17"/>
        <source>massXpert: Sequence editor</source>
        <translation>massXpert: Éditeur de séquence</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="434"/>
        <source>Force account of the left modif in selected sequence </source>
        <translation>Forcer la prise en compte de la modif de gauche dans la séquence sélectionnée</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="437"/>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="454"/>
        <source>Force</source>
        <translation>Forcer</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="451"/>
        <source>Force account of the right modif in selected sequence </source>
        <translation>Forcer la prise en compte de la modif de droite dans la séquence sélectionnée</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="605"/>
        <source>Selections and regions</source>
        <translation>Sélections et régions</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="611"/>
        <source>Selected regions behave as oligomers (are capped residual chains)</source>
        <translation>Les régions sélectionnées se comportent comme des oligomères (chaînes résiduelles cappées)</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="614"/>
        <source>Oligomers</source>
        <translation>Oligomères</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="632"/>
        <source>Selected regions behave as residual chains (are uncapped residual chains)</source>
        <translation>Les régions sélectionnées se comportent comme des chaînes résiduelles (chaînes résiduelles non cappées)</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="635"/>
        <source>Residual chains</source>
        <translation>Chaînes résiduelles</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="644"/>
        <source>One region might be selected more than once</source>
        <translation>Une région peut être sélectionnée plus d&apos;une fois</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="647"/>
        <source>Multi-selection</source>
        <translation>Multi-sélection</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="657"/>
        <source>There might be more than one region selected</source>
        <translation>Plus d&apos;une région peut être sélectionnée</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceEditorWnd.ui" line="660"/>
        <source>Multi-region</source>
        <translation>Multi-région</translation>
    </message>
</context>
<context>
    <name>SequenceImportDlg</name>
    <message>
        <location filename="../gui/ui/sequenceImportDlg.ui" line="14"/>
        <source>massXpert: Sequence purification</source>
        <translation>massXpert: Purification de séquence</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceImportDlg.ui" line="31"/>
        <source>List of imported sequences</source>
        <translation>Liste des séquences importées</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequenceImportDlg.ui" line="47"/>
        <source>&amp;Sequence</source>
        <translation>&amp;Séquence</translation>
    </message>
</context>
<context>
    <name>SequencePurificationDlg</name>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="41"/>
        <source>&amp;Initial sequence</source>
        <translation>Séquence &amp;initiale</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="73"/>
        <source>Puri&amp;fied sequence</source>
        <translation>Séquence puri&amp;fiée</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="127"/>
        <source>Purification options</source>
        <translation>Options de purification</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="168"/>
        <source>Lowercase</source>
        <translation>Minuscules</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="175"/>
        <source>Uppercase</source>
        <translation>Majuscules</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="201"/>
        <source>&amp;Numerals</source>
        <translation>&amp;Numériques</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="208"/>
        <source>&amp;Spaces</source>
        <translation>&amp;Espaces</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="221"/>
        <source>P&amp;unctuation</source>
        <translation>P&amp;onctuation</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="247"/>
        <source>&amp;Others (RegExp):</source>
        <translation>&amp;Autres (RegExp):</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="299"/>
        <source>Remove all characters tagged from the Initial Sequence.</source>
        <translation>Enlever tous les caractères étiquetés de la Séquence initiale.</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="302"/>
        <source>When invalid characters are found in a text, they are tagged upon display of the text in the text edit widgets. Removes all tagged characters from the text in the Initial Sequence text edit widget. The resulting text is displayed in the Purified Sequence text edit widget.</source>
        <translation>Quand des caractères incorrects sont trouvés dans le texte, ils sont étiquetés lors de l&apos;affichage du text dans les contrôles d&apos;édition de texte. Enlève tous les caractères étiquetés du texte se trouvant dans le contôle d&apos;édition de texte Séquence Initiale. Le texte obtenu est affiché dans le contrôle d&apos;édition de texte Séquence purifiée.</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="305"/>
        <source>Remove &amp;tagged from initial</source>
        <translation>Enlever &amp;étiquetés de initial</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="312"/>
        <source>Tests the text in the Purified Sequence text edit widget and tags incorrect characters.</source>
        <translation>Tester le texte dans le contrôle d&apos;édition de texte Séquence purifiée et étiquette les caractères incorrects.</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="315"/>
        <source>Parses the text in the Purified Sequence text edit widget and tags all the invalid characters. The sequence that was displayed in this text edit widget is moved to the Initial Sequence text edit widget.</source>
        <translation>Vérifie le texte dans le contrôle d&apos;édition Séquence purifiée et étiquette tous les caractères invalides. La séquence précédemment affichée dans ce contrôle d&apos;édition de texte est déplacée dans le contrôle d&apos;édition de texte Séquence initiale.</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="318"/>
        <source>T&amp;est purified</source>
        <translation>T&amp;ester séquence purifiée</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="344"/>
        <source>Purifies the Initial Sequence text from all the characters matching the Purification Options that are selected.</source>
        <translation>Purifie la Séquence initiale de tous les caractères correspondant aux options de purification sélectionnées.</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="347"/>
        <source>Purifies the Initial Sequence text from all the characters matching the Purification Options that are selected. The purified sequence will be displayed in the right text edit widget. At second round the sequences are recycled from right to left and purification occurs for the sequence in the Purified Sequence text edit widget.</source>
        <translation>Purifie la Séquence initial de tous les caractères correspondant aux options de purification sélectionnées. La séquence purifiée sera affichée dans le contrôle d&apos;édition de droite. Lors d&apos;un deuxième tour de purification, les séquences sont recyclées de la droite vers la gauche et la purification a lieu sur la séquence se trouvant dans le contrôle d&apos;édition de texte Séquence purifiée.</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="350"/>
        <source>&amp;Purify initial (options)</source>
        <translation>&amp;Purifier initiale (options)</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="357"/>
        <source>Resets the Initial Sequence to the initial text.</source>
        <translation>Réinitialise la Séquence initiale avec texte originel.</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="360"/>
        <source>&amp;Reset to initial </source>
        <translation>&amp;Réinitialiser à l&apos;original</translation>
    </message>
    <message>
        <location filename="../gui/ui/sequencePurificationDlg.ui" line="13"/>
        <source>massXpert: Sequence purification</source>
        <translation>massXpert: Purification de séquence</translation>
    </message>
</context>
<context>
    <name>SpectrumCalculationDlg</name>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="14"/>
        <source>massXpert: Spectrum calculator</source>
        <translation>massXpert : calculateur de spectre</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="28"/>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="34"/>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="312"/>
        <source>Input data</source>
        <translation>Données entrées</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="40"/>
        <source>m/z</source>
        <translation>m/z</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="48"/>
        <source>Formula:</source>
        <translation>Formule:</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="58"/>
        <source>Formula of the non-ionized analyte</source>
        <translation>Formule de l&apos;analyte non ionisé</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="67"/>
        <source>z: </source>
        <translation>z: </translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="87"/>
        <source>m/z: </source>
        <translation>m/z: </translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="113"/>
        <source>Mass type</source>
        <translation>Type de masse</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="119"/>
        <source>Mono</source>
        <translation>Mono</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="129"/>
        <source>Avg</source>
        <translation>Moy</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="136"/>
        <source>Isotopic cluster</source>
        <translation>Cluster isotopique</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="146"/>
        <source>Spectrum</source>
        <translation>Spectre</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="155"/>
        <source>gaussian</source>
        <translation>gaussienne</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="165"/>
        <source>lorentzian</source>
        <translation>lorentzienne</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="172"/>
        <source>Points:</source>
        <translation>Points:</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="192"/>
        <source>Increment:</source>
        <translation>Incrément:</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="199"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="209"/>
        <source>Min. probability:</source>
        <translation>Probabilité min.:</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="216"/>
        <source>Can be like 1e-6</source>
        <translation>Peut être de la forme 1e-6</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="223"/>
        <source>Max. &amp;peaks:</source>
        <translation>&amp;Pics max.:</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="233"/>
        <source>Maximum number of peaks that must be calculated</source>
        <translation>Nombre maximum de pics qui doivent être calculés</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="252"/>
        <source>Resolution</source>
        <translation>Résolution</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="258"/>
        <source>&amp;Resolution:</source>
        <translation>&amp;Résolution:</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="268"/>
        <source>FWHM:</source>
        <translation>FWHM:</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="278"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:6pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Resolution of the mass spectrometer.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:6pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Résolution du spectromètre de masse.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="306"/>
        <source>Feedback</source>
        <translation>Retour</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="324"/>
        <source>Processing</source>
        <translation>Processus en cours</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="346"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="352"/>
        <source>Choose a file in which to store the data for the plotting of the isotopic peak. </source>
        <translation>Choisir un fichier dans lequel stocker les données pour faire le graphe du pic isotopique. </translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="355"/>
        <source>&amp;Output file...</source>
        <translation>&amp;Fichier de sortie...</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="362"/>
        <source>If checked all the numerical output will be generated with locale specificity.</source>
        <translation>Si coché, les données numériques seront générées en prenant en compte la localisation.</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="365"/>
        <source>&amp;Locale</source>
        <translation>&amp;Localisation</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="372"/>
        <source>&amp;Execute</source>
        <translation>&amp;Exécuter</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="379"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:6pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Abort the ongoing computation. In this case, the data that will be displayed will be incomplete and are shown only for forensic tasks.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:6pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Abandonner le calcul. Dans ce cas les données incomplètes produites seront affichées pour enquête.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="385"/>
        <source>&amp;Abort</source>
        <translation>&amp;Abandonner</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="409"/>
        <source>Log</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="419"/>
        <source>Results</source>
        <translation>Résultats</translation>
    </message>
    <message>
        <location filename="../gui/ui/spectrumCalculationDlg.ui" line="452"/>
        <source>Credit: the isotopic pattern calculation algorithm was partly adapted from the work of Dirk Nolting (nolting@uni-duesseldorf.de)</source>
        <translation>Crédit: l&apos;algorithme de calcul du patron isotopique a été partiellement adapté du travail de Dirk Nolting (nolting@uni-duesseldorf.de)</translation>
    </message>
</context>
<context>
    <name>massXpert::AboutDlg</name>
    <message>
        <location filename="../gui/aboutDlg.cpp" line="59"/>
        <source>massXpert, version %1.</source>
        <translation>massXpert, version française %1.</translation>
    </message>
</context>
<context>
    <name>massXpert::Application</name>
    <message>
        <location filename="../gui/application.cpp" line="103"/>
        <location filename="../gui/application.cpp" line="128"/>
        <source>massXpert - Configuration Settings</source>
        <translation>massXpert - Paramètres de Configuration</translation>
    </message>
    <message>
        <location filename="../gui/application.cpp" line="104"/>
        <source>%1@%2
Failed to configure massXpert.
The software will not work as intended.</source>
        <translation>%1@%2
Echec de la configuration de massXpert.
Le logiciel ne fonctionnera pas comme attendu.</translation>
    </message>
    <message>
        <location filename="../gui/application.cpp" line="129"/>
        <source>%1@%2
Failed to parse the polymer chemistry definition catalogues.
The software will not work as intended.</source>
        <translation>%1@%2
Echec de la lecture des catalogues de définition de chimie des polymères.
Le logiciel ne fonctionnera pas comme attendu.</translation>
    </message>
    <message>
        <location filename="../gui/application.cpp" line="438"/>
        <source>massXpert - Polymer chemistry definition</source>
        <translation>massXpert - Définition de chimie de polymère</translation>
    </message>
    <message>
        <location filename="../gui/application.cpp" line="439"/>
        <source>Modified polymer chemistry definition(s): %1
Do you want to save your changes?</source>
        <translation>Définition(s) de chimie de polymère modifiée(s) : %1
Voulez-vous sauvegarder les modifications ?</translation>
    </message>
    <message>
        <location filename="../gui/application.cpp" line="547"/>
        <source>massXpert - Polymer Sequence Editor</source>
        <translation>massXpert - Éditeur de Séquence de Polymère</translation>
    </message>
    <message>
        <location filename="../gui/application.cpp" line="548"/>
        <source>Modified sequence(s): %1
Do you want to really quit?</source>
        <translation>Séquence(s) modifiée(s): %1
Voulez-vous vraiment quitter?</translation>
    </message>
    <message>
        <location filename="../gui/application.cpp" line="613"/>
        <source>massXpert - mz Lab</source>
        <translation>massXpert - mz Lab</translation>
    </message>
    <message>
        <location filename="../gui/application.cpp" line="614"/>
        <source>Modified mz Lab(s): %1
Close the labs?</source>
        <translation>Mz Lab(s) modifiés: %1
Fermer les labs?</translation>
    </message>
</context>
<context>
    <name>massXpert::AtomDefDlg</name>
    <message>
        <location filename="../gui/atomDefDlg.cpp" line="213"/>
        <source>Type Name</source>
        <translation>Taper Nom</translation>
    </message>
    <message>
        <location filename="../gui/atomDefDlg.cpp" line="214"/>
        <source>Type Symbol</source>
        <translation>Taper Symbole</translation>
    </message>
    <message>
        <location filename="../gui/atomDefDlg.cpp" line="358"/>
        <location filename="../gui/atomDefDlg.cpp" line="579"/>
        <location filename="../gui/atomDefDlg.cpp" line="589"/>
        <location filename="../gui/atomDefDlg.cpp" line="642"/>
        <location filename="../gui/atomDefDlg.cpp" line="655"/>
        <location filename="../gui/atomDefDlg.cpp" line="697"/>
        <location filename="../gui/atomDefDlg.cpp" line="723"/>
        <location filename="../gui/atomDefDlg.cpp" line="731"/>
        <source>massXpert - Atom definition</source>
        <translation>massXpert -Définition d&apos;atome</translation>
    </message>
    <message>
        <location filename="../gui/atomDefDlg.cpp" line="359"/>
        <source>Please, select an atom first.</source>
        <translation>Sélectionner un atome d&apos;abord.</translation>
    </message>
    <message>
        <location filename="../gui/atomDefDlg.cpp" line="580"/>
        <source>An atom with same name exists already.</source>
        <translation>Un atome du même nom existe déjà.</translation>
    </message>
    <message>
        <location filename="../gui/atomDefDlg.cpp" line="590"/>
        <source>An atom with same symbol exists already.</source>
        <translation>Un atome avec le même symbole existe déjà.</translation>
    </message>
    <message>
        <location filename="../gui/atomDefDlg.cpp" line="643"/>
        <location filename="../gui/atomDefDlg.cpp" line="656"/>
        <source>Failed to convert %1 to a double.</source>
        <translation>Echec de la conversion de %1 en double.</translation>
    </message>
    <message>
        <location filename="../gui/atomDefDlg.cpp" line="693"/>
        <source>
The number of atoms in in the list widget 
and in the list of atoms is not identical.
</source>
        <translation>
Le nombre d&apos;atomes dans le widget liste 
et celui de la liste d&apos;atomes ne sont pas identiques.
</translation>
    </message>
    <message>
        <location filename="../gui/atomDefDlg.cpp" line="710"/>
        <source>
Atom at index %1 has not the same
name as the list widget item at the
same index.
</source>
        <translation>
L&apos;atome à l&apos;index %1 n&apos;a pas le même nom
que celui de l&apos;item du widget liste au même index.
</translation>
    </message>
    <message>
        <location filename="../gui/atomDefDlg.cpp" line="716"/>
        <source>
Atom at index %1 failed to validate.
</source>
        <translation>L&apos;atome à l&apos;index %1 n&apos;a pu être validé.
</translation>
    </message>
</context>
<context>
    <name>massXpert::CalculatorChemPadDlg</name>
    <message>
        <location filename="../gui/calculatorChemPadDlg.cpp" line="188"/>
        <source>Error with chemical pad line</source>
        <translation>Erreur dans une ligne de pavé chimique</translation>
    </message>
    <message>
        <location filename="../gui/calculatorChemPadDlg.cpp" line="217"/>
        <source>Error with chemical pad line: r color component is bad:</source>
        <translation>Erreur dans une ligne de pavé chimique: 
la composante r de la couleur est mauvaise:</translation>
    </message>
    <message>
        <location filename="../gui/calculatorChemPadDlg.cpp" line="229"/>
        <source>Error with chemical pad line: g color component is bad:</source>
        <translation>Erreur dans une ligne de pavé chimique: 
la composante g de la couleur est mauvaise:</translation>
    </message>
    <message>
        <location filename="../gui/calculatorChemPadDlg.cpp" line="241"/>
        <source>Error with chemical pad line: b color component is bad:</source>
        <translation>Erreur dans une ligne de pavé chimique: 
la composante b de la couleur est mauvaise:</translation>
    </message>
    <message>
        <location filename="../gui/calculatorChemPadDlg.cpp" line="254"/>
        <source>Error with creation of color with: </source>
        <translation>Erreur de création de couleur avec:</translation>
    </message>
    <message>
        <location filename="../gui/calculatorChemPadDlg.cpp" line="266"/>
        <source>Error with chemical pad line: color name cannot be empty.</source>
        <translation>Erreur dans une ligne de pavé chimique: 
le nom de la couleur ne peut être vide.</translation>
    </message>
    <message>
        <location filename="../gui/calculatorChemPadDlg.cpp" line="615"/>
        <source>Error parsing the chem_pad.conf file.</source>
        <translation>Échec de l&apos;analyse du fichier chem_pad.conf.</translation>
    </message>
    <message>
        <location filename="../gui/calculatorChemPadDlg.cpp" line="639"/>
        <source>No section title</source>
        <translation>Section sans titre</translation>
    </message>
</context>
<context>
    <name>massXpert::CalculatorRecorderDlg</name>
    <message>
        <location filename="../gui/calculatorRecorderDlg.cpp" line="58"/>
        <source>Calculator recorder
===================

</source>
        <translation>Enregistreur de calculatrice
===================

</translation>
    </message>
</context>
<context>
    <name>massXpert::CalculatorWnd</name>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="77"/>
        <location filename="../gui/calculatorWnd.cpp" line="89"/>
        <location filename="../gui/calculatorWnd.cpp" line="336"/>
        <location filename="../gui/calculatorWnd.cpp" line="468"/>
        <location filename="../gui/calculatorWnd.cpp" line="553"/>
        <location filename="../gui/calculatorWnd.cpp" line="565"/>
        <location filename="../gui/calculatorWnd.cpp" line="615"/>
        <location filename="../gui/calculatorWnd.cpp" line="628"/>
        <location filename="../gui/calculatorWnd.cpp" line="674"/>
        <location filename="../gui/calculatorWnd.cpp" line="687"/>
        <location filename="../gui/calculatorWnd.cpp" line="746"/>
        <location filename="../gui/calculatorWnd.cpp" line="759"/>
        <location filename="../gui/calculatorWnd.cpp" line="836"/>
        <location filename="../gui/calculatorWnd.cpp" line="849"/>
        <location filename="../gui/calculatorWnd.cpp" line="903"/>
        <location filename="../gui/calculatorWnd.cpp" line="963"/>
        <location filename="../gui/calculatorWnd.cpp" line="975"/>
        <location filename="../gui/calculatorWnd.cpp" line="1052"/>
        <location filename="../gui/calculatorWnd.cpp" line="1075"/>
        <location filename="../gui/calculatorWnd.cpp" line="1096"/>
        <location filename="../gui/calculatorWnd.cpp" line="1111"/>
        <location filename="../gui/calculatorWnd.cpp" line="1126"/>
        <location filename="../gui/calculatorWnd.cpp" line="1228"/>
        <location filename="../gui/calculatorWnd.cpp" line="1558"/>
        <location filename="../gui/calculatorWnd.cpp" line="1587"/>
        <source>massXpert - Calculator</source>
        <translation>massXpert - Calculatrice</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="78"/>
        <source>Polymer chemistry definition filepath empty.</source>
        <translation>Le chemin de fichier de la définition de chimie de polymère est vide.</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="90"/>
        <source>Failed to initialize the calculator window.</source>
        <translation>Échec de l&apos;initialisation de la fenêtre de la calculatrice.</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="337"/>
        <source>Failed to populate the polymer chemistry data comboboxes.
The calculator will not work properly.</source>
        <translation>Échec de la population des boîtes combo des données de chimie de polymère.
La calculatrice ne fonctionnera pas correctement.</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="349"/>
        <source>Add formula to memory</source>
        <translation>Mettre la formule en mémoire</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="352"/>
        <source>Remove formula from memory</source>
        <translation>Ôter la formule de la mémoire</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="355"/>
        <source>Clear whole memory</source>
        <translation>Vider la mémoire</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="358"/>
        <source>Simplify formula</source>
        <translation>Simplifier la formule</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="386"/>
        <source>%1 %2[*]</source>
        <translation>%1 %2[*]</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="387"/>
        <source>massXpert - Calculator:</source>
        <translation>massXpert - Calculatrice:</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="469"/>
        <source>Chemical pad config file not found.
The chemical pad will not work properly.</source>
        <translation>Le fichier de configuration du pavé chimique n&apos;a pas été trouvé.
Le pavé chimique ne fonctionnera pas correctement.</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="487"/>
        <source>Done: mono: %2 -- avg: %3
</source>
        <translation>Fait: mono: %2 -- moy: %3
</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="554"/>
        <location filename="../gui/calculatorWnd.cpp" line="616"/>
        <location filename="../gui/calculatorWnd.cpp" line="675"/>
        <location filename="../gui/calculatorWnd.cpp" line="747"/>
        <location filename="../gui/calculatorWnd.cpp" line="837"/>
        <source>Error with seed mono mass</source>
        <translation>Erreur avec la masse mono semée</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="566"/>
        <location filename="../gui/calculatorWnd.cpp" line="629"/>
        <location filename="../gui/calculatorWnd.cpp" line="688"/>
        <location filename="../gui/calculatorWnd.cpp" line="760"/>
        <location filename="../gui/calculatorWnd.cpp" line="850"/>
        <location filename="../gui/calculatorWnd.cpp" line="964"/>
        <location filename="../gui/calculatorWnd.cpp" line="976"/>
        <source>Error with seed avg mass</source>
        <translation>Erreur avec la masse moy semée</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="904"/>
        <location filename="../gui/calculatorWnd.cpp" line="1229"/>
        <location filename="../gui/calculatorWnd.cpp" line="1559"/>
        <source>No atom list is available</source>
        <translation>Pas de liste d&apos;atomes disponible</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="992"/>
        <source>===================================================================
Seed masses: %1 -- %2
</source>
        <translation>===================================================================
Masses semis: %1 -- %2
</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="1053"/>
        <source>Error accounting formula &apos;%1&apos;</source>
        <translation>Échec de la prise en compte de la formule &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="1076"/>
        <source>Error accounting formula</source>
        <translation>Erreur de prise en compte de formule</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="1097"/>
        <source>Error accounting monomer</source>
        <translation>Erreur de prise en compte de monomère</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="1112"/>
        <source>Error accounting modif</source>
        <translation>Erreur de prise en compte de modif</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="1127"/>
        <source>Error accounting sequence</source>
        <translation>Erreur de prise en compte de séquence</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="1588"/>
        <source>Failed to parse the formula.</source>
        <translation>Échec de l&apos;analyse de la formule.</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="574"/>
        <source>Added seed(mono: %1 ; avg: %2) 
to result(was mono:%3 ; avg: %4) 
</source>
        <translation>Masse semis(mono: %1 ; moy: %2) ajoutée
au résultat(était mono: %3 ; avg: %4)
</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="637"/>
        <source>Sent seed(mono: %1 ; avg: %2) 
to result(was mono:%3 ; avg: %4) 
</source>
        <translation>Masse semis(mono: %1 ; moy: %2) envoyéeau résultat(était mono: %3 ; avg: %4)</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="696"/>
        <source>Removed seed(mono: %1 ; avg: %2) 
from result(was mono:%3 ; avg: %4) 
</source>
        <translation>Masse semis(mono: %1 ; moy: %2) ôtéedu résultat(était mono: %3 ; avg: %4)</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="768"/>
        <source>Added result(mono: %1 ; avg: %2) 
to seed(was mono:%3 ; avg: %4) 
</source>
        <translation>Masse résultat(mono: %1 ; moy: %2) ajoutéeau semis(était mono: %3 ; avg: %4)</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="799"/>
        <source>Sent result(mono: %1 ; avg: %2) 
to seed(was mono:%3 ; avg: %4) 
</source>
        <translation>Masse résultat(mono: %1 ; moy: %2) envoyéeau semis(était mono: %3 ; avg: %4)</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="858"/>
        <source>Removed result(mono: %1 ; avg: %2) 
from seed(was mono:%3 ; avg: %4) 
</source>
        <translation>Masse résultat(mono: %1 ; moy: %2) ôtéedu semis(était mono: %3 ; avg: %4)</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="1275"/>
        <source>Accounting formula: %1(%2 times)... </source>
        <translation>Prise en compte de formule: %1(%2 fois)...</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="1323"/>
        <source>Accounting monomer: %1(%2 times)... </source>
        <translation>Prise en compte de monomère: %1(%2 fois)...</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="1370"/>
        <source>Accounting modif: %1(%2 times)... </source>
        <translation>Prise en compte de modification: %1(%2 fois)...</translation>
    </message>
    <message>
        <location filename="../gui/calculatorWnd.cpp" line="1412"/>
        <source>Accounting sequence: %1(%2 times)... </source>
        <translation>Prise en compte de séquence: %1(%2 fois)...</translation>
    </message>
</context>
<context>
    <name>massXpert::CleavageDlg</name>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="93"/>
        <source>AMU</source>
        <translation>AMU</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="93"/>
        <source>PCT</source>
        <translation>PCT</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="93"/>
        <source>PPM</source>
        <translation>PPM</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="97"/>
        <source>AMU: atom mass unit 
PCT: percent 
PPM: part per million</source>
        <translation>AMU: unité de masse atomique 
PCT: pourcentage 
PPM: partie par million</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="101"/>
        <source>Toggle Filtering</source>
        <translation>Inverser le filtrage</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="123"/>
        <source>To Clipboard</source>
        <translation>Vers le Presse-Papiers</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="124"/>
        <source>To File</source>
        <translation>Vers un Fichier</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="125"/>
        <source>Select File</source>
        <translation>Sélectionner un Fichier</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="126"/>
        <source>Calculate spectrum</source>
        <translation>Calculer le spectre</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="236"/>
        <source>massxpert - Polymer Cleavage</source>
        <translation>massxpert - Clivage de polymère</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="237"/>
        <source>Cleavage simulations with
multi-region selection are notsupported.</source>
        <translation>Les Simulations de clivage avec
sélection multi-région ne sont pas supportées.</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="329"/>
        <source>There are incomplete cross-links</source>
        <translation>Certains pontages covalents sont incomplets</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="479"/>
        <source>Oligomers (empty list)</source>
        <translation>Oligomères (liste vide)</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="481"/>
        <source>Oligomers (one item)</source>
        <translation>Oligomères (un élément)</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="483"/>
        <source>Oligomers (%1 items)</source>
        <translation>Oligomères (%1 éléments)</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="894"/>
        <source>massXpert - Export Data</source>
        <translation>massXpert - Exporter Données</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="895"/>
        <source>Failed to open file in append mode.</source>
        <translation>Échec d&apos;ouverture de fichier en mode append.</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="917"/>
        <source>Select file to export data to</source>
        <translation>Sélectionner un fichier où exporter les données</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="328"/>
        <location filename="../gui/cleavageDlg.cpp" line="421"/>
        <source>massXpert - Polymer Cleavage</source>
        <translation>massXpert - Clivage de Polymère</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="422"/>
        <source>Failed to perform cleavage.</source>
        <translation>Echec du clivage.</translation>
    </message>
    <message>
        <location filename="../gui/cleavageDlg.cpp" line="919"/>
        <source>Data files(*.dat *.DAT)</source>
        <translation>Fichiers de données(*.dat *.DAT)</translation>
    </message>
</context>
<context>
    <name>massXpert::CleaveOligomerTableView</name>
    <message>
        <location filename="../gui/cleaveOligomerTableView.cpp" line="78"/>
        <source>Copy Mono To Clipboard</source>
        <translation>Copier Mono vers le Presse-Papiers</translation>
    </message>
    <message>
        <location filename="../gui/cleaveOligomerTableView.cpp" line="79"/>
        <source>Copies the monoisotopic mass list to the clipboard</source>
        <translation>Copie la liste des masses monoisotopiques vers le presse-papiers</translation>
    </message>
    <message>
        <location filename="../gui/cleaveOligomerTableView.cpp" line="84"/>
        <source>Copy Avg To Clipboard</source>
        <translation>Copier Moy vers le Presse-Papiers</translation>
    </message>
    <message>
        <location filename="../gui/cleaveOligomerTableView.cpp" line="85"/>
        <source>Copies the average mass list to the clipboard</source>
        <translation>Copie la liste des masses moyennes vers le presse-papiers</translation>
    </message>
    <message>
        <location filename="../gui/cleaveOligomerTableView.cpp" line="90"/>
        <source>Copy Mass List</source>
        <translation>Copier Liste des Masses</translation>
    </message>
    <message>
        <location filename="../gui/cleaveOligomerTableView.cpp" line="414"/>
        <location filename="../gui/cleaveOligomerTableView.cpp" line="500"/>
        <source>massXpert - Cleavage</source>
        <translation>massXpert - Clivage</translation>
    </message>
    <message>
        <location filename="../gui/cleaveOligomerTableView.cpp" line="415"/>
        <location filename="../gui/cleaveOligomerTableView.cpp" line="501"/>
        <source>%1@%2
The monomer indices do not correspond to a valid polymer sequence range.
Avoid modifying the sequence while working with cleavages.</source>
        <translation>%1@%2
Les indices de monomère ne correspondent plus à un intervalle de séquence de polymère valable.
Éviter de modifier la séquence pendant les travaux de clivage.</translation>
    </message>
</context>
<context>
    <name>massXpert::CleaveOligomerTableViewModel</name>
    <message>
        <location filename="../gui/cleaveOligomerTableViewModel.cpp" line="143"/>
        <source>Part. cleav.</source>
        <translation>Cliv. part.</translation>
    </message>
    <message>
        <location filename="../gui/cleaveOligomerTableViewModel.cpp" line="145"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../gui/cleaveOligomerTableViewModel.cpp" line="147"/>
        <source>Coords</source>
        <translation>Coords</translation>
    </message>
    <message>
        <location filename="../gui/cleaveOligomerTableViewModel.cpp" line="149"/>
        <source>Mono</source>
        <translation>Mono</translation>
    </message>
    <message>
        <location filename="../gui/cleaveOligomerTableViewModel.cpp" line="151"/>
        <source>Avg</source>
        <translation>Moy</translation>
    </message>
    <message>
        <location filename="../gui/cleaveOligomerTableViewModel.cpp" line="153"/>
        <source>Charge</source>
        <translation>Charge</translation>
    </message>
    <message>
        <location filename="../gui/cleaveOligomerTableViewModel.cpp" line="155"/>
        <source>Modif?</source>
        <translation>Modif?</translation>
    </message>
</context>
<context>
    <name>massXpert::CleaveSpecDefDlg</name>
    <message>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="207"/>
        <source>Type Spec Name</source>
        <translation>Taper Nom de Spécif</translation>
    </message>
    <message>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="208"/>
        <source>Type Pattern</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="349"/>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="563"/>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="574"/>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="632"/>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="646"/>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="674"/>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="729"/>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="755"/>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="763"/>
        <source>massXpert - Cleavage definition</source>
        <translation>massXpert - Définition de clivage</translation>
    </message>
    <message>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="350"/>
        <source>Please, select a cleavage first.</source>
        <translation>Sélectionner un clivage d&apos;abord.</translation>
    </message>
    <message>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="365"/>
        <source>Type Rule Name</source>
        <translation>Taper Nom de Règle</translation>
    </message>
    <message>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="564"/>
        <source>A cleavage with same name exists already.</source>
        <translation>Un clivage du même nom existe déjà.</translation>
    </message>
    <message>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="575"/>
        <source>The cleavage failed to parse.</source>
        <translation>Le clivage n&apos;a pu être analysé.</translation>
    </message>
    <message>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="633"/>
        <source>A cleavage rule with same name exists already.</source>
        <translation>Une règle de clivage du même nom existe déjà.</translation>
    </message>
    <message>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="647"/>
        <source>The left code is not known.</source>
        <translation>Le code gauche n&apos;est pas connu.</translation>
    </message>
    <message>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="659"/>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="687"/>
        <source>massXpert - Monomer definition</source>
        <translation>massXpert - Définition de monomère</translation>
    </message>
    <message>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="660"/>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="688"/>
        <source>The formula failed to validate.</source>
        <translation>La formule n&apos;a pu être validée.</translation>
    </message>
    <message>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="675"/>
        <source>The right code is not known.</source>
        <translation>Le code droit n&apos;est pas connu.</translation>
    </message>
    <message>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="725"/>
        <source>
The number of cleavages in the list widget 
and in the list of cleavages is not identical.
</source>
        <translation>
Le nombre de clivages dans le widget liste 
and celui dans la liste des clivages n&apos;est pas identique.
</translation>
    </message>
    <message>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="742"/>
        <source>
Cleavage at index %1 has not the same
name as the list widget item at the
same index.
</source>
        <translation>Le clivage à l&apos;index %1 n&apos;a pas le même
nom que l&apos;item dans le widget liste au même index.
</translation>
    </message>
    <message>
        <location filename="../gui/cleaveSpecDefDlg.cpp" line="748"/>
        <source>
Cleavage at index %1 failed to validate.
</source>
        <translation>Le clivage à l&apos;index %1 n&apos;a pu être validé.
</translation>
    </message>
</context>
<context>
    <name>massXpert::CompositionTreeViewModel</name>
    <message>
        <location filename="../gui/compositionTreeViewModel.cpp" line="58"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../gui/compositionTreeViewModel.cpp" line="58"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="../gui/compositionTreeViewModel.cpp" line="58"/>
        <source>Count</source>
        <translation>Décompte</translation>
    </message>
    <message>
        <location filename="../gui/compositionTreeViewModel.cpp" line="58"/>
        <source>Modif</source>
        <translation>Modif</translation>
    </message>
</context>
<context>
    <name>massXpert::CompositionsDlg</name>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="75"/>
        <source>To Clipboard</source>
        <translation>Vers le Presse-Papiers</translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="76"/>
        <source>To File</source>
        <translation>Vers un Fichier</translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="77"/>
        <source>Select File</source>
        <translation>Sélectionner un Fichier</translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="227"/>
        <location filename="../gui/compositionsDlg.cpp" line="268"/>
        <source>Incomplete cross-links: %1</source>
        <translation>Pontages incomplets: %1</translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="275"/>
        <source>Not accounting for cross-links</source>
        <translation>Pontages non pris en compte</translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="209"/>
        <location filename="../gui/compositionsDlg.cpp" line="288"/>
        <location filename="../gui/compositionsDlg.cpp" line="423"/>
        <source>massXpert - Compositions</source>
        <translation>massXpert - Compositions</translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="289"/>
        <location filename="../gui/compositionsDlg.cpp" line="424"/>
        <source>Failed validating input data.</source>
        <translation>Échec de la validation des données entrées.</translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="660"/>
        <source>massXpert - Export Data</source>
        <translation>massXpert - Exporter Données</translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="661"/>
        <source>Failed to open file in append mode.</source>
        <translation>Échec d&apos;ouverture de fichier en mode append.</translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="681"/>
        <source>Select file to export data to</source>
        <translation>Sélectionner un fichier où exporter les données</translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="210"/>
        <source>Selection data are no more valid.
Please update these data.</source>
        <translation>Les données de sélection ne sont plus valables.
Actualiser les données.</translation>
    </message>
    <message>
        <location filename="../gui/compositionsDlg.cpp" line="683"/>
        <source>Data files(*.dat *.DAT)</source>
        <translation>Fichiers de données(*.dat *.DAT)</translation>
    </message>
</context>
<context>
    <name>massXpert::ConfigurationSettingsDlg</name>
    <message>
        <location filename="../gui/configurationSettingsDlg.cpp" line="106"/>
        <source>The massXpert software program&apos;s components have been built and should be located in the following system places:

- the binary program in %1
- the data in %2
- the plugins in %3
- the localization files in %4
- the user manual in %5


</source>
        <translation>Les composants du logiciel massXpert ont été construits et devraient se trouver dans les emplacements système suivants:

- le programme binaire en %1
- les données en %2
- les plugins en %3
- les fichiers de localisation (traduction) en %4
- le manuel de l&apos;utilisateur en %5


 </translation>
    </message>
    <message>
        <location filename="../gui/configurationSettingsDlg.cpp" line="120"/>
        <source>However, it appears that the configuration on this system is not typical. The software package might have been relocated.

You are given the opportunity to locate the massXpert software main directories.</source>
        <translation>Cependant, il semble que la configuration sur ce système n&apos;est pas typique. Le paquet logiciel a peut-être été déplacé.

Vous pouvez localiser les principaux répertoires du logiciel massXpert.</translation>
    </message>
    <message>
        <location filename="../gui/configurationSettingsDlg.cpp" line="129"/>
        <location filename="../gui/configurationSettingsDlg.cpp" line="134"/>
        <location filename="../gui/configurationSettingsDlg.cpp" line="139"/>
        <source>Should be %1</source>
        <translation>Devrait être %1</translation>
    </message>
    <message>
        <location filename="../gui/configurationSettingsDlg.cpp" line="185"/>
        <source>Locate the system data directory</source>
        <translation>Localiser le répertoire système des données</translation>
    </message>
    <message>
        <location filename="../gui/configurationSettingsDlg.cpp" line="192"/>
        <location filename="../gui/configurationSettingsDlg.cpp" line="220"/>
        <location filename="../gui/configurationSettingsDlg.cpp" line="248"/>
        <location filename="../gui/configurationSettingsDlg.cpp" line="335"/>
        <location filename="../gui/configurationSettingsDlg.cpp" line="352"/>
        <location filename="../gui/configurationSettingsDlg.cpp" line="369"/>
        <source>massXpert - Configuration Settings</source>
        <translation>massXpert - Paramètres de Configuration</translation>
    </message>
    <message>
        <location filename="../gui/configurationSettingsDlg.cpp" line="193"/>
        <location filename="../gui/configurationSettingsDlg.cpp" line="336"/>
        <source>%1@%2
Failed to verify the consistency of the data directory.
Please ensure that the package is installed correctly
</source>
        <translation>%1@%2
Echec de la vérification de la cohérence du répertoire des données.
Vérifier que le paquet logiciel est installé correctement</translation>
    </message>
    <message>
        <location filename="../gui/configurationSettingsDlg.cpp" line="213"/>
        <source>Locate the system plugin directory</source>
        <translation>Localiser le répertoire système des plugins</translation>
    </message>
    <message>
        <location filename="../gui/configurationSettingsDlg.cpp" line="221"/>
        <location filename="../gui/configurationSettingsDlg.cpp" line="353"/>
        <source>%1@%2
Failed to verify the consistency of the plugin directory.
Please ensure that the package is installed correctly
</source>
        <translation>%1@%2
Echec de la vérification de la cohérence du répertoire des plugins.
Vérifier que le paquet est installé correctement</translation>
    </message>
    <message>
        <location filename="../gui/configurationSettingsDlg.cpp" line="241"/>
        <source>Locate the system localization directory</source>
        <translation>Localiser le répertoire système des localisations</translation>
    </message>
    <message>
        <location filename="../gui/configurationSettingsDlg.cpp" line="249"/>
        <location filename="../gui/configurationSettingsDlg.cpp" line="370"/>
        <source>%1@%2
Failed to verify the consistency of the localization directory.
Please ensure that the package is installed correctly
</source>
        <translation>%1@%2Echec de la vérification de la cohérence du répertoire des localisations.Vérifier que le paquet est installé correctement</translation>
    </message>
</context>
<context>
    <name>massXpert::CrossLinkerDefDlg</name>
    <message>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="230"/>
        <source>Type Name</source>
        <translation>Taper Nom</translation>
    </message>
    <message>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="231"/>
        <source>Type Formula</source>
        <translation>Taper Formule</translation>
    </message>
    <message>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="373"/>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="594"/>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="608"/>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="622"/>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="630"/>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="685"/>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="723"/>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="752"/>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="760"/>
        <source>massXpert - CrossLinker definition</source>
        <translation>massXpert - Définition des agents pontants</translation>
    </message>
    <message>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="374"/>
        <source>Please, select a crossLinker first.</source>
        <translation>Sélectionner un agent de pontage d&apos;abord.</translation>
    </message>
    <message>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="595"/>
        <source>A crossLinker with same name exists already.</source>
        <translation>Un agent pontant du même nom existe déjà.</translation>
    </message>
    <message>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="609"/>
        <source>The name of the cross-linker is already used by a modification.</source>
        <translation>Le nom du pontage est déjà utilisé par une modification.</translation>
    </message>
    <message>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="631"/>
        <source>The formula failed to validate.</source>
        <translation>La formule n&apos;a pu être validée.</translation>
    </message>
    <message>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="686"/>
        <source>The modification is not known to the polymer chemistry definition.</source>
        <translation>La modification n&apos;est pas connue dans la définition de chimie du polymère.</translation>
    </message>
    <message>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="717"/>
        <source>
The number of crossLinkers in the list widget 
and in the list of crossLinkers is not identical.
</source>
        <translation>
Le nombre d&apos;agents pontants dans le widget list 
et celui de la liste des agents pontants ne sont pas identiques.
</translation>
    </message>
    <message>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="736"/>
        <source>
CrossLinker at index %1 has not the same
name as the list widget item at the
same index.
</source>
        <translation>L&apos;agent pontant à l&apos;index %1 n&apos;a pas le même
nom que celui au même index du widget liste.

</translation>
    </message>
    <message>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="389"/>
        <source>Select modification</source>
        <translation>Sélectionner modification</translation>
    </message>
    <message>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="623"/>
        <source>The formula is empty, please enter a formula like &apos;-H+H&apos; if no reaction is required.</source>
        <translation>La formule est vide, entrer une fomule comme &apos;-H+H&apos; si aucune réaction n&apos;est requise.</translation>
    </message>
    <message>
        <location filename="../gui/crossLinkerDefDlg.cpp" line="742"/>
        <source>
CrossLinker at index %1 failed to validate.
Please, make sure that there are either no or at least two modifications for the cross-link.
</source>
        <translation>
L&apos;agent pontant à  l&apos;index %1 n&apos;a pu être validé.
Assurez-vous qu&apos;il y a soit aucune soit au moins deux modifications pour l&apos;agent  pontant.
</translation>
    </message>
</context>
<context>
    <name>massXpert::FragSpecDefDlg</name>
    <message>
        <location filename="../gui/fragSpecDefDlg.cpp" line="214"/>
        <source>Type Spec Name</source>
        <translation>Taper Nom de Spécif</translation>
    </message>
    <message>
        <location filename="../gui/fragSpecDefDlg.cpp" line="215"/>
        <source>Type Spec Formula</source>
        <translation>Taper Formule de Spécif</translation>
    </message>
    <message>
        <location filename="../gui/fragSpecDefDlg.cpp" line="357"/>
        <location filename="../gui/fragSpecDefDlg.cpp" line="569"/>
        <location filename="../gui/fragSpecDefDlg.cpp" line="590"/>
        <location filename="../gui/fragSpecDefDlg.cpp" line="604"/>
        <location filename="../gui/fragSpecDefDlg.cpp" line="724"/>
        <location filename="../gui/fragSpecDefDlg.cpp" line="738"/>
        <location filename="../gui/fragSpecDefDlg.cpp" line="752"/>
        <location filename="../gui/fragSpecDefDlg.cpp" line="766"/>
        <location filename="../gui/fragSpecDefDlg.cpp" line="778"/>
        <location filename="../gui/fragSpecDefDlg.cpp" line="823"/>
        <location filename="../gui/fragSpecDefDlg.cpp" line="851"/>
        <location filename="../gui/fragSpecDefDlg.cpp" line="859"/>
        <source>massXpert - Fragmentation definition</source>
        <translation>massXpert - Définition de Fragmentation</translation>
    </message>
    <message>
        <location filename="../gui/fragSpecDefDlg.cpp" line="358"/>
        <source>Please, select a fragmentation first.</source>
        <translation>Sélectionnner une fragmentation d&apos;abord.</translation>
    </message>
    <message>
        <location filename="../gui/fragSpecDefDlg.cpp" line="373"/>
        <source>Type Rule Name</source>
        <translation>Taper Nom de Règle</translation>
    </message>
    <message>
        <location filename="../gui/fragSpecDefDlg.cpp" line="570"/>
        <source>A fragmentation with same name exists already.</source>
        <translation>Une fragmentation du même nom existe déjà.</translation>
    </message>
    <message>
        <location filename="../gui/fragSpecDefDlg.cpp" line="591"/>
        <source>The fragmentation end is not correct. Choose either &apos;NE&apos; or &apos;LE&apos; or &apos;RE&apos;.</source>
        <translation>L&apos;extrémité de fragmentation n&apos;est pas correcte. Choisir soit &apos;NE&apos;, soit &apos;LE&apos;, soit &apos;RE&apos;.</translation>
    </message>
    <message>
        <location filename="../gui/fragSpecDefDlg.cpp" line="605"/>
        <location filename="../gui/fragSpecDefDlg.cpp" line="779"/>
        <source>The formula failed to validate.</source>
        <translation>La formule n&apos;a pu être validée.</translation>
    </message>
    <message>
        <location filename="../gui/fragSpecDefDlg.cpp" line="725"/>
        <source>A fragmentation rule with same name exists already.</source>
        <translation>Une règle de fragmentation du même nom existe déjà.</translation>
    </message>
    <message>
        <location filename="../gui/fragSpecDefDlg.cpp" line="739"/>
        <source>The previous code is not known.</source>
        <translation>Le code précédent n&apos;est pas connu.</translation>
    </message>
    <message>
        <location filename="../gui/fragSpecDefDlg.cpp" line="753"/>
        <source>The current code is not known.</source>
        <translation>Le code courant n&apos;est pas connu.</translation>
    </message>
    <message>
        <location filename="../gui/fragSpecDefDlg.cpp" line="767"/>
        <source>The next code is not known.</source>
        <translation>Le code suivant n&apos;est pas connu.</translation>
    </message>
    <message>
        <location filename="../gui/fragSpecDefDlg.cpp" line="817"/>
        <source>
The number of fragmentations in the list widget 
and in the list of fragmentations is not identical.
</source>
        <translation>
Le nombre des fragmentations dans le widget liste 
et celui dans la liste des fragmentations ne sont pas identiques. 
</translation>
    </message>
    <message>
        <location filename="../gui/fragSpecDefDlg.cpp" line="836"/>
        <source>
Fragmentation at index %1 has not the same
name as the list widget item at the
same index.
</source>
        <translation>La fragmentation à l&apos;index %1 n&apos;a pas le même
nom que l&apos;item du widget liste au même index.

</translation>
    </message>
    <message>
        <location filename="../gui/fragSpecDefDlg.cpp" line="843"/>
        <source>
Fragmentation at index %1 failed to validate.
</source>
        <translation>La fragmentation à l&apos;index %1 n&apos;a pu être validée.
</translation>
    </message>
</context>
<context>
    <name>massXpert::FragmentOligomerTableView</name>
    <message>
        <location filename="../gui/fragmentOligomerTableView.cpp" line="77"/>
        <source>Copy Mono To Clipboard</source>
        <translation>Copier Mono vers le Presse-Papiers</translation>
    </message>
    <message>
        <location filename="../gui/fragmentOligomerTableView.cpp" line="78"/>
        <source>Copies the monoisotopic mass list to the clipboard</source>
        <translation>Copie la liste des masses monoisotopiques vers le presse-papiers</translation>
    </message>
    <message>
        <location filename="../gui/fragmentOligomerTableView.cpp" line="83"/>
        <source>Copy Avg To Clipboard</source>
        <translation>Copier Moy vers le Presse-Papiers</translation>
    </message>
    <message>
        <location filename="../gui/fragmentOligomerTableView.cpp" line="84"/>
        <source>Copies the average mass list to the clipboard</source>
        <translation>Copie la liste des masses moyennes vers le presse-papiers</translation>
    </message>
    <message>
        <location filename="../gui/fragmentOligomerTableView.cpp" line="89"/>
        <source>Copy Mass List</source>
        <translation>Copier Liste des Masses</translation>
    </message>
    <message>
        <location filename="../gui/fragmentOligomerTableView.cpp" line="410"/>
        <location filename="../gui/fragmentOligomerTableView.cpp" line="484"/>
        <source>massXpert - Fragmentation</source>
        <translation>massXpert - Fragmentation</translation>
    </message>
    <message>
        <location filename="../gui/fragmentOligomerTableView.cpp" line="411"/>
        <location filename="../gui/fragmentOligomerTableView.cpp" line="485"/>
        <source>%1@%2
The monomer indices do not correspond to a valid polymer sequence range.
Avoid modifying the sequence while working with fragmentations.</source>
        <translation>%1@%2
Les indices de monomère ne correspondent plus à un intervalle de séquence de polymère valable.
Éviter de modifier la séquence pendant les travaux de fragmentation.</translation>
    </message>
</context>
<context>
    <name>massXpert::FragmentOligomerTableViewModel</name>
    <message>
        <location filename="../gui/fragmentOligomerTableViewModel.cpp" line="135"/>
        <source>Frag. Pattern</source>
        <translation>Patron de fragm.</translation>
    </message>
    <message>
        <location filename="../gui/fragmentOligomerTableViewModel.cpp" line="137"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../gui/fragmentOligomerTableViewModel.cpp" line="139"/>
        <source>Coords</source>
        <translation>Coords</translation>
    </message>
    <message>
        <location filename="../gui/fragmentOligomerTableViewModel.cpp" line="141"/>
        <source>Mono</source>
        <translation>Mono</translation>
    </message>
    <message>
        <location filename="../gui/fragmentOligomerTableViewModel.cpp" line="143"/>
        <source>Avg</source>
        <translation>Moy</translation>
    </message>
    <message>
        <location filename="../gui/fragmentOligomerTableViewModel.cpp" line="145"/>
        <source>Charge</source>
        <translation>Charge</translation>
    </message>
    <message>
        <location filename="../gui/fragmentOligomerTableViewModel.cpp" line="147"/>
        <source>Modif?</source>
        <translation>Modif?</translation>
    </message>
</context>
<context>
    <name>massXpert::FragmentationDlg</name>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="92"/>
        <source>AMU</source>
        <translation>AMU</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="92"/>
        <source>PCT</source>
        <translation>PCT</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="92"/>
        <source>PPM</source>
        <translation>PPM</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="96"/>
        <source>AMU: atom mass unit 
PCT: percent 
PPM: part per million</source>
        <translation>AMU: unité de masse atomique 
PCT: pourcentage 
PPM: partie par million</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="100"/>
        <source>Toggle Filtering</source>
        <translation>Inverser le Filtrage</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="123"/>
        <source>To Clipboard</source>
        <translation>Vers le Presse-Papiers</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="124"/>
        <source>To File</source>
        <translation>Vers un Fichier</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="125"/>
        <source>Select File</source>
        <translation>Sélectionner un Fichier</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="214"/>
        <location filename="../gui/fragmentationDlg.cpp" line="223"/>
        <source>massxpert - Fragmentation</source>
        <translation>massxpert - Fragmentation</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="224"/>
        <source>Fragmentation simulations with
multi-region selection are not supported.</source>
        <translation>Les simulations de fragmentation avec
des sélections multi-région ne sont pas supportées.</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="375"/>
        <source>massXpert - Fragmentation</source>
        <translation>massXpert - Fragmentation</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="376"/>
        <source>Failed to validate the decomposition formula.</source>
        <translation>Échec de la validation de la formule de décomposition.</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="452"/>
        <source>massXpert - Polymer Cleavage</source>
        <translation>massXpert - Clivage de Polymère</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="453"/>
        <source>Failed to perform fragmentation.</source>
        <translation>Échec de la fragmentation.</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="507"/>
        <source>Oligomers (empty list)</source>
        <translation>Oligomères (liste vide)</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="509"/>
        <source>Oligomers (one item)</source>
        <translation>Oligomères (un élément)</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="511"/>
        <source>Oligomers (%1 items)</source>
        <translation>Oligomères (%1 éléments)</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="898"/>
        <source>massXpert - Export Data</source>
        <translation>massXpert - Exporter Données</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="899"/>
        <source>Failed to open file in append mode.</source>
        <translation>Échec d&apos;ouverture de fichier en mode append.</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="921"/>
        <source>Select file to export data to</source>
        <translation>Sélectionner un fichier où exporter les données</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="215"/>
        <source>No oligomer is selected. Select an oligomer first</source>
        <translation>Aucun oligomère sélectionné. Sélectionner un oligomère d&apos;abord</translation>
    </message>
    <message>
        <location filename="../gui/fragmentationDlg.cpp" line="923"/>
        <source>Data files(*.dat *.DAT)</source>
        <translation>Fichiers de données(*.dat *.DAT)</translation>
    </message>
</context>
<context>
    <name>massXpert::IsotopicPatternCalculator</name>
    <message>
        <location filename="../lib/isotopicPatternCalculator.cpp" line="155"/>
        <source>Aborting...</source>
        <translation>Abandon...</translation>
    </message>
    <message>
        <location filename="../lib/isotopicPatternCalculator.cpp" line="274"/>
        <source>Starting calculations.</source>
        <translation>Démarrage des calculs.</translation>
    </message>
    <message>
        <location filename="../lib/isotopicPatternCalculator.cpp" line="291"/>
        <source>Calculating sum of probabilities</source>
        <translation>Calcul de la somme des probabilités</translation>
    </message>
    <message>
        <location filename="../lib/isotopicPatternCalculator.cpp" line="295"/>
        <source>Calculating relative intensities</source>
        <translation>Calcul des intensités relatives</translation>
    </message>
    <message>
        <location filename="../lib/isotopicPatternCalculator.cpp" line="299"/>
        <source>Removing too low probability peaks</source>
        <translation>Élimination des pics de trop faible probabilité</translation>
    </message>
    <message>
        <location filename="../lib/isotopicPatternCalculator.cpp" line="304"/>
        <source>Aborted</source>
        <translation>Abandonné</translation>
    </message>
    <message>
        <location filename="../lib/isotopicPatternCalculator.cpp" line="307"/>
        <source>Done computing the isotopic peaks.</source>
        <translation>Calcul des pics isotopiques achevé.</translation>
    </message>
    <message>
        <location filename="../lib/isotopicPatternCalculator.cpp" line="557"/>
        <source>Accounting atom: </source>
        <translation>Prise en compte de l&apos;atome: </translation>
    </message>
</context>
<context>
    <name>massXpert::MainWindow</name>
    <message>
        <location filename="../gui/mainWindow.cpp" line="117"/>
        <location filename="../gui/mainWindow.cpp" line="171"/>
        <location filename="../gui/mainWindow.cpp" line="377"/>
        <location filename="../gui/mainWindow.cpp" line="467"/>
        <source>Select a polymer chemistry definition or click Cancel to browse</source>
        <translation>Sélectionner une définition de chimie de polymère ou cliquer Annuler pour naviguer</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="119"/>
        <location filename="../gui/mainWindow.cpp" line="173"/>
        <location filename="../gui/mainWindow.cpp" line="379"/>
        <location filename="../gui/mainWindow.cpp" line="469"/>
        <source>Polymer chemistry definition:</source>
        <translation>Définition de chimie de polymère:</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="125"/>
        <location filename="../gui/mainWindow.cpp" line="181"/>
        <location filename="../gui/mainWindow.cpp" line="387"/>
        <location filename="../gui/mainWindow.cpp" line="477"/>
        <source>Open definition file</source>
        <oldsource>Open Definition File</oldsource>
        <translation>Ouvrir fichier de définition</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="192"/>
        <location filename="../gui/mainWindow.cpp" line="231"/>
        <location filename="../gui/mainWindow.cpp" line="245"/>
        <location filename="../gui/mainWindow.cpp" line="326"/>
        <location filename="../gui/mainWindow.cpp" line="340"/>
        <location filename="../gui/mainWindow.cpp" line="399"/>
        <location filename="../gui/mainWindow.cpp" line="417"/>
        <location filename="../gui/mainWindow.cpp" line="488"/>
        <source>massXpert</source>
        <translation>massXpert</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="219"/>
        <location filename="../gui/mainWindow.cpp" line="313"/>
        <source>Open sequence file</source>
        <oldsource>Open Sequence File</oldsource>
        <translation>Ouvrir fichier de séquence</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="221"/>
        <source>Sequence files(*.mxp *.mXp *.MXP);; All files(*.*)</source>
        <translation>Fichiers de séquence (*.mxp *.mXp *.MXP);; Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="246"/>
        <location filename="../gui/mainWindow.cpp" line="341"/>
        <source>%1@%2
Failed to open sequence in the editor window.</source>
        <translation>%1@%2
Échec de l&apos;ouverture d&apos;une séquence dans la fenêtre d&apos;éditeur de séquence.</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="315"/>
        <source>mxp files(*.mxp *.MXP);; All files(*.*)</source>
        <translation>Fichiers mxp (*.mxp *.MXP);; Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="389"/>
        <source>XML files(*.xml *.XML);; All files(*.*)</source>
        <translation>Fichiers XML(*.xml *.XML);; Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="418"/>
        <source>%1@%2
Failed to create sequence in the editor window.</source>
        <translation>%1@%2
Échec de la création de la séquence dans la fenêtre d&apos;éditeur de séquence.</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="518"/>
        <source>E&amp;xit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="519"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="520"/>
        <source>Exit the application</source>
        <translation>Quitter l&apos;application</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="525"/>
        <source>&amp;Open...</source>
        <translation>&amp;Ouvrir...</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="526"/>
        <location filename="../gui/mainWindow.cpp" line="547"/>
        <location filename="../gui/mainWindow.cpp" line="567"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="528"/>
        <source>Open an existing polymer chemistry definition file</source>
        <translation>Ouvrir un fichier de définition de chimie de polymère existant</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="535"/>
        <source>&amp;New...</source>
        <translation>&amp;Nouveau...</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="536"/>
        <location filename="../gui/mainWindow.cpp" line="577"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="538"/>
        <source>Create a new polymer chemistry definition file</source>
        <translation>Créer un fichier de définition de chimie de polymère</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="546"/>
        <source>&amp;Open calculator</source>
        <oldsource>&amp;Open Calculator</oldsource>
        <translation>&amp;Ouvrir calculatrice</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="549"/>
        <source>Open a new calculator window</source>
        <translation>Ouvrir une nouvelle fenêtre de calculatrice </translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="566"/>
        <source>&amp;Open sequence</source>
        <oldsource>&amp;Open Sequence</oldsource>
        <translation>&amp;Ouvrir séquence</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="569"/>
        <source>Open an existing polymer sequence file</source>
        <translation>Ouvrir un fichier de séquence de polymère existant</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="576"/>
        <source>&amp;New sequence</source>
        <oldsource>&amp;New Sequence</oldsource>
        <translation>&amp;Nouvelle séquence</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="579"/>
        <source>Create a new polymer sequence file</source>
        <translation>Créer un nouveau fichier de séquence de polymère</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="594"/>
        <source>&amp;About</source>
        <translation>&amp;À propos</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="595"/>
        <source>Ctrl+H</source>
        <translation>Ctrl+H</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="596"/>
        <source>Show the application&apos;s About box</source>
        <translation>Montrer l&apos;À propos de l&apos;application</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="599"/>
        <source>About &amp;Qt</source>
        <translation>À propos de &amp;Qt</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="600"/>
        <source>Show the Qt library&apos;s About box</source>
        <translation>Montrer l&apos;À propos de la librairie Qt</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="607"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="613"/>
        <source>Xpert&amp;Def</source>
        <translation>Xpert&amp;Def</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="620"/>
        <source>Xpert&amp;Calc</source>
        <translation>Xpert&amp;Calc</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="626"/>
        <source>Xpert&amp;Edit</source>
        <translation>Xpert&amp;Edit</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="640"/>
        <source>&amp;Plugins</source>
        <translation>&amp;Plugins</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="645"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="654"/>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="303"/>
        <source>Select a sample sequence file or click Cancel to browse</source>
        <translation>Sélectionner un fichier de séquence échantillon ou cliquer Annuler pour naviguer</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="305"/>
        <source>Sample sequence file:</source>
        <translation>Fichier de séquence échantillon:</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="327"/>
        <source>File %1 not found.</source>
        <translation>Fichier %1 non trouvé.</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="557"/>
        <source>&amp;Open sample sequence</source>
        <oldsource>&amp;Open Sample Sequence</oldsource>
        <translation>&amp;Ouvrir séquence échantillon</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="558"/>
        <source>Ctrl+O,S</source>
        <translation>Ctrl+O,S</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="560"/>
        <source>Open an sample polymer sequence file</source>
        <translation>Ouvrir un fichier de séquence de polymère échantillon</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="586"/>
        <source>&amp;mz Lab</source>
        <translation>&amp;mz Lab</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="587"/>
        <source>Ctrl+M,Z</source>
        <translation>Ctrl+M,Z</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="589"/>
        <source>Open a new mz lab window</source>
        <translation>Ouvrir une nouvelle fenêtre mz lab</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="634"/>
        <source>Xpert&amp;Miner</source>
        <translation>Xpert&amp;Miner</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="127"/>
        <location filename="../gui/mainWindow.cpp" line="183"/>
        <location filename="../gui/mainWindow.cpp" line="479"/>
        <source>XML files(*.xml *.XML)</source>
        <translation>Fichiers XML(*.xml *.XML)</translation>
    </message>
    <message>
        <location filename="../gui/mainWindow.cpp" line="193"/>
        <location filename="../gui/mainWindow.cpp" line="232"/>
        <location filename="../gui/mainWindow.cpp" line="400"/>
        <location filename="../gui/mainWindow.cpp" line="489"/>
        <source>File(%1) not found.</source>
        <translation>Fichier(%1) non trouvé.</translation>
    </message>
</context>
<context>
    <name>massXpert::MassSearchDlg</name>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="98"/>
        <location filename="../gui/massSearchDlg.cpp" line="101"/>
        <location filename="../gui/massSearchDlg.cpp" line="116"/>
        <location filename="../gui/massSearchDlg.cpp" line="122"/>
        <source>AMU: atom mass unit 
PCT: percent 
PPM: part per million</source>
        <translation>AMU: unité de masse atomique 
PCT: pourcentage 
PPM: partie par million</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="112"/>
        <location filename="../gui/massSearchDlg.cpp" line="327"/>
        <source>AMU</source>
        <translation>AMU</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="112"/>
        <location filename="../gui/massSearchDlg.cpp" line="327"/>
        <source>PCT</source>
        <translation>PCT</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="112"/>
        <location filename="../gui/massSearchDlg.cpp" line="327"/>
        <source>PPM</source>
        <translation>PPM</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="129"/>
        <source>Toggle Mono Filtering</source>
        <translation>Inverser le Filtrage Mono</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="139"/>
        <source>Toggle Avg Filtering</source>
        <translation>Inverser le Filtrage Moy</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="166"/>
        <source>To Clipboard</source>
        <translation>Vers le Presse-Papiers</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="167"/>
        <source>To File</source>
        <translation>Vers un Fichier</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="168"/>
        <source>Select File</source>
        <translation>Sélectionner un Fichier</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="808"/>
        <location filename="../gui/massSearchDlg.cpp" line="822"/>
        <location filename="../gui/massSearchDlg.cpp" line="872"/>
        <location filename="../gui/massSearchDlg.cpp" line="890"/>
        <source>massXpert - Polymer Mass Search</source>
        <translation>massXpert - Recherche de Masses dans Polymère</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="809"/>
        <source>%1@%2
Failed to parse the masses to search for MONO.</source>
        <translation>%1@%2
Échec de la vérification des masses à rechercher pour MONO.</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="823"/>
        <source>%1@%2
Failed to parse the masses to search for AVG.</source>
        <translation>%1@%2Échec de la vérification des masses à rechercher pour MOY.</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="836"/>
        <location filename="../gui/massSearchDlg.cpp" line="847"/>
        <source>massxpert - Mass Search</source>
        <translation>massXpert - Recherche de Masses</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="837"/>
        <source>Enter a valid MONO tolerance value</source>
        <translation>Entrer une valeur de tolérance MONO correcte</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="848"/>
        <source>Enter a valid AVG tolerance value</source>
        <translation>Entrer une valeur de tolérance MOY correcte</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="873"/>
        <source>%1@%2
Failed to search for the MONO masses.</source>
        <translation>%1@%2
Échec de la recherche des masses MONO.</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="891"/>
        <source>%1@%2
Failed to search for the AVG masses.</source>
        <translation>%1@%2Échec de la recherche des masses MOY.</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="988"/>
        <source>[ %1 - %2 ]</source>
        <translation>[ %1 - %2 ]</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="1005"/>
        <source>MONO</source>
        <translation>MONO</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="1006"/>
        <source>AVG</source>
        <translation>MOY</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="1660"/>
        <source>massXpert - Export Data</source>
        <translation>massXpert - Exporter Données</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="1661"/>
        <source>Failed to open file in append mode.</source>
        <translation>Échec d&apos;ouverture de fichier en mode append.</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="1683"/>
        <source>Select File To Export Data To</source>
        <translation>Sélectionner un Fichier où Exporter les Données</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="421"/>
        <source>%1 at pos. %2</source>
        <translation>%1 en pos. %2</translation>
    </message>
    <message>
        <location filename="../gui/massSearchDlg.cpp" line="1685"/>
        <source>Data files(*.dat *.DAT)</source>
        <translation>Fichiers de données(*.dat *.DAT)</translation>
    </message>
</context>
<context>
    <name>massXpert::MassSearchOligomerTableView</name>
    <message>
        <location filename="../gui/massSearchOligomerTableView.cpp" line="81"/>
        <source>Copy Mono To Clipboard</source>
        <translation>Copier Mono vers le Presse-Papiers</translation>
    </message>
    <message>
        <location filename="../gui/massSearchOligomerTableView.cpp" line="82"/>
        <source>Copies the monoisotopic mass list to the clipboard</source>
        <translation>Copie la liste des masses monoisotopiques vers le presse-papiers</translation>
    </message>
    <message>
        <location filename="../gui/massSearchOligomerTableView.cpp" line="87"/>
        <source>Copy Avg To Clipboard</source>
        <translation>Copier Moy vers le Presse-Papiers</translation>
    </message>
    <message>
        <location filename="../gui/massSearchOligomerTableView.cpp" line="88"/>
        <source>Copies the average mass list to the clipboard</source>
        <translation>Copie la liste des masses moyennes vers le presse-papiers</translation>
    </message>
    <message>
        <location filename="../gui/massSearchOligomerTableView.cpp" line="93"/>
        <source>Copy Mass List</source>
        <translation>Copier Liste des Masses</translation>
    </message>
    <message>
        <location filename="../gui/massSearchOligomerTableView.cpp" line="438"/>
        <location filename="../gui/massSearchOligomerTableView.cpp" line="498"/>
        <source>massXpert - Mass Search</source>
        <translation>massXpert - Recherche de masse</translation>
    </message>
    <message>
        <location filename="../gui/massSearchOligomerTableView.cpp" line="439"/>
        <location filename="../gui/massSearchOligomerTableView.cpp" line="499"/>
        <source>%1@%2
The monomer indices do not correspond to a valid polymer sequence range.
Avoid modifying the sequence while working with mass searches.</source>
        <translation>%1@%2
Les indices de monomère ne correspondent plus à un intervalle de séquence de polymère valable.
Éviter de modifier la séquence pendant les travaux de clivage.</translation>
    </message>
</context>
<context>
    <name>massXpert::MassSearchOligomerTableViewModel</name>
    <message>
        <location filename="../gui/massSearchOligomerTableViewModel.cpp" line="141"/>
        <source>Searched</source>
        <translation>Recherché</translation>
    </message>
    <message>
        <location filename="../gui/massSearchOligomerTableViewModel.cpp" line="143"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../gui/massSearchOligomerTableViewModel.cpp" line="145"/>
        <source>Coords</source>
        <translation>Coords</translation>
    </message>
    <message>
        <location filename="../gui/massSearchOligomerTableViewModel.cpp" line="147"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../gui/massSearchOligomerTableViewModel.cpp" line="149"/>
        <source>Mono</source>
        <translation>Mono</translation>
    </message>
    <message>
        <location filename="../gui/massSearchOligomerTableViewModel.cpp" line="151"/>
        <source>Avg</source>
        <translation>Moy</translation>
    </message>
    <message>
        <location filename="../gui/massSearchOligomerTableViewModel.cpp" line="153"/>
        <source>Modif?</source>
        <translation>Modif?</translation>
    </message>
</context>
<context>
    <name>massXpert::ModifDefDlg</name>
    <message>
        <location filename="../gui/modifDefDlg.cpp" line="178"/>
        <source>Type Name</source>
        <translation>Taper Nom</translation>
    </message>
    <message>
        <location filename="../gui/modifDefDlg.cpp" line="179"/>
        <source>Type Formula</source>
        <translation>Taper Formule</translation>
    </message>
    <message>
        <location filename="../gui/modifDefDlg.cpp" line="234"/>
        <location filename="../gui/modifDefDlg.cpp" line="374"/>
        <location filename="../gui/modifDefDlg.cpp" line="387"/>
        <location filename="../gui/modifDefDlg.cpp" line="401"/>
        <location filename="../gui/modifDefDlg.cpp" line="412"/>
        <location filename="../gui/modifDefDlg.cpp" line="429"/>
        <location filename="../gui/modifDefDlg.cpp" line="471"/>
        <location filename="../gui/modifDefDlg.cpp" line="497"/>
        <location filename="../gui/modifDefDlg.cpp" line="505"/>
        <source>massXpert - Modif definition</source>
        <translation>massXpert - Définition de modif</translation>
    </message>
    <message>
        <location filename="../gui/modifDefDlg.cpp" line="375"/>
        <source>A modif with same name exists already.</source>
        <translation>Une modif du même nom existe déjà.</translation>
    </message>
    <message>
        <location filename="../gui/modifDefDlg.cpp" line="388"/>
        <source>The name of the modification is already used by a cross-linker.</source>
        <translation>La même modification est déjà utilisée par un agent pontant.</translation>
    </message>
    <message>
        <location filename="../gui/modifDefDlg.cpp" line="402"/>
        <source>The formula failed to validate.</source>
        <translation>La formule n&apos;a pu être validée.</translation>
    </message>
    <message>
        <location filename="../gui/modifDefDlg.cpp" line="430"/>
        <source>The targets string is invalid.</source>
        <translation>La chaîne des cibles est incorrecte.</translation>
    </message>
    <message>
        <location filename="../gui/modifDefDlg.cpp" line="466"/>
        <source>
The number of modifs in the list widget 
and in the list of modifs is not identical.
</source>
        <translation>
Le nombre des modifs dans le widget liste 
et celui dans la liste des modifs ne sont pas identiques.
</translation>
    </message>
    <message>
        <location filename="../gui/modifDefDlg.cpp" line="484"/>
        <source>
Modif at index %1 has not the same
name as the list widget item at the
same index.
</source>
        <translation>La modif à l&apos;index %1 n&apos;a pas le même nom
que l&apos;item du widget liste au même index.
</translation>
    </message>
    <message>
        <location filename="../gui/modifDefDlg.cpp" line="490"/>
        <source>
Modif at index %1  failed to validate.
</source>
        <translation>
La modif à l&apos;index %1 n&apos;a pu être validée.
</translation>
    </message>
    <message>
        <location filename="../gui/modifDefDlg.cpp" line="413"/>
        <source>The max count value is invalid.</source>
        <translation>Le nombre max est incorrect.</translation>
    </message>
    <message>
        <location filename="../gui/modifDefDlg.cpp" line="229"/>
        <source>A modif with same name is used in cross-linker %1.</source>
        <translation>Une modif du même nom existe déjà dans l&apos;agent pontant %1.</translation>
    </message>
</context>
<context>
    <name>massXpert::MonomerCodeEvaluator</name>
    <message>
        <location filename="../gui/monomerCodeEvaluator.cpp" line="183"/>
        <location filename="../gui/monomerCodeEvaluator.cpp" line="214"/>
        <location filename="../gui/monomerCodeEvaluator.cpp" line="244"/>
        <location filename="../gui/monomerCodeEvaluator.cpp" line="256"/>
        <location filename="../gui/monomerCodeEvaluator.cpp" line="288"/>
        <source>Bad char: %1.</source>
        <translation>Mauvais caract: %1.</translation>
    </message>
</context>
<context>
    <name>massXpert::MonomerCrossLinkDlg</name>
    <message>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="290"/>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="364"/>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="460"/>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="679"/>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="705"/>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="776"/>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="791"/>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="832"/>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="884"/>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="984"/>
        <source>massXpert - Cross-link monomers</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="291"/>
        <source>%1@%2
The monomer index does not correspond to a valid polymer sequence range.
Avoid modifying the sequence while working with cross-links.</source>
        <translation>%1@%2
L&apos;index du monomère ne correspond pas à un intervalle de séquence de polymère valide.
Eviter de modifier la séquence au cours d&apos;opérations impliquant les pontages.</translation>
    </message>
    <message>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="365"/>
        <source>%1@%2
CrossLinker &apos;%3&apos; is not known.</source>
        <translation>%1@%2
L&apos;agent pontant &apos;%3&apos; n&apos;est pas connu.</translation>
    </message>
    <message>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="680"/>
        <source>%1@%2
One monomer position is erroneous:</source>
        <translation>%1@%2
Une position de monomère est erronnée:</translation>
    </message>
    <message>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="777"/>
        <source>%1@%2
The number of monomers engaged in the .crossLink cannot be less than 2.</source>
        <translation>%1@%2
Le nombre de monomères engagés dans le pontage ne peut pas être inférieur à 2.</translation>
    </message>
    <message>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="792"/>
        <source>%1@%2
The number of monomers engaged in the .crossLink must match the number of modifications defined in the crossLinker.</source>
        <translation>%1@%2
Le nombre de monomères engagés dans le pontage doit correspondre au nombre des modifications définies dans l&apos;agent pontant.</translation>
    </message>
    <message>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="833"/>
        <source>%1@%2
Cross-link with %3 failed.</source>
        <translation>%1@%2
Le pontage avec %3 a échoué.</translation>
    </message>
    <message>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="985"/>
        <source>%1@%2
Uncross-link for %3 failed.</source>
        <translation>%1@%2
Le dé-pontage pour %3 a échoué.</translation>
    </message>
    <message>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="461"/>
        <source>%1@%2
The polymer seqeunce has changed.
Avoid modifying the sequence while working with cross-links.</source>
        <translation>%1@%2￼
La séquence de polymère a changé.
Eviter de modifier la séquence pendant les travaux de pontage.</translation>
    </message>
    <message>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="706"/>
        <source>%1@%2
The polymer sequence has changed, continue cross-link?</source>
        <translation>%1@%2￼
La séquence de polymère a changé, continuer le pontage?</translation>
    </message>
    <message>
        <location filename="../gui/monomerCrossLinkDlg.cpp" line="885"/>
        <source>%1@%2
The polymer sequence has changed, continue uncross-link?</source>
        <translation>%1@%2￼
La séquence de polymère a changé, continuer avec le dépontage?</translation>
    </message>
</context>
<context>
    <name>massXpert::MonomerDefDlg</name>
    <message>
        <location filename="../gui/monomerDefDlg.cpp" line="205"/>
        <source>Type Name</source>
        <translation>Taper Nom</translation>
    </message>
    <message>
        <location filename="../gui/monomerDefDlg.cpp" line="206"/>
        <source>Type Code</source>
        <translation>Taper Code</translation>
    </message>
    <message>
        <location filename="../gui/monomerDefDlg.cpp" line="207"/>
        <source>Type Formula</source>
        <translation>Taper Formule</translation>
    </message>
    <message>
        <location filename="../gui/monomerDefDlg.cpp" line="368"/>
        <location filename="../gui/monomerDefDlg.cpp" line="378"/>
        <location filename="../gui/monomerDefDlg.cpp" line="391"/>
        <location filename="../gui/monomerDefDlg.cpp" line="432"/>
        <location filename="../gui/monomerDefDlg.cpp" line="458"/>
        <location filename="../gui/monomerDefDlg.cpp" line="466"/>
        <location filename="../gui/monomerDefDlg.cpp" line="612"/>
        <location filename="../gui/monomerDefDlg.cpp" line="634"/>
        <source>massXpert - Monomer definition</source>
        <translation>massXpert -Définition de Monomère</translation>
    </message>
    <message>
        <location filename="../gui/monomerDefDlg.cpp" line="369"/>
        <source>A monomer with same name exists already.</source>
        <translation>Un monomère du même nom existe déjà.</translation>
    </message>
    <message>
        <location filename="../gui/monomerDefDlg.cpp" line="379"/>
        <source>A monomer with same code exists already.</source>
        <translation>Un monomère du même code existe déjà.</translation>
    </message>
    <message>
        <location filename="../gui/monomerDefDlg.cpp" line="392"/>
        <source>The formula failed to validate.</source>
        <translation>La formule n&apos;a pu être validée.</translation>
    </message>
    <message>
        <location filename="../gui/monomerDefDlg.cpp" line="426"/>
        <source>
The number of monomers in the list widget 
and in the list of monomers is not identical.
</source>
        <translation>
Le nombre de monomères dans le widget liste
et celui dans la liste des monomères ne sont pas identiques.</translation>
    </message>
    <message>
        <location filename="../gui/monomerDefDlg.cpp" line="445"/>
        <source>
Monomer at index %1 has not the same
name as the list widget item at the
same index.
</source>
        <translation>
Le monomère à l&apos;index %1 n&apos;a pas le même nom
que celui de l&apos;item du widget liste au même index.
</translation>
    </message>
    <message>
        <location filename="../gui/monomerDefDlg.cpp" line="451"/>
        <source>
Monomer at index %1  failed to validate.
</source>
        <translation>
Le monomère à l&apos;index %1 n&apos;a pu être validé.
</translation>
    </message>
    <message>
        <location filename="../gui/monomerDefDlg.cpp" line="581"/>
        <location filename="../gui/monomerDefDlg.cpp" line="582"/>
        <source>Bad formula</source>
        <translation>Formule erronée</translation>
    </message>
    <message>
        <location filename="../gui/monomerDefDlg.cpp" line="613"/>
        <source>Threshold value is not valid</source>
        <translation>La valeur de seuil est incorrecte</translation>
    </message>
</context>
<context>
    <name>massXpert::MonomerModificationDlg</name>
    <message>
        <location filename="../gui/monomerModificationDlg.cpp" line="324"/>
        <location filename="../gui/monomerModificationDlg.cpp" line="350"/>
        <location filename="../gui/monomerModificationDlg.cpp" line="605"/>
        <location filename="../gui/monomerModificationDlg.cpp" line="671"/>
        <location filename="../gui/monomerModificationDlg.cpp" line="689"/>
        <location filename="../gui/monomerModificationDlg.cpp" line="847"/>
        <source>massXpert - Modify monomers</source>
        <translation>massXpert - Modifier les monomères</translation>
    </message>
    <message>
        <location filename="../gui/monomerModificationDlg.cpp" line="325"/>
        <source>%1@%2
The monomer index does not correspond to a valid polymer sequence range.
Avoid modifying the sequence while working with modifications.</source>
        <translation>%1@%2
L&apos;index du monomère ne correspond pas à un intervalle de séquence de polymère valide.
Eviter de modifier la séquence au cours d&apos;opérations impliquant les pontages.</translation>
    </message>
    <message>
        <location filename="../gui/monomerModificationDlg.cpp" line="351"/>
        <source>%1@%2
The monomer selected does not correspond to a valid sequence monomer.
Avoid modifying the sequence while working with modifications.</source>
        <translation>%1@%2
Le monomère sélectionné ne correspond pas à un intervalle de séquence de polymère valide.
Eviter de modifier la séquence au cours d&apos;opérations impliquant les pontages.</translation>
    </message>
    <message>
        <location filename="../gui/monomerModificationDlg.cpp" line="730"/>
        <source>Pos. %1: %2 --------------</source>
        <translation>Pos. %1: %2 --------------</translation>
    </message>
    <message>
        <location filename="../gui/monomerModificationDlg.cpp" line="771"/>
        <source>New operation: modify with %1
</source>
        <translation>Nouvelle opération: modifier avec %1
</translation>
    </message>
    <message>
        <location filename="../gui/monomerModificationDlg.cpp" line="496"/>
        <source>%1 at pos. %2</source>
        <translation>%1 en pos. %2</translation>
    </message>
    <message>
        <location filename="../gui/monomerModificationDlg.cpp" line="606"/>
        <source>No target is selected.</source>
        <translation>Aucune cible sélectionnée.</translation>
    </message>
    <message>
        <location filename="../gui/monomerModificationDlg.cpp" line="672"/>
        <source>The defined modification failed to parse.</source>
        <translation>La modification déifinie n&apos;a pu être analysée.</translation>
    </message>
    <message>
        <location filename="../gui/monomerModificationDlg.cpp" line="690"/>
        <source>No modification is selected in the list.</source>
        <translation>Aucune modification sélectionnée dans la liste.</translation>
    </message>
    <message>
        <location filename="../gui/monomerModificationDlg.cpp" line="848"/>
        <source>%1@%2
The monomer to unmodify does not exist in the sequence anymore.
Avoid modifying the sequence while working with modifications.</source>
        <translation>%1@%2￼
Le monomère à démodifier n&apos;existe plus dans la séquence.
Eviter de modifier la séquence pendant les travaux de modification.</translation>
    </message>
</context>
<context>
    <name>massXpert::MzCalculationDlg</name>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="118"/>
        <source>To Clipboard</source>
        <translation>Vers le Presse-Papiers</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="119"/>
        <source>To File</source>
        <translation>Vers un Fichier</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="120"/>
        <source>Select File</source>
        <translation>Sélectionner un Fichier</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="121"/>
        <source>Calculate spectrum</source>
        <translation>Calculer le spectre</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="287"/>
        <location filename="../gui/mzCalculationDlg.cpp" line="439"/>
        <location filename="../gui/mzCalculationDlg.cpp" line="506"/>
        <location filename="../gui/mzCalculationDlg.cpp" line="535"/>
        <location filename="../gui/mzCalculationDlg.cpp" line="575"/>
        <location filename="../gui/mzCalculationDlg.cpp" line="599"/>
        <source>massXpert - m/z Ratio Calculator</source>
        <translation>massXpert - Calculatrice de Ratios m/z</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="288"/>
        <source>%1@%2
Failed to validate the formula.</source>
        <translation>%1@%2
Échec de la validation de la formule.</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="507"/>
        <source>%1@%2
Failed to validate initial ionization rule.</source>
        <translation>%1@%2
Échec de la validation de la règle d&apos;ionisation.</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="536"/>
        <source>%1@%2
Failed to validate target ionization rule.</source>
        <translation>%1@%2Échec de la validation de la règle d&apos;ionisation cible.</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="576"/>
        <location filename="../gui/mzCalculationDlg.cpp" line="600"/>
        <source>%1@%2
Failed to read mono mass.</source>
        <translation>%1@%2
Échec de lecture de la masse mono.</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="812"/>
        <source>massXpert - Export Data</source>
        <translation>massXpert - Exporter Données</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="813"/>
        <source>Failed to open file in append mode.</source>
        <translation>Échec d&apos;ouverture de fichier en mode append.</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="835"/>
        <source>Select File To Export Data To</source>
        <translation>Sélectionner un Fichier où Exporter les Données</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="440"/>
        <source>%1@%2
Failed to perform reionization.</source>
        <translation>%1@%2￼
Echec de la réionisation.</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationDlg.cpp" line="837"/>
        <source>Data files(*.dat *.DAT)</source>
        <translation>Fichiers de données(*.dat *.DAT)</translation>
    </message>
</context>
<context>
    <name>massXpert::MzCalculationTreeViewModel</name>
    <message>
        <location filename="../gui/mzCalculationTreeViewModel.cpp" line="58"/>
        <source>Mono</source>
        <translation>Mono</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationTreeViewModel.cpp" line="58"/>
        <source>Avg</source>
        <translation>Moy</translation>
    </message>
    <message>
        <location filename="../gui/mzCalculationTreeViewModel.cpp" line="58"/>
        <source>Charge</source>
        <translation>Charge</translation>
    </message>
</context>
<context>
    <name>massXpert::MzLabFormulaBasedActionsDlg</name>
    <message>
        <location filename="../gui/mzLabFormulaBasedActionsDlg.cpp" line="64"/>
        <source>massXpert: mz Lab - Formula-based Actions</source>
        <translation>massXpert: mz Lab - Actions sur formule</translation>
    </message>
    <message>
        <location filename="../gui/mzLabFormulaBasedActionsDlg.cpp" line="138"/>
        <location filename="../gui/mzLabFormulaBasedActionsDlg.cpp" line="157"/>
        <location filename="../gui/mzLabFormulaBasedActionsDlg.cpp" line="174"/>
        <location filename="../gui/mzLabFormulaBasedActionsDlg.cpp" line="207"/>
        <location filename="../gui/mzLabFormulaBasedActionsDlg.cpp" line="224"/>
        <location filename="../gui/mzLabFormulaBasedActionsDlg.cpp" line="267"/>
        <location filename="../gui/mzLabFormulaBasedActionsDlg.cpp" line="284"/>
        <source>massXpert: mz Lab</source>
        <translation>massXpert: mz Lab</translation>
    </message>
    <message>
        <location filename="../gui/mzLabFormulaBasedActionsDlg.cpp" line="139"/>
        <source>%1@%2
Please, enter a valid formula.</source>
        <translation>%1@%2
Entrer une formule correcte.</translation>
    </message>
    <message>
        <location filename="../gui/mzLabFormulaBasedActionsDlg.cpp" line="158"/>
        <source>%1@%2
The formula &apos;%3&apos; is not valid.</source>
        <translation>%1@%2
La formule &apos;%3&apos; est incorrecte.</translation>
    </message>
    <message>
        <location filename="../gui/mzLabFormulaBasedActionsDlg.cpp" line="175"/>
        <location filename="../gui/mzLabFormulaBasedActionsDlg.cpp" line="225"/>
        <location filename="../gui/mzLabFormulaBasedActionsDlg.cpp" line="285"/>
        <source>%1@%2
Please, select one input list first.</source>
        <translation>%1@%2
Sélectionner une liste entrée d&apos;abord.</translation>
    </message>
    <message>
        <location filename="../gui/mzLabFormulaBasedActionsDlg.cpp" line="208"/>
        <source>%1@%2
Please, enter a valid increment.</source>
        <translation>%1@%2￼
Enter un incrément correct.</translation>
    </message>
    <message>
        <location filename="../gui/mzLabFormulaBasedActionsDlg.cpp" line="268"/>
        <source>%1@%2
Failed to validate ionization rule.</source>
        <translation>%1@%2￼
Échec de la validation de la règle d&apos;ionisation.</translation>
    </message>
</context>
<context>
    <name>massXpert::MzLabInputOligomerTableView</name>
    <message>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="192"/>
        <source>massXpert: mzLab</source>
        <translation>massXpert: mzLab</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="193"/>
        <source>You are switching mass types.Ok to continue ?</source>
        <translation>Vous intervertissez les types de masse. Ok pour continuer?</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="591"/>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="617"/>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="682"/>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="757"/>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="809"/>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="824"/>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="910"/>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="939"/>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="978"/>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="993"/>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="1111"/>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="1143"/>
        <source>massXpert - Drag and drop</source>
        <translation>massXpert - Glisser et déposer</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="592"/>
        <source>%1@%2
Failed to parse line.</source>
        <translation>%1@%2
Échec de l&apos;analyse de la ligne.</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="618"/>
        <source>%1@%2
Failed to parse line:
%3</source>
        <translation>%1@%2
Échec de l&apos;analyse de la ligne:
%3</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="683"/>
        <source>%1@%2
Failed to convert %3 to double.
Make sure the proper delimiter is used.</source>
        <translation>%1@%2
Échec de la conversion de %3 en double.
S&apos;assurer que le délimiteur correct est utilisé.</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="810"/>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="979"/>
        <source>%1@%2
Failed to convert %3 to double</source>
        <translation>%1@%2
Échec de la conversion de %3 en double</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="758"/>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="911"/>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="1112"/>
        <source>%1@%2
Failed to validate the ionization rule</source>
        <translation>%1@%2
Échec de la validation de la règle d&apos;ionisation</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="825"/>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="994"/>
        <source>%1@%2
Failed to convert %3 to int</source>
        <translation>%1@%2
Échec de la conversion de %3 en entier</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="940"/>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="1144"/>
        <source>%1@%2
Charge of a fragmentation oligomer cannot be 0</source>
        <translation>%1@%2
La charge d&apos;un oligomère de fragmentatIon ne peut être 0</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="1394"/>
        <source>massXpert - Cleavage</source>
        <translation>massXpert - Clivage</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableView.cpp" line="1395"/>
        <source>%1@%2
The monomer indices do not correspond to a valid polymer sequence range.
Avoid modifying the sequence while working with oligomers.</source>
        <translation>%1@%2
Les indices de monomère ne correspondent plus à un intervalle de séquence de polymère valable.
Éviter de modifier la séquence pendant les travaux avec les oligomères.</translation>
    </message>
</context>
<context>
    <name>massXpert::MzLabInputOligomerTableViewDlg</name>
    <message>
        <location filename="../gui/mzLabInputOligomerTableViewDlg.cpp" line="100"/>
        <location filename="../gui/mzLabInputOligomerTableViewDlg.cpp" line="157"/>
        <source>massXpert: mz Lab - %1</source>
        <translation>massXpert: mz Lab - %1</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableViewDlg.cpp" line="293"/>
        <source>Which type of masses are dealt with here?</source>
        <translation>Quel type de masse sont gérées ici ?</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableViewDlg.cpp" line="295"/>
        <source>mono masses</source>
        <translation>masse mono</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableViewDlg.cpp" line="298"/>
        <source>avg masses</source>
        <translation>masses moy</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableViewDlg.cpp" line="376"/>
        <location filename="../gui/mzLabInputOligomerTableViewDlg.cpp" line="750"/>
        <location filename="../gui/mzLabInputOligomerTableViewDlg.cpp" line="777"/>
        <source>massXpert: mz Lab</source>
        <translation>massXpert: mz Lab</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableViewDlg.cpp" line="377"/>
        <source>%1@%2
Failed accounting for formula &apos;%3&apos;.</source>
        <translation>%1@%2￼
Échec de la prise en compte de la formule &apos;%3&apos;.</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableViewDlg.cpp" line="677"/>
        <source>m/z threshold </source>
        <translation>seuil m/z</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableViewDlg.cpp" line="679"/>
        <source>m threshold </source>
        <translation>seuil m</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableViewDlg.cpp" line="751"/>
        <source>%1@%2
Failed to get oligomer&apos;s charge.</source>
        <translation>%1@%2￼
Échec de la détermination de la charge de l&apos;oligomère.</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableViewDlg.cpp" line="778"/>
        <source>%1@%2
Failed to set oligomer&apos;s charge.</source>
        <translation>%1@%2￼
Échec du réglage de la charge de l&apos;oligomère.</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableViewDlg.cpp" line="854"/>
        <source>massXpert:mz Lab - Input list</source>
        <translation>massXpert:mz Lab - Liste entrée</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableViewDlg.cpp" line="855"/>
        <source>%1@%2
Failed to validate ionization rule.</source>
        <translation>%1@%2￼
Échec de la validation de la règle d&apos;ionisation.</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableViewDlg.cpp" line="926"/>
        <source>ioniz. </source>
        <translation>ionis.</translation>
    </message>
</context>
<context>
    <name>massXpert::MzLabInputOligomerTableViewModel</name>
    <message>
        <location filename="../gui/mzLabInputOligomerTableViewModel.cpp" line="162"/>
        <source>m/z</source>
        <translation>m/z</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableViewModel.cpp" line="164"/>
        <source>z</source>
        <translation>z</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableViewModel.cpp" line="166"/>
        <source>name</source>
        <translation>nom</translation>
    </message>
    <message>
        <location filename="../gui/mzLabInputOligomerTableViewModel.cpp" line="168"/>
        <source>coords</source>
        <translation>coords</translation>
    </message>
</context>
<context>
    <name>massXpert::MzLabMassBasedActionsDlg</name>
    <message>
        <location filename="../gui/mzLabMassBasedActionsDlg.cpp" line="63"/>
        <source>massXpert: mz Lab - Mass-based Actions</source>
        <translation>massXpert: mz Lab - Actions sur masse</translation>
    </message>
    <message>
        <location filename="../gui/mzLabMassBasedActionsDlg.cpp" line="122"/>
        <location filename="../gui/mzLabMassBasedActionsDlg.cpp" line="140"/>
        <location filename="../gui/mzLabMassBasedActionsDlg.cpp" line="156"/>
        <location filename="../gui/mzLabMassBasedActionsDlg.cpp" line="188"/>
        <location filename="../gui/mzLabMassBasedActionsDlg.cpp" line="207"/>
        <location filename="../gui/mzLabMassBasedActionsDlg.cpp" line="223"/>
        <source>massXpert: mz Lab</source>
        <translation>massXpert: mz Lab</translation>
    </message>
    <message>
        <location filename="../gui/mzLabMassBasedActionsDlg.cpp" line="123"/>
        <source>%1@%2
Please, enter a valid mass.</source>
        <translation>%1@%2
Entrer une mass correcte.</translation>
    </message>
    <message>
        <location filename="../gui/mzLabMassBasedActionsDlg.cpp" line="141"/>
        <location filename="../gui/mzLabMassBasedActionsDlg.cpp" line="208"/>
        <source>%1@%2
Failed to convert %3 to double</source>
        <translation>%1@%2
Échec de la conversion de %3 en double</translation>
    </message>
    <message>
        <location filename="../gui/mzLabMassBasedActionsDlg.cpp" line="157"/>
        <location filename="../gui/mzLabMassBasedActionsDlg.cpp" line="224"/>
        <source>%1@%2
Please, select one input list first.</source>
        <translation>%1@%2
Sélectionner une liste entrée d&apos;abord.</translation>
    </message>
    <message>
        <location filename="../gui/mzLabMassBasedActionsDlg.cpp" line="189"/>
        <source>%1@%2
Please, enter a valid threshold.</source>
        <translation>%1@%2￼
Entrer un seuil correct.</translation>
    </message>
</context>
<context>
    <name>massXpert::MzLabMatchBasedActionsDlg</name>
    <message>
        <location filename="../gui/mzLabMatchBasedActionsDlg.cpp" line="66"/>
        <source>massXpert: mz Lab - Match-based Actions</source>
        <translation>massXpert: mz Lab - Actions sur correspondance</translation>
    </message>
    <message>
        <location filename="../gui/mzLabMatchBasedActionsDlg.cpp" line="71"/>
        <source>AMU</source>
        <translation>AMU</translation>
    </message>
    <message>
        <location filename="../gui/mzLabMatchBasedActionsDlg.cpp" line="71"/>
        <source>PCT</source>
        <translation>PCT</translation>
    </message>
    <message>
        <location filename="../gui/mzLabMatchBasedActionsDlg.cpp" line="71"/>
        <source>PPM</source>
        <translation>PPM</translation>
    </message>
    <message>
        <location filename="../gui/mzLabMatchBasedActionsDlg.cpp" line="77"/>
        <source>AMU: atom mass unit 
PCT: percent 
PPM: part per million</source>
        <translation>AMU: unité de masse atomique 
PCT: pourcentage 
PPM: partie par million</translation>
    </message>
    <message>
        <location filename="../gui/mzLabMatchBasedActionsDlg.cpp" line="213"/>
        <location filename="../gui/mzLabMatchBasedActionsDlg.cpp" line="426"/>
        <source>massXpert: mz Lab</source>
        <translation>massXpert: mz Lab</translation>
    </message>
    <message>
        <location filename="../gui/mzLabMatchBasedActionsDlg.cpp" line="214"/>
        <source>This feature performs matches between two input lists.
Please, select two input lists first.</source>
        <translation>Cette fonctionnalité recherche des paires entre deux listes entrée.
Sélectionner deux listes entrée d&apos;abord.</translation>
    </message>
    <message>
        <location filename="../gui/mzLabMatchBasedActionsDlg.cpp" line="254"/>
        <source>massXpert - mz Lab</source>
        <translation>massXpert - mz Lab</translation>
    </message>
    <message>
        <location filename="../gui/mzLabMatchBasedActionsDlg.cpp" line="255"/>
        <source>The lists to match are not of the same mass type.

Continue?</source>
        <translation>Les listes à rechercher ne sont pas du même type de masse.
Continuer?</translation>
    </message>
    <message>
        <location filename="../gui/mzLabMatchBasedActionsDlg.cpp" line="387"/>
        <source>%1 vs %2</source>
        <translation>%1 vs %2</translation>
    </message>
    <message>
        <location filename="../gui/mzLabMatchBasedActionsDlg.cpp" line="409"/>
        <source>To perform a match operation one must have
two lists of (m/z,z) pairs already available.

One list must be selected in the Catalogue 1
and the other list must be selected in the Catalogue 2.

There are two kinds of &quot;match work&quot; operations : 
One that aims at finding matches between (m/z,z) pairs
in each of the two lists and one that aims at finding
(m/z,z) pairs that have no match in the other list.

How are match (or non-match) works performed ?
One match work is performed by searching in
the first list (Catalogue 1) all the (m/z,z) pairs 
that are also in the second list (Catalogue 2).

One non-match work is performed by searching in the
first list (Catalogue 1) all the (m/z,z) pairs that are
not found in the second list (Catalogue 2).
</source>
        <translation>Pour effecter une opération de recherche de correspondances
il faut disposer de deux listes de paires (m/z,z).

Une liste doit être sélectionnée dans le Catalogue 1
et l&apos;autre liste doit être sélectionnée dans le Catalogue 2.

Il y a deux types de &quot;recherche de correspondances&quot; :
un type qui vise à rechercher des correspondances entre paires (m/z,z)
dans chacune des deux listes et un autre type qui vise à rechercher les paires (m/z,z)
qui n&apos;ont pas de correspondance dans l&apos;autre liste.

Comment sont menées les opérations de recherche de 
correspondance (ou de non-correspondance) ? 

La recherche de correspondances 
est menée en recherchant dans la première liste des paires (m/z,z) (Catalogue 1)
toutes les paires que l&apos;on retrouve aussi dans l&apos;autre liste (Catalogue 2).

La recherche de non-correspondances est menée en recherchant 
dans la première liste des paires (m/z,z) (Catalogue 1) toutes les paires 
que l&apos;on ne retrouve pas dans l&apos;autre list (Catalogue 2).</translation>
    </message>
</context>
<context>
    <name>massXpert::MzLabOutputOligomerTableView</name>
    <message>
        <location filename="../gui/mzLabOutputOligomerTableView.cpp" line="387"/>
        <source>massXpert - mz Lab</source>
        <translation>massXpert - mz Lab</translation>
    </message>
    <message>
        <location filename="../gui/mzLabOutputOligomerTableView.cpp" line="388"/>
        <source>%1@%2
The monomer indices do not correspond to a valid polymer sequence range.
Avoid modifying the sequence while working with oligomers.</source>
        <translation>%1@%2
Les indices de monomère ne correspondent plus à un intervalle de séquence de polymère valable.
Éviter de modifier la séquence pendant les travaux avec les oligomères.</translation>
    </message>
</context>
<context>
    <name>massXpert::MzLabOutputOligomerTableViewDlg</name>
    <message>
        <location filename="../gui/mzLabOutputOligomerTableViewDlg.cpp" line="88"/>
        <location filename="../gui/mzLabOutputOligomerTableViewDlg.cpp" line="131"/>
        <source>massXpert: mz Lab - %1</source>
        <translation>massXpert: mz Lab - %1</translation>
    </message>
</context>
<context>
    <name>massXpert::MzLabOutputOligomerTableViewModel</name>
    <message>
        <location filename="../gui/mzLabOutputOligomerTableViewModel.cpp" line="156"/>
        <source>m/z 1</source>
        <translation>m/z 1</translation>
    </message>
    <message>
        <location filename="../gui/mzLabOutputOligomerTableViewModel.cpp" line="158"/>
        <source>z 1</source>
        <translation>z 1</translation>
    </message>
    <message>
        <location filename="../gui/mzLabOutputOligomerTableViewModel.cpp" line="160"/>
        <source>m/z 2</source>
        <translation>m/z 2</translation>
    </message>
    <message>
        <location filename="../gui/mzLabOutputOligomerTableViewModel.cpp" line="162"/>
        <source>z 2</source>
        <translation>z 2</translation>
    </message>
    <message>
        <location filename="../gui/mzLabOutputOligomerTableViewModel.cpp" line="164"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../gui/mzLabOutputOligomerTableViewModel.cpp" line="166"/>
        <source>Match</source>
        <translation>Correspondance</translation>
    </message>
</context>
<context>
    <name>massXpert::MzLabWnd</name>
    <message>
        <location filename="../gui/mzLabWnd.cpp" line="65"/>
        <location filename="../gui/mzLabWnd.cpp" line="78"/>
        <source>massXpert - Calculator</source>
        <translation>massXpert - Calculatrice</translation>
    </message>
    <message>
        <location filename="../gui/mzLabWnd.cpp" line="66"/>
        <source>Polymer chemistry definition filepath empty.</source>
        <translation>Le chemin de fichier de la définition de chimie de polymère est vide.</translation>
    </message>
    <message>
        <location filename="../gui/mzLabWnd.cpp" line="79"/>
        <source>Failed to initialize the mz lab window.</source>
        <translation>Échec de l&apos;initialisation de la fenêtre du mz lab.</translation>
    </message>
    <message>
        <location filename="../gui/mzLabWnd.cpp" line="83"/>
        <location filename="../gui/mzLabWnd.cpp" line="288"/>
        <source>%1 %2[*]</source>
        <translation>%1 %2[*]</translation>
    </message>
    <message>
        <location filename="../gui/mzLabWnd.cpp" line="84"/>
        <location filename="../gui/mzLabWnd.cpp" line="289"/>
        <source>massXpert: mz Lab - </source>
        <translation>massXpert: mz Lab - </translation>
    </message>
    <message>
        <location filename="../gui/mzLabWnd.cpp" line="315"/>
        <location filename="../gui/mzLabWnd.cpp" line="493"/>
        <source>massXpert: mz Lab</source>
        <translation>massXpert: mz Lab</translation>
    </message>
    <message>
        <location filename="../gui/mzLabWnd.cpp" line="316"/>
        <source>%1@%2
The formula &apos;%3&apos; is not valid.</source>
        <translation>%1@%2
La formule &apos;%3&apos; est incorrecte.</translation>
    </message>
    <message>
        <location filename="../gui/mzLabWnd.cpp" line="629"/>
        <source>massXpert - mz Lab</source>
        <translation>massXpert - mz Lab</translation>
    </message>
    <message>
        <location filename="../gui/mzLabWnd.cpp" line="464"/>
        <source>Give a name to the new input list</source>
        <translation>Donner un nom à la nouvelle liste entrée</translation>
    </message>
    <message>
        <location filename="../gui/mzLabWnd.cpp" line="465"/>
        <location filename="../gui/mzLabWnd.cpp" line="479"/>
        <source>List name:</source>
        <translation>Nom de liste:</translation>
    </message>
    <message>
        <location filename="../gui/mzLabWnd.cpp" line="467"/>
        <source>New input list name</source>
        <translation>Nom de la nouvelle liste entrée</translation>
    </message>
    <message>
        <location filename="../gui/mzLabWnd.cpp" line="478"/>
        <source>Confirm the name of the new input list</source>
        <translation>Confirmer le nom de la nouvelle liste entrée</translation>
    </message>
    <message>
        <location filename="../gui/mzLabWnd.cpp" line="494"/>
        <source>%1@%2
The &apos;%3&apos; name is already taken.</source>
        <translation>%1@%2
Le nom &apos;%3&apos; est déjà pris.</translation>
    </message>
    <message>
        <location filename="../gui/mzLabWnd.cpp" line="630"/>
        <source>The mz Lab has been modified.
OK to exit?</source>
        <translation>Le  mz Lab est modifié.
OK pour quitter?</translation>
    </message>
</context>
<context>
    <name>massXpert::PkaPhPiDlg</name>
    <message>
        <location filename="../gui/pkaPhPiDlg.cpp" line="92"/>
        <source>To Clipboard</source>
        <translation>Vers le Presse-Papiers</translation>
    </message>
    <message>
        <location filename="../gui/pkaPhPiDlg.cpp" line="93"/>
        <source>To File</source>
        <translation>Vers un Fichier</translation>
    </message>
    <message>
        <location filename="../gui/pkaPhPiDlg.cpp" line="94"/>
        <source>Select File</source>
        <translation>Sélectionner un Fichier</translation>
    </message>
    <message>
        <location filename="../gui/pkaPhPiDlg.cpp" line="225"/>
        <location filename="../gui/pkaPhPiDlg.cpp" line="243"/>
        <location filename="../gui/pkaPhPiDlg.cpp" line="285"/>
        <location filename="../gui/pkaPhPiDlg.cpp" line="302"/>
        <source>massXpert - pKa pH pI</source>
        <translation>massXpert - pKa pH pI</translation>
    </message>
    <message>
        <location filename="../gui/pkaPhPiDlg.cpp" line="226"/>
        <location filename="../gui/pkaPhPiDlg.cpp" line="286"/>
        <source>Failed validating input data.</source>
        <translation>Échec de la validation des données entrées.</translation>
    </message>
    <message>
        <location filename="../gui/pkaPhPiDlg.cpp" line="244"/>
        <location filename="../gui/pkaPhPiDlg.cpp" line="303"/>
        <source>Failed computing charges.</source>
        <translation>Échec du calcul des charges.</translation>
    </message>
    <message>
        <location filename="../gui/pkaPhPiDlg.cpp" line="495"/>
        <source>massXpert - Export Data</source>
        <translation>massXpert - Exporter Données</translation>
    </message>
    <message>
        <location filename="../gui/pkaPhPiDlg.cpp" line="496"/>
        <source>Failed to open file in append mode.</source>
        <translation>Échec d&apos;ouverture de fichier en mode append.</translation>
    </message>
    <message>
        <location filename="../gui/pkaPhPiDlg.cpp" line="516"/>
        <source>Select File To Export Data To</source>
        <translation>Sélectionner un Fichier où Exporter les Données</translation>
    </message>
    <message>
        <location filename="../gui/pkaPhPiDlg.cpp" line="518"/>
        <source>Data files(*.dat *.DAT)</source>
        <translation>Fichiers de données(*.dat *.DAT)</translation>
    </message>
</context>
<context>
    <name>massXpert::PolChemDefWnd</name>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="71"/>
        <location filename="../gui/polChemDefWnd.cpp" line="291"/>
        <location filename="../gui/polChemDefWnd.cpp" line="495"/>
        <location filename="../gui/polChemDefWnd.cpp" line="805"/>
        <location filename="../gui/polChemDefWnd.cpp" line="828"/>
        <source>massXpert - Polymer Chemistry Definition</source>
        <translation>massXpert - Définition de Chimie de Polymère</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="72"/>
        <source>%1@%2
Failed to initialize the polymer chemistry definition window.</source>
        <translation>%1@%2
Échec de l&apos;initialization de la fenêtre de définition de chimie de polymère.</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="292"/>
        <source>%1@%2
Failed to render the polymer chemistry definition file.</source>
        <translation>%1@%2Échec du rendu du fichier de définition de chimie de polymère.</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="308"/>
        <source>Correctly opened file.</source>
        <translation>Fichier correctement ouvert.</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="321"/>
        <source>Name of the polymer chemistry definition cannot be empty.</source>
        <translation>Le nom de la définition de chimie de polymère ne peut être vide.</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="496"/>
        <source>Edit the atom definition first.</source>
        <translation>Éditer la définition d&apos;atomes d&apos;abord.</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="509"/>
        <source>Untitled</source>
        <translation>Sans-Titre</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="516"/>
        <source>%1 %2[*]</source>
        <translation>%1 %2[*]</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="517"/>
        <source>massXpert - Polymer Chemistry Definition:</source>
        <translation>massXpert - Définition de Chimie de Polymère :</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="728"/>
        <source>Name cannot be empty.</source>
        <translation>Le nom ne peut être vide.</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="737"/>
        <source>Validation of left cap failed.</source>
        <translation>Échec de la validation de la coiffe gauche.</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="744"/>
        <source>Validation of right cap failed.</source>
        <translation>Échec de la validation de la coiffe droite.</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="751"/>
        <source>Validation of ionization rule failed.</source>
        <translation>Échec de la validation de la règle d&apos;ionisation.</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="799"/>
        <source>Total number of errors: %1
</source>
        <translation>Nombre total d&apos;erreurs: %1
</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="810"/>
        <source>Polymer chemistry definition data validation succeeded.</source>
        <translation>Succès de la validation des données de la définition de chimie de polymère.</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="829"/>
        <source>The document has been modified.
Do you want to save your changes?</source>
        <translation>Le document a été modifié.
Voulez-vous sauvegarder les modifications ?</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="885"/>
        <location filename="../gui/polChemDefWnd.cpp" line="954"/>
        <source>File save failed.</source>
        <translation>Échec de la sauvegarde du fichier.</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="899"/>
        <location filename="../gui/polChemDefWnd.cpp" line="968"/>
        <source>File save succeeded.</source>
        <translation>Succès de la sauvegarde du fichier.</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="939"/>
        <source>Save Polymer Chemistry Definition File</source>
        <translation>Sauvegarder le Fichier de Définition de Chimie de Polymère</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="946"/>
        <source>File path is empty</source>
        <translation>Le chemin de fichier est vide</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="354"/>
        <location filename="../gui/polChemDefWnd.cpp" line="389"/>
        <location filename="../gui/polChemDefWnd.cpp" line="422"/>
        <source>Formula(%1) is invalid.</source>
        <translation>Formule(%1) incorrecte.</translation>
    </message>
    <message>
        <location filename="../gui/polChemDefWnd.cpp" line="942"/>
        <source>XML files(*.xml *.XML)</source>
        <translation>Fichiers XML(*.xml *.XML)</translation>
    </message>
</context>
<context>
    <name>massXpert::PolymerModificationDlg</name>
    <message>
        <location filename="../gui/polymerModificationDlg.cpp" line="237"/>
        <source>massXpert - Polymer Modification</source>
        <translation>massXpert - Modification de Polymère</translation>
    </message>
    <message>
        <location filename="../gui/polymerModificationDlg.cpp" line="238"/>
        <source>Failed to find formula(%1) in list.</source>
        <translation>Formule(%1) non trouvée dans la liste.</translation>
    </message>
</context>
<context>
    <name>massXpert::SequenceEditorFindDlg</name>
    <message>
        <location filename="../gui/sequenceEditorFindDlg.cpp" line="159"/>
        <location filename="../gui/sequenceEditorFindDlg.cpp" line="219"/>
        <location filename="../gui/sequenceEditorFindDlg.cpp" line="243"/>
        <location filename="../gui/sequenceEditorFindDlg.cpp" line="293"/>
        <source>massXpert - Find Sequence</source>
        <translation>massXpert - Recherche de Séquence</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorFindDlg.cpp" line="160"/>
        <location filename="../gui/sequenceEditorFindDlg.cpp" line="244"/>
        <source>%1@%2
 Failed to validate motif: %3</source>
        <translation>%1@%2
 Echec de la validation du motif: %3</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorFindDlg.cpp" line="220"/>
        <location filename="../gui/sequenceEditorFindDlg.cpp" line="294"/>
        <source>Failed to find sequence motif: %1</source>
        <translation>Echec de la recherche du motif de séquence: %1</translation>
    </message>
</context>
<context>
    <name>massXpert::SequenceEditorGraphicsView</name>
    <message>
        <location filename="../gui/sequenceEditorGraphicsView.cpp" line="1087"/>
        <source>Rendering vignettes</source>
        <translation>Rendu des vignettes en cours</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorGraphicsView.cpp" line="1100"/>
        <source>Rendering vignettes: %1 - %2</source>
        <translation>Rendu des vignettes en cours : %1 - %2</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorGraphicsView.cpp" line="1144"/>
        <source>Done rendering vignettes</source>
        <translation>Rendu des vignettes terminé</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorGraphicsView.cpp" line="1188"/>
        <source>Scaling vignettes</source>
        <translation>Mise à l&apos;échelle des vignettes</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorGraphicsView.cpp" line="1201"/>
        <source>Scaling vignettes: %1</source>
        <translation>Mise à l&apos;échelle des vignettes : %1</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorGraphicsView.cpp" line="1211"/>
        <source>Done scaling vignettes</source>
        <translation>Mise à l&apos;échelle des vignettes terminée</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorGraphicsView.cpp" line="1371"/>
        <source>Positioning vignettes</source>
        <translation>Positionnement des vignettes</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorGraphicsView.cpp" line="1376"/>
        <source>Done positioning vignettes</source>
        <translation>Positionnement des vignettes terminé</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorGraphicsView.cpp" line="1397"/>
        <source>Done drawing sequence</source>
        <translation>Dessin de la séquence terminé</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorGraphicsViewKeyboardHandling.cpp" line="251"/>
        <location filename="../gui/sequenceEditorGraphicsViewKeyboardHandling.cpp" line="329"/>
        <location filename="../gui/sequenceEditorGraphicsViewKeyboardHandling.cpp" line="383"/>
        <source>massxpert</source>
        <translation>massxpert</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorGraphicsViewKeyboardHandling.cpp" line="252"/>
        <location filename="../gui/sequenceEditorGraphicsViewKeyboardHandling.cpp" line="330"/>
        <location filename="../gui/sequenceEditorGraphicsViewKeyboardHandling.cpp" line="384"/>
        <source>Sequence editing with multi-region selection is not supported.</source>
        <translation>L&apos;édition avec sélection multi-région n&apos;est pas supportée.</translation>
    </message>
</context>
<context>
    <name>massXpert::SequenceEditorWnd</name>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="82"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="854"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="985"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1002"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1018"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1037"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1054"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1088"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1105"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1125"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1173"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1196"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1219"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1239"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1259"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1367"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2029"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2199"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2508"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2526"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2612"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2652"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2700"/>
        <source>massXpert - Polymer Sequence Editor</source>
        <translation>massXpert - Éditeur de Séquence de Polymère</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="83"/>
        <source>Failed to pre-initialize the polymer sequence editor window.</source>
        <translation>Échec de la pré-initialisation de la fenêtre d&apos;édition de séquence de polymère.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="457"/>
        <source>massXpert - Export Data</source>
        <translation>massXpert - Exporter Données</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="458"/>
        <source>Failed to open file in append mode.</source>
        <translation>Échec d&apos;ouverture de fichier en mode append.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="478"/>
        <source>Select File To Export Data To</source>
        <translation>Sélectionner un Fichier où Exporter les Données</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="499"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="500"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="501"/>
        <source>Closes the sequence</source>
        <translation>Ferme la séquence</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="505"/>
        <source>&amp;Save</source>
        <translation>&amp;Sauvegarder</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="506"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="507"/>
        <source>Saves the sequence</source>
        <translation>Sauvegarde la séquence</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="511"/>
        <source>Save&amp;as</source>
        <oldsource>Save&amp;As</oldsource>
        <translation>Sauvegarder &amp;Nouveau</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="512"/>
        <source>Ctrl+Alt+S</source>
        <translation>Ctrl+Alt+S</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="513"/>
        <source>Saves the sequence in another file</source>
        <translation>Sauvegarde la séquence dans un nouveau fichier</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="519"/>
        <source>&amp;Import raw</source>
        <oldsource>&amp;Import Raw</oldsource>
        <translation>&amp;Importer brut</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="520"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="526"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="521"/>
        <source>Imports a raw text file</source>
        <translation>Importe un fichier texte brut</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="525"/>
        <source>&amp;Import PDB</source>
        <translation>&amp;Import PDB</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="527"/>
        <source>Imports a PDB file</source>
        <translation>Importe un fichier PDB</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="533"/>
        <source>Export to &amp;clipboard</source>
        <oldsource>Export to &amp;Clipboard</oldsource>
        <translation>Exporter vers le &amp;presse-papiers</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="536"/>
        <source>Export as text to the clipboard</source>
        <translation>Exporter au format texte vers le presse-papiers</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="541"/>
        <source>Export to &amp;file</source>
        <oldsource>Export to &amp;File</oldsource>
        <translation>Exporter vers un &amp;fichier</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="544"/>
        <source>Export as text to file</source>
        <translation>Exporter au format texte vers un fichier</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="549"/>
        <source>&amp;Select export file</source>
        <oldsource>&amp;Select Export File</oldsource>
        <translation>&amp;Sélectionner le fichier pour l&apos;export</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="552"/>
        <source>Select file to export as text to</source>
        <translation>Sélectionner le fichier où exporter les données au format texte</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="558"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copier</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="559"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="560"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="576"/>
        <source>Copy the selected region of the sequence</source>
        <translation>Copier la région de séquence sélectionnée</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="566"/>
        <source>C&amp;ut</source>
        <translation>C&amp;ouper</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="567"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="568"/>
        <source>Cut the selected region of the sequence</source>
        <translation>Couper la région de séquence sélectionnée</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="574"/>
        <source>&amp;Paste</source>
        <translation>Co&amp;ller</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="575"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="582"/>
        <source>&amp;Find sequence</source>
        <oldsource>&amp;Find Sequence</oldsource>
        <translation>&amp;Rechercher séquence</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="583"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="584"/>
        <source>Find a sequence in the polymer sequence</source>
        <translation>Rechercher un séquence dans la séquence de polymère</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="591"/>
        <source>Modify &amp;monomer(s)</source>
        <oldsource>Modify &amp;Monomer(s)</oldsource>
        <translation>Modifier &amp;monomère(s)</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="594"/>
        <source>Modifies monomer(s)</source>
        <translation>Modifie le(s) monomère(s)</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="599"/>
        <source>Modify &amp;polymer</source>
        <oldsource>Modify &amp;Polymer</oldsource>
        <translation>Modifier &amp;polymère</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="602"/>
        <source>Modifies the polymer</source>
        <translation>Modifier le polymère</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="620"/>
        <source>Cross-&amp;link monomers</source>
        <oldsource>Cross-&amp;link Monomers</oldsource>
        <translation>&amp;Ponter monomères</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="623"/>
        <source>Cross-link monomers</source>
        <translation>Ponter des monomères</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="628"/>
        <source>&amp;Cleave</source>
        <translation>&amp;Cliver</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="631"/>
        <source>Cleaves the polymer</source>
        <translation>Clive le polymère (digestion enzymatique ou chimique)</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="636"/>
        <source>Fra&amp;gment</source>
        <translation>Fra&amp;gmenter</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="639"/>
        <source>Fragments the polymer</source>
        <translation>Fragmenter le polymère (l&apos;oligomère sélectionné)</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="644"/>
        <source>Search &amp;masses</source>
        <oldsource>&amp;Mass search</oldsource>
        <translation>Rerchercher &amp;masse(s)</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="647"/>
        <source>Search oligomers based on mass</source>
        <translation>Recherche des oligomères sur la base de leur masse</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="652"/>
        <source>Compute m/z &amp;ratios</source>
        <oldsource>Compute m/z &amp;Ratios</oldsource>
        <translation>Calculer &amp;ratios m/z</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="655"/>
        <source>Compute ion charge families</source>
        <translation>Calcule des familles d&apos;ions multichargés</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="660"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="663"/>
        <source>Determine compositions</source>
        <oldsource>Determine Compositions</oldsource>
        <translation>Déterminer compositions</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="668"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="671"/>
        <source>pKa pH pI</source>
        <translation>pKa pH pI</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="685"/>
        <source>New calculator (whole seq. masses)</source>
        <translation>Nouvelle calculatrice (masses de séquence entière)</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="688"/>
        <source>Start new calculator preseeded with whole sequence masses</source>
        <translation>Lancer une nouvelle calculatrice avec les masses de la séquence entière</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="697"/>
        <source>New calculator (selected seq. masses)</source>
        <translation>Nouvelle calculatrice (masses de séquence sélectionnée)</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="700"/>
        <source>Start new calculator preseeded with selected sequence masses</source>
        <translation>Lancer une nouvelle calculatrice avec les masses de la séquence sélectionnée</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="712"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="720"/>
        <source>&amp;Import</source>
        <translation>&amp;Import</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="729"/>
        <source>&amp;Edit</source>
        <translation>&amp;Éditer</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="737"/>
        <source>&amp;Chemistry</source>
        <translation>&amp;Chimie</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="751"/>
        <source>&amp;Calculator</source>
        <translation>&amp;Calculatrice</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="789"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1699"/>
        <source>Not accounting for cross-links</source>
        <translation>Pontages non pris en compte</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="849"/>
        <source>Ready.</source>
        <translation>Prêt.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="855"/>
        <source>%1@%2
Failed to populate the monomer code list.</source>
        <translation>%1@%2
Échec de la population de la liste des codes de monomère.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="986"/>
        <source>%1@%2
Filepath is empty, or file does not exist.</source>
        <translation>%1@%2
Le chemin de fichier est vide, ou le fichier n&apos;existe pas.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1003"/>
        <source>%1@%2
Failed to get the polymer chemistry definition name.</source>
        <translation>%1@%2
Nom de la définition de chimie de polymère non trouvé.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1019"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1106"/>
        <source>%1@%2
Failed to prepare the polymer chemistry definition.</source>
        <translation>%1@%2
Échec de la préparation de la définition de chimie de polymère.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1038"/>
        <source>%1@%2
Failed to load the polymer file.</source>
        <translation>%1@%2
Échec du chargement du fichier de polymère.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1055"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1126"/>
        <source>%1@%2
Failed to draw the polymer sequence.</source>
        <translation>%1@%2
Échec du dessin de la séquence de polymère.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1089"/>
        <source>%1@%2
Failed to find the corresponding polymer chemistry filename.</source>
        <translation>%1@%2
Nom de fichier de la chimie de polymère correspondante non trouvé.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1174"/>
        <source>%1@%2
Failed to get the polymer chemistry definition specification: %3.</source>
        <translation>%1@%2
Spécification de définition de chimie de polymère non trouvée: %3.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1197"/>
        <source>%1@%2
Failed to render the polymer chemistry definition xml file.</source>
        <translation>%1@%2
Échec du rendu du fichier xml de définition de chimie de polymère.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1220"/>
        <source>%1@%2
Failed to parse the monomer dictionary file.</source>
        <translation>%1@%2
Échec de la vérification du fichier dictionnaire de monomères.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1240"/>
        <source>%1@%2
Failed to parse the modification dictionary file.</source>
        <translation>%1@%2Échec de la vérification du fichier dictionnaire de modifications.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1260"/>
        <source>%1@%2
Failed to parse the cross-linker dictionary file.</source>
        <translation>%1@%2
Echec de l&apos;analyse du fichier dictionnaire des agents pontants.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1307"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1390"/>
        <source>Masses</source>
        <translation>Masses</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1311"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1394"/>
        <source>m/z ratios</source>
        <translation>ratios m/z</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1368"/>
        <source>Ionization rule formula is not valid.</source>
        <translation>La formule de la règle d&apos;ionisation est incorrecte.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1840"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1847"/>
        <source>%1 %2[*]</source>
        <translation>%1 %2[*]</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1841"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1848"/>
        <source>massXpert - Polymer Sequence Editor:</source>
        <translation>massXpert - Éditeur de Séquence de Polymère:</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1842"/>
        <source>Untitled</source>
        <translation>Sans-Titre</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="1869"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2068"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2113"/>
        <source>File save failed.</source>
        <translation>Échec de la sauvegarde du fichier.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2082"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2127"/>
        <source>File save succeeded.</source>
        <translation>Succès de la sauvegarde du fichier.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2099"/>
        <source>Save Polymer Sequence File</source>
        <translation>Sauvegarder le Fichier de Séquence de Polymère</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2101"/>
        <source>mXp files(*.mxp *.mXp *.MXP)</source>
        <translation>Fichiers mxp (*.mxp *.mXp* .MXP)</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2105"/>
        <source>Filepath is empty.</source>
        <translation>Le chemin de fichier est vide.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2148"/>
        <source>Import Raw Text File</source>
        <translation>Importer Fichier de Texte Brut</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2200"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2509"/>
        <source>Failed to import the sequence.</source>
        <translation>Échec de l&apos;import de séquence.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2225"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2234"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2271"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2448"/>
        <source>massXpert - Import PDB Protein Sequence</source>
        <translation>massXpert - Import de séquence de protéine PDB</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2227"/>
        <source>PDB files (*.PDB *.Pdb *.pdb)</source>
        <translation>Fichiers PDB (*.PDB *.Pdb *.pdb)</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2235"/>
        <source>File not found.</source>
        <translation>Fichier non trouvé.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2272"/>
        <source>Failed to load the dictionary.</source>
        <translation>Échec de la lecture du dictionnaire.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2449"/>
        <source>Failed to import the sequence(s).</source>
        <translation>Échec de l&apos;import de(s) séquence(s).</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2527"/>
        <source>Failed to import the sequence:
Number of chain sequences differs from number of chain IDs.</source>
        <translation>Échec de l&apos;import de séquence:
Le nombre de séquences de chaîne diffère du nombre d&apos;identificateurs de chaîne.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2701"/>
        <source>Failed to paste the sequence.</source>
        <translation>Échec du coller de séquence.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2838"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2847"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2877"/>
        <source>massxpert</source>
        <translation>massxpert</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2839"/>
        <source>No oligomer is selected. Select an oligomer first</source>
        <translation>Aucun oligomère sélectionné. Sélectionner un oligomère d&apos;abord</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2878"/>
        <source>Fragmentation calculations do not
take into account partial cross-links.
These partial cross-links are ignored.</source>
        <translation>Les calculs de fragmentation ne prennent
pas en compte les pontages covalents incomplets.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2994"/>
        <source>massXpert - pKa pH pI</source>
        <translation>massXpert - pKa pH pI</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="3083"/>
        <source>Incomplete cross-links: %1</source>
        <translation>Pontages incomplets: %1</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="3109"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="3120"/>
        <source>NOT_SET</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="676"/>
        <location filename="../gui/sequenceEditorWnd.cpp" line="679"/>
        <source>Decimal places</source>
        <translation>Décimales</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="748"/>
        <source>&amp;Options</source>
        <translation>&amp;Options</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2030"/>
        <source>The document %1 has been modified.
Do you want to save your changes?</source>
        <translation>Le document %1 a été modifié.
Sauver les modifications?</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2613"/>
        <source>Cut operations are not supported in multi-region selection mode.</source>
        <translation>Les opérations Couper ne sont pas supportées en mode de sélection multi-région.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2653"/>
        <source>Paste operations are not supported in multi-region selection mode.</source>
        <translation>Les opération Coller ne sont pas supportées en mode de sélection multi-région.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2848"/>
        <source>Fragmentation simulations are not supported
in multi-region selection mode.</source>
        <translation>Les simulations de fragmentation ne sont pas supportées
en mode de sélection multi-région.</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="480"/>
        <source>Data files(*.dat *.DAT)</source>
        <translation>Fichiers de données(*.dat *.DAT)</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2150"/>
        <source>Any file type(*)</source>
        <translation>Tout type de fichier(*)</translation>
    </message>
    <message>
        <location filename="../gui/sequenceEditorWnd.cpp" line="2995"/>
        <source>%1@%2
Failed to render xml file(%3).</source>
        <translation>%1@%2
Échec du rendu du fichier(%3).</translation>
    </message>
</context>
<context>
    <name>massXpert::SequenceImportDlg</name>
    <message>
        <location filename="../gui/sequenceImportDlg.cpp" line="99"/>
        <source>massXpert - Polymer Sequence Importer</source>
        <translation>massXpert - Import de séquences de polymères</translation>
    </message>
    <message>
        <location filename="../gui/sequenceImportDlg.cpp" line="100"/>
        <source>Failed to import the sequence, The number of chain IDs is different from the number of chain sequences.</source>
        <translation>Échec de l&apos;import de séquence, Le nombre d&apos;identificateurs de chaîne est différent du nombre de séquences de chaîne.</translation>
    </message>
</context>
<context>
    <name>massXpert::SequencePurificationDlg</name>
    <message>
        <location filename="../gui/sequencePurificationDlg.cpp" line="182"/>
        <source>Task already performed...</source>
        <translation>Tâche déjà effectuée...</translation>
    </message>
    <message>
        <location filename="../gui/sequencePurificationDlg.cpp" line="183"/>
        <location filename="../gui/sequencePurificationDlg.cpp" line="466"/>
        <source>Abort</source>
        <translation>Abandonner</translation>
    </message>
    <message>
        <location filename="../gui/sequencePurificationDlg.cpp" line="187"/>
        <location filename="../gui/sequencePurificationDlg.cpp" line="470"/>
        <source>Formatting progression</source>
        <translation>Pogression de mise en forme</translation>
    </message>
    <message>
        <location filename="../gui/sequencePurificationDlg.cpp" line="465"/>
        <source>Task remaining to perform...</source>
        <translation>Tâche restant à effectuer...</translation>
    </message>
</context>
<context>
    <name>massXpert::SpectrumCalculationDlg</name>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="342"/>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="450"/>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="534"/>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="556"/>
        <source>massXpert: Spectrum Calculator</source>
        <translation>massXpert : calculateur de spectre</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="343"/>
        <source>Failed to deep-copy atom list.</source>
        <translation>Échec de la copie-profonde de la liste d&apos;atomes.</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="374"/>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="389"/>
        <source>INPUT
=====

Formula: %1
Charge: %2
Mono Mass: %3 	 Avg mass: %4
Total number of atoms: %5	Total number of isotopes: %6

</source>
        <translation>ENTRÉE
=====

Formule: %1
Charge: %2
Masse mono: %3 	 Masse moy: %4
Nombre total d&apos;atomes: %5	Nombre total d&apos;isotopes: %6

</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="451"/>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="557"/>
        <source>Please, fix the minimum probability.</source>
        <translation>Veuillez régler la probabilité minimale.</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="535"/>
        <source>Please, fix the resolution or the FWHM.</source>
        <translation>Veuillez régler la résolution ou le FWHM.</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="738"/>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="756"/>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="2002"/>
        <source>Formula error</source>
        <translation>Erreur de formule</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="780"/>
        <source>Formula fine</source>
        <translation>Formule correcte</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="846"/>
        <source>Set a valid FWHM value</source>
        <translation>Régler une valeur de FWHM correcte</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="862"/>
        <source>Will use the resolution</source>
        <translation>Emploi de la résolution</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="937"/>
        <source>Fix the FWHM value, please</source>
        <translation>Veuillez corriger la valeur de FWHM</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="949"/>
        <source>Set a valid resolution value</source>
        <translation>Veuillez régler une valeur de résolution correcte</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="962"/>
        <source>Will use the FWHM</source>
        <translation>Emploi de FWHM</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1010"/>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1135"/>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1244"/>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1332"/>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1781"/>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1965"/>
        <source>massXpert - Spectrum Calculator</source>
        <translation>massXpert : calculateur de spectre</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1011"/>
        <source>Failed to fetch the mz ratio.</source>
        <translation>Échec de la lecture du ratio m/z.</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1136"/>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1333"/>
        <source>Please fix the input data first.</source>
        <translation>Veuillez d&apos;abord corriger les données en entrée.</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1155"/>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1163"/>
        <source>FWHM: %1 	 Max. peaks %2 	 Min. probability: %3

</source>
        <translation>FWHM: %1 	 Pics max. %2 	 Probabilité min.: %3

</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1245"/>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1782"/>
        <source>Failed to export the isotopic pattern data to file.</source>
        <translation>Échec de l&apos;export des données du profil isotopique vers le fichier.</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1274"/>
        <source>

Data have been written to file %1

</source>
        <translation>

Les données ont été écrites dans le fichier %1

</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1277"/>
        <source>

Also check the results tab

</source>
        <translation>

Consulter également l&apos;onglet des résultats

</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1303"/>
        <source>

Data have been written to the results tab

</source>
        <translation>

Les données ont été écrites dans l&apos;onglet des résultats

</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1391"/>
        <source>Sorting all oligomer-related sub-spectra...</source>
        <translation>Tri des sous-spectres de tous les oligomères...</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1489"/>
        <source>Computing sets of overlapping/non-overlapping sub-spectra...</source>
        <translation>Calcul des groupes de sub-spectres chevauchants/non-chevauchants...</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1615"/>
        <source>Creating final spectrum... </source>
        <translation>Création du spectre final...</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1933"/>
        <source>Simulating a spectrum with calculation of an isotopic cluster for each oligomer.

</source>
        <translation>Simulation d&apos;un spectre par calcul d&apos;un massif isotopique pour chaque oligomère.</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1938"/>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="2117"/>
        <source>There are %1 oligomers
Calculating sub-spectrum for each

</source>
        <translation>Il y a %1 oligomères
Calcul d&apos;un sub-spectre pour chacun d&apos;eux</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1966"/>
        <source>Elemental composition not found for current oligomer.
 Uncheck the Isotopic cluster checkbox for this calculation.</source>
        <translation>Composition élémentale non trouvée pour l&apos;oligomère courant.
Décocher la case Massif isotopique pour ce calcul.</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1979"/>
        <source>Computing isotopic cluster for oligomer %1
	formula: %2.
</source>
        <translation>Calcul du massif isotopique pour l&apos;oligomère %1
	formule: %2.</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="1997"/>
        <source> Validating formula... </source>
        <translation> Validation de la formule...</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="2004"/>
        <source> Failure.

</source>
        <translation> Échec.</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="2011"/>
        <source>Success.
</source>
        <translation>Succès.</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="2032"/>
        <source>	mono m/z: %1
	charge: %2
	fwhm: %3
	increment: %4

</source>
        <translation>	m/z mono: %1
	charge: %2
	fwhm: %3
	incrément: %4

</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="2072"/>
        <source>		Done computing the cluster

</source>
        <translation>		Calcul du massif achevé

</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="2094"/>
        <source>Done computing *all* the clusters

</source>
        <translation>Calcul de tous les massifs achevé

</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="2112"/>
        <source>Simulating a spectrum without calculation of isotopic clusters for each oligomer.

</source>
        <translation>Simulation d&apos;un spectre sans calcul des massifs isotopiques de chaque oligomère.

</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="2158"/>
        <source>Computing peak shape for oligomer %1
</source>
        <translation>Calcul de la forme du pic pour l&apos;oligomère %1</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="2163"/>
        <source>	mono m/z: %1
</source>
        <translation>	m/z mono: %1</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="2167"/>
        <source>	avg m/z: %1
</source>
        <translation>	m/z moy: %1</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="2171"/>
        <source>	charge: %2
	fwhm: %3
	increment: %4

</source>
        <translation>	charge: %2
	fwhm: %3
	incrément: %4

</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="2205"/>
        <source>		Done computing the peak shape

</source>
        <translation>		Calcul de la forme du pic achevé</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="2227"/>
        <source>Done computing *all* the peak shapes

</source>
        <translation>Calcul de la forme de *tous* les pics achevé

</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="2269"/>
        <source>Export Raw Text File</source>
        <translation>Exporter Fichier Texte Brut</translation>
    </message>
    <message>
        <location filename="../gui/spectrumCalculationDlg.cpp" line="2271"/>
        <source>Any file type(*)</source>
        <translation>Tout type de fichier(*)</translation>
    </message>
</context>
</TS>
