/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// Local includes
#include "fragmentOligomerTableViewModel.hpp"
#include "fragmentOligomerTableViewSortProxyModel.hpp"
#include "application.hpp"


namespace massXpert
{

  FragmentOligomerTableViewSortProxyModel::
  FragmentOligomerTableViewSortProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
  {
  }


  FragmentOligomerTableViewSortProxyModel::
  ~FragmentOligomerTableViewSortProxyModel()
  {
  }


  void  
  FragmentOligomerTableViewSortProxyModel::setPatternFilter(QString value)
  {
    m_patternFilter = value;
  }


  void  
  FragmentOligomerTableViewSortProxyModel::setMonoFilter(double value)
  {
    m_monoFilter = value;
  }


  void  
  FragmentOligomerTableViewSortProxyModel::setAvgFilter(double value)
  {
    m_avgFilter = value;
  }


  void  
  FragmentOligomerTableViewSortProxyModel::setTolerance(double value)
  {
    m_tolerance = value;
  }


  void  
  FragmentOligomerTableViewSortProxyModel::setChargeFilter(int value)
  {
    m_chargeFilter = value;
  }


  bool 
  FragmentOligomerTableViewSortProxyModel::lessThan(const QModelIndex &left, 
						    const QModelIndex &right) 
    const
  {
    Application *application = static_cast<Application *>(qApp);
    QLocale locale = application->locale();

    QVariant leftData = sourceModel()->data(left);
    QVariant rightData = sourceModel()->data(right);
  
    if (leftData.type() == QVariant::Int)
      {
	// The Partial column
      
	int leftValue = leftData.toInt();
	int rightValue = rightData.toInt();

	return(leftValue < rightValue);
      }
    if (leftData.type() == QVariant::Bool)
      {
	// The Modif column
      
	int leftValue = leftData.toInt();
	int rightValue = rightData.toInt();

	return(leftValue < rightValue);
      }
    else if (leftData.type() == QVariant::String && 
	    (left.column() == FRAGMENT_OLIGO_MONO_COLUMN ||
	      left.column() == FRAGMENT_OLIGO_AVG_COLUMN))
      {
	bool ok = false;
      
	double leftValue = locale.toDouble(leftData.toString(), &ok);
	Q_ASSERT(ok);
	double rightValue = locale.toDouble(rightData.toString(), &ok);
	Q_ASSERT(ok);
      
	return(leftValue < rightValue);
      }
  
    if (leftData.type() == QVariant::String)
      {
	QString leftString = leftData.toString();
	QString rightString = rightData.toString();
      
	if(!leftString.isEmpty() && leftString.at(0) == '[')
	  return sortCoordinates(leftString, rightString);
	else if (!leftString.isEmpty() && leftString.contains('#'))
	  return sortName(leftString, rightString);
	else
	  return(QString::localeAwareCompare(leftString, rightString) < 0);
      }
    
    return true;
  }


  bool
  FragmentOligomerTableViewSortProxyModel::sortCoordinates(QString left,
							   QString right) const
  {
    QString local = left;

    local.remove(0, 1);
    local.remove(local.size() - 1, 1);
   
    int dash = local.indexOf('-');
   
    QString leftStartStr = local.left(dash);
   
    bool ok = false;
    int leftStartInt = leftStartStr.toInt(&ok);
   
    local.remove(0, dash + 1);
   
    QString leftEndStr = local;
   
    ok = false;
    int leftEndInt = leftEndStr.toInt(&ok);

    //    qDebug() << "left coordinates" << leftStartInt << "--" << leftEndInt;
   
    local = right;

    local.remove(0, 1);
    local.remove(local.size() - 1, 1);
   
    dash = local.indexOf('-');
   
    QString rightStartStr = local.left(dash);
   
    ok = false;
    int rightStartInt = rightStartStr.toInt(&ok);
   
    local.remove(0, dash + 1);
   
    QString rightEndStr = local;
   
    ok = false;
    int rightEndInt = rightEndStr.toInt(&ok);

    //    qDebug() << "right coordinates" << rightStartInt << "--" << rightEndInt;


    if (leftStartInt < rightStartInt)
      return true;
   
    if (leftEndInt < rightEndInt)
      return true;
   
    return false;
  }


  bool
  FragmentOligomerTableViewSortProxyModel::sortName(QString left,
						    QString right) const
  {
    // The format is this way:
    //
    // <fragspecname>#<numb>#(<fragrulename>)#z=<charge> if there is a
    // fragrule that has been taken into account or
    //
    //<fragspecname>#<numb>#z=<charge> if no fragrule has been taken
    //into account.
    

    // left QString.
    //
    QString local = left;

    QStringList leftList = local.split("#", QString::SkipEmptyParts);

    // First item is necessarily <fragspecname>, second item is
    // nessarily <numb> and last item is necessarily z=<charge>.

    QString leftFragSpecName = leftList.first();
    
    bool ok = false;
    int leftNumberInt = leftList.at(1).toInt(&ok);

    QString leftChargeString = leftList.last().section('=', 1, 1);
    
    ok = false;
    int leftChargeInt = leftChargeString.toInt(&ok);

    // And now check if there was a fragmentation rule.
    QString leftFragRuleName;
    
    if (leftList.size() == 4)
      {
	leftFragRuleName = leftList.at(2);
      }
    
    // right QString.
    //
    local = right;

    QStringList rightList = local.split("#", QString::SkipEmptyParts);

    // First item is necessarily <fragspecname>, second item is
    // nessarily <numb> and last item is necessarily z=<charge>.

    QString rightFragSpecName = rightList.first();
    
    ok = false;
    int rightNumberInt = rightList.at(1).toInt(&ok);

    QString rightChargeString = rightList.last().section('=', 1, 1);
    
    ok = false;
    int rightChargeInt = rightChargeString.toInt(&ok);

    // And now check if there was a fragmentation rule.
    QString rightFragRuleName;
    
    if (rightList.size() == 4)
      {
	rightFragRuleName = rightList.at(2);
      }
    
    // Test first the fragmentation name string.
    if (QString::localeAwareCompare(leftFragSpecName, 
				    rightFragSpecName) < 0)
      {
	return true;
      }
    else if (QString::localeAwareCompare(leftFragSpecName, 
					 rightFragSpecName) == 0)
      {
	if (leftNumberInt < rightNumberInt)
	  {
	    return true;
	  }
	else if (leftNumberInt == rightNumberInt)
	  {
	    if (QString::localeAwareCompare(leftFragRuleName, 
					    rightFragRuleName) < 0)
	      {
		return true;
	      }
	    else if (QString::localeAwareCompare(leftFragRuleName, 
						 rightFragRuleName) == 0)
	      {
		if (leftChargeInt < rightChargeInt)
		  {
		    return true;
		  }
	      }
	  }
      }
       
    return false;
  }


  bool 
  FragmentOligomerTableViewSortProxyModel::filterAcceptsRow 
 (int sourceRow, const QModelIndex & sourceParent) const 
  {
    Application *application = static_cast<Application *>(qApp);
    QLocale locale = application->locale();
    
   if (filterKeyColumn() == FRAGMENT_OLIGO_PATTERN_COLUMN)
      {
	QModelIndex index = sourceModel()->index(sourceRow, 
						   FRAGMENT_OLIGO_PATTERN_COLUMN,
						   sourceParent);

	QString pattern = sourceModel()->data(index).toString();
      
	if(m_patternFilter == pattern)
	  return true;
	else
	  return false;
      }
    else if (filterKeyColumn() == FRAGMENT_OLIGO_MONO_COLUMN)
      {
	QModelIndex index = sourceModel()->index(sourceRow, 
						   FRAGMENT_OLIGO_MONO_COLUMN, 
						   sourceParent);
	bool ok = false;
	
	QVariant data = sourceModel()->data(index);
	
	double mass = locale.toDouble(data.toString(), &ok);
	Q_ASSERT(ok);
	
	double left = m_monoFilter - m_tolerance;
	double right = m_monoFilter + m_tolerance;
      
	if(left <= mass && mass <= right)
	  return true;
	else 
	  return false;
      }
    else if (filterKeyColumn() == FRAGMENT_OLIGO_AVG_COLUMN)
      {
	QModelIndex index = sourceModel()->index(sourceRow, 
						   FRAGMENT_OLIGO_AVG_COLUMN,
						   sourceParent);
	bool ok = false;
	
	QVariant data = sourceModel()->data(index);
	
	double mass = locale.toDouble(data.toString(), &ok);
	Q_ASSERT(ok);
      
	double left = m_avgFilter - m_tolerance;
	double right = m_avgFilter + m_tolerance;
      
	if(left <= mass && mass <= right)
	  return true;
	else
	  return false;
      }
    else if (filterKeyColumn() == FRAGMENT_OLIGO_CHARGE_COLUMN)
      {
	// The column of the charge.
	QModelIndex index = sourceModel()->index(sourceRow, 5, sourceParent);
	int charge = sourceModel()->data(index).toInt();
      
	if(m_chargeFilter == charge)
	  return true;
	else
	  return false;
      }
  
    return QSortFilterProxyModel::filterAcceptsRow(sourceRow, sourceParent);
  }



  void 
  FragmentOligomerTableViewSortProxyModel::applyNewFilter()
  {
    invalidateFilter();
  }

} // namespace massXpert
