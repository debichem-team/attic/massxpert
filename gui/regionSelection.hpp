/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.filomace.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef REGION_SELECTION_HPP
#define REGION_SELECTION_HPP


/////////////////////// Qt includes
#include <QList>
#include <QRect>
#include <QGraphicsRectItem>
#include <QPointF>


/////////////////////// Local includes


namespace massXpert
{

  class SequenceEditorGraphicsView;
  class Coordinates;
  class Monomer;
  
  class RegionSelection
  {
  private:
    SequenceEditorGraphicsView *mp_view;
    const int m_rectangleCount;
    
    QList<QRectF *> m_rectangleList;
    QList<QGraphicsRectItem *> m_markList;
    
    const Monomer *mp_startMonomer;
    const Monomer *mp_endMonomer;
    
    QPointF m_startPoint;
    QPointF m_endPoint;
    
    int m_startIndex;
    int m_endIndex;

    void setCoordinates(int, int);
    void setCoordinates(const QPointF &, const QPointF &);
    int drawMark();
    int eraseMark();
    
  public:
    RegionSelection(SequenceEditorGraphicsView *);
    
    ~RegionSelection(void);
    
    SequenceEditorGraphicsView *view();

    const Monomer *startMonomer() const;
    const Monomer *endMonomer() const;
    
    int startIndex() const;
    int endIndex() const;
        
    int redrawMark();
    int drawMark(int, int);
    int drawMark(const QPointF &, const QPointF &);

    bool encompassing(int = -1, int = -1);

    Coordinates *coordinates();
    
    void debugSelectionIndicesPutStdErr();
  };
      
} // namespace massXpert

#endif // REGION_SELECTION_HPP
