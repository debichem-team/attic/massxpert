/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Qt includes
#include <QMessageBox>


/////////////////////// Local includes
#include "application.hpp"
#include "sequenceEditorFindDlg.hpp"


namespace massXpert
{

  class SequenceEditorWnd;
  class SequenceEditorGraphicsView;


  SequenceEditorFindDlg::SequenceEditorFindDlg(QWidget *parent,
						Polymer *polymer)
    : QDialog(parent), mp_polymer(polymer)
  {
    Q_ASSERT(parent);
    Q_ASSERT(polymer);
  
    m_ui.setupUi(this);

    mp_editorWnd = static_cast<SequenceEditorWnd *>(parent);
  
    m_reprocessMotif = false;
    
    // Allocate the motif that we'll be using throughout the life cycle
    // of this dialog window.
    mpa_motif = new Sequence();
  
    QSettings settings 
     (static_cast<Application *>(qApp)->configSettingsFilePath(), 
       QSettings::IniFormat);
  
    settings.beginGroup("sequence_editor_find_dlg");

    restoreGeometry(settings.value("geometry").toByteArray());
  
    m_history = settings.value("history").toStringList();
  
    settings.endGroup();

    m_ui.motifToFindComboBox->addItems(m_history);
    m_ui.motifToFindComboBox->insertItem(0, "");
    m_ui.motifToFindComboBox->setCurrentIndex(0);
  
  
    
    connect(m_ui.findPushButton,
	     SIGNAL(clicked()),
	     this,
	     SLOT(find()));
  
    connect(m_ui.nextPushButton,
	     SIGNAL(clicked()),
	     this,
	     SLOT(next()));

    connect(m_ui.motifToFindComboBox,
	     SIGNAL(editTextChanged(const QString &)),
	     this,
	     SLOT(motifComboBoxEditTextChanged(const QString &)));
  
    connect(m_ui.clearHistoryPushButton,
	     SIGNAL(clicked()),
	     this,
	     SLOT(clearHistory()));
  }


  SequenceEditorFindDlg::~SequenceEditorFindDlg()
  {
    delete mpa_motif;
  }


  void 
  SequenceEditorFindDlg::closeEvent(QCloseEvent *event)
  {
    if (event)
      printf("%s", "");
  
    QSettings settings 
     (static_cast<Application *>(qApp)->configSettingsFilePath(), 
       QSettings::IniFormat);
  
    settings.beginGroup("sequence_editor_find_dlg");
  
    settings.setValue("geometry", saveGeometry());
  
    settings.setValue("history", m_history);
    
    settings.endGroup();
  }


  SequenceEditorWnd *
  SequenceEditorFindDlg::editorWnd()
  {
    return mp_editorWnd;
  }


  void
  SequenceEditorFindDlg::find()
  {
    // First we should validate the sequence to search. Is it correct ?

    // Create a sequence in which we'll set the text of the line edit
    // and we'll ask to make a monomer list with it.

    QString text = m_ui.motifToFindComboBox->currentText();
  
    mpa_motif->setMonomerText(text);
  
    if (mpa_motif->makeMonomerList(mp_polymer->polChemDef(), true) == -1)
      {
	QMessageBox::warning(this,
			      tr("massXpert - Find Sequence"),
			      tr("%1@%2\n Failed to validate motif: %3")
			      .arg(__FILE__)
			      .arg(__LINE__)
			      .arg(text)); 
	return;
      }
  
    // At this point we have a valid motif.

    // Prepend that motif to the history and to the comboBox.
    if (!m_history.contains(text))
      {
	m_history.prepend(text);
	m_ui.motifToFindComboBox->insertItem(0 + 1, text);
      }
  
    // And now search for that motif in the polymer sequence.
  
    m_startSearchIndex = 0;
  
    if (mp_polymer->findForwardMotif(mpa_motif, mp_polymer->polChemDef(),
				      &m_startSearchIndex) == 1)
      {
	// A motif was successfully found, index now contains the value
	// corresponding to the index of the first monomer in the
	// polymer sequence that matched the sequence. We should
	// highlight the sequence that was found, between
	// [m_startSearchIndex and m_startSearchIndex + motif->size - 1]
      
	// For example, polymer is:

	// MAMISGMSGRKAS

	// and motif is SGR

	// m_startSearchIndex is 4(fifth monomer) and thus the end index
	// is [4 + 3 -1] that is 6.

	// Set the selection to that interval in the polymer sequence
	// editor graphics view.

	m_endSearchIndex = m_startSearchIndex + mpa_motif->size() - 1;
      
	mp_editorWnd->mpa_editorGraphicsView->
	  setSelection(m_startSearchIndex, m_endSearchIndex,
			false, false);
      
	// Before returning, increment the m_startSearchIndex by one
	// unit, so that when the user clicks next, we'll not fall on
	// the same polymer sequence.
      
	++m_startSearchIndex;
      
	m_reprocessMotif = false;
      
	return;
      }
    else
      QMessageBox::warning(this,
			    tr("massXpert - Find Sequence"),
			    tr("Failed to find sequence motif: %1")
			    .arg(text));
  }


  void
  SequenceEditorFindDlg::next()
  {
    QString text = m_ui.motifToFindComboBox->currentText();
  
    // Only reprocess the motif if required.
    if (m_reprocessMotif)
      {
	// First we should validate the sequence to search. Is it correct ?
      
	// Create a sequence in which we'll set the text of the line edit
	// and we'll ask to make a monomer list with it.
      
	mpa_motif->setMonomerText(text);
      
	if(mpa_motif->makeMonomerList(mp_polymer->polChemDef(), true) == -1)
	  {
	    QMessageBox::warning(this,
				  tr("massXpert - Find Sequence"),
				  tr("%1@%2\n Failed to validate motif: %3")
				  .arg(__FILE__)
				  .arg(__LINE__)
				  .arg(text)); 
	    return;
	  }
      }
  
    // At this point, we can perform the search, at the last index value
    // set by last find() call.

    if (mp_polymer->findForwardMotif(mpa_motif, 
				      mp_polymer->polChemDef(),
				      &m_startSearchIndex) == 1)
      {
	// A motif was successfully found, index now contains the value
	// corresponding to the index of the first monomer in the
	// polymer sequence that matched the sequence. We should
	// highlight the sequence that was found, between
	// [m_startSearchIndex and m_startSearchIndex + motif->size - 1]
      
	// For example, polymer is:

	// MAMISGMSGRKAS

	// and motif is SGR

	// m_startSearchIndex is 4(fifth monomer) and thus the end index
	// is [4 + 3 -1] that is 6.

	// Set the selection to that interval in the polymer sequence
	// editor graphics view.

	m_endSearchIndex = m_startSearchIndex + mpa_motif->size() - 1;
      
	mp_editorWnd->mpa_editorGraphicsView->
	  setSelection(m_startSearchIndex, m_endSearchIndex,
			false, false);
      
	// Before returning, increment the m_startSearchIndex by one
	// unit, so that when the user clicks next, we'll not fall on
	// the same polymer sequence.
      
	++m_startSearchIndex;

	return;
      }
    else
      QMessageBox::warning(this,
			    tr("massXpert - Find Sequence"),
			    tr("Failed to find sequence motif: %1")
			    .arg(text)); 

  }


  void 
  SequenceEditorFindDlg::motifComboBoxEditTextChanged(const QString &text)
  {
    m_reprocessMotif = true;
  }


  void
  SequenceEditorFindDlg::clearHistory()
  {
    m_history.clear();
  
    m_ui.motifToFindComboBox->clear();
  
    QSettings settings 
     (static_cast<Application *>(qApp)->configSettingsFilePath(), 
       QSettings::IniFormat);
  
    settings.beginGroup("sequence_editor_find_dlg");
  
    settings.setValue("history", m_history);
  
    settings.endGroup();
  }

} // namespace massXpert
