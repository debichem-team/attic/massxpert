/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef MZ_LAB_OUTPUT_OLIGOMER_TABLE_VIEW_MODEL_HPP
#define MZ_LAB_OUTPUT_OLIGOMER_TABLE_VIEW_MODEL_HPP


/////////////////////// Qt includes
#include <QTableView>


/////////////////////// Local includes
#include "oligomer.hpp"
#include "oligomerList.hpp"
#include "mzLabOutputOligomerTableView.hpp"
#include "mzLabWnd.hpp"


namespace massXpert
{


  enum
    {
      MZ_LAB_OUTPUT_OLIGO_1_MASS_COLUMN,
      MZ_LAB_OUTPUT_OLIGO_1_CHARGE_COLUMN,
      MZ_LAB_OUTPUT_OLIGO_2_MASS_COLUMN,
      MZ_LAB_OUTPUT_OLIGO_2_CHARGE_COLUMN,
      MZ_LAB_OUTPUT_OLIGO_ERROR_COLUMN,
      MZ_LAB_OUTPUT_OLIGO_MATCH_COLUMN,
      MZ_LAB_OUTPUT_OLIGO_TOTAL_COLUMNS,
    };
  

  class MzLabWnd;
  

  class MzLabOutputOligomerTableViewModel : public QAbstractTableModel
  {
    Q_OBJECT
  
    private:
    QList<OligomerPair *> *mp_oligomerPairList;
    
    MzLabOutputOligomerTableView *mp_tableView;
    
    MzLabWnd *mp_mzLabWnd;
    MzLabOutputOligomerTableViewDlg *mp_parentDlg;
    
    // Fixme: does not this belong to the dialog? Maybe a pointer to
    // the other one should be better.
    MassType m_massType;
    
  public:
    MzLabOutputOligomerTableViewModel(QList <OligomerPair *> *, QObject *);
    ~MzLabOutputOligomerTableViewModel();
  
    void setParentDlg(MzLabOutputOligomerTableViewDlg *);
    MzLabOutputOligomerTableViewDlg *parentDlg();
    
    void setMzLabWnd(MzLabWnd *);
    MzLabWnd *mzLabWnd();
    
    void setTableView(MzLabOutputOligomerTableView *);
    MzLabOutputOligomerTableView *tableView();
  
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    QVariant headerData(int, Qt::Orientation, 
                        int role = Qt::DisplayRole) const;
    
    bool setData(const QModelIndex &, const QVariant &,
                 int role = Qt::EditRole);
    
    QVariant data(const QModelIndex &parent = QModelIndex(), 
                  int role = Qt::DisplayRole) const;

    void addOligomerPair(OligomerPair *);
  };

} // namespace massXpert


#endif // MZ_LAB_OUTPUT_OLIGOMER_TABLE_VIEW_MODEL_HPP
