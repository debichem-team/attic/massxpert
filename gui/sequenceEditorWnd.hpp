/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef SEQUENCE_EDITOR_WND_HPP
#define SEQUENCE_EDITOR_WND_HPP

/////////////////////// Qt includes
#include <QMainWindow>
#include <QGraphicsScene>
#include <QClipboard>
#include <QProgressBar>


/////////////////////// Local includes
#include "ui_sequenceEditorWnd.h"
#include "calcOptions.hpp"
#include "polymer.hpp"
#include "ionizeRule.hpp"
#include "polChemDef.hpp"
#include "monomerCodeEvaluator.hpp"
#include "sequenceEditorGraphicsView.hpp"
#include "sequenceEditorFindDlg.hpp"


namespace massXpert
{

  class SequenceEditorGraphicsView;


  class SequenceEditorWnd : public QMainWindow
  {
    Q_OBJECT
  
    private:
    Ui::SequenceEditorWnd m_ui;
  
    // The results-exporting strings. ////////////////////////////////
    QString *mpa_resultsString;
    QString m_resultsFilePath;
    //////////////////////////////////// The results-exporting strings.

    QMenu *fileMenu;
    QMenu *fileImportMenu;
    QMenu *editMenu;
    QMenu *chemistryMenu;
    QMenu *optionsMenu;
    QMenu *calculatorMenu;
    
    QAction *closeAct;
    QAction *saveAct;
    QAction *saveAsAct;
    QAction *importRawAct;
    QAction *importPdbProtAct;
    QAction *exportClipboardAct;
    QAction *exportFileAct;
    QAction *exportSelectFileAct;
    
    QAction *clipboardCopyAct;
    QAction *clipboardCutAct;
    QAction *clipboardPasteAct;
    QAction *findSequenceAct;
  
    QAction *modifMonomerAct;
    QAction *modifPolymerAct;
    QAction *crossLinkMonomersAct;
    QAction *cleaveAct;
    QAction *fragmentAct;
    QAction *massSearchAct;
    QAction *mzCalculationAct;
    QAction *compositionsAct;
    QAction *pkaPhPiAct;
    
    QAction *decimalPlacesOptionsAct;

    QAction *newCalculatorWholeSequenceMassesAct;
    QAction *newCalculatorSelectedSequenceMassesAct;

    // Allocated but reparented to QStatusBar.
    QProgressBar *mp_progressBar;
  
    Polymer *mpa_polymer;
    PolChemDef *mp_polChemDef;
    CalcOptions m_calcOptions;
    IonizeRule m_ionizeRule;

    QList<Prop *> m_propList;

    void writeSettings();
    void readSettings();

    void closeEvent(QCloseEvent *event);
    void focusInEvent(QFocusEvent *event);
    void focusOutEvent(QFocusEvent *event);
  
    void createActions();
    void createMenus();

    void keyPressEvent(QKeyEvent *);

    // The results-exporting functions. ////////////////////////////////
    void prepareResultsTxtString();
    //////////////////////////////////// The results-exporting functions.

  public:
    SequenceEditorWnd();
    ~SequenceEditorWnd();

    bool m_forciblyClose;
    bool m_noDelistWnd;
    bool m_postInitialized;
  
  
    SequenceEditorGraphicsView *mpa_editorGraphicsView;
    QGraphicsScene *mpa_editorGraphicsScene;


    // Before the creation of the polChemDef/polymer relationship. 
    bool preInitialize();
    // Creates the polChemDef/polymer relationship. 
    bool postInitialize();
  
    QProgressBar *progressBar();
  
    bool openSequence(QString &);
    bool newSequence(QString &);
    bool maybeSave();
  
    void updateMonomerPosition(int);
  
    PolChemDef *preparePolChemDef(const QString &);
    void populateCalculationOptions();
    bool populateMonomerCodeList(bool = false);

    bool readFile(const QString &);
  
    const PolChemDef *polChemDef() const;
    PolChemDef *polChemDef();
    
    Polymer *polymer();
    QList<Prop *> *propList();

    const CalcOptions &calcOptions() const;
    const IonizeRule& ionizeRule() const;
    
    void clearCompletionsListSelection();
    void completionsListSelectAt(int);

    void setWindowModified(bool);

    void updateWindowTitle();

    void getsFocus();
  
    void updateWholeSequenceMasses(bool = false);

    void wholeSequenceMasses(double * = 0, double * = 0);
    void selectedSequenceMasses(double * = 0, double * = 0);
    
    void updateSelectedSequenceMasses(bool = false);

    void updatePolymerEndsModifs();

    void newCalculator(QString, QString);
    
  signals:
    void polymerSequenceModifiedSignal();
				  
  public slots:
    bool save();
    bool saveAs();

    // Sequence importers.
    void importRaw();
    void importPdbProt();

    void exportClipboard();
    void exportFile();
    bool exportSelectFile();
    
    void clipboardCopy(QClipboard::Mode = QClipboard::Clipboard);
    void clipboardCut(QClipboard::Mode = QClipboard::Clipboard);
    void clipboardPaste(QClipboard::Mode = QClipboard::Clipboard);
    void clipboardClear(QClipboard::Mode);
    void findSequence();
  
    void vignetteSizeChanged();
    void nameLineEditChanged(const QString & text);

    void calculationOptionsChanged();

    void leftModifOptionsChanged();
    void forceLeftModifOptionsChanged();
    void rightModifOptionsChanged();
    void forceRightModifOptionsChanged();
    
    void multiRegionSelectionOptionChanged(int);
    bool isMultiRegionSelection();

    void regionSelectionOligomerOptionChanged(bool);
    void regionSelectionResChainOptionChanged(bool);
    
    int coordinatesManuallyEdited(const QString &);
    
    void multiSelectionRegionOptionChanged(int);
    bool isMultiSelectionRegion();

    void monomerModifOptionChanged(int);

    void monomerCrossLinkOptionChanged(int);
     
    void modifMonomer();

    void modifPolymer();
    void modifLeftEnd();
    void modifRightEnd();

    void crossLinkMonomers();
  
    void cleave();
    void fragment();
    void massSearch();
    void mzCalculation();
    void compositions();
    void pkaPhPi();

    void decimalPlacesOptions();

    void newCalculatorWholeSequenceMasses();
    void newCalculatorSelectedSequenceMasses();
    
    void crossLinksPartiallyEncompassedSlot(int);

    void vignetteListWidgetItemDoubleClicked(QListWidgetItem *);
  };

} // namespace massXpert


#endif // EDITOR_WND_HPP
