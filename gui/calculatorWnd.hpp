/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef CALCULATOR_WND_HPP
#define CALCULATOR_WND_HPP


/////////////////////// Qt includes
#include<QMainWindow>


/////////////////////// Local includes
#include "ui_calculatorWnd.h"
#include "ponderable.hpp"
#include "polChemDef.hpp"
#include "calculatorRecorderDlg.hpp"
#include "calculatorChemPadDlg.hpp"

  
namespace massXpert
{

  enum MxpFormulaHandling
    {
      MXP_FORMULA_HANDLING_IMMEDIATE = 1 << 0,
      MXP_FORMULA_HANDLING_PRINT = 1 << 1,
      MXP_FORMULA_HANDLING_WITH_SPACE = 1 << 2,
      MXP_FORMULA_HANDLING_PRINT_WITH_SPACE =
      (MXP_FORMULA_HANDLING_PRINT | MXP_FORMULA_HANDLING_WITH_SPACE),
    };
  
  
  class CalculatorWnd : public QMainWindow
  {
    Q_OBJECT
  
    private:
    Ui::CalculatorWnd m_ui;
  
    Ponderable m_seedPonderable;
    Ponderable m_tempPonderable;
    Ponderable m_resultPonderable;

    CalculatorRecorderDlg m_recorderDlg;
    CalculatorChemPadDlg m_chemPadDlg;
    
    PolChemDef m_polChemDef;
  
    void closeEvent(QCloseEvent *event);
  
  public:
    bool m_forciblyClose;
    bool m_noDelistWnd;

    int m_formulaHandling;
  
    CalculatorWnd(QString &, 
                  const QString & = QString(),
                  const QString & = QString());
    ~CalculatorWnd();

    bool initialize();
  
    const PolChemDef &polChemDef();
    QString polChemDefName();
      
    void updateWindowTitle();
    bool populatePolChemDefComboBoxes();

    bool setupChemicalPad();
  
    void recordResult();
    void updateSeedResultLineEdits();
  
    void recorderDlgClosed();
    void chemPadDlgClosed();

    int accountFormula(const QString & = QString(""), int = 1);
    int accountMonomer();
    int accountModif();
    int accountSequence();

    void setFormulaHandling(int);

    void addFormulaToMemory();
    void removeFormulaFromMemory();
    void clearWholeMemory();
    QString simplifyFormula();
			  
  public slots:
    void addToResult();
    void sendToResult();
    void removeFromResult();
    void clearSeed();
    void addToSeed();
    void sendToSeed();
    void removeFromSeed();
    void clearResult();
    void showRecorder(int);

    void formulaActionsComboBoxActivated(int);
    
    void showChemPad(int);

    void apply(const QString & = QString(""));

    void mzCalculation();  
    void isotopicPatternCalculation();
  };

} // namespace massXpert


#endif // CALCULATOR_WND_HPP
