/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/

/////////////////////// Qt includes
#include<QDebug>
#include<QMessageBox>
#include <QKeyEvent>


/////////////////////// Local includes
#include "chemPadButton.hpp"
#include "calculatorWnd.hpp"

namespace massXpert
{

  ChemPadButton::ChemPadButton(const QString & text,
			       const QString &formula,
			       QWidget *calculator_wnd,
			       QWidget *parent,
			       const QColor &backgroundColor,
			       const QColor &foregroundColor)
    : QPushButton(text, parent)
  {
    Q_ASSERT(calculator_wnd);
  
    mp_calculatorWnd = static_cast<CalculatorWnd *>(calculator_wnd);
    m_formula = formula;
    
    if (backgroundColor.isValid() && foregroundColor.isValid())
      {
	QString bgColorString = QString ("rgb(%1,%2,%3)")
	  .arg(backgroundColor.red())
	  .arg(backgroundColor.green())
	  .arg(backgroundColor.blue());
	
	QString fgColorString = QString ("rgb(%1,%2,%3)")
	  .arg(foregroundColor.red())
	  .arg(foregroundColor.green())
	  .arg(foregroundColor.blue());

	setStyleSheet(QString ("QPushButton {background: %1 ; color: %2}")
		      .arg(bgColorString)
		      .arg(fgColorString));
      }

    connect(this,
	    SIGNAL(clicked()),
	    this,
	    SLOT(clicked()));
  }
  
  
  ChemPadButton::~ChemPadButton()
  {
    
  }


  void 
  ChemPadButton::clicked()
  {
    // We have to account for the formula corresponding to this button.
     
    return mp_calculatorWnd->apply(m_formula);
  }


} // namespace massXpert
