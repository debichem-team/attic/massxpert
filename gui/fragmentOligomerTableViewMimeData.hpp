/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef FRAGMENT_OLIGOMER_TABLE_VIEW_MIME_DATA_HPP
#define FRAGMENT_OLIGOMER_TABLE_VIEW_MIME_DATA_HPP


/////////////////////// Qt includes
#include <QMimeData>


/////////////////////// Local includes
#include "fragmentOligomerTableView.hpp"
#include "sequenceEditorWnd.hpp"
#include "fragmentationDlg.hpp"


namespace massXpert
{
  
  class FragmentOligomerTableViewMimeData : public QMimeData
  {
    Q_OBJECT
    
    private:
    const FragmentOligomerTableView *mp_tableView;
    SequenceEditorWnd *mp_editorWnd;
    FragmentationDlg *mp_fragmentationDlg;
    
    QStringList m_formats;
    

    QString selectionAsPlainText() const;
    
  protected:
    QVariant retrieveData(const QString &, QVariant::Type ) const;
    
  public:
    FragmentOligomerTableViewMimeData(const FragmentOligomerTableView *,
                                      SequenceEditorWnd *,
                                      FragmentationDlg *);
    ~FragmentOligomerTableViewMimeData();
    
    const FragmentOligomerTableView *tableView() const;
    QStringList formats() const;
  };
  
} // namespace massXpert


#endif // FRAGMENT_OLIGOMER_TABLE_VIEW_MIME_DATA_HPP
