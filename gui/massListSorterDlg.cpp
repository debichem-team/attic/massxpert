/* massXpert - the true massist's program.
   --------------------------------------
   Copyright (C) 2006,2007 Filippo Rusconi

   http://www.filomace.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package (see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique (FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.


   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include<QtGui>
#include<QtGlobal>
#include<QLocale>
#include<QVBoxLayout>
#include<QLabel>
#include<QGroupBox>
#include<QObject>
#include<QTextEdit>
#include<QComboBox>
#include<QPushButton>
#include<QMainWindow>

#include "massListSorterDlg.hpp"
#include "massList.hpp"

using massXpert::MassList;


MassListSorterDlg::MassListSorterDlg (QWidget *parent)
  : QDialog (parent, Qt::Dialog)
{
  mp_parent = static_cast<QMainWindow*>(parent);
  
  QVBoxLayout *mainLayout = new QVBoxLayout;
  
  QLabel *label = new QLabel (tr ("Mass List Sorter Plugin"));
    
  mainLayout->addWidget (label, 0, Qt::AlignHCenter);
    
  createEditorGroupBox ();
  mainLayout->addWidget(mp_editorGroupBox);

  createActionGroupBox ();
  mainLayout->addWidget (mp_actionGroupBox);
  
  setLayout (mainLayout);

  setAttribute (Qt::WA_DeleteOnClose);
  
  setWindowTitle (tr ("Mass List Sorter Plugin"));

  connect(mp_parent, 
          SIGNAL (aboutToClose ()),
          this,
          SLOT (parentClosing ()));
}


void
MassListSorterDlg::parentClosing ()
{
  QDialog::reject ();
}


void 
MassListSorterDlg::createEditorGroupBox ()
{
    mp_editorGroupBox = new QGroupBox (tr ("Manipulated Mass Lists"));

    QHBoxLayout *layout = new QHBoxLayout;

    mp_inputEditor = new QTextEdit;
    layout->addWidget (mp_inputEditor);

    mp_outputEditor = new QTextEdit;
    layout->addWidget (mp_outputEditor);
    
    mp_editorGroupBox->setLayout(layout);
}


void 
MassListSorterDlg::createActionGroupBox ()
{
  mp_actionGroupBox = new QGroupBox (tr ("Actions"));

  QHBoxLayout *layout = new QHBoxLayout;

  mp_actionComboBox = new QComboBox ();

  QStringList menuItems = QStringList () 
    << tr ("Ascending Order")
    << tr ("Descending Order");
    
  mp_actionComboBox->addItems (menuItems);

  layout->addWidget (mp_actionComboBox);

  mp_executePushButton = new QPushButton (tr ("&Execute"));
  connect(mp_executePushButton, 
	  SIGNAL (clicked ()), 
	  this, 
	  SLOT (execute ()));

  layout->addWidget (mp_executePushButton);
  
  mp_actionGroupBox->setLayout (layout);
}


void 
MassListSorterDlg::execute ()
{
  // What's the task to be performed?

  QString comboText = mp_actionComboBox->currentText ();
  
  // What's the text to work on?
  QString docText = mp_inputEditor->toPlainText ();
  
  // Make a mass list with the text.
  MassList inputMassList ("INPUT_MASS_LIST", docText, QLocale ());

  // Check the result.
  if (inputMassList.makeMassList () == -1)
    return;
  
  // Do the sort.
  if (comboText == tr ("Ascending Order"))
    inputMassList.sortAscending ();
  else if (comboText == tr ("Descending Order"))
    inputMassList.sortDescending ();
      
  // Convert list to text.
  inputMassList.makeMassText ();
  
  // Clear the previous output text edit widget.
  mp_outputEditor->clear ();

  // Output the new text.  
  mp_outputEditor->setPlainText (inputMassList.massText ());
}

