/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Qt includes


/////////////////////// Local includes
#include "cleaveOligomerTableViewMimeData.hpp"


namespace massXpert
{
  
  CleaveOligomerTableViewMimeData::CleaveOligomerTableViewMimeData 
  (const CleaveOligomerTableView *tableView,
   SequenceEditorWnd *editorWnd,
   CleavageDlg *cleavageDlg)
    : mp_tableView(tableView), 
      mp_editorWnd(editorWnd), 
      mp_cleavageDlg(cleavageDlg)
  {
    m_formats << "text/plain";
  }
  
  
  CleaveOligomerTableViewMimeData::~CleaveOligomerTableViewMimeData()
  {
  }
  

  const CleaveOligomerTableView *
  CleaveOligomerTableViewMimeData::tableView() const
  {
    return mp_tableView;
  }
  

  QStringList
  CleaveOligomerTableViewMimeData::formats() const
  {
    return m_formats;
  }

  QVariant
  CleaveOligomerTableViewMimeData::retrieveData(const QString& format,
						QVariant::Type preferredType)
    const
  {
    if (format == "text/plain")
      {
	return selectionAsPlainText();
      }
    else
      {
	return QMimeData::retrieveData(format, preferredType);
      }
  }
  
  
  QString 
  CleaveOligomerTableViewMimeData::selectionAsPlainText() const
  {
    // We just do an export in the form of a simple text. Only the
    // selected items should go in the export.
    QString *text = mp_tableView->selectedOligomersAsPlainText();
    
    QString returnedString = *text;
    
    delete text;
    
    return returnedString;
  }
  

} // namespace massXpert


