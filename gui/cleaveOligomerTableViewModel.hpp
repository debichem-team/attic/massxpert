/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef CLEAVE_OLIGOMER_TABLE_VIEW_MODEL_HPP
#define CLEAVE_OLIGOMER_TABLE_VIEW_MODEL_HPP


/////////////////////// Qt includes
#include <QDebug>
#include <QAbstractTableModel>


/////////////////////// Local includes
#include "oligomerList.hpp"
#include "cleavageDlg.hpp"

namespace massXpert
{

  enum
    {
      CLEAVE_OLIGO_PARTIAL_COLUMN,
      CLEAVE_OLIGO_NAME_COLUMN,
      CLEAVE_OLIGO_COORDINATES_COLUMN,
      CLEAVE_OLIGO_MONO_COLUMN,
      CLEAVE_OLIGO_AVG_COLUMN,
      CLEAVE_OLIGO_CHARGE_COLUMN,
      CLEAVE_OLIGO_MODIF_COLUMN,
      CLEAVE_OLIGO_TOTAL_COLUMNS
    };
  

  class CleaveOligomerTableViewModel : public QAbstractTableModel
  {
    Q_OBJECT

    private: 
    OligomerList *mp_oligomerList;
    CleavageDlg *mp_parentDlg;
    CleaveOligomerTableView *mp_tableView;
    
  public:
    CleaveOligomerTableViewModel(OligomerList *, QObject *);
    ~CleaveOligomerTableViewModel();
    
    const OligomerList *oligomerList();
    CleavageDlg *parentDlg();
    
    void setTableView(CleaveOligomerTableView *);
    CleaveOligomerTableView *tableView();
    
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    QVariant headerData(int, Qt::Orientation, 
                        int role = Qt::DisplayRole) const;
    
    QVariant data(const QModelIndex &parent = QModelIndex(), 
                  int role = Qt::DisplayRole) const;

    int addOligomers(const OligomerList &);
    int removeOligomers(int, int);
  };
  
} // namespace massXpert


#endif // CLEAVE_OLIGOMER_TABLE_VIEW_MODEL_HPP
