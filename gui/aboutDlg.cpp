/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006, 2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Qt includes
#include <QGraphicsPixmapItem>


/////////////////////// Local includes
#include "aboutDlg.hpp"
#include "config.h"
#include "about_gpl_v30.hpp"
#include "about_history.hpp"


namespace massXpert
{

  AboutDlg::AboutDlg(QWidget *parent)
    : QDialog(parent)
  {
    Q_ASSERT(parent);
  
    m_ui.setupUi(this);

    QString text = QString(tr("massXpert, version %1.").arg(VERSION));
    m_ui.programVersionLabel->setText(text);
  

    connect(m_ui.closePushButton,
	    SIGNAL(clicked()),
	    this,
	    SLOT(accept()));
    

    setupGraphicsView();
  

    m_ui.aboutTextEdit->setText("This software is licensed under the "
				 "General Public License version 3. \n"
				 "Please, read the license on the \"General Public License\" tab.\n"
				 "NO WARRANTY \n"
				 "\n"
				 "\n  15. Disclaimer of Warranty.\n"
				 "\n"
				 "  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY\n"
				 "APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT\n"
				 "HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM ""AS IS"" WITHOUT WARRANTY\n"
				 "OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,\n"
				 "THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR\n"
				 "PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM\n"
				 "IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF\n"
				 "ALL NECESSARY SERVICING, REPAIR OR CORRECTION.\n"
				 "\n"
				 "  16. Limitation of Liability.\n"
				 "\n"
				 "  IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING\n"
				 "WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS\n"
				 "THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY\n"
				 "GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE\n"
				 "USE OR INABILITY TO USE THE PROGRAM(INCLUDING BUT NOT LIMITED TO LOSS OF\n"
				 "DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD\n"
				 "PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),\n"
				 "EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF\n"
				 "SUCH DAMAGES.\n");
    

  
  
    m_ui.copyrightLabel->setText("massXpert Copyright 2000,2001,2002,2003,2004,2005,2006,2007,2008 by Filippo Rusconi");
  
  
    m_ui.featuresTextEdit->setHtml("<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
				    " p, li { white-space: pre-wrap; }\n"
				    " </style></head><body style=\" font-family:'Sans Serif'; font-size:12pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
				    "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>\n"
				    "<br>\n"
				    "<br>\n"
				    "<em>massXpert</em> provides the following main features:\n"
				    "<br>\n"
				    "<ul>\n"
				    "<li>highly configurable sequence editor using user-defined monomer vignettes</li>\n"
				    "<li>definition of atoms with any number of isotopic mass/abundance pairs</li>\n"
				    "<li>definition of brand new polymer chemistries</li>\n"
				    "<li>polymer sequence cleavage using user-defined flexible cleavage agents</li>\n"
				    "<li>gas-phase fragmentation of oligomers using user-defined flexible fragmentation rules</li>\n"
				    "<li>powerful mass search in a polymer sequence with associated ion charge values ranges</li>\n"
				    "<li>powerful Copy/Paste paradigm allowing the pasted polymer sequence to be purified</li>\n"
				    "<li>calculation of isotopic pattern of any chemical starting from a formula</li>\n"
				    "</ul>\n"
				    "<br>\n"
				    "Visit http://www.massxpert.org\n"
				    "</body></html>\n");

    m_ui.gplTextEdit->setText(gpl);
    m_ui.historyTextEdit->setHtml(history);
  }


  void
  AboutDlg::setupGraphicsView()
  {
    mp_graphicsScene = new QGraphicsScene();

    QPixmap pixmap(":/images/splashscreen.png");

    QGraphicsPixmapItem *pixmapItem = mp_graphicsScene->addPixmap(pixmap);
    if (!pixmapItem)
      return;
    m_ui.graphicsView->setScene(mp_graphicsScene);
  }


  void 
  AboutDlg::showHowToCiteTab()
  {
    m_ui.tabWidget->setCurrentIndex(1);
  }
  

  AboutDlg::~AboutDlg()
  {
  }

} // namespace massXpert
