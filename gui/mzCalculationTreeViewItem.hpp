/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef MZ_CALCULATION_TREEVIEW_ITEM_HPP
#define MZ_CALCULATION_TREEVIEW_ITEM_HPP


/////////////////////// Qt includes
#include <QList>
#include <QVariant>
#include <QTreeView>
#include <QMainWindow>


/////////////////////// Local includes
#include "ionizable.hpp"


namespace massXpert
{

  enum
    {
      MZ_CALC_LEVEL_COLUMN,
      MZ_CALC_MONO_COLUMN,
      MZ_CALC_AVG_COLUMN,
      MZ_CALC_TOTAL_COLUMNS
    };


  class MzCalculationTreeViewItem 
  {
  private:
    QList<MzCalculationTreeViewItem *> m_childItemsList;
    QList<QVariant> m_itemData;
    MzCalculationTreeViewItem *mp_parentItem;
    Ionizable *mp_ionizable;
      
  public:
    MzCalculationTreeViewItem(const QList<QVariant> &data,
			       MzCalculationTreeViewItem *parent = 0);

    ~MzCalculationTreeViewItem();
  
    MzCalculationTreeViewItem *parent();
  
    void appendChild(MzCalculationTreeViewItem *item);
    void insertChild(int index,
		      MzCalculationTreeViewItem *item);
  
    MzCalculationTreeViewItem *child(int row);
    MzCalculationTreeViewItem *takeChild(int row);
    const  QList<MzCalculationTreeViewItem *> &childItems();
    
    int childCount() const;
    int columnCount() const;

    QVariant data(int column) const;
    bool setData(int column, const QVariant & value);
  
    int row() const;

    void setIonizable(Ionizable *);
    Ionizable *ionizable();
  };

} // namespace massXpert


#endif // MZ_CALCULATION_TREEVIEW_ITEM_HPP
