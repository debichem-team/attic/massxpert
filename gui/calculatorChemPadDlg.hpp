/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef CALCULATOR_CHEM_PAD_DLG_HPP
#define CALCULATOR_CHEM_PAD_DLG_HPP


/////////////////////// Qt includes
#include<QGroupBox>
#include<QColor>
#include<QHash>
#include<QDialog>
#include<QScrollArea>
#include<QGridLayout>
#include<QVBoxLayout>

/////////////////////// Local includes
// #include "ui_calculatorChemPadDlg.h"
#include "chemPadButton.hpp"
#include "calculatorChemPadGroupBox.hpp"


namespace massXpert
{

  class CalculatorChemPadDlg : public QDialog
  {
    Q_OBJECT
  
    private:
    //    Ui::CalculatorChemPadDlg m_ui;

    QGroupBox *mpa_mainGroupBox;
    QVBoxLayout *mpa_mainGroupBoxVBLayout;
    
    QVBoxLayout *mpa_mainVBoxLayout;
    QScrollArea *mpa_scrollArea;
    QVBoxLayout *mpa_scrollAreaVBoxLayout;
    
    CalculatorChemPadGroupBox *mp_lastGroupbox;
    QGridLayout *mp_lastGridLayout;

    QHash<QString, QColor> m_colorHash;

    int m_columnCount;
    int m_row;
    int m_column;

    void keyPressEvent(QKeyEvent *);
    void keyReleaseEvent(QKeyEvent *);

  public:
    CalculatorChemPadDlg(QWidget * = 0);
    ~CalculatorChemPadDlg();
  
    bool parseFile(QString &);
    bool setup(QString &, const QString & = QString());

    bool parseColumnCount(QString &);
    QColor parseColor(QString &);

    QStringList parseSeparator(QString &);
    ChemPadButton *parseKey (QString &, QStringList &);
			      
  public slots:
    void close();
  };

} // namespace massXpert


#endif // CALCULATOR_CHEM_PAD_DLG_HPP
