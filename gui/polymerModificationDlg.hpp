/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef POLYMER_MODIFICATION_DLG_HPP
#define POLYMER_MODIFICATION_DLG_HPP


/////////////////////// Qt includes
#include <QMainWindow>


/////////////////////// Local includes
#include "globals.hpp"
#include "ui_polymerModificationDlg.h"
#include "sequenceEditorWnd.hpp"


namespace massXpert
{

  class PolymerModificationDlg : public QDialog
  {
    Q_OBJECT
  
    private:
    Ui::PolymerModificationDlg m_ui;
    SequenceEditorWnd *mp_editorWnd;
  
    void updateModificationLineEdits();
  
    bool populateModificationList();

    void closeEvent(QCloseEvent *event); 
  
  public:
    PolymerModificationDlg(QWidget *parent, PolymerEnd = MXT_END_NONE);
    ~PolymerModificationDlg();
    
    bool parseModifDefinition(Modif *);
				   
  public slots:
    void modify();
    void unmodify();
  };

} // namespace massXpert


#endif // POLYMER_MODIFICATION_DLG_HPP
