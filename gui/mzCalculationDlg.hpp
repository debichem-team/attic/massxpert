/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#ifndef MZ_CALCULATION_DLG_HPP
#define MZ_CALCULATION_DLG_HPP


/////////////////////// Qt includes
#include <QMainWindow>
#include <QSortFilterProxyModel>


/////////////////////// Local includes
#include "ui_mzCalculationDlg.h"
#include "polChemDef.hpp"
#include "mzCalculationTreeViewModel.hpp"
#include "mzCalculationTreeViewSortProxyModel.hpp"
#include "ionizable.hpp"
#include "oligomerList.hpp"

namespace massXpert
{

  class MzCalculationTreeViewModel;
  class MzCalculationTreeViewSortProxyModel;
  class PolChemDef;

  class MzCalculationDlg : public QDialog
  {
    Q_OBJECT
  
    private:
    Ui::MzCalculationDlg m_ui;

    // The results-exporting strings. ////////////////////////////////
    QString *mpa_resultsString;
    QString m_resultsFilePath;
    //////////////////////////////////// The results-exporting strings.

    const PolChemDef *mp_polChemDef;

    const QList<Atom *> &m_atomList;

    OligomerList m_oligomerList;
      
    Formula m_formula;

    IonizeRule m_ionizeRule;
    
    QList<Ionizable *> m_ionizableList;

    MzCalculationTreeViewModel *mpa_mzTreeViewModel;
    MzCalculationTreeViewSortProxyModel *mpa_mzProxyModel;
  
    void closeEvent(QCloseEvent *event); 
  
  public:
    MzCalculationDlg(QWidget *, const PolChemDef *, const IonizeRule *,
		     double = 0, double = 0);
  
    ~MzCalculationDlg();
  
    void setupTreeView();
  
    bool getSrcIonizeRuleData(IonizeRule *);
    bool getDestIonizeRuleData(IonizeRule *);

    Ponderable *getSourcePonderable();
  
    void freeIonizableList();
    void emptyIonizableList();

    // The results-exporting functions. ////////////////////////////////
    void prepareResultsTxtString();
    bool exportResultsClipboard();
    bool exportResultsFile();
    bool selectResultsFile();
    bool calculateSpectrum();
    //////////////////////////////////// The results-exporting functions.

  public slots:
    void calculate();
    void formulaCheckBoxToggled(bool);
    void exportResults(int);
  };

} // namespace massXpert


#endif // MZ_CALCULATION_DLG_HPP
