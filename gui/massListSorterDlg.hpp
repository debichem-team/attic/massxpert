/* massXpert - the true massist's program.
   --------------------------------------
   Copyright (C) 2006,2007 Filippo Rusconi

   http://www.filomace.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package (see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique (FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.


   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#ifndef MASS_LIST_SORTER_DLG_HPP
#define MASS_LIST_SORTER_DLG_HPP

#include <QDialog>
#include <QTextDocument>
#include <QHash>

class QGroupBox;
class QComboBox;
class QPushButton;
class QTextEdit;
class QMainWindow;


class MassListSorterDlg : public QDialog
{
    Q_OBJECT

private:
  void createEditorGroupBox ();
  void createActionGroupBox ();

  QMainWindow *mp_parent;
  
  
  // Allocated but ownership taken by the dialog.  Thus mp_ and not
  // mpa_.
  QTextEdit *mp_inputEditor;
  QTextEdit *mp_outputEditor;
  QGroupBox *mp_editorGroupBox;
  QGroupBox *mp_actionGroupBox;
  QComboBox *mp_actionComboBox;
  QPushButton *mp_executePushButton;

 private slots:
  void execute ();
  void parentClosing ();
  
public:
  MassListSorterDlg (QWidget *parent = 0);
};



#endif // MASS_LIST_SORTER_DLG_HPP
