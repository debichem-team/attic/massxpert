/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.


   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef APPLICATION_HPP
#define APPLICATION_HPP


/////////////////////// Qt includes
#include <QtGui>
#include <QtXml>
#include <QtGlobal>
#include <QLocale>
#include <QSplashScreen>


/////////////////////// Local includes
#include "userSpec.hpp"
#include "configSettings.hpp"
#include "polChemDef.hpp"
#include "polChemDefSpec.hpp"
#include "polChemDefWnd.hpp"
#include "calculatorWnd.hpp"
#include "mzLabWnd.hpp"
#include "sequenceEditorWnd.hpp"


namespace massXpert
{

  class Application : public QApplication
  {
    Q_OBJECT

    private:
    QLocale m_locale;
    UserSpec m_userSpec;
    ConfigSettings m_configSettings;
    QString m_configSettingsFilePath;
    SequenceEditorWnd *mp_lastFocusedWnd;

    // List of all the polymer chemistry definition specifications that
    // were created during parsing of all the different catalogue files
    // on the system.
    QList<PolChemDefSpec *> m_polChemDefCatList;

    // List of all the polymer chemistry definitions that are loaded in
    // memory and usable to load polymer sequences.
    QList<PolChemDef *> m_polChemDefList;

    // The QList of all the polymer chemistry definition windows.
    QList<PolChemDefWnd *> m_polChemDefWndList;

    // The QList of all the calculator windows.
    QList<CalculatorWnd *> m_calculatorWndList;

    // The QList of all the sequence editor windows.
    QList<SequenceEditorWnd *> m_sequenceEditorWndList;

    // The QList of all the mz lab windows.
    QList<MzLabWnd *> m_mzLabWndList;


    // The splash screen, that we'll remove automatically after
    // 2 seconds.
    QSplashScreen *mpa_splash;

  public:
    Application(int &argc, char **argv);
    ~Application();

    const QLocale &locale();
    UserSpec *userSpec();
    ConfigSettings *configSettings();
    bool systemConfigManuallySetSettings();
    bool systemConfigReadFileSettings();
    const QString &configSettingsFilePath();
    void initializeDecimalPlacesOptions();

    void setLastFocusedWnd(SequenceEditorWnd *);

    QList<PolChemDefSpec *> *polChemDefCatList();
    QList<PolChemDef *> *polChemDefList();

    void polChemDefCatStringList(QStringList &stringList);
    PolChemDefSpec *polChemDefSpecName(const QString &);
    PolChemDefSpec *polChemDefSpecFilePath(const QString &);
    PolChemDef *polChemDefName(const QString &);

    // Window lists for the three modules(polymer chemistry definition
    // windows(Def stuff), calculator windows(Calc stuff), editor
    // windows(Editor stuff), mz lab windows...
    QList<PolChemDefWnd *> *polChemDefWndList();
    QList<CalculatorWnd *> *calculatorWndList();
    QList<SequenceEditorWnd *> *sequenceEditorWndList();
    QList<MzLabWnd *> *mzLabWndList();

    bool closeAllPolChemDefWnd(bool);
    bool closeAllCalculatorWnd();
    bool closeAllSequenceEditorWnd(bool);
    bool closeAllMzLabWnd(bool);
    bool prepareShutdown();

    bool isSequenceEditorWnd(SequenceEditorWnd *);

  public slots:
    void destroySplash();
  };

} // namespace massXpert


#endif // APPLICATION_HPP
