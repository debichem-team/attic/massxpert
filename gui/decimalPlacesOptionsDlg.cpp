/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Qt includes
#include <QMessageBox>


/////////////////////// Local includes
#include "globals.hpp"
#include "application.hpp"
#include "decimalPlacesOptionsDlg.hpp"


namespace massXpert
{
  
  DecimalPlacesOptionsDlg::DecimalPlacesOptionsDlg(QWidget *parent)
    : QDialog(parent)
  {
    m_ui.setupUi(this);
    
    mp_editorWnd = static_cast<SequenceEditorWnd *>(parent);
    
    QSettings settings 
     (static_cast<Application *>(qApp)->configSettingsFilePath(), 
       QSettings::IniFormat);
    
    settings.beginGroup("decimal_places_options_dlg");
    
    restoreGeometry(settings.value("geometry").toByteArray());

    settings.endGroup();

    m_ui.atomDecimalPlacesSpinBox->setValue(MXP_ATOM_DEC_PLACES);
    m_ui.oligomerDecimalPlacesSpinBox->setValue(MXP_OLIGOMER_DEC_PLACES);
    m_ui.polymerDecimalPlacesSpinBox->setValue(MXP_POLYMER_DEC_PLACES); 
    m_ui.pKaPhPiDecimalPlacesSpinBox->setValue(MXP_PH_PKA_DEC_PLACES);
    
    connect(m_ui.validatePushButton,
	     SIGNAL(clicked()),
	     this,
	     SLOT(validate()));
  }


DecimalPlacesOptionsDlg::~DecimalPlacesOptionsDlg()
{
}


void 
DecimalPlacesOptionsDlg::closeEvent(QCloseEvent *event)
{
  if (event)
    printf("%s", "");
  
  QSettings settings 
   (static_cast<Application *>(qApp)->configSettingsFilePath(), 
     QSettings::IniFormat);
  
  settings.beginGroup("decimal_places_options_dlg");

  settings.setValue("geometry", saveGeometry());
  
  settings.endGroup();
}


SequenceEditorWnd *
DecimalPlacesOptionsDlg::editorWnd()
{
  return mp_editorWnd;
}


void 
DecimalPlacesOptionsDlg::validate()
{
  
  MXP_ATOM_DEC_PLACES = m_ui.atomDecimalPlacesSpinBox->value(); 
  MXP_OLIGOMER_DEC_PLACES = m_ui.oligomerDecimalPlacesSpinBox->value(); 
  MXP_POLYMER_DEC_PLACES = m_ui.polymerDecimalPlacesSpinBox->value(); 
  MXP_PH_PKA_DEC_PLACES = m_ui.pKaPhPiDecimalPlacesSpinBox->value(); 

  QSettings settings 
   (static_cast<Application *>(qApp)->configSettingsFilePath(), 
     QSettings::IniFormat);
  
  settings.beginGroup("decimal_places_options");

  settings.setValue("MXP_ATOM_DEC_PLACES", MXP_ATOM_DEC_PLACES);
  settings.setValue("MXP_OLIGOMER_DEC_PLACES", MXP_OLIGOMER_DEC_PLACES);
  settings.setValue("MXP_POLYMER_DEC_PLACES", MXP_POLYMER_DEC_PLACES);
  settings.setValue("MXP_PH_PKA_DEC_PLACES", MXP_PH_PKA_DEC_PLACES);
  
  settings.endGroup();

  // At this point ask that masses be redisplayed, for the sequence
  // editor window.

  SequenceEditorWnd *seqEditorWnd = editorWnd();
  
  seqEditorWnd->updateWholeSequenceMasses();
  seqEditorWnd->updateSelectedSequenceMasses();
}

} // namespace massXpert
