/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.filomace.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Qt includes
#include <QMessageBox>
#include <QDebug>
#include <QSettings>
#include <QDir>
#include <QFileDialog>

/////////////////////// Local includes
#include "config.h"
#include "application.hpp"
#include "configurationSettingsDlg.hpp"


namespace massXpert
{

  ConfigurationSettingsDlg::ConfigurationSettingsDlg(ConfigSettings *
						      configSettings)
    : mp_configSettings(configSettings)
  {
    if (!initialize())
      {
	qDebug() << "Failed to initialize the "
	  "configuration settings dialog window";
      }
  }
  
  
  void 
  ConfigurationSettingsDlg::closeEvent(QCloseEvent *event)
  {  
    QSettings settings 
     (static_cast<Application *>(qApp)->configSettingsFilePath(), 
       QSettings::IniFormat);
  
    settings.beginGroup("configuration_settings_dlg");

    settings.setValue("geometry", saveGeometry());

    settings.endGroup();
  }

  

  ConfigurationSettingsDlg::~ConfigurationSettingsDlg()
  {
  }

  
  bool
  ConfigurationSettingsDlg::initialize()
  {
    m_ui.setupUi(this);

    QSettings settings 
     (static_cast<Application *>(qApp)->configSettingsFilePath(), 
       QSettings::IniFormat);
    
    settings.beginGroup("configuration_settings_dlg");

    restoreGeometry(settings.value("geometry").toByteArray());
    
    settings.endGroup();


    // At this point set the General concepts text depending on the
    // platform.

    QString text = tr("The massXpert software program's components "
		       "have been built and should be located in "
		       "the following system places:\n\n"
		       "- the binary program in %1\n"
		       "- the data in %2\n"
		       "- the localization files in %3\n"
		       "- the user manual in %4\n\n\n")
      .arg(QString(MASSXPERT_BIN_DIR))
      .arg(QString(MASSXPERT_DATA_DIR))
      .arg(QString(MASSXPERT_LOCALE_DIR))
      .arg(QString(MASSXPERT_USERMAN_DIR));
    
    text += tr("However, it appears that the configuration on this system "
		"is not typical. The software package might have been " 
		"relocated.\n\n"
		"You are given the opportunity to locate the massXpert "
		"software main directories.");
    
    m_ui.generalConceptsTextEdit->setText(text);


    m_ui.dataDirGroupBox->setTitle(tr("Should be %1")
				    .arg(QString(MASSXPERT_DATA_DIR)));
    m_ui.dataDirLineEdit->setText(QString(MASSXPERT_DATA_DIR));


    m_ui.localizationDirGroupBox->setTitle(tr("Should be %1")
				    .arg(QString(MASSXPERT_LOCALE_DIR)));
    m_ui.localizationDirLineEdit->setText(QString(MASSXPERT_LOCALE_DIR));
 

    // Make the connections.

    connect(m_ui.browseDataDirPushButton, 
	     SIGNAL(clicked()),
	     this, 
	     SLOT(browseDataDirPushButtonClicked()));
  
    connect(m_ui.browseLocalizationDirPushButton, 
	     SIGNAL(clicked()),
	     this, 
	     SLOT(browseLocalizationDirPushButtonClicked()));
  
    connect(m_ui.saveSettingsPushButton, 
	     SIGNAL(clicked()),
	     this, 
	     SLOT(saveSettingsPushButtonClicked()));

    connect(m_ui.eraseSettingsPushButton,
	     SIGNAL(clicked()),
	     this, 
	     SLOT(eraseSettingsPushButtonClicked()));

   connect(m_ui.cancelPushButton, 
	    SIGNAL(clicked()),
	    this, 
	    SLOT(cancelPushButtonClicked()));
   
    return true;
  }

  
  void 
  ConfigurationSettingsDlg::browseDataDirPushButtonClicked()
  {
    QDir dir(QFileDialog::getExistingDirectory
	     (this, 
	       tr("Locate the system data directory"),
	       QString(MASSXPERT_DATA_DIR),
	       QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks));
    
    if (!checkDataDir(dir))
      {
	QMessageBox::warning(this,
			      tr("massXpert - Configuration Settings"),
			      tr("%1@%2\n"
				  "Failed to verify the consistency "
				  "of the data directory.\n"
				  "Please ensure that the package "
				  "is installed correctly\n")
			      .arg(__FILE__)
			      .arg(__LINE__),
			      QMessageBox::Ok);
	return;
      }
    
    m_ui.dataDirLineEdit->setText(dir.absolutePath());
  }

  
  void
  ConfigurationSettingsDlg::browseLocalizationDirPushButtonClicked()
  {
    QDir dir(QFileDialog::getExistingDirectory
	     (this, 
	       tr("Locate the system localization directory"),
	       QString(MASSXPERT_LOCALE_DIR),
	       QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks));
    
    if (!checkLocalizationDir(dir))
      {
	QMessageBox::warning(this,
			      tr("massXpert - Configuration Settings"),
			      tr("%1@%2\n"
				  "Failed to verify the consistency "
				  "of the localization directory.\n"
				  "Please ensure that the package "
				  "is installed correctly\n")
			      .arg(__FILE__)
			      .arg(__LINE__),
			      QMessageBox::Ok);
	return;
      }
    
    m_ui.localizationDirLineEdit->setText(dir.absolutePath());
  }
  
  
  bool 
  ConfigurationSettingsDlg::checkDataDir(const QDir &dir)
  {
    // At this point we must ensure that the
    // pol-chem-defs/massxpert-pol-chem-defs-cat catalogue exists.
    
    QString filePath(dir.absolutePath() +
		      QDir::separator() +
		      QString("pol-chem-defs") +
		      QDir::separator() +
		      QString("massxpert-pol-chem-defs-cat"));
    
    return QFile::exists(filePath);
  }
  
  
  bool 
  ConfigurationSettingsDlg::checkLocalizationDir(const QDir &dir)
  {
    // At the moment there is the french translation: massxpert_fr.qm

    QString filePath(dir.absolutePath() +
		      QDir::separator() +
		      "massxpert_fr.qm");
    
    return QFile::exists(filePath);
  }
  

  void
  ConfigurationSettingsDlg::saveSettingsPushButtonClicked()
  {
    // We have to check that all the data are correct and we then set
    // the values to the m_configSettings passed as parameter.

    QDir dir;
    
    dir.setPath(m_ui.dataDirLineEdit->text());
        
    if (!checkDataDir(dir))
      {
	QMessageBox::warning(this,
			      tr("massXpert - Configuration Settings"),
			      tr("%1@%2\n"
				  "Failed to verify the consistency "
				  "of the data directory.\n"
				  "Please ensure that the package "
				  "is installed correctly\n")
			      .arg(__FILE__)
			      .arg(__LINE__),
			      QMessageBox::Ok);
	return;
      }
    

    dir.setPath(m_ui.localizationDirLineEdit->text());
    
    if (!checkLocalizationDir(dir))
      {
	QMessageBox::warning(this,
			      tr("massXpert - Configuration Settings"),
			      tr("%1@%2\n"
				  "Failed to verify the consistency "
				  "of the localization directory.\n"
				  "Please ensure that the package "
				  "is installed correctly\n")
			      .arg(__FILE__)
			      .arg(__LINE__),
			      QMessageBox::Ok);
	return;
      }
    
    // Ok, all the bits are correct. Set them to the configSettings
    // object passed as parameter.
    QString text;
    
    text = m_ui.dataDirLineEdit->text();
    mp_configSettings->setSystemDataDir(text);
    mp_configSettings->setSystemPolChemDefCatDir(text + 
						  QDir::separator() + 
						  "pol-chem-defs");
    
    text = m_ui.localizationDirLineEdit->text();
    mp_configSettings->setSystemLocalizationDir(text);
    

    // OK, at this point we can store all these data in the
    // configuration settings for the current user using the
    // QSettings system.
    
    QSettings settings 
     (static_cast<Application *>(qApp)->configSettingsFilePath(), 
       QSettings::IniFormat);

    settings.beginGroup("system_data_config_settings");
    
    settings.setValue("system_data_dir", 
		       mp_configSettings->systemDataDir());
    
    settings.setValue("system_pol_chem_def_cat_dir", 
		       mp_configSettings->systemPolChemDefCatDir());
    
    settings.setValue("system_localization_dir", 
		       mp_configSettings->systemLocalizationDir());

    settings.endGroup();

    // At this point we can accept !
    accept();
  }


  void
  ConfigurationSettingsDlg::eraseSettingsPushButtonClicked()
  {
    QSettings settings 
     (static_cast<Application *>(qApp)->configSettingsFilePath(), 
       QSettings::IniFormat);

    settings.remove("system_data_config_settings");
  }
  

  void
  ConfigurationSettingsDlg::cancelPushButtonClicked()
  {
    // This will let the caller know that nothing should be done.
    reject();
  }

} // namespace massXpert
