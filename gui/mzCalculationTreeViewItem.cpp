/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Local includes
#include "mzCalculationTreeViewItem.hpp"
#include "application.hpp"


namespace massXpert
{

  MzCalculationTreeViewItem::MzCalculationTreeViewItem 
 (const QList<QVariant> &data,
   MzCalculationTreeViewItem *parent)
  {
    mp_parentItem = parent;
    m_itemData = data;
    mp_ionizable = 0;
  }


  MzCalculationTreeViewItem::~MzCalculationTreeViewItem()
  {
    qDeleteAll(m_childItemsList);
  }


  MzCalculationTreeViewItem *
  MzCalculationTreeViewItem::parent()
  {
    return mp_parentItem;
  }


  void 
  MzCalculationTreeViewItem::appendChild(MzCalculationTreeViewItem *
					  item)
  {
    m_childItemsList.append(item);
  }


  void 
  MzCalculationTreeViewItem::insertChild(int index,
					  MzCalculationTreeViewItem *item)
  {
    m_childItemsList.insert(index, item);
  }


  MzCalculationTreeViewItem *
  MzCalculationTreeViewItem::child(int row)
  {
    return m_childItemsList.value(row);
  }


  MzCalculationTreeViewItem *
  MzCalculationTreeViewItem::takeChild(int row)
  {
    MzCalculationTreeViewItem *item = m_childItemsList.takeAt(row);
  
    return item;
  }


  const QList<MzCalculationTreeViewItem *> &
  MzCalculationTreeViewItem::childItems()
  {
    return m_childItemsList;
  }



  int 
  MzCalculationTreeViewItem::childCount() const
  {
    return m_childItemsList.count();
  }


  int 
  MzCalculationTreeViewItem::columnCount() const
  {
    return m_itemData.count();
  }


  QVariant 
  MzCalculationTreeViewItem::data(int column) const
  {
    return m_itemData.value(column);
  }


  bool 
  MzCalculationTreeViewItem::setData(int column, 
				      const QVariant & value)
  {
    m_itemData [column].setValue(value.toString());

    return true;
  }

  
  int
  MzCalculationTreeViewItem::row() const
  {
    if (mp_parentItem)
      return mp_parentItem->
	m_childItemsList.indexOf 
	(const_cast<MzCalculationTreeViewItem *>(this));
  
    return 0;
  }


  void
  MzCalculationTreeViewItem::setIonizable(Ionizable *ionizable)
  {
    Q_ASSERT(ionizable);
    mp_ionizable = ionizable;
  }


  Ionizable *
  MzCalculationTreeViewItem::ionizable()
  {
    return mp_ionizable;
  }

} // namespace massXpert
