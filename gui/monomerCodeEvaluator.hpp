/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef MONOMER_CODE_EVALUATOR_HPP
#define MONOMER_CODE_EVALUATOR_HPP


/////////////////////// Qt includes
#include <QLineEdit>


/////////////////////// Local includes
#include "sequenceEditorWnd.hpp"


namespace massXpert
{

  class Polymer;
  class SequenceEditorWnd;


  class MonomerCodeEvaluator : public QObject
  {
    Q_OBJECT
  
    private:
    const QPointer<Polymer> mp_polymer;
    SequenceEditorWnd *mp_editorWnd;
  
    QLineEdit *m_elabCodeLineEdit;
    QLineEdit *m_errorCodeLineEdit;

    QList<int> m_completionsList;
  
  
    QString m_evalCode;
    QString m_elabCode;
  
  
  public:
    MonomerCodeEvaluator(Polymer * = 0,
			  SequenceEditorWnd * = 0,
			  QLineEdit * = 0,
			  QLineEdit * = 0);
  
    ~MonomerCodeEvaluator();
  
    void setSequenceEditorWnd(SequenceEditorWnd *);
    void setEdits(QLineEdit *, QLineEdit *);
  
    bool evaluateCode(const QString &);

    void escapeKey();
    bool newKey(QString);

    bool upperCaseChar(QChar);
    bool lowerCaseChar(QChar);
  
    int autoComplete(QString &);
    void emptyCompletionsList();
    bool reportCompletions();
  
    void reportError(QString &);
  
    void debugCompletionsPutStdErr();
  };

} // namespace massXpert


#endif // MONOMER_CODE_EVALUATOR_HPP
