/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef FRAGMENT_OLIGOMER_TABLE_VIEW_HPP
#define FRAGMENT_OLIGOMER_TABLE_VIEW_HPP


/////////////////////// Qt includes
#include <QTableView>


/////////////////////// Local includes
#include "oligomerList.hpp"


namespace massXpert
{

  class FragmentationDlg;


  class FragmentOligomerTableView : public QTableView
  {
    Q_OBJECT

    private:
    FragmentationDlg *mp_parentDlg;
  
    OligomerList *mp_oligomerList;
    
    // For drag operations.
    QPoint m_dragStartPos;
    void startDrag();
    
    ///////// Contextual menu for copying to clipboard of mono/avg
    ///////// masses.
    QMenu *contextMenu;
    QAction *copyMonoAct;
    QAction *copyAvgAct;
    ///////// 

  protected:
  
    void currentChanged(const QModelIndex &, const QModelIndex &);  
    void copyMassList(int);

  public:
    FragmentOligomerTableView(QWidget *parent = 0);
    ~FragmentOligomerTableView();
  
    void setOligomerList(OligomerList *);
    const OligomerList *oligomerList();

    FragmentationDlg *parentDlg();
    void setParentDlg(FragmentationDlg *);
    
    int selectedOligomers(OligomerList *, int) const;
    QString *selectedOligomersAsPlainText(QString = "$",
                                          bool = false,
                                          bool = false,
                                          MassType = MXT_MASS_MONO) const;

    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);

  public slots:
    void itemActivated(const QModelIndex &);
    void copyMono();
    void copyAvg();
  };

} // namespace massXpert


#endif // FRAGMENT_OLIGOMER_TABLE_VIEW_HPP
