/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.filomace.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef CLEAVE_SPEC_DEF_DLG_HPP
#define CLEAVE_SPEC_DEF_DLG_HPP


/////////////////////// Local includes
#include "ui_cleaveSpecDefDlg.h"


namespace massXpert
{

  class PolChemDef;
  class PolChemDefWnd;
  class CleaveSpec;
  class CleaveRule;


  class CleaveSpecDefDlg : public QDialog
  {
    Q_OBJECT

    private:
    Ui::CleaveSpecDefDlg m_ui;

    PolChemDef *mp_polChemDef;
    PolChemDefWnd *mp_polChemDefWnd;

    QList<CleaveSpec *> *mp_list;

    void closeEvent(QCloseEvent *event);
  

  public:
    CleaveSpecDefDlg(PolChemDef *, PolChemDefWnd *);
    ~CleaveSpecDefDlg();
  
    bool initialize();

    void updateCleaveSpecIdentityDetails(CleaveSpec *);
    void updateCleaveRuleDetails(CleaveRule *);
  
    void clearAllDetails();

    void setModified();
		     		    
  public slots:

    // ACTIONS
    void addCleaveSpecPushButtonClicked();
    void removeCleaveSpecPushButtonClicked();
    void moveUpCleaveSpecPushButtonClicked();
    void moveDownCleaveSpecPushButtonClicked();

    void addCleaveRulePushButtonClicked();
    void removeCleaveRulePushButtonClicked();
    void moveUpCleaveRulePushButtonClicked();
    void moveDownCleaveRulePushButtonClicked();

    void applyCleaveSpecPushButtonClicked();
    void applyCleaveRulePushButtonClicked();

    bool validatePushButtonClicked();

    // VALIDATION
    bool validate();  

    // WIDGETRY
    void cleaveSpecListWidgetItemSelectionChanged();
    void cleaveRuleListWidgetItemSelectionChanged();
  };

} // namespace massXpert


#endif // CLEAVE_SPEC_DEF_DLG_HPP
 
