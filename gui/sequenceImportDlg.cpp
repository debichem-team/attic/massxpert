/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Qt includes
#include <QMessageBox>


/////////////////////// Local includes
#include "sequenceImportDlg.hpp"


namespace massXpert
{

  enum 
    {
      NONE = 0 << 1,
      LEFT = 1 << 1,
      RIGHT = 2 << 1,
    };


  SequenceImportDlg::SequenceImportDlg(QWidget *parent,
				       QStringList *chainSequenceStringList,
				       QStringList *chainIdStringList)
    : QDialog(parent)
  {
    Q_ASSERT(parent);
    
    // We are taking ownership of the strings
    Q_ASSERT(chainSequenceStringList);
    mpa_chainSequenceStringList = chainSequenceStringList;
        
    Q_ASSERT(chainIdStringList);
    mpa_chainIdStringList = chainIdStringList;

    mp_editorWnd = static_cast<SequenceEditorWnd *>(parent);
    
    m_ui.setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose);

    connect(m_ui.importedSequencesListWidget,
	    SIGNAL(itemSelectionChanged()),
	    this,
	    SLOT(listWidgetSelectionChanged()));
    
    setupDialog();
  }


  SequenceImportDlg::~SequenceImportDlg()
  {
    delete mpa_chainIdStringList;
    delete mpa_chainSequenceStringList;
  }


  void
  SequenceImportDlg::setupDialog()
  {
    if (mpa_chainSequenceStringList->size() != mpa_chainIdStringList->size())
      {
	QMessageBox::critical(this, 
			      tr("massXpert - Polymer Sequence Importer"),
			      tr("Failed to import the sequence, "
				 "The number of chain IDs is different from "
				 "the number of chain sequences."),
			      QMessageBox::Ok);	
	
	return;
      }

    // If the lists are empty we have nothing to do.
    if (!mpa_chainSequenceStringList->size())
      return;
    
    //  Now populate the ID string list widget.

    QString iterString;
    
    for (int iter = 0; iter < mpa_chainSequenceStringList->size(); ++iter)
      {
	// First add a new item in the list widget.
	iterString = mpa_chainIdStringList->at(iter);
	m_ui.importedSequencesListWidget->addItem(iterString);
      }
    
    // Select the first ID item, of which we'll display the
    // corresponding sequence.

    m_ui.importedSequencesListWidget->setCurrentRow(0);
    
    // And now add the string to the text edit widget.
    iterString = mpa_chainSequenceStringList->at(0);
    m_ui.importedSequencesTextEdit->setPlainText(iterString);
  }
  
  void
  SequenceImportDlg::listWidgetSelectionChanged()
  {
    // Get the index of the currently selected item.
    int rowIndex = m_ui.importedSequencesListWidget->currentRow();

    // And now put the corresponding string to the text edit widget.
    QString chainSequence = mpa_chainSequenceStringList->at(rowIndex);

//     qDebug() << __FILE__ << __LINE__
// 	     << "Current item index:" << rowIndex << "\n"
// 	     << "Current sequence:"
// 	     << chainSequence;
    
    m_ui.importedSequencesTextEdit->clear();
    m_ui.importedSequencesTextEdit->setPlainText(chainSequence);
  }
  

} // namespace massXpert
