/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef MZ_LAB_OUTPUT_OLIGOMER_TABLE_VIEW_DLG_HPP
#define MZ_LAB_OUTPUT_OLIGOMER_TABLE_VIEW_DLG_HPP


/////////////////////// Qt includes
#include <QMainWindow>


/////////////////////// Local includes
#include "ui_mzLabOutputOligomerTableViewDlg.h"
#include "mzLabOutputOligomerTableViewModel.hpp"
#include "mzLabOutputOligomerTableViewSortProxyModel.hpp"
#include "mzLabInputOligomerTableViewModel.hpp"


namespace massXpert
{
  
  class MzLabOutputOligomerTableView;
  class MzLabOutputOligomerTableViewModel;
  class MzLabOutputOligomerTableViewSortProxyModel;

  class MzLabOutputOligomerTableViewDlg : public QDialog
  {
    Q_OBJECT
  
    private:
    Ui::MzLabOutputOligomerTableViewDlg m_ui;

    // The list of oligomerPair instances that will be handed in the
    // table view. Note these oligomerPair instances are transferred
    // to us by MzLabWnd, and thus, although we did not alloate them,
    // we are given ownership on them, and are thus responsible for
    // freeing them. Hence, the mpa_prefix that indicated "allocated
    // here" ownership -> free that stuff when non more needed.
    QList <OligomerPair *> *mpa_oligomerPairList;
    
    MassType m_massType;

    // The dialog windows that hold the data that were used to perform
    // the matches that we are reporting in *this dialog window.
    MzLabInputOligomerTableViewDlg *mp_inputOligomerTableViewDlg1;
    MzLabInputOligomerTableViewDlg *mp_inputOligomerTableViewDlg2;
    
    QString m_name;

    MzLabWnd *mp_mzLabWnd;

    MzLabOutputOligomerTableViewModel *mpa_oligomerTableViewModel;
    MzLabOutputOligomerTableViewSortProxyModel *mpa_proxyModel;

    void setupTableView();
    
    void closeEvent(QCloseEvent *event);
  
  public:
    MzLabOutputOligomerTableViewDlg(QWidget *, 
				    QList<OligomerPair *> *,
				    MassType,
				    MzLabInputOligomerTableViewDlg *,
				    MzLabInputOligomerTableViewDlg *,
				    const QString & = QString(),
                                    const QString & = QString());
  
    ~MzLabOutputOligomerTableViewDlg();

    void setName(const QString &);
    QString name();
    
  public slots:
    void exportToClipboard();
    void exportSelectedAsNewList();
  };
  
} // namespace massXpert


#endif // MZ_LAB_OUTPUT_OLIGOMER_TABLE_VIEW_DLG_HPP
