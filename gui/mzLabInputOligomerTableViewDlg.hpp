/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.


   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef MZ_LAB_INPUT_OLIGOMER_TABLE_VIEW_DLG_HPP
#define MZ_LAB_INPUT_OLIGOMER_TABLE_VIEW_DLG_HPP
// #pragma message("including MZ_LAB_INPUT_OLIGOMER_TABLE_VIEW_DLG_HPP")

/////////////////////// Qt includes
#include <QMainWindow>


/////////////////////// Local includes
#include "globals.hpp"
#include "ui_mzLabInputOligomerTableViewDlg.h"
#include "mzLabInputOligomerTableViewSortProxyModel.hpp"
#include "sequenceEditorWnd.hpp"

namespace massXpert
{

  class MzLabInputOligomerTableView;
  class MzLabInputOligomerTableViewModel;
  class MzLabInputOligomerTableViewSortProxyModel;

  class MzLabInputOligomerTableViewDlg : public QDialog
  {
    Q_OBJECT

    private:
    Ui::MzLabInputOligomerTableViewDlg m_ui;

    MzLabWnd *mp_mzLabWnd;
    SequenceEditorWnd *mp_sequenceEditorWnd;

    QString m_name;
    QString m_history;

    // The list of oligomers that will be handled via the QTableView
    // widget.
    OligomerList m_oligomerList;

    // As of version 3.6.0, there is no more any difference between
    // "normal" oligomers and fragment oligomers since there has been
    // a total rewrite of the way fragmentation-generated oligomers
    // ionize. So there is no more the m_isFragment member datum.
    
    MassType m_massType;
    // Indicates if data were already imported with a mass type, so
    // that if another import is done with another mass type, then the
    // user is alerted that he is mixing masses of different mass
    // types. When the dialog is constructed the value is "NONE" since
    // we do not know what kind of masses the user will import.
    MassType m_previousMassType;

    MzLabInputOligomerTableViewModel *mpa_oligomerTableViewModel;
    MzLabInputOligomerTableViewSortProxyModel *mpa_proxyModel;

    void setupTableView();

    void closeEvent(QCloseEvent *event);

  public:
    MzLabInputOligomerTableViewDlg(QWidget *, QString, MassType);

    ~MzLabInputOligomerTableViewDlg();

    void setName(const QString &);
    QString name();

    const OligomerList &oligomerList();

    SequenceEditorWnd *sequenceEditorWnd();

    QString textFieldDelimiter();
    
    int duplicateOligomerData(const OligomerList &);

    MassType massType();
    MassType previousMassType();
    void setPreviousMassType(MassType);

    MassType massTypeQuery();

    void applyFormula(const Formula &, bool);
    void applyMass(double, bool);
    void applyThreshold(double, bool, bool);
    void applyChargeIncrement(int, bool);
    void applyIonizeRule(const IonizeRule &, bool);

  public slots:
    void monoMassCheckBoxStateChanged(int);
    void avgMassCheckBoxStateChanged(int);
    void exportToClipboard();
    void getFromClipboard();
    SequenceEditorWnd *connectSeqEditorWnd(const QString &);
  };

} // namespace massXpert


#endif // MZ_LAB_INPUT_OLIGOMER_TABLE_VIEW_DLG_HPP
// #pragma message("done...including MZ_LAB_INPUT_OLIGOMER_TABLE_VIEW_DLG_HPP")
