/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// Local includes
#include "massSearchOligomerTableViewModel.hpp"
#include "massSearchOligomerTableViewSortProxyModel.hpp"
#include "application.hpp"


namespace massXpert
{

  MassSearchOligomerTableViewSortProxyModel::
  MassSearchOligomerTableViewSortProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
  {
  }


  MassSearchOligomerTableViewSortProxyModel::
  ~MassSearchOligomerTableViewSortProxyModel()
  {
  }



  void  
  MassSearchOligomerTableViewSortProxyModel::setSearchedFilter(double value)
  {
    m_searchedFilter = value;
  }


  void  
  MassSearchOligomerTableViewSortProxyModel::setErrorFilter(double value)
  {
    m_errorFilter = value;
  }


  void  
  MassSearchOligomerTableViewSortProxyModel::setMonoFilter(double value)
  {
    m_monoFilter = value;
  }


  void  
  MassSearchOligomerTableViewSortProxyModel::setAvgFilter(double value)
  {
    m_avgFilter = value;
  }


  void  
  MassSearchOligomerTableViewSortProxyModel::setTolerance(double value)
  {
    m_tolerance = value;
  }


  bool 
  MassSearchOligomerTableViewSortProxyModel::lessThan(const QModelIndex &left, 
						      const QModelIndex &right) 
    const
  {
    Application *application = static_cast<Application *>(qApp);
    QLocale locale = application->locale();

    QVariant leftData = sourceModel()->data(left);
    QVariant rightData = sourceModel()->data(right);
    
    if (leftData.type() == QVariant::Int)
      {
	// The Partial column
      
	int leftValue = leftData.toInt();
	int rightValue = rightData.toInt();

	return(leftValue < rightValue);
      }
    if (leftData.type() == QVariant::Bool)
      {
	// The Modif column
      
	int leftValue = leftData.toInt();
	int rightValue = rightData.toInt();

	return(leftValue < rightValue);
      }
    else if (leftData.type() == QVariant::String && 
	    (left.column() == MASS_SEARCH_OLIGO_SEARCHED_COLUMN ||
	      left.column() == MASS_SEARCH_OLIGO_ERROR_COLUMN ||
	      left.column() == MASS_SEARCH_OLIGO_MONO_COLUMN ||
	      left.column() == MASS_SEARCH_OLIGO_AVG_COLUMN))
      {
	bool ok = false;
	
	double leftValue = locale.toDouble(leftData.toString(), &ok);
	Q_ASSERT(ok);
	double rightValue = locale.toDouble(rightData.toString(), &ok);
	Q_ASSERT(ok);
	
	return(leftValue < rightValue);
      }

    if (leftData.type() == QVariant::String)
      {
	QString leftString = leftData.toString();
	QString rightString = rightData.toString();
      
	if(!leftString.isEmpty() && leftString.at(0) == '[')
	  return sortCoordinates(leftString, rightString);
	else if (!leftString.isEmpty() && leftString.contains('#'))
	  return sortName(leftString, rightString);
	else
	  return(QString::localeAwareCompare(leftString, rightString) < 0);
      }
  

  
    return true;
  }


  bool
  MassSearchOligomerTableViewSortProxyModel::sortCoordinates(QString left,
							     QString right) const
  {
    QString local = left;

    local.remove(0, 1);
    local.remove(local.size() - 1, 1);
   
    int dash = local.indexOf('-');
   
    QString leftStartStr = local.left(dash);
   
    bool ok = false;
    int leftStartInt = leftStartStr.toInt(&ok);
   
    local.remove(0, dash + 1);
   
    QString leftEndStr = local;
   
    ok = false;
    int leftEndInt = leftEndStr.toInt(&ok);

    //    qDebug() << "left coordinates" << leftStartInt << "--" << leftEndInt;
   
    local = right;

    local.remove(0, 1);
    local.remove(local.size() - 1, 1);
   
    dash = local.indexOf('-');
   
    QString rightStartStr = local.left(dash);
   
    ok = false;
    int rightStartInt = rightStartStr.toInt(&ok);
   
    local.remove(0, dash + 1);
   
    QString rightEndStr = local;
   
    ok = false;
    int rightEndInt = rightEndStr.toInt(&ok);

    //    qDebug() << "right coordinates" << rightStartInt << "--" << rightEndInt;


    if (leftStartInt < rightStartInt)
      return true;
   
    if (leftEndInt < rightEndInt)
      return true;
   
    return false;
  }


  bool
  MassSearchOligomerTableViewSortProxyModel::sortName(QString left,
						      QString right) const
  {
    QString local = left;
  

    int diesis = local.indexOf('#');
   
    QString leftPartialStr = local.left(diesis);

    bool ok = false;
    int leftPartialInt = leftPartialStr.toInt(&ok);

    local.remove(0, diesis + 1);

    QString leftOligoStr = local;
   
    ok = false;
    int leftOligoInt = leftOligoStr.toInt(&ok);

    //    qDebug() << "left: partial#oligo" 
    // 	     << leftPartialInt << "#" << leftOligoInt;
   
    local = right;
  
    diesis = local.indexOf('#');
   
    QString rightPartialStr = local.left(diesis);

    ok = false;
    int rightPartialInt = rightPartialStr.toInt(&ok);

    local.remove(0, diesis + 1);

    QString rightOligoStr = local;
   
    ok = false;
    int rightOligoInt = rightOligoStr.toInt(&ok);

    //    qDebug() << "right: partial#oligo" 
    // 	     << rightPartialInt << "#" << rightOligoInt;
   
    if (leftPartialInt < rightPartialInt)
      return true;
   
    if (leftOligoInt < rightOligoInt)
      return true;
   
    return false;
  }


  bool 
  MassSearchOligomerTableViewSortProxyModel::filterAcceptsRow 
 (int sourceRow, const QModelIndex & sourceParent) const 
  {
    Application *application = static_cast<Application *>(qApp);
    QLocale locale = application->locale();

    int filterKeyColumnIndex = filterKeyColumn();
  
    if (filterKeyColumnIndex == MASS_SEARCH_OLIGO_SEARCHED_COLUMN)
      {
	// The column of the searched mass.
	QModelIndex index = 
	  sourceModel()->index(sourceRow,
				 MASS_SEARCH_OLIGO_SEARCHED_COLUMN,
				 sourceParent);

	bool ok = false;
	
	QVariant data = sourceModel()->data(index);
	
	double searched = locale.toDouble(data.toString(), &ok);
	Q_ASSERT(ok);
      
	if(m_searchedFilter == searched)
	  return true;
	else
	  return false;
      }
    else if (filterKeyColumnIndex == MASS_SEARCH_OLIGO_ERROR_COLUMN)
      {
	// The column of the error.
	QModelIndex index = 
	  sourceModel()->index(sourceRow, 
				 MASS_SEARCH_OLIGO_ERROR_COLUMN, 
				 sourceParent);

	bool ok = false;
	
	QVariant data = sourceModel()->data(index);
	
	double error = locale.toDouble(data.toString(), &ok);
	Q_ASSERT(ok);
      
	double left = m_errorFilter - m_tolerance;
	double right = m_errorFilter + m_tolerance;

	if(left <= error && error <= right)
	  return true;
	else 
	  return false;
      }
    else if (filterKeyColumnIndex == MASS_SEARCH_OLIGO_MONO_COLUMN)
      {
	// The column of the mono mass.
	QModelIndex index = 
	  sourceModel()->index(sourceRow, 
				 MASS_SEARCH_OLIGO_MONO_COLUMN,
				 sourceParent);

	bool ok = false;
	
	QVariant data = sourceModel()->data(index);
	
	double mass = locale.toDouble(data.toString(), &ok);
	Q_ASSERT(ok);
      
	double left = m_monoFilter - m_tolerance;
	double right = m_monoFilter + m_tolerance;
      
	if(left <= mass && mass <= right)
	  return true;
	else 
	  return false;
      }
    else if (filterKeyColumnIndex == MASS_SEARCH_OLIGO_AVG_COLUMN)
      {
	// The column of the avg mass.
	QModelIndex index = 
	  sourceModel()->index(sourceRow, 
				 MASS_SEARCH_OLIGO_AVG_COLUMN,
				 sourceParent);

	bool ok = false;
	
	QVariant data = sourceModel()->data(index);
	
	double mass = locale.toDouble(data.toString(), &ok);
	Q_ASSERT(ok);
      
	double left = m_avgFilter - m_tolerance;
	double right = m_avgFilter + m_tolerance;
      
	if(left <= mass && mass <= right)
	  return true;
	else
	  return false;
      }
  
    return QSortFilterProxyModel::filterAcceptsRow(sourceRow, sourceParent);
  }



  void 
  MassSearchOligomerTableViewSortProxyModel::applyNewFilter()
  {
    invalidateFilter();
  }

} // namespace massXpert
