/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.filomace.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef POL_CHEM_DEF_WND_HPP
#define POL_CHEM_DEF_WND_HPP


/////////////////////// Local includes
#include "ui_polChemDefWnd.h"
#include "polChemDef.hpp"
#include "atomDefDlg.hpp"
#include "monomerDefDlg.hpp"
#include "modifDefDlg.hpp"
#include "cleaveSpecDefDlg.hpp"
#include "fragSpecDefDlg.hpp"
#include "crossLinkerDefDlg.hpp"


namespace massXpert
{

  class PolChemDefWnd : public QMainWindow
  {
    Q_OBJECT

    friend class AtomDefDlg;
    friend class MonomerDefDlg;
    friend class ModifDefDlg;
    friend class CleaveSpecDefDlg;
    friend class FragSpecDefDlg;
    friend class CrossLinkerDefDlg;
  
  private:
    Ui::PolChemDefWnd m_ui;

    AtomDefDlg *mpa_atomDefDlg;
    MonomerDefDlg *mpa_monomerDefDlg;
    ModifDefDlg *mpa_modifDefDlg;
    CleaveSpecDefDlg *mpa_cleaveSpecDefDlg;
    FragSpecDefDlg *mpa_fragSpecDefDlg;
    CrossLinkerDefDlg *mpa_crossLinkerDefDlg;


    void closeEvent(QCloseEvent *event);
  

  public:
    bool m_forciblyClose;
    bool m_noDelistWnd;

    PolChemDef m_polChemDef;

    PolChemDefWnd(const QString & = QString());
    ~PolChemDefWnd();
  
    bool initialize();

    bool maybeSave();
    bool saveFile();

    bool setSingularData();

    bool checkAtomList();

    void updateWindowTitle();
  
  public slots:

    // SINGULAR ENTITIES
    void nameEditFinished();
    void leftCapEditFinished();
    void rightCapEditFinished();
    void ionizeFormulaEditFinished();
    void ionizeChargeChanged();
    void ionizeLevelChanged();
  
    // PLURAL ENTITIES
    void atomPushButtonClicked();
    void monomerPushButtonClicked();
    void modifPushButtonClicked();
    void crossLinkerPushButtonClicked();
    void cleavagePushButtonClicked();
    void fragmentationPushButtonClicked();

  
    // SAVE/SAVE AS
    bool save();
    bool saveAs();

    // VALIDATION
    int validate();  
  };

} // namespace massXpert


#endif // POL_CHEM_DEF_WND_HPP
 
