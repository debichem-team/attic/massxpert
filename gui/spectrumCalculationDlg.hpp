/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.filomace.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef SPECTRUM_CALCULATION_DLG_HPP
#define SPECTRUM_CALCULATION_DLG_HPP


/////////////////////// Qt includes
#include <QDialog>
#include <QTextDocument>


/////////////////////// Local includes
#include "ui_spectrumCalculationDlg.h"
#include "polChemDef.hpp"
#include "atom.hpp"
#include "atomCount.hpp"
#include "formula.hpp"
#include "peakShape.hpp"
#include "globals.hpp"
#include "oligomerList.hpp"


namespace massXpert
{

  enum ValidationError
    {
      MXP_VALIDATION_ERRORS_NONE = 0x0000,
      MXP_VALIDATION_FORMULA_ERRORS = 1 << 0,
      MXP_VALIDATION_RESOLUTION_ERRORS = 1 << 1,
      MXP_VALIDATION_FWHM_ERRORS = 1 << 2,
    };
  
  enum SpectrumCalculationMode
    {
      MXP_SPECTRUM_CALCULATION_MODE_NONE = 0x0000,
      MXP_SPECTRUM_CALCULATION_MODE_CLUSTER = 1 << 0,
      MXP_SPECTRUM_CALCULATION_MODE_SPECTRUM = 1 << 1,
    };
      

  class SpectrumCalculationDlg : public QDialog
  {
    Q_OBJECT
  
    private:
    Ui::SpectrumCalculationDlg m_ui;

    SpectrumCalculationMode m_mode;

    const QList<Atom *> &m_atomList;

    OligomerList *mp_oligomerList;
      
    int m_validationErrors;
    
    QString m_filePath;
    Formula m_formula;

    double m_minimumProbability;
    int m_maximumPeaks;

    double m_resolution;

    PeakShapeConfig m_config;
    
    int m_charge;

    bool m_withMonoMass;
    bool m_withCluster;
        
    const PolChemDef &m_polChemDef;

    QString m_resultsString;
            
    // The point list that will hold the sum of all the
    // gaussians/lorentzian curves obtained for the single peak shapes
    // of the spectrum.

    QList<QPointF *>m_patternPointList;

    double m_mono;
    double m_avg;
    
    bool m_aborted;
  
    void closeEvent(QCloseEvent *event); 

    void setupDialog();
    
    bool fetchFormulaMass(double * = 0, double * = 0);
    bool fetchMzRatio(double * = 0, double * = 0);
        
    bool fetchValidateInputData();
    bool fetchValidateInputDataIsotopicClusterMode();
    bool fetchValidateInputDataSpectrumMode();
    
    void setInErrorStatus(const QString &);

    bool updateMzRatio();
    bool updateIncrement();
    
    void debugPutStdErrBitset(QString /*file*/, int /*line*/, 
                              int = 0 /*value*/, 
                              const QString & = QString() /*descString*/);
                                                                         
    void executeCluster();
    void executeSpectrum();
    void executeSpectrumWithCluster(QList<QList<QPointF *> *> *, 
                                    QHash<QList<QPointF *> *, double> *);
    void executeSpectrumWithoutCluster(QList<QList<QPointF *> *> *, 
                                       QHash<QList<QPointF *> *, double> *);
                                                                           
  private slots:
    void massTypeRadioButtonToggled(bool);
    void isotopicClusterCheckBoxToggled(bool);
    
    void formulaEdited(const QString &);
    void chargeChanged(int);
    void pointsChanged(int);
    void resolutionChanged(int);
    void fwhmEdited(const QString &);

    void execute();
    void abort();
    void outputFile();
  public:
    SpectrumCalculationDlg(QWidget *parent,
                           const QList<Atom *> &,
                           SpectrumCalculationMode);
    ~SpectrumCalculationDlg();

    void setOligomerList(OligomerList *);
    
  signals:
    void spectrumCalculationAborted();
                                
  public slots:
    void spectrumCalculationProgressValueChanged(int);
    void spectrumCalculationMessageChanged(QString);
  };

} // namespace massXpert


#endif // SPECTRUM_CALCULATION_DLG_HPP
