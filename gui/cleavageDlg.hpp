/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.


   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef CLEAVAGE_DLG_HPP
#define CLEAVAGE_DLG_HPP


/////////////////////// Qt includes
#include <QMainWindow>
#include <QSortFilterProxyModel>


/////////////////////// Local includes
#include "ui_cleavageDlg.h"
#include "cleaver.hpp"
#include "sequenceEditorWnd.hpp"
#include "cleaveOligomerTableViewModel.hpp"
#include "cleaveOligomerTableViewSortProxyModel.hpp"


namespace massXpert
{

  class CleaveOligomerTableViewModel;
  class CleaveOligomerTableViewSortProxyModel;

  class CleavageDlg : public QDialog
  {
    Q_OBJECT

    private:
    Ui::CleavageDlg m_ui;
    SequenceEditorWnd *mp_editorWnd;

    // The results-exporting strings. ////////////////////////////////
    QString *mpa_resultsString;
    QString m_resultsFilePath;
    //////////////////////////////////// The results-exporting strings.

    OligomerList m_oligomerList;

    const QPointer<Polymer> mp_polymer;
    const PolChemDef *mp_polChemDef;
    CalcOptions m_calcOptions;
    const IonizeRule *mp_ionizeRule;


    Cleaver *mpa_cleaver;

    CleaveOligomerTableViewModel *mpa_oligomerTableViewModel;
    CleaveOligomerTableViewSortProxyModel *mpa_proxyModel;

    void closeEvent(QCloseEvent *event);

    // For the filtering of the data in the treeview.
    QAction *filterAct;
    double m_tolerance;
    QWidget *mp_focusWidget;

    void setupTableView();

  public:
    CleavageDlg(QWidget *,
                Polymer *, const PolChemDef *,
                const CalcOptions &, const IonizeRule *);

    ~CleavageDlg();

    bool populateSelectedOligomerData();
    void populateCleaveSpecListWidget();

    SequenceEditorWnd *editorWnd();

    void updateCleavageDetails(const CalcOptions &);
    void updateOligomerSequence(QString *);

    bool calculateTolerance(double);

    // The results-exporting functions. ////////////////////////////////
    void prepareResultsTxtString();
    bool exportResultsToClipboard();
    bool exportResultsFile();
    bool selectResultsFile();
    bool calculateSpectrum();
    //////////////////////////////////// The results-exporting functions.

  public slots:
    void cleave();
    void exportResults(int);
    void filterOptions(bool);
    void filterOptionsToggled();
    void filterPartial();
    void filterMonoMass();
    void filterAvgMass();
    void filterCharge();
  };

} // namespace massXpert


#endif // CLEAVAGE_DLG_HPP
