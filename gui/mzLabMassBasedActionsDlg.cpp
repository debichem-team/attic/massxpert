/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006-2013 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.


   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Qt includes
#include <QMessageBox>
#include <QCloseEvent>
#include <QDebug>


/////////////////////// Local includes
#include "mzLabMassBasedActionsDlg.hpp"
#include "mzLabInputOligomerTableViewDlg.hpp"
#include "mzLabWnd.hpp"
#include "application.hpp"


namespace massXpert
{

  MzLabMassBasedActionsDlg::MzLabMassBasedActionsDlg
  (QWidget *parent) : QDialog(parent)
  {
    Q_ASSERT(parent);

    mp_mzLabWnd = static_cast<MzLabWnd *>(parent);

    m_ui.setupUi(this);

    setWindowTitle(tr("massXpert: mz Lab - Mass-based Actions"));
    
    connect(m_ui.applyMassPushButton,
            SIGNAL(clicked()),
            this,
            SLOT(applyMassPushButton()));

    connect(m_ui.applyThresholdPushButton,
            SIGNAL(clicked()),
            this,
            SLOT(applyThresholdPushButton()));

    QSettings settings 
     (static_cast<Application *>(qApp)->configSettingsFilePath(), 
       QSettings::IniFormat);
    
    settings.beginGroup("mz_lab_wnd_mass_based_actions");

    restoreGeometry(settings.value("geometry").toByteArray());

    settings.endGroup();
  }
  

  MzLabMassBasedActionsDlg::~MzLabMassBasedActionsDlg()
  {
  }


  void
  MzLabMassBasedActionsDlg::closeEvent(QCloseEvent *event)
  {
    QSettings settings 
     (static_cast<Application *>(qApp)->configSettingsFilePath(), 
       QSettings::IniFormat);
    
    settings.beginGroup("mz_lab_wnd_mass_based_actions");
    
    settings.setValue("geometry", saveGeometry());
    
    settings.endGroup();

    QDialog::closeEvent(event);
  }


  void
  MzLabMassBasedActionsDlg::applyMassPushButton()
  {
    // The actual work of doing the calculation is performed by the
    // MzLabInputOligomerTableViewDlg itself. So we have to get a
    // pointer to that dialog window. But first, check if the mass
    // entered in the line edit is correct.

    QString text = m_ui.massLineEdit->text();

    if (text.isEmpty())
      {
	QMessageBox::warning(this,
			      tr("massXpert: mz Lab"),
			      tr("%1@%2\n"
				  "Please, enter a valid mass.")
			      .arg(__FILE__)
			      .arg(__LINE__));
	return;
      }
    
    Application *application = static_cast<Application *>(qApp);
    QLocale locale = application->locale();
    
    bool ok = false;
    
    double mass = locale.toDouble(text, &ok);
    
    if (!mass && !ok)
      {
	QMessageBox::warning(this,
			      tr("massXpert: mz Lab"),
			      tr("%1@%2\n"
				  "Failed to convert %3 to double")
			      .arg(__FILE__)
			      .arg(__LINE__)
			      .arg(text));
	return;
      }

    // Get the pointer to the input list dialog window.

    MzLabInputOligomerTableViewDlg *dlg = 0;
    dlg = mp_mzLabWnd->inputListDlg();
    if (!dlg)
      {
        QMessageBox::warning(this,
                             tr("massXpert: mz Lab"),
                             tr("%1@%2\n"
                                "Please, select one input list first.")
                             .arg(__FILE__)
                             .arg(__LINE__));
	return;
      }
    
    // Is the calculation to be performed inside the input list dialog
    // window, or are we going to create a new mz list with the
    // results of that calculation?
    bool inPlace = mp_mzLabWnd->inPlaceCalculation();
    
    // Finally actually have the calculation performed by the mz input
    // list dialog window.
    dlg->applyMass(mass, inPlace);
  }


 void
  MzLabMassBasedActionsDlg::applyThresholdPushButton()
  {
    // The actual work of doing the calculation is performed by the
    // MzLabInputOligomerTableViewDlg itself. So we have to get a
    // pointer to that dialog window. But first, check if the threshold
    // entered in the line edit is empty/correct or not.

   QString text = m_ui.thresholdLineEdit->text();

    if (text.isEmpty())
      {
	QMessageBox::warning(this,
			      tr("massXpert: mz Lab"),
			      tr("%1@%2\n"
				  "Please, enter a valid threshold.")
			      .arg(__FILE__)
			      .arg(__LINE__));
	return;
      }


    Application *application = static_cast<Application *>(qApp);
    QLocale locale = application->locale();
    
    bool ok = false;
    
    double threshold = locale.toDouble(text, &ok);
    
    if (!threshold && !ok)
      {
	QMessageBox::warning(this,
			      tr("massXpert: mz Lab"),
			      tr("%1@%2\n"
				  "Failed to convert %3 to double")
			      .arg(__FILE__)
			      .arg(__LINE__)
			      .arg(text));
	return;
      }
    
    // Get the pointer to the input list dialog window.

    MzLabInputOligomerTableViewDlg *dlg = 0;
    dlg = mp_mzLabWnd->inputListDlg();
    if (!dlg)
      {
        QMessageBox::warning(this,
                             tr("massXpert: mz Lab"),
                             tr("%1@%2\n"
                                "Please, select one input list first.")
                             .arg(__FILE__)
                             .arg(__LINE__));
	return;
      }
    
    // Is the calculation to be performed inside the input list dialog
    // window, or are we going to create a new mz list with the
    // results of that calculation?
    bool inPlace = mp_mzLabWnd->inPlaceCalculation();

    // Are the calculations to be performed on mz values ?
    bool onMz = m_ui.mzThresholdRadioButton->isChecked();

    // Finally actually have the calculation performed by the mz input
    // list dialog window.
    dlg->applyThreshold(threshold, inPlace, onMz);
  }


} // namespace massXpert
