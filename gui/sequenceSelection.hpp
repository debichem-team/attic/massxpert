/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.filomace.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef SEQUENCE_SELECTION_HPP
#define SEQUENCE_SELECTION_HPP


/////////////////////// Qt includes
#include <QList>
#include <QRect>
#include <QGraphicsRectItem>
#include <QPointF>


/////////////////////// Local includes
#include "regionSelection.hpp"


namespace massXpert
{

  class SequenceEditorGraphicsView;
  class CoordinateList;
  
  class SequenceSelection
  {
  private:
    SequenceEditorGraphicsView *mp_view;
    QList<RegionSelection *> m_regionSelectionList;
    
  public:
    SequenceSelection(SequenceEditorGraphicsView * = 0);
    
    ~SequenceSelection(void);
    
    void setView(SequenceEditorGraphicsView *);
    SequenceEditorGraphicsView *view();
    
    const QList<RegionSelection *>& regionSelectionList() const;
        
    int selectRegion(const QPointF &, const QPointF &, 
		      bool = true, bool = false);
    int selectRegion(int, int, bool = true, bool = false);

    int reselectRegions();
    int deselectRegions();
    int deselectRegion(RegionSelection *regionSelection);
    int deselectLastRegion();
    int deselectRegionsButLast();
    int deselectMultiSelectionRegionsButFirstSelection();
    
    RegionSelection *regionSelectionEncompassing(const QPointF &, 
						  const QPointF &,
						  int * = 0);
    
    RegionSelection *regionSelectionEncompassing(int, int,
						  int * = 0);
    
    double manhattanLength();
    bool isManhattanLength(const QPointF &, const QPointF &);
    
    int regionSelectionCount();

    bool selectionIndices(CoordinateList * = 0);
        
    void debugSelectionPutStdErr();
  };
      
} // namespace massXpert

#endif // SEQUENCE_SELECTION_HPP
