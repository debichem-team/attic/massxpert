/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.filomace.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef CONFIGURATION_SETTINGS_DLG_HPP
#define CONFIGURATION_SETTINGS_DLG_HPP


/////////////////////// Qt includes
#include <QDir>


/////////////////////// Local includes
#include "ui_configurationSettingsDlg.h"
#include "configSettings.hpp"


namespace massXpert
{

  class ConfigurationSettingsDlg : public QDialog
  {
    Q_OBJECT

    private:
    Ui::ConfigurationSettingsDlg m_ui;

    void closeEvent(QCloseEvent *event);
    
    ConfigSettings *mp_configSettings;
    

  public:
    ConfigurationSettingsDlg(ConfigSettings * = 0);
    ~ConfigurationSettingsDlg();
  
    bool initialize();

    bool checkDataDir(const QDir &);
    bool checkLocalizationDir(const QDir &);
				
  public slots:
    void browseDataDirPushButtonClicked();
    void browseLocalizationDirPushButtonClicked();

    void saveSettingsPushButtonClicked();
    void eraseSettingsPushButtonClicked();
    void cancelPushButtonClicked();
  };
  
} // namespace massXpert


#endif // CONFIGURATION_SETTINGS_DLG_HPP
 
