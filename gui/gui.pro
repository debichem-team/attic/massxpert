TEMPLATE = app
LANGUAGE = C++
TARGET="massXpert"

# for i in $(ls -1 *.hpp | grep -v ^moc.* | grep -v ^qrc.*) ; do echo "    $i" \\;done
HEADERS += \
    aboutDlg.hpp \
    about_gpl_v20.hpp \
    about_gpl_v30.hpp \
    about_history.hpp \
    application.hpp \
    atomDefDlg.hpp \
    calculatorChemPadDlg.hpp \
    calculatorRecorderDlg.hpp \
    calculatorWnd.hpp \
    chemPadButton.hpp \
    cleavageDlg.hpp \
    cleaveOligomerTreeView.hpp \
    cleaveOligomerTreeViewItem.hpp \
    cleaveOligomerTreeViewMimeData.hpp \
    cleaveOligomerTreeViewModel.hpp \
    cleaveOligomerTreeViewSortProxyModel.hpp \
    cleaveSpecDefDlg.hpp \
    compositionTreeView.hpp \
    compositionTreeViewItem.hpp \
    compositionTreeViewModel.hpp \
    compositionTreeViewSortProxyModel.hpp \
    compositionsDlg.hpp \
    configurationSettingsDlg.hpp \
    crossLinkerDefDlg.hpp \
    fragSpecDefDlg.hpp \
    fragmentOligomerTreeView.hpp \
    fragmentOligomerTreeViewItem.hpp \
    fragmentOligomerTreeViewModel.hpp \
    fragmentOligomerTreeViewSortProxyModel.hpp \
    fragmentationDlg.hpp \
    isotopicPatternCalculationDlg.hpp \
    mainWindow.hpp \
    massSearchDlg.hpp \
    massSearchOligomerTreeView.hpp \
    massSearchOligomerTreeViewItem.hpp \
    massSearchOligomerTreeViewModel.hpp \
    massSearchOligomerTreeViewSortProxyModel.hpp \
    modifDefDlg.hpp \
    monomerCodeEvaluator.hpp \
    monomerCrossLinkDlg.hpp \
    monomerDefDlg.hpp \
    monomerModificationDlg.hpp \
    mzCalculationDlg.hpp \
    mzCalculationTreeView.hpp \
    mzCalculationTreeViewItem.hpp \
    mzCalculationTreeViewModel.hpp \
    mzCalculationTreeViewSortProxyModel.hpp \
    mzLabInputOligomerTreeView.hpp \
    mzLabInputOligomerTreeViewDlg.hpp \
    mzLabInputOligomerTreeViewItem.hpp \
    mzLabInputOligomerTreeViewModel.hpp \
    mzLabInputOligomerTreeViewSortProxyModel.hpp \
    mzLabOutputOligomerTreeView.hpp \
    mzLabOutputOligomerTreeViewDlg.hpp \
    mzLabOutputOligomerTreeViewItem.hpp \
    mzLabOutputOligomerTreeViewModel.hpp \
    mzLabOutputOligomerTreeViewSortProxyModel.hpp \
    mzLabWnd.hpp \
    pkaPhPiDlg.hpp \
    plugin_interfaces.hpp \
    polChemDefWnd.hpp \
    polymerModificationDlg.hpp \
    sequenceEditorFindDlg.hpp \
    sequenceEditorGraphicsView.hpp \
    sequenceEditorGraphicsViewKeySequenceHandling.hpp \
    sequenceEditorGraphicsViewKeyboardHandling.hpp \
    sequenceEditorWnd.hpp \
    sequencePurificationDlg.hpp
# for i in $(ls -1 *.cpp | grep -v ^moc.* | grep -v ^qrc.*) ; do echo "    $i" \\;done
SOURCES += \
    aboutDlg.cpp \
    application.cpp \
    atomDefDlg.cpp \
    calculatorChemPadDlg.cpp \
    calculatorRecorderDlg.cpp \
    calculatorWnd.cpp \
    chemPadButton.cpp \
    cleavageDlg.cpp \
    cleaveOligomerTreeView.cpp \
    cleaveOligomerTreeViewItem.cpp \
    cleaveOligomerTreeViewMimeData.cpp \
    cleaveOligomerTreeViewModel.cpp \
    cleaveOligomerTreeViewSortProxyModel.cpp \
    cleaveSpecDefDlg.cpp \
    compositionTreeView.cpp \
    compositionTreeViewItem.cpp \
    compositionTreeViewModel.cpp \
    compositionTreeViewSortProxyModel.cpp \
    compositionsDlg.cpp \
    configurationSettingsDlg.cpp \
    crossLinkerDefDlg.cpp \
    fragSpecDefDlg.cpp \
    fragmentOligomerTreeView.cpp \
    fragmentOligomerTreeViewItem.cpp \
    fragmentOligomerTreeViewModel.cpp \
    fragmentOligomerTreeViewSortProxyModel.cpp \
    fragmentationDlg.cpp \
    isotopicPatternCalculationDlg.cpp \
    main.cpp \
    mainWindow.cpp \
    massSearchDlg.cpp \
    massSearchOligomerTreeView.cpp \
    massSearchOligomerTreeViewItem.cpp \
    massSearchOligomerTreeViewModel.cpp \
    massSearchOligomerTreeViewSortProxyModel.cpp \
    modifDefDlg.cpp \
    monomerCodeEvaluator.cpp \
    monomerCrossLinkDlg.cpp \
    monomerDefDlg.cpp \
    monomerModificationDlg.cpp \
    mzCalculationDlg.cpp \
    mzCalculationTreeView.cpp \
    mzCalculationTreeViewItem.cpp \
    mzCalculationTreeViewModel.cpp \
    mzCalculationTreeViewSortProxyModel.cpp \
    mzLabInputOligomerTreeView.cpp \
    mzLabInputOligomerTreeViewDlg.cpp \
    mzLabInputOligomerTreeViewItem.cpp \
    mzLabInputOligomerTreeViewModel.cpp \
    mzLabInputOligomerTreeViewSortProxyModel.cpp \
    mzLabOutputOligomerTreeView.cpp \
    mzLabOutputOligomerTreeViewDlg.cpp \
    mzLabOutputOligomerTreeViewItem.cpp \
    mzLabOutputOligomerTreeViewModel.cpp \
    mzLabOutputOligomerTreeViewSortProxyModel.cpp \
    mzLabWnd.cpp \
    pkaPhPiDlg.cpp \
    polChemDefWnd.cpp \
    polymerModificationDlg.cpp \
    sequenceEditorFindDlg.cpp \
    sequenceEditorGraphicsView.cpp \
    sequenceEditorGraphicsViewKeySequenceHandling.cpp \
    sequenceEditorGraphicsViewKeyboardHandling.cpp \
    sequenceEditorWnd.cpp \
    sequencePurificationDlg.cpp

# for i in $(ls -1 ui/*.ui) ; do echo "    $i" \\;done
FORMS += \
    ui/aboutDlg.ui \
    ui/atomDefDlg.ui \
    ui/calculatorChemPadDlg.ui \
    ui/calculatorRecorderDlg.ui \
    ui/calculatorWnd.ui \
    ui/cleavageDlg.ui \
    ui/cleaveSpecDefDlg.ui \
    ui/compositionsDlg.ui \
    ui/configurationSettingsDlg.ui \
    ui/crossLinkerDefDlg.ui \
    ui/fragSpecDefDlg.ui \
    ui/fragmentationDlg.ui \
    ui/isotopicPatternCalculationDlg.ui \
    ui/massSearchDlg.ui \
    ui/modifDefDlg.ui \
    ui/monomerCrossLinkDlg.ui \
    ui/monomerDefDlg.ui \
    ui/monomerModificationDlg.ui \
    ui/mzCalculationDlg.ui \
    ui/mzLabInputOligomerTreeViewDlg.ui \
    ui/mzLabOutputOligomerTreeViewDlg.ui \
    ui/mzLabWnd.ui \
    ui/pkaPhPiDlg.ui \
    ui/polChemDefWnd.ui \
    ui/polymerModificationDlg.ui \
    ui/sequenceEditorFindDlg.ui \
    ui/sequenceEditorWnd.ui \
    ui/sequencePurificationDlg.ui

CONFIG += qt warn_on release

QT += xml
QT += svg
QT += network

PRE_TARGETDEPS += ../lib/libmasslib.a

INCLUDEPATH += -I ../ ../lib

LIBS += ../lib/libmasslib.a

RESOURCES = application.qrc

ICON = images/massXpert.icns

UI_DIR = ../../build-work/$(TARGET)-ui
MOC_DIR = ../../build-work/$(TARGET)-moc
OBJECTS_DIR = ../../build-work/$(TARGET)-obj
##universal tiger
CONFIG += link_prl x86 ppc
QMAKE_MAC_SDK=/Developer/SDKs/MacOSX10.4u.sdk
QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.3
target.path = /Applications
INSTALLS = target

ICONS.files = images/massXpert.icns
ICONS.path = Contents/Resources
QMAKE_BUNDLE_DATA += ICONS

DATA.files = ../data
DATA.path = Contents/Resources
QMAKE_BUNDLE_DATA += DATA

PLUGINS.files += ../../build-work/Release/libmassListSorterPlugin.dylib
PLUGINS.files += ../../build-work/Release/libnumeralsLocaleConverterPlugin.dylib
PLUGINS.files += ../../build-work/Release/libseqToolsPlugin.dylib

PLUGINS.path = Contents/Plugins
QMAKE_BUNDLE_DATA += PLUGINS

TRANSLATIONS.files += massxpert_fr.qm
TRANSLATIONS.path = Contents/Locales
QMAKE_BUNDLE_DATA += TRANSLATIONS

QMAKE_INFO_PLIST = Info.plist

