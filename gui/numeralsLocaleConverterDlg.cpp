/* massXpert - the true massist's program.
   --------------------------------------
   Copyright (C) 2006,2007 Filippo Rusconi

   http://www.filomace.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package (see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique (FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.


   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include<QtGlobal>
#include<QLocale>
#include<QMainWindow>

#include "numeralsLocaleConverterDlg.hpp"
#include "massList.hpp"

using massXpert::MassList;


NumeralsLocaleConverterDlg::NumeralsLocaleConverterDlg 
(QWidget *parent)
  : QDialog(parent, Qt::Dialog)
{
  mp_parent = static_cast<QMainWindow *>(parent);
  
  m_ui.setupUi(this);

  setAttribute(Qt::WA_DeleteOnClose);
  
  connect(mp_parent, 
	   SIGNAL(aboutToClose()),
	   this,
	   SLOT(parentClosing()));

  connect(m_ui.convertPushButton,
	   SIGNAL(clicked()),
	   this,
	   SLOT(convert()));
}


void
NumeralsLocaleConverterDlg::parentClosing()
{
  QDialog::reject();
}




void 
NumeralsLocaleConverterDlg::convert()
{
  // The input/output locales?

  QString inputLocaleString = m_ui.inputLocaleLineEdit->text();
  QString outputLocaleString = m_ui.outputLocaleLineEdit->text();
  
  // Check if these are correct locales.

  QLocale inputLocale(inputLocaleString);
  QLocale outputLocale(outputLocaleString);
  
  // What's the text to work on?
  QString docText = m_ui.inputTextEdit->toPlainText();
  
  // Make a mass list with the text.
  MassList inputMassList("INPUT_MASS_LIST", 
			     docText, inputLocaleString);

  // Check the result.
  if(inputMassList.makeMassList() == -1)
    return;
  
  // Before converting the numerical list to text, we need to change
  // the locale.
  inputMassList.setLocale(outputLocaleString);
  
  // Convert list to text.
  inputMassList.makeMassText();
  
  // Clear the previous output text edit widget.
  m_ui.outputTextEdit->clear();

  // Output the new text.  
  m_ui.outputTextEdit->setPlainText(inputMassList.massText());
}

