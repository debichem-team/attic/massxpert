/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef SEQUENCE_PURIFICATION_DLG_HPP
#define SEQUENCE_PURIFICATION_DLG_HPP


/////////////////////// Local includes
#include "ui_sequencePurificationDlg.h"
#include "sequence.hpp"
#include "sequenceEditorWnd.hpp"


namespace massXpert
{

  class SequencePurificationDlg : public QDialog
  {
    Q_OBJECT
  
    private:
    Ui::SequencePurificationDlg m_ui;
    SequenceEditorWnd *mp_editorWnd;

    bool m_firstRound;
    Sequence *mpa_origSequence;
    QList<int> m_errorList;
    QTextDocument m_leftTextDocument;
    QTextDocument m_rightTextDocument;
  
  public:
    SequencePurificationDlg(QWidget *, Sequence *, QList<int> *);
  
    ~SequencePurificationDlg();
  
    SequenceEditorWnd *editorWnd();
  
    void setupDocuments();
  
    void doTest(QTextDocument &);  
    void setOrigText();
    bool findContinuum(QList<int> &list, int, int&);
  
  public slots:
    void purify();
    void test();
    void removeTagged();
    void reset();
  };

} // namespace massXpert


#endif // SEQUENCE_PURIFICATION_DLG_HPP
