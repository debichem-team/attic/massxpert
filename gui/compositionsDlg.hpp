/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef COMPOSITIONS_DLG_HPP
#define COMPOSITIONS_DLG_HPP


/////////////////////// Qt includes
#include <QMainWindow>
#include <QSortFilterProxyModel>


/////////////////////// Local includes
#include "ui_compositionsDlg.h"
#include "sequenceEditorWnd.hpp"
#include "compositionTreeViewModel.hpp"
#include "compositionTreeViewSortProxyModel.hpp"


namespace massXpert
{

  enum
    {
      MXP_TARGET_ELEMENTAL = 0,
      MXP_TARGET_MONOMERIC = 1
    };


  class CompositionTreeViewModel;
  class CompositionTreeViewSortProxyModel;

  class CompositionsDlg : public QDialog
  {
    Q_OBJECT
  
    private:
    Ui::CompositionsDlg m_ui;

    // The results-exporting strings. ////////////////////////////////
    QString *mpa_resultsString;
    QString m_resultsFilePath;
    //////////////////////////////////// The results-exporting strings.

    SequenceEditorWnd *mp_editorWnd;

    const QPointer<Polymer> mp_polymer;
    CalcOptions *mp_calcOptions;
    IonizeRule *mp_ionizeRule;


    CoordinateList m_coordinateList;

    QList<Monomer *> m_monomerList;

    CompositionTreeViewModel *mpa_compositionTreeViewModel;
    CompositionTreeViewSortProxyModel *mpa_compositionProxyModel;
    CompositionTreeViewSortProxyModel *mpa_avgProxyModel;
  
    bool fetchValidateInputData();
  
    void closeEvent(QCloseEvent *event); 
  
  public:
    CompositionsDlg(QWidget *, Polymer *, 
		     CalcOptions *, IonizeRule *);
  
    ~CompositionsDlg();
  
    SequenceEditorWnd *editorWnd();
  
    void setupTreeView();
  
    void updateIonizationData();

    void freeMonomerList();

    // The results-exporting functions. ////////////////////////////////
    void prepareResultsTxtString(int);
    bool exportResultsClipboard();
    bool exportResultsFile();
    bool selectResultsFile();
    //////////////////////////////////// The results-exporting functions.

  public slots:
    void updateSelectionData();
    void monomericComposition();
    void elementalComposition();
    void exportResults(int);
  };

} // namespace massXpert


#endif // COMPOSITIONS_DLG_HPP

