/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#ifndef MZ_LAB_WND_HPP
#define MZ_LAB_WND_HPP


/////////////////////// Qt includes
#include <QMainWindow>


/////////////////////// Local includes
#include "ui_mzLabWnd.h"
#include "polChemDef.hpp"


namespace massXpert
{

  class MzLabInputOligomerTableViewDlg;
  class MainWindow;
  class OligomerList;
  class OligomerPair;
  
  class MzLabWnd : public QMainWindow
  {
    Q_OBJECT

    private:
    Ui::MzLabWnd m_ui;

    QList<double> m_listM1;
    QList<double> m_listM2;

    QStringList m_modifList;
        
    PolChemDef m_polChemDef;
    
    QList<MzLabInputOligomerTableViewDlg *> m_dlgList;
    MainWindow *mp_mainWindow;

    IonizeRule m_ionizeRule;

    void destroyDlg(const QString &);

    double calculateTolerance(double);
				      
  private slots:
    void massBasedActionsPushButton();
    void formulaBasedActionsPushButton();
    void matchBasedActionsPushButton();
    
    void deleteInputListItem();
    void inputListWidgetItemClicked(QListWidgetItem *);

    
    void writeSettings();

    void updateWindowTitle();

    void ionizationChargeChanged(int);
    void ionizationLevelChanged(int);
    void ionizationFormulaChanged(const QString &);
            
  protected:
    void closeEvent(QCloseEvent *event); 

  public:
    bool m_forciblyClose;
    bool m_noDelistWnd;
    
    MzLabWnd(QString &);
    ~MzLabWnd();

    bool initialize();

    const PolChemDef &polChemDef() const;
    PolChemDef *polChemDef();
    
    const IonizeRule &ionizeRule() const;
    
    MzLabInputOligomerTableViewDlg *findDlg(const QString &);
    MzLabInputOligomerTableViewDlg *newInputList(QString, MassType);

    bool inputListDlg(MzLabInputOligomerTableViewDlg **);
    bool inputListsDlg(MzLabInputOligomerTableViewDlg **,
                       MzLabInputOligomerTableViewDlg **);
    MzLabInputOligomerTableViewDlg *inputListDlg();

    bool inPlaceCalculation();
    
    bool maybeSave();

  public slots:
    MzLabInputOligomerTableViewDlg *newInputList();
  };

} // namespace massXpert


#endif /* MZ_LAB_WND_HPP */
