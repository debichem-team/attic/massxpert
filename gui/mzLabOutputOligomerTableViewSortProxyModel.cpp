/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// Local includes
#include "mzLabOutputOligomerTableViewModel.hpp"
#include "mzLabOutputOligomerTableViewSortProxyModel.hpp"
#include "application.hpp"


namespace massXpert
{

  MzLabOutputOligomerTableViewSortProxyModel::
  MzLabOutputOligomerTableViewSortProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
  {
  }


  MzLabOutputOligomerTableViewSortProxyModel::
  ~MzLabOutputOligomerTableViewSortProxyModel()
  {
  }

  
  bool 
  MzLabOutputOligomerTableViewSortProxyModel::lessThan
  (const QModelIndex &left, const QModelIndex &right) const
  {
    Application *application = static_cast<Application *>(qApp);
    QLocale locale = application->locale();

    QVariant leftData = sourceModel()->data(left);
    QVariant rightData = sourceModel()->data(right);
  
    // qDebug() << __FILE__ << __LINE__
    //          << "leftData:" << leftData
    //          << "rightData:" << rightData;
    
    if(leftData.type() == QVariant::Int)
      {
	// The Partial column
        
	int leftValue = leftData.toInt();
	int rightValue = rightData.toInt();
        
	return(leftValue < rightValue);
      }
    else if(leftData.type() == QVariant::Bool)
      {
	// The Modif column
        
	int leftValue = leftData.toInt();
	int rightValue = rightData.toInt();
        
	return(leftValue < rightValue);
      }
    else if(leftData.type() == QVariant::Double)
      {
	bool ok = false;
        
	double leftValue = leftData.toDouble(&ok);
	Q_ASSERT(ok);
	double rightValue = rightData.toDouble(&ok);
	Q_ASSERT(ok);
        
        // qDebug() << __FILE__ << __LINE__
        //          << "leftval:" << leftValue
        //          << "rightval:" << rightValue;
        
	return(leftValue < rightValue);
      }
    else if(leftData.type() == QVariant::String)
      {
	QString leftString = leftData.toString();
	QString rightString = rightData.toString();
        
        // What's the column ?
        
        int column = left.column();
        
        if(column == MZ_LAB_OUTPUT_OLIGO_1_MASS_COLUMN)
          {
            bool ok = false;
            
            double leftValue = locale.toDouble(leftString, &ok);
            Q_ASSERT(ok);
            double rightValue = locale.toDouble(rightString, &ok);
            Q_ASSERT(ok);
            
            // qDebug() << __FILE__ << __LINE__
            //          << "leftval:" << leftValue
            //          << "rightval:" << rightValue;
            
            return(leftValue < rightValue);
          }
        else if(column == MZ_LAB_OUTPUT_OLIGO_1_CHARGE_COLUMN)
          {
            bool ok = false;
            
            int leftValue = locale.toInt(leftString, &ok);
            Q_ASSERT(ok);
            int rightValue = locale.toInt(rightString, &ok);
            Q_ASSERT(ok);
            
            // qDebug() << __FILE__ << __LINE__
            //          << "leftval:" << leftValue
            //          << "rightval:" << rightValue;
            
            return(leftValue < rightValue);
          }
        else if(column == MZ_LAB_OUTPUT_OLIGO_2_MASS_COLUMN)
          {
            bool ok = false;
            
            double leftValue = locale.toDouble(leftString, &ok);
            Q_ASSERT(ok);
            double rightValue = locale.toDouble(rightString, &ok);
            Q_ASSERT(ok);
            
            // qDebug() << __FILE__ << __LINE__
            //          << "leftval:" << leftValue
            //          << "rightval:" << rightValue;
            
            return(leftValue < rightValue);
          }
        else if(column == MZ_LAB_OUTPUT_OLIGO_2_CHARGE_COLUMN)
          {
            bool ok = false;
            
            int leftValue = locale.toInt(leftString, &ok);
            Q_ASSERT(ok);
            int rightValue = locale.toInt(rightString, &ok);
            Q_ASSERT(ok);
            
            // qDebug() << __FILE__ << __LINE__
            //          << "leftval:" << leftValue
            //          << "rightval:" << rightValue;
            
            return(leftValue < rightValue);
          }
        else if(column == MZ_LAB_OUTPUT_OLIGO_ERROR_COLUMN)
          {
            bool ok = false;
            
            double leftValue = locale.toDouble(leftString, &ok);
            Q_ASSERT(ok);
            double rightValue = locale.toDouble(rightString, &ok);
            Q_ASSERT(ok);
            
            // qDebug() << __FILE__ << __LINE__
            //          << "leftval:" << leftValue
            //          << "rightval:" << rightValue;
            
            return(leftValue < rightValue);
          }
        else if(column == MZ_LAB_OUTPUT_OLIGO_MATCH_COLUMN)
          {
            bool ok = false;
            
            int leftValue = locale.toInt(leftString, &ok);
            Q_ASSERT(ok);
            int rightValue = locale.toInt(rightString, &ok);
            Q_ASSERT(ok);
            
            // qDebug() << __FILE__ << __LINE__
            //          << "leftval:" << leftValue
            //          << "rightval:" << rightValue;
            
            return(leftValue < rightValue);
          }
      }
    
    return true;
  }


} // namespace massXpert
