/* massXpert - the true massist's program.
   -------------------------------------- 
   Copyright(C) 2006, 2007 Filippo Rusconi

   http://www.filomace.org/massXpert

   This file is part of the massXpert project.
   
   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   
   
   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the 

   Free Software Foundation, Inc.,
   
   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Qt includes
#include <QApplication>
#include <QTranslator>
#include <QtGlobal>
#include <QTimer>


/////////////////////// Std includes
#include <iostream>


/////////////////////// Local includes
#include "globals.hpp"
#include "config.h"
#include "application.hpp"
#include "mainWindow.hpp"


using std::cout;

using massXpert::Application;
using massXpert::MainWindow;

void printGreetings();
void printHelp();
void printVersion();
void printConfig();


int 
main(int argc, char **argv)
{
  Q_INIT_RESOURCE(application);
  
  for (int iter = 0 ; iter < argc ; ++iter)
    {
      QString argument = argv [iter];

      if (argument == "--help" ||
	  argument == "-h" ||
	  argument == "?")
	{
	  printHelp();
	  return 0;
	}
      else if (argument == "--version" ||
	       argument == "-v")
	{
	  printVersion();
	  return 0;
	}
      else if (argument == "--config" ||
	       argument == "-c")
	{
	  printConfig();
	  return 0;
	}
      else
	printGreetings();
    }
        

  // Qt stuff starts here.
  Application application(argc, argv);

  ////////////////////////////////// Linguist/Localization stuff.
  ////////////////////////////////// Linguist/Localization stuff.
  ////////////////////////////////// Linguist/Localization stuff.

  QString locale = QLocale::system().name();
  QTranslator translator;

  QString fileName(QString("%1_").arg(TARGET) + locale);

//   qDebug() << __FILE__ << __LINE__ 
// 	    << "Current QLocale is:" << locale
// 	    << "Loading corresponding translation:"
// 	    << QString(application.configSettings()->
// 			systemLocalizationDir() + 
// 			QDir::separator() + fileName);
  
  if (!translator.load(fileName, application.configSettings()->
			systemLocalizationDir()))
    {
      qDebug() << __FILE__ << __LINE__ 
	       << QObject::tr("Failed loading the translation file:")
	       << QString(application.configSettings()->
			  systemLocalizationDir() + 
			  QDir::separator() + fileName)
	       << "\n";
    }
  
  application.installTranslator(&translator);

  ////////////////////////////////// Linguist/Localization stuff.

  application.processEvents();
    
  // We do not do this anymore.
  //    application.sendUsagePing();
    
  MainWindow mainWin;
  mainWin.show();

  return application.exec();
}


void 
printHelp()
{
  QString help = QObject::tr("The following options are available:\n");
  help += QObject::tr("? | -h | --help : print this help\n");
  help += QObject::tr("-v | --version : print version\n");
  help += QObject::tr("-c | --config : print configuration\n");
  help += QObject::tr("\n");
  
  cout << help.toStdString() ;
}


void 
printGreetings()
{
  QString version(VERSION);
  

  QString greetings = QObject::tr("massXpert, version %1\n\n")
    .arg(VERSION);

  greetings += QObject::tr("Type 'massXpert --help' for help\n\n");
  
  greetings += QObject::tr("massXpert is Copyright 2000, 2001,\n"
			    "2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009 \n"
			    "by Filippo Rusconi.\n\n"
			    "massXpert comes with ABSOLUTELY NO WARRANTY.\n"
			    "massXpert is free software, "
			    "covered by the GNU General\n"
			    "Public License Version 3, "
			    "and you are welcome to change it\n"
			    "and/or distribute copies of it under "
			    "certain conditions.\n"
			    "Check the file COPYING in the distribution "
			    "and/or the\n"
			    "'Help/About(Ctrl+H)' menu item of the program.\n"
			    "\nHappy massXpert'ing!\n\n");


  cout << greetings.toStdString();
}


void 
printVersion()
{
  QString version = QObject::tr("massXpert, version %1 -- "
				 "Compiled against Qt, version %2\n")
    .arg(VERSION)
    .arg(QT_VERSION_STR);
  
  cout << version.toStdString();
}


void 
printConfig()
{
  QString config = QObject::tr("massXpert: "
				"Compiled with the following configuration:\n"
				"EXECUTABLE BINARY FILE: = %1\n"
				"MASSXPERT_BIN_DIR = %2\n"
				"MASSXPERT_DATA_DIR = %3\n"
				"MASSXPERT_LOCALE_DIR = %4\n"
				"MASSXPERT_USERMAN_DIR = %5\n")
    .arg(TARGET)
    .arg(MASSXPERT_BIN_DIR)
    .arg(MASSXPERT_DATA_DIR)
    .arg(MASSXPERT_LOCALE_DIR)
    .arg(MASSXPERT_USERMAN_DIR);
  
  cout << config.toStdString();
}

