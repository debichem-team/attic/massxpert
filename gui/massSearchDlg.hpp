/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef MASS_SEARCH_DLG_HPP
#define MASS_SEARCH_DLG_HPP


/////////////////////// Qt includes
#include <QMainWindow>
#include <QSortFilterProxyModel>


/////////////////////// Local includes
#include "ui_massSearchDlg.h"
#include "sequenceEditorWnd.hpp"
#include "massSearchOligomerTableViewModel.hpp"
#include "massSearchOligomerTableViewSortProxyModel.hpp"


namespace massXpert
{

  class MassSearchOligomerTableViewModel;
  class MassSearchOligomerTableViewSortProxyModel;

  class MassSearchDlg : public QDialog
  {
    Q_OBJECT
  
    private:
    Ui::MassSearchDlg m_ui;
    SequenceEditorWnd *mp_editorWnd;

    // The results-exporting strings. ////////////////////////////////
    QString *mpa_resultsString;
    QString m_resultsFilePath;
    //////////////////////////////////// The results-exporting strings.

    const QPointer<Polymer> mp_polymer;
    CalcOptions m_calcOptions;
    IonizeRule *mp_ionizeRule;

    double m_monoTolerance;
    double m_avgTolerance;

    double m_currentMass;
    int m_foundOligosCount;
    int m_testedOligosCount;
    int m_progressValue;
    
    int m_ionizeStart;
    int m_ionizeEnd;

    bool m_abort;
    bool m_sequenceEmbedded;

    // For the filtering of the data in the treeview.
    QAction *monoFilterAct;
    QAction *avgFilterAct;
    double m_filterTolerance;
    QWidget *mp_monoFocusWidget;
    QWidget *mp_avgFocusWidget;

    QList<double> m_monoMassesList;
    QList<double> m_avgMassesList;

    OligomerList m_monoOligomerList;
    OligomerList m_avgOligomerList;

    MassSearchOligomerTableViewModel *mpa_monoOligomerTableViewModel;
    MassSearchOligomerTableViewModel *mpa_avgOligomerTableViewModel;

    MassSearchOligomerTableViewSortProxyModel *mpa_monoProxyModel;
    MassSearchOligomerTableViewSortProxyModel *mpa_avgProxyModel;
  
    void closeEvent(QCloseEvent *event); 
  
  public:
    MassSearchDlg(QWidget *, Polymer *, const CalcOptions &, IonizeRule *);
  
    ~MassSearchDlg();
  
    SequenceEditorWnd *editorWnd();
  
    bool populateSelectedOligomerData();
    bool populateToleranceTypeComboBoxes();
    void setupTableViews();
  
    void updateIonizationData();
    void updateProgressDetails(int, bool = false, Oligomer * = 0);
  
    void updateMassSearchDetails(const CalcOptions &);
    void updateOligomerSequence(QString *);
  
    bool checkTolerance(int type);
    bool calculateTolerance(double, int);
    bool calculateFilterTolerance(double, int);

    bool parseMassText(int);
    bool searchMasses(int);
    bool searchMass(double, const Coordinates &, int);
  
    void freeAllOligomerLists();
    void emptyAllMassLists();

    bool updateSelectionData(bool = true);

    // The results-exporting functions. ////////////////////////////////
    void prepareResultsTxtString();
    bool exportResultsClipboard();
    bool exportResultsFile();
    bool selectResultsFile();
    //////////////////////////////////// The results-exporting functions.

  public slots:

    void updateWholeSelectedSequenceData();
    
    void search();
    void abort();

    void exportResults(int);

    // MONO set of filtering widgets
    void monoFilterOptions(bool);
    void monoFilterOptionsToggled();

    void monoFilterSearched();
    void monoFilterError();
    void monoFilterMonoMass();
    void monoFilterAvgMass();

    // AVG set of filtering widgets
    void avgFilterOptions(bool);
    void avgFilterOptionsToggled();

    void avgFilterSearched();
    void avgFilterError();
    void avgFilterMonoMass();
    void avgFilterAvgMass();
  };

} // namespace massXpert


#endif // MASS_SEARCH_DLG_HPP

