Name:           massxpert
Version:        2.1.0
Release:        1%{?dist}
Summary:        Mass Spectrometry

Group:          Applications/Engineering
License:        GPLv3
URL:            http://massxpert.org
Source0:        http://download.tuxfamily.org/massxpert/source/massxpert-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  qt-devel
BuildRequires:  libICE-devel
BuildRequires:  libSM-devel
BuildRequires:  libX11-devel
BuildRequires:  libXcursor-devel
BuildRequires:  libXext-devel
BuildRequires:  libXfixes-devel
BuildRequires:  libXi-devel
BuildRequires:  libXinerama-devel
BuildRequires:  libXrandr-devel
BuildRequires:  libXrender-devel
BuildRequires:  fontconfig-devel
BuildRequires:  libpng-devel
BuildRequires:  zlib-devel
BuildRequires:  cmake
BuildRequires:  desktop-file-utils
BuildRequires:  tex(latex)
Requires:       qt-x11

%description
This package contains the massxpert mass spectrometric software suite,
a software program that aims at letting users predict/analyze mass
spectrometric data on (bio)polymers.


%package doc
Summary:        Documentation for %{name}
Group:          Documentation
License:        GPLv3
BuildArch:      noarch
%description doc
This package contains the massxpert documentation.
One single, big PDF file.



%prep
%setup -q -n massxpert-%{version}


%build
%cmake
make %{?_smp_mflags}

# build the manual (don't install it, include it in %doc)
pushd usermanual
    %cmake -DMASSXPERT_USERMAN_DIR=/dev/null -DLATEX_OUTPUT_PATH=./doc
    make
popd


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

# COPYING file is installed automatically (but wrongly)
rm -rf %{buildroot}%{_defaultdocdir}

mkdir -p %{buildroot}%{_datadir}/pixmaps
install -p -m 644 gui/images/massxpert-icon-32.xpm %{buildroot}%{_datadir}/pixmaps/
desktop-file-validate %{buildroot}%{_datadir}/applications/massxpert.desktop


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc COPYING
%{_bindir}/massxpert
%{_datadir}/massxpert/
%{_datadir}/applications/massxpert.desktop
%{_datadir}/pixmaps/massxpert-icon-32.xpm
%{_libdir}/massxpert/
%{_mandir}/man*/massxpert*.gz


%files doc
%defattr(-,root,root,-)
%doc usermanual/doc/massxpert.pdf


%changelog
* Thu Jan 14 2010 Thomas Spura <tomspur@fedoraproject.org> - 2.1.0-1
- new version

* Thu Dec 22 2009 Thomas Spura <tomspur@fedoraproject.org> - 2.0.9-1
- new version
- create doc subpackage (with one big file)
- delete cmake-libdir.patch, applied in this version

* Sat Oct 24 2009 Thomas Spura <tomspur@fedoraproject.org> - 2.0.7-2
- patch cmake-libdir, so that cmake installs into %%{_libdir} also on 64-bit
  systems

* Sat Oct 24 2009 Thomas Spura <tomspur@fedoraproject.org> - 2.0.7-1
- Initial package
