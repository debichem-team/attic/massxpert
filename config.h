#define VERSION "3.6.0"
#define TARGET ""

#define MASSXPERT_BIN_DIR "/usr/local/bin"
#define MASSXPERT_DATA_DIR "/usr/local/share/massxpert"
#define MASSXPERT_PLUGIN_DIR "/usr/local/lib/massxpert/plugins"
#define MASSXPERT_LOCALE_DIR "/usr/local/share/massxpert/locales"
#define MASSXPERT_USERMAN_DIR "/usr/local/share/doc/massxpert/usermanual"
