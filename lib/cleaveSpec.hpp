/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef CLEAVE_SPEC_HPP
#define CLEAVE_SPEC_HPP


/////////////////////// Qt includes
#include <QString>
#include <QList>


/////////////////////// Local includes
#include "cleaveMotif.hpp"
#include "cleaveRule.hpp"


namespace massXpert
{

  //! The CleaveSpec class provides a cleavage specification.
  /*! Cleavage specifications determine the specificity of cleavage in a
    polymer seuqence using a simple syntax. For example, trypsin is able
    to cleave after lysyl and arginyl residues. Its cleavage pattern is
    thus "Lys/;Arg/;-Lys/Pro". Note that a provision can be made: if a
    lysyl residue is followed by a prolyl residue, then it is not
    cleaved by trypsin.

    A cleavage specification might not be enough information to
    determine the manner in which a polymer is cleaved. Cleavage rules
    might be required to refine the specification.  A cleavage
    specification might hold as many cleavage rules as required.
  */
  class CleaveSpec : public PolChemDefEntity
  {
  private:
    //! Pattern.
    QString m_pattern;

    //! List of cleavage motifs.
    QList<CleaveMotif *> m_motifList; // "Lys/" and -Lys/Pro and...
  
    //! List of cleavage rules.
    QList<CleaveRule *> m_ruleList; // Cleavage conditions...
  
  
  public:
    CleaveSpec(const PolChemDef *, QString, QString = QString());
  
    CleaveSpec(const CleaveSpec &);
    ~CleaveSpec();
  
    void clone(CleaveSpec *) const;
    void mold(const CleaveSpec &);
    CleaveSpec & operator =(const CleaveSpec &);
  
    void setPattern(const QString &);
    const QString &pattern();
  
    QList<CleaveMotif*> *motifList();
    QList<CleaveRule*> *ruleList();

    static int isNameInList(const QString &, 
			     const QList<CleaveSpec*> &,
			     CleaveSpec *other = 0);

    bool parse();
  
    bool validate();
  
    bool renderXmlClsElement(const QDomElement &, int);

    QString *formatXmlClsElement(int , const QString & = QString("  "));
  };

} // namespace massXpert


#endif // CLEAVE_SPEC_HPP
