/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.filomace.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#ifndef ISOTOPIC_PATTERN_CALCULATOR_HPP
#define ISOTOPIC_PATTERN_CALCULATOR_HPP

/////////////////////// Local includes
#include "peakCentroid.hpp"
#include "formula.hpp"
#include "peakShape.hpp"
#include "peakShapeConfig.hpp"

namespace massXpert
{

  class IsotopicPatternCalculator : public QObject
  {
    Q_OBJECT
    
    private:
    Formula m_formula;
    int m_charge;
    
    int m_maxPeakCentroids;
    double m_minProb;
        
    const QList<Atom *> &m_atomRefList;
    
    QList<QPointF *> m_pointList;
    
    PeakShapeConfig m_config;

    QList<PeakCentroid *> m_peakCentroidList;
    QList<PeakCentroid *> m_tempPeakCentroidList;
    QList<PeakShape *> m_peakShapeList;

    bool m_aborted;
    int m_abortCheckCount;
    int m_progressValueNew;
    int m_progressValueOld;

    double m_sumProbabilities;
    double m_greatestProbability;
        
    bool seedPeakCentroidList();

    void freeClearPeakCentroidList();
    void freeClearTempPeakCentroidList();
    void freeClearPeakShapeList();
    void freeClearPointList();
    
    int accountAtomCount(const AtomCount *);
    int updatePeakCentroidListWithAtom(const Atom *);
    int updatePeakCentroidWithIsotope(PeakCentroid *, const Isotope *);
    int mergePeakCentroidsWithSameMz();
    int removePeakCentroidsInExcess();
    int calculateSumOfProbabilities();
    int calculateRelativeIntensity();
    int removeTooLowProbPeaks();


  public:
    IsotopicPatternCalculator(Formula /* formula */,
                              int /* charge */,
                              int /* maxPeaks */,
                              double /* minProb */,
                              const QList<Atom *> & /* atomRefList */,
                              PeakShapeConfig = PeakShapeConfig());
    
    ~IsotopicPatternCalculator();

    const QList<PeakCentroid *> &peakCentroidList() const;
    QString *peakCentroidListAsString() const;

    const QList<QPointF *> &pointList() const;
    QString *pointListAsString() const;
    QList<QPointF *> *duplicatePointList() const;
    int transferPoints(QList<QPointF *> *);
    
    QPointF *firstPoint() const;
    QPointF *lastPoint() const;
    
    int calculatePeakCentroids();
    int calculatePeakShapes();
    const QList<QPointF *> &sumPeakShapes();
    

  signals:
    void isotopicCalculationProgressValueChanged(int);
    void isotopicCalculationMessageChanged(QString);

  public slots:
    void isotopicCalculationAborted();
  };
  
}// namespace massXpert

#endif // ISOTOPIC_PATTERN_CALCULATOR_HPP

