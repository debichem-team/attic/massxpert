/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef FRAG_RULE_HPP
#define FRAG_RULE_HPP


/////////////////////// Qt includes
#include <QString>


/////////////////////// Local includes
#include "polChemDefEntity.hpp"
#include "formula.hpp"
#include "monomer.hpp"


namespace massXpert
{

  //! The FragRule class provides a fragmentation rule.
  /*! Fragmentation rules characterize in more detail the chemical
    reaction that governs the fragmentation of the polymer in the
    gas-phase. The rule is a conditional rule. Its logic is based on the
    presence of given monomers at the place of the fragmentation and
    before or after that precise location.

    In saccharide chemistry, fragmentations are a very complex
    topic. This is because a given monomer will fragment according to a
    given chemistry if it is preceded in the sequence by a monomer of a
    given identity and according to another chemistry if its direct
    environment is different.

    This paradigm is implemented using a sequence environment logic
    based on conditions that can be formulated thanks to three monomer codes:

    \li the monomer at which the fragmentation takes place(current code);
    \li the monomer preceeding the current code(previous code);
    \li the monomer following the current code(following code);

    The use of these codes is typically according to this logic:

    "If current monomer is Glu and that previous monomer is Gly and
    following monomer is Arg, then fragmentation should occur according
    to this formula : "-H2O".
  */
  class FragRule : public PolChemDefEntity, public Formula
  {
  private:
    //! Previous code.
    QString m_prevCode;

    //! Current code.
    QString m_currCode;

    //! Next code.
    QString m_nextCode;

    //! Comment.
    QString m_comment; 

  public:
    FragRule(const PolChemDef *, QString, 
	      QString = QString(),
	      QString = QString(), 
	      QString = QString(),
	      QString = QString(), 
	      const QString & = QString());
  
    FragRule(const FragRule &);
    ~FragRule();
  
    FragRule *clone() const;
    void clone(FragRule *) const;
    void mold(const FragRule &);
    FragRule & operator =(const FragRule &);
  
    void setPrevCode(const QString &);
    QString prevCode() const;

    void setCurrCode(const QString &);
    QString currCode() const;

    void setNextCode(const QString &);
    QString nextCode() const;

    QString formula() const;
    
    void setComment(const QString &);
    QString comment() const;

    static int isNameInList(const QString &, 
			     const QList<FragRule*> &,
			     FragRule *other = 0);
  
    bool validate();
  
    bool renderXmlFgrElement(const QDomElement &);
  
    QString *formatXmlFgrElement(int , const QString & = QString("  "));
  };

} // namespace massXpert


#endif // FRAG_RULE_HPP
