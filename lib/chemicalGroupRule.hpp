/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef CHEMICAL_GROUP_RULE_HPP
#define CHEMICAL_GROUP_RULE_HPP


/////////////////////// Qt includes
#include <QString>
#include <QList>
#include <QDomElement>


namespace massXpert
{

  enum 
    {
      MXP_CHEMGROUP_RULE_LOST = 0,
      MXP_CHEMGROUP_RULE_PRESERVED = 1,
    };


  class ChemicalGroupRule
  {
  private:
    QString m_entity;
  
    QString m_name;
  
    int m_outcome;

  public:
    ChemicalGroupRule(QString = QString(), QString = QString(),
		       int = MXP_CHEMGROUP_RULE_LOST);
    ~ChemicalGroupRule() { }

    void setEntity(QString);
    QString entity();
  
    void setName(QString);
    QString name();
  
    void setOutcome(int);
    int outcome();

    bool renderXmlElement(const QDomElement &element);
  };

} // namespace massXpert


#endif // CHEMICAL_GROUP_RULE_HPP
