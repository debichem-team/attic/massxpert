/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef ISOTOPE_HPP
#define ISOTOPE_HPP


/////////////////////// Qt includes
///////////////////////
#include <QtGlobal>
#include <QtXml>


namespace massXpert
{

  //! The Isotope class provides an isotope.
  /*! Isotopes are the basic elements of chemical elements(also known
    as atoms). An isotope is characterized by two data: 

    \li the isotope mass;

    \li the isotope abundance.

    The carbon(symbol 'C') element, for example is mainly found in
    nature in the form of two isotopes: \f$\mathrm{^{12}C}\f$ and
    \f$\mathrm{^{13}C}\f$ . The \f$\mathrm{^{12}C}\f$ has an abundance
    of 0.99(that is 99 percent) while the heavier isotope has an
    abundance of 0.01(that is 1 percent).

    Consequently, the main member data of this class are the mass value
    and the abundance value.
  */
  class Isotope
  {
  protected:
    //! Mass.
    double m_mass;
  
    //! Abundance.
    double m_abundance;
  
  public:
    Isotope(double = 0, double = 0);
    Isotope(const Isotope &);
    virtual ~Isotope() {}
  
    virtual Isotope * clone() const;
    virtual void clone(Isotope *) const;
    virtual void mold(const Isotope &);
    virtual Isotope & operator =(const Isotope &);
  
    void setMass(double);
    double mass() const;
    QString mass(const QLocale &, int = 10) const;
  
    void setAbundance(double);
    void incrementAbundance(double);
    void decrementAbundance(double);
    double abundance() const;
    QString abundance(const QLocale &, int = 10) const;

    virtual bool operator ==(const Isotope &) const;
    virtual bool operator !=(const Isotope &) const;
    virtual bool operator <(const Isotope &) const;
    static bool lessThan(const Isotope*, const Isotope *);  

    bool renderXmlIsotopeElement(const QDomElement &, int);
    QString *formatXmlIsotopeElement(int,
				      const QString & = QString("  "));
  };

} // namespace massXpert


#endif // ISOTOPE_HPP

