/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Qt includes
#include <QtGlobal>
#include <QDebug>

/////////////////////// Local includes
#include "ponderable.hpp"


namespace massXpert
{

  //! Constructs a ponderable initialized with masses.
  /*! Both the \p mono and \p avg masses default to 0.
  
    \param mono monoisotopic mass initializer. Defaults to 0.

    \param avg average mass initializer. Defaults to 0.
  */
  Ponderable::Ponderable(double mono, double avg) 
  {
    m_mono = mono; m_avg = avg; 
  }


  //! Constructs a copy of \p other.
  /*! The copying is shallow, as the property list is not used for the
    initialization of the constructed ponderable.
  
    \param other ponderable to be used as a mold.
  */
  Ponderable::Ponderable(const Ponderable &other)
    : m_mono(other.m_mono), m_avg(other.m_avg)
  {
  }


  //! Destroys the ponderable.
  Ponderable::~Ponderable()
  {  
  }


  //! Creates a new ponderable and initializes it with the data in this.
  /*! The initialization is shallow, as the prop list is not used to
    perform the initialization.

    \return the newly allocated ponderable which must be deleted when no
    longer in use.
  */
  Ponderable *
  Ponderable::clone() const
  {
    Ponderable *other = new Ponderable(*this);
  
    return other;
  }


  //! Modifies \p other to be identical to \p this.
  /*! The copying of the data is shallow as the data in the prop
    list are not copied.

    \param other ponderable.
  */
  void
  Ponderable::clone(Ponderable *other) const
  {
    Q_ASSERT(other);
  
    if (other == this)
      return;
  
    other->m_mono = m_mono;
    other->m_avg = m_avg;
  }


  //! Modifies \p this  to be identical to \p other.
  /*! The copying of the data is shallow as the data in the prop
    list are not copied.

    \param other ponderable to be used as a mold.
  */
  void
  Ponderable::mold(const Ponderable &other)
  {
    if (&other == this)
      return;
  
    m_mono = other.m_mono;
    m_avg = other.m_avg;
  }


  //! Assigns other to \p this ponderable.
  /*! This is a shallow assignment as the data in the prop list are
    not copied.  

    \param other ponderable used as the mold to set values to \p this
    instance.
  
    \return a reference to \p this ponderable.
  */
  Ponderable &
  Ponderable::operator =(const Ponderable &other)
  {
    if (&other != this)
      mold(other);
  
    return *this;
  }


  //! Sets the monoisotopic and average masses.
  /*! No check is performed as to the sign of the values passed as
    parameters.
  
    \param mono monoisotopic initializer.
  
    \param avg average initializer.
  */
  void
  Ponderable::setMasses(double mono, double avg)
  {
    m_mono = mono;
    m_avg = avg;
  }


  void 
  Ponderable::setMass(double mass, MassType type)
  {
    if (type & MXT_MASS_MONO)
      m_mono = mass;
    else if (type & MXT_MASS_AVG)
      m_avg = mass;
    
    return;
  }
  

  void 
  Ponderable::incrementMass(double mass, MassType type)
  {
    if (type & MXT_MASS_MONO)
      m_mono += mass;
    else if (type & MXT_MASS_AVG)
      m_avg += mass;
    
    return;
  }
  


  //! Updates the arguments with the monoisotopic and average masses.
  /*! Any argument might be a 0 pointer, in which case the function does
    update it.
  
    \param mono receives the monoisotopic mass. If 0, no updating will
    take place.

    \param avg receives the average mass. If 0, no updating will take
    place.
  */
  void
  Ponderable::masses(double *mono, double *avg) const
  {
    Q_ASSERT(mono && avg);
  
    *mono = m_mono;
    *avg = m_avg;
  }


  double 
  Ponderable::mass(MassType type) const
  {
    if (type == MXT_MASS_MONO)
      return m_mono;
    
    return m_avg;
  }
  

  //! Clears the monoisotopic and average masses by setting both to 0.
  void
  Ponderable::clearMasses()
  {
    m_mono = 0.0;
    m_avg = 0.0;
  }


  //! Sets the monoisotopic mass to \p mass.
  /*! No check is performed as to the sign of the value passed as
    parameter.

    \param mass new monoisotopic mass.
  */
  void
  Ponderable::setMono(double mass)
  {
    m_mono = mass;
  }


  //! Sets the monoisotopic mass to \p mass.
  /*! Conversion from string to double is performed by using the locale
    conversion rule for \p locale.
  
    \param mass new monoisotopic mass in string form.
    \param locale QLocale to be used for the conversion to double
  
    \return true if the conversion succeeded, false otherwise; in which
    case the mono mass of this ponderable is not modified.
  */
  bool
  Ponderable::setMono(const QString &mass, const QLocale &locale)
  {
    bool ok = false;
  
    double value = locale.toDouble(mass, &ok);
  
    if (!ok)
      return false;
  
    m_mono = value;

    return true;
  }

  
  //! Increments the monoisotopic mass by \p mass.
  /*! No check is performed as to the sign of the value passed as
    parameter.
  
    \param mass value by which the monoisotopic mass is incremented. If
    that value is negative, the mass will actually be decremented.
  */
  void
  Ponderable::incrementMono(double mass)
  {
    m_mono += mass;
  }


  //! Decrements the monoisotopic mass by \p mass.
  /*! No check is performed as to the sign of the value passed as
    parameter.
  
    \param mass value by which the monoisotopic mass is decremented. If
    that value is negative, the mass will actually be incremented.
  */
  void
  Ponderable::decrementMono(double mass)
  {
    m_mono -= mass;
  }


  //! Returns the monoisotopic mass.
  /*!  \return the monoisotopic mass.
   */
  double
  Ponderable::mono() const
  {
    return m_mono;
  }


  //! Returns a reference to the monoisotopic mass.
  /*! The monoisotopic mass is returned as a modifiable reference.
  
    \return the monoisotopic mass.
  */
  double &
  Ponderable::rmono()
  {
    return m_mono;
  }


  //! Returns the monoisotopic mass as a string.
  /*! The string is created by taking into account the locale passed as
    argument. Check QLocale doc in case QLocale() is passed as argument.
  
    \param locale locale used to construct the string.
  
    \return the monoisotopic mass as a locale-complying string.
  */
  QString 
  Ponderable::mono(const QLocale &locale, int decimalPlaces) const
  {
    if (decimalPlaces < 0)
      decimalPlaces = 0;
    
    QString mass;
    mass = locale.toString(m_mono, 'f', decimalPlaces);
    return mass;
  }


  //! Sets the average mass to \p mass.
  /*! No check is performed as to the sign of the value passed as
    parameter.

    \param mass new average mass.
  */
  void
  Ponderable::setAvg(double mass)
  {
    m_avg = mass;
  }


  //! Sets the average mass to \p mass.
  /*! Conversion from string to double is performed by using the locale
    conversion rule for \p locale.
  
    \param mass new average mass in string form.
    \param locale QLocale to be used for the conversion to double
  
    \return true if the conversion succeeded, false otherwise; in which
    case the avg mass of this ponderable is not modified.
  */
  bool
  Ponderable::setAvg(const QString &mass, const QLocale &locale)
  {
    bool ok = false;
  
    double value = locale.toDouble(mass, &ok);
  
    if (!ok)
      return false;
  
    m_avg = value;
  
    return true;
  }


  //! Increments the average mass by \p mass.
  /*! No check is performed as to the sign of the value passed as
    parameter.
  
    \param mass value by which the average mass is incremented. If that
    value is negative, the mass will actually be decremented.
  */
  void
  Ponderable::incrementAvg(double mass)
  {
    m_avg += mass;
  }


  //! Decrements the average mass by \p mass.
  /*! No check is performed as to the sign of the value passed as
    parameter.
  
    \param mass value by which the average mass is decremented. If that
    value is negative, the mass will actually be incremented.
  */
  void
  Ponderable::decrementAvg(double mass)
  {
    m_avg -= mass;
  }


  //! Returns the average mass.
  /*!  \return the average mass.
   */
  double
  Ponderable::avg() const
  {
    return m_avg;
  }


  //! Returns a reference to the average mass.
  /*! The average mass is returned as a modifiable reference.
  
    \return the average mass.
  */
  double &
  Ponderable::ravg()
  {
    return m_avg;
  }


  //! Returns the average mass as a string.
  /*! The string is created by taking into account the locale passed as
    parameter. Check QLocale doc in case QLocale() is passed as argument.
  
    \param locale locale used to construct the string.

    \return the average mass as a locale-complying string.
  */
  QString 
  Ponderable::avg(const QLocale &locale, int decimalPlaces) const
  {
    if (decimalPlaces < 0)
      decimalPlaces = 0;
    
    QString mass;
    mass = locale.toString(m_avg, 'f', decimalPlaces);
    return mass;
  }




  //! Equality operator.  
  /*! Test the equality between \p other and \c this. The properties of
    the ponderables involved are not tested.

    \param other Ponderable to be compared to \c this.
  
    \return true if \p other and \c this are identical, false otherwise.
  */
  bool 
  Ponderable::operator ==(const Ponderable &other) const
  {
    if (m_mono == other.m_mono && m_avg == other.m_avg)
      return true;
  
    return false;
  }


  //! Inquality operator.  
  /*! Test the inequality between \p other and \c this. The properties of
    the ponderables involved are not tested.

    \param other Ponderable to be compared to \c this.
  
    \return true if \p other and \c this differ, false otherwise.
  */
  bool 
  Ponderable::operator !=(const Ponderable &other) const
  {
    if (m_mono != other.m_mono || m_avg != other.m_avg)
      {
        // qDebug() << __FILE__ << __LINE__ 
        //          << "m_mono:" << m_mono 
        //          << "/" 
        //          << "other.m_mono:" << other.m_mono
        //          << "--"
        //          << "m_avg:" << m_avg
        //          << "/" 
        //          << "other.m_avg:" << other.m_avg;
        
        return true;
      }
    
    return false;
  }


  //! Calculates the monoisotopic and average masses.
  /*! Virtual function that should be re-implemented in derived classes
    so as to compute the monoisotopic and average masses for the
    ponderable entity. This function does actually nothing.
  
    \return always true.
  */
  bool 
  Ponderable::calculateMasses()
  {
    return true;
  }


  //! Increments the masses in the arguments.
  /*! The masses pointed at by the arguments \p mono and \p avg are
    incremented by the monoisotopic and average masses. The incrementing
    is performed \p times times.
  
    \param mono monoisotopic mass to be incremented. If 0, no
    modification is performed.
  
    \param avg average mass that should be incremented. If 0, no
    modification is performed.
  
    \param times integer indicating the number of times the operations
    must be performed.
  
    \return true if calculation succeeded, false otherwise.
  */
  bool 
  Ponderable::accountMasses(double *mono, double *avg, int times) const
  {
    if (mono)
      *mono += m_mono * times;
  
    if (avg)
      *avg += m_avg * times;
  
    return true;
  }

} // namespace massXpert
