/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Local includes
#include "isotope.hpp"


namespace massXpert
{

  //! Constructs an isotope initialized with mass and abundance.
  /*! Both the \p mono and \p avg masses default to 0.
  
    \param mass mass initializer. Defaults to 0. Cannot be negative.

    \param abundance abundance initializer. Defaults to 0. Cannot be
    negative.
  */
  Isotope::Isotope(double mass, double abundance)
  {
    Q_ASSERT(mass >= 0);
    Q_ASSERT(abundance >= 0);

    m_mass = mass;
    m_abundance = abundance;
  }


  //! Constructs a copy of \p other.
  /*!
    \param other isotope to be used as a mold.
  */
  Isotope::Isotope(const Isotope &other)
  {
    m_mass = other.m_mass;
    m_abundance = other.m_abundance;
  }


  //! Creates a new isotope.
  /*! The newly created isotope is initialized using \c this.
  
    \return The new isotope, which must be deleted when no longer in use.
  */
  Isotope *
  Isotope::clone() const
  {
    Isotope *other = new Isotope(*this);
  
    return other;
  }


  //! Modifies \p other to be identical to \p this.
  /*!  \param other isotope.
   */
  void 
  Isotope::clone(Isotope *other) const
  {
    Q_ASSERT(other);
  
    if (other == this)
      return;
  
    other->m_mass = m_mass;
    other->m_abundance = m_abundance;
  }


  //! Modifies \p this  to be identical to \p other.
  /*!  \param other isotope to be used as a mold.
   */
  void 
  Isotope::mold(const Isotope &other)
  { 
    if (&other == this)
      return;
  
    m_mass = other.m_mass;
    m_abundance = other.m_abundance;
  }


  //! Assigns other to \p this isotope.
  /*! \param other isotope used as the mold to set values to \p this
    instance.
  
    \return a reference to \p this isotope.
  */
  Isotope &
  Isotope::operator =(const Isotope &other)
  {
    if (&other != this)
      mold(other);
  
    return *this;
  }


  //! Sets the mass.
  /*!  \param mass mass. Cannot be negative.
   */
  void
  Isotope::setMass(double mass)
  {
    Q_ASSERT(mass >= 0);

    m_mass = mass;
  }


  //! Returns the mass.
  /*!  
    \return the mass.
  */
  double
  Isotope::mass() const
  {
    return m_mass;
  }


  //! Returns the mass as a string.
  /*! The string is created by taking into account the locale passed as
    argument. Check QLocale doc in case QLocale() is passed as argument.
  
    \param locale locale used to construct the string.
  
    \return the mass as a locale-complying string.
  */
  QString 
  Isotope::mass(const QLocale &locale, int decimalPlaces) const
  {
    if (decimalPlaces < 0)
      decimalPlaces = 0;
    
    return locale.toString(m_mass, 'f', decimalPlaces);
  }


  //! Sets the abundance.
  /*!  \param abundance abundance. Cannot be negative.
   */
  void
  Isotope::setAbundance(double abundance)
  {
    Q_ASSERT(abundance >= 0);
  
    m_abundance = abundance;
  }


  //! Increments  abundance by \p abundance.
  /*!  \param abundance value by which to increment the isotope
    abundance. Cannot be negative.
  */
  void
  Isotope::incrementAbundance(double abundance)
  {
    Q_ASSERT(abundance >= 0);
  
    m_abundance += abundance;
  }


  //! Decrements  abundance by \p abundance.
  /*!  \param abundance value by which to decrement the isotope
    abundance. Cannot be negative.
  */
  void
  Isotope::decrementAbundance(double abundance)
  {
    Q_ASSERT(abundance >= 0);
  
    m_abundance -= abundance;
  }


  //! Returns the abundance.
  /*!  \return the abundance.
   */
  double
  Isotope::abundance() const
  {
    return m_abundance;
  }


  //! Returns the abundance as a string.
  /*! The string is created by taking into account the locale passed as
    argument. Check QLocale doc in case QLocale() is passed as argument.
  
    \return the abundance as a string.
  */
  QString 
  Isotope::abundance(const QLocale &locale, int decimalPlaces) const
  {
    if (decimalPlaces < 0)
      decimalPlaces = 0;
    
    return locale.toString(m_abundance, 'f', decimalPlaces);
  }


  //! Tests equality.
  /*!  \param other isotope to be compared with \p this.

    \return true if the isotopes are identical, false otherwise.
  */
  bool
  Isotope::operator ==(const Isotope &other) const
  {
    return(m_mass == other.m_mass && m_abundance == other.m_abundance);
  }


  //! Tests inequality.
  /*!  \param other isotope to be compared with \p this.
  
    \return true if the isotopes are different, false otherwise.
  */
  bool
  Isotope::operator !=(const Isotope &other) const
  {
    return(m_mass != other.m_mass || m_abundance != other.m_abundance);
  }


  //! Tests if less than.
  /*!  \param other isotope to be compared with \p this.
  
    \return true if the this isotope has a lesser mass than \p other.
  */
  bool
  Isotope::operator <(const Isotope &other) const
  {
    return(m_mass < other.m_mass);
  }


  bool 
  Isotope::lessThan(const Isotope *first, const Isotope *second)
  {
    Q_ASSERT(first && second);
  
    return(first->m_mass < second->m_mass);
  }
  


  //! Parses an isotope XML element and sets the data to the isotope.
  /*! The XML element is parsed and the data extracted from the XML data
    are set to \p this instance.

    The data are checked for validity: the mass and the abundance cannot
    be negative.

    The DTD says this: <!ELEMENT isotope(mass , abund)>
  
    A typical isotope element is as follows:
  
    \verbatim
    <isotope>
    <mass>1.0078250370</mass>
    <abund>99.9885000000</abund>
    </isotope>
    \endverbatim 

    \param element a reference to a const QDomElement instance that
    points at an isotope XML element.
  
    \return true if the parsing is successful, false otherwise.

    \sa formatXmlIsotopeElement(int offset, const QString &indent)
  */
  bool
  Isotope::renderXmlIsotopeElement(const QDomElement &element, int version)
  {
    // For the time being the version is not necessary here. As of
    // version up to 2, the current function works ok.
    if (version)
      ;
  
    QDomElement child;

    if (element.tagName() != "isotope")
      return false;
  
    child = element.firstChildElement("mass");

    if (child.isNull())
      return false;

    m_mass =(child.text().toDouble());

    if (m_mass < 0)
      return false;
  
    child = child.nextSiblingElement("abund");

    if (child.isNull())
      return false;

    m_abundance =(child.text().toDouble());

    if (m_abundance < 0)
      return false;
  
    return true;
  }


  //! Formats a string suitable to use as an XML element.
  /*!  Formats a string suitable to be used as an XML element in a
    polymer chemistry definition file as part of an atom element.

    The DTD says this: <!ELEMENT isotope(mass , abund)>
  
    A typical isotope element is as follows(~ represent space
    characters, actually):
  
    \verbatim
    <isotope>
    <mass>1.0078250370</mass>
    <abund>99.9885000000</abund>
    </isotope>  
    \endverbatim
  
    \param offset  times the \p indent string must be used as a lead in the
    formatting of elements.
  
    \param indent string used to create the leading space that is placed
    at the beginning of indented XML elements inside the XML
    element. Defaults to two spaces(QString(" ")).
  
    \return a dynamically allocated string that needs to be freed after
    use.

    \sa renderXmlIsotopeElement(const QDomElement &element)
  */
  QString *
  Isotope::formatXmlIsotopeElement(int offset, const QString &indent)
  {
    int newOffset;
    int iter = 0;
  
    QString lead("");
    QString *string = new QString();

    // Prepare the lead.
    newOffset = offset;  
    while(iter < newOffset)
      {
	lead += indent;
	++iter;
      }

    *string += QString("%1<isotope>\n").arg(lead);
  
    // Prepare the lead.
    ++newOffset;
    lead.clear();
    iter = 0;
    while(iter < newOffset)
      {
	lead += indent;
	++iter;
      }
  
    // Continue with indented elements.
  
    *string += QString("%1<mass>%2</mass>\n")
      .arg(lead)
      .arg(mass(QLocale()));
  
    *string += QString("%1<abund>%2</abund>\n")
      .arg(lead)
      .arg(abundance(QLocale()));
    
    // Prepare the lead.
    --newOffset;
    lead.clear();
    iter = 0;
    while(iter < newOffset)
      {
	lead += indent;
	++iter;
      }

    *string += QString("%1</isotope>\n").arg(lead);
    
    return string;
  }

} // namespace massXpert
