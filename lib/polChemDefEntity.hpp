/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef POL_CHEM_DEF_ENTITY_HPP
#define POL_CHEM_DEF_ENTITY_HPP


/////////////////////// Qt includes
#include <QString>


/////////////////////// Local includes


namespace massXpert
{

  class PolChemDef;
  
  //! The PolChemDefEntity class provides a chemical entity.
  /*! A polymer chemistry definition entity is something that relates to
    a polymer chemistry definition and as such has to maintain a close
    relationship with a polymer chemistry definition instance. That
    relationship is automatically generated upon construction, as a
    polymer definition chemical entity cannot be constructed without a
    pointer to a polymer chemistry definition instance. Aside from the
    pointer to a polymer chemistry definition, a polymer chemistry
    definition entity is also characterized by a name.
  */
  class PolChemDefEntity
  {
  protected:
    //! Pointer to the reference polymer chemistry definition.
    const PolChemDef *mp_polChemDef;

    //! Name.
    QString m_name;
  
  public:
    PolChemDefEntity(const PolChemDef *, 
		      const QString & = QString("NOT_SET"));

    PolChemDefEntity(const PolChemDefEntity &);

    virtual ~PolChemDefEntity();
    
    virtual void clone(PolChemDefEntity *) const;
    virtual PolChemDefEntity *clone() const;
    virtual void mold(const PolChemDefEntity &);
    virtual PolChemDefEntity & operator =(const PolChemDefEntity &);
  
    void setName(const QString &);
    QString name() const;

    const PolChemDef *polChemDef() const;
    void setPolChemDef(PolChemDef *);
      
    virtual bool operator ==(const PolChemDefEntity &) const;
    virtual bool operator !=(const PolChemDefEntity &) const;

    virtual bool validate() const;
  };

} // namespace massXpert


#endif // POL_CHEM_DEF_ENTITY_HPP
