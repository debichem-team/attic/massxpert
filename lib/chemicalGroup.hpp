/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef CHEMICAL_GROUP_HPP
#define CHEMICAL_GROUP_HPP


/////////////////////// Qt includes
#include <QString>
#include <QList>


/////////////////////// Local includes
#include "chemicalGroupRule.hpp"
#include "prop.hpp"


namespace massXpert
{

  enum
    {
      MXP_CHEMGROUP_NEVER_TRAPPED = 1 << 0,
      MXP_CHEMGROUP_LEFT_TRAPPED = 1 << 1,
      MXP_CHEMGROUP_RIGHT_TRAPPED = 1 << 2,
    };


  class ChemicalGroup
  {
  private:
    QString m_name;

    float m_pka;

    bool m_acidCharged;
  
    int m_polRule;

    QList<ChemicalGroupRule *> m_ruleList;

  public:
    ChemicalGroup(QString = QString(), float = 7.0, 
		   bool = true, int = MXP_CHEMGROUP_NEVER_TRAPPED);
    ChemicalGroup(const ChemicalGroup &);
  
    ~ChemicalGroup();
  
    void setName(QString);
    QString name() const;
  
    void setPka(float);
    float pka() const;
  
    void setAcidCharged(bool);
    bool isAcidCharged() const;
  
    void setPolRule(int);
    int polRule() const;

    QList<ChemicalGroupRule *> &ruleList();

    ChemicalGroupRule *findRuleEntity(QString, int* = 0) const;
    ChemicalGroupRule *findRuleName(QString, int* = 0) const;
    ChemicalGroupRule *findRule(QString, QString, int* = 0) const;

    bool renderXmlMnmElement(const QDomElement &);
    bool renderXmlMdfElement(const QDomElement &);
  
  };


  class ChemicalGroupProp : public Prop
  {
  public:
    ChemicalGroupProp(const QString & = QString(), 
		       ChemicalGroup * = 0);
    ~ChemicalGroupProp();
    virtual void deleteData();
    
    virtual void *clone() const;
    virtual void cloneOut(void *) const;
    virtual void cloneIn(const void *);  
  
    bool renderXmlElement(const QDomElement &, int = 1);
    QString *formatXmlElement(int, const QString & = QString("  "));
  };

} // namespace massXpert


#endif // CHEMICAL_GROUP_HPP
