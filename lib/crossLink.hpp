/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef CROSS_LINK_HPP
#define CROSS_LINK_HPP
//#warning "Entering CROSS_LINK_HPP"


/////////////////////// Local includes
#include "crossLinker.hpp"
#include "monomer.hpp"

namespace massXpert
{
  
  class Polymer;

  
  enum CrossLinkEncompassed
    {
      MXP_CROSS_LINK_ENCOMPASSED_NO = 0,
      MXP_CROSS_LINK_ENCOMPASSED_PARTIAL = 1,
      MXP_CROSS_LINK_ENCOMPASSED_FULL = 2,
    };

  class CrossLink : public CrossLinker
  {
  protected:
    // We do not own these two monomers and polymer.
    QPointer<Polymer> mp_polymer;
    QList<const Monomer *> m_monomerList;
  
    QString m_comment;

  public:
    CrossLink(const PolChemDef *,
	       Polymer *,
	       const QString &,
	       const QString &,
	       const QString & = QString());
  
    CrossLink(const CrossLink &);

    CrossLink(const CrossLinker &,
	       Polymer *,
	       const QString & = QString());

    ~CrossLink();
  
    bool setMonomerAt(Monomer *, int);
    bool appendMonomer(const Monomer *);
    const Monomer *monomerAt(int);
    bool removeMonomerAt(int);
  
    void setPolymer(Polymer *);
    Polymer *polymer();
  
    void setComment(const QString &);
    const QString &comment()const;

    virtual bool operator ==(const CrossLink &) const;
  
    int populateMonomerList(QString);
    QList<const Monomer *> *monomerList();
    int monomerIndexList(QList<int> *);
    QString monomerIndexText();
    QString monomerPosText();
  
    int involvesMonomer(const Monomer *) const;
    const Monomer *firstMonomer() const;
    int encompassedBy(int, int, int *in = 0, int * = 0);
    int encompassedBy(const CoordinateList &, int *in = 0, int * = 0);

    int involvedMonomers(QList<Monomer *> *);
  
  
    bool validate();

    virtual bool calculateMasses();

    virtual bool accountMasses(double * = 0, double * = 0, int = 1);
    virtual bool accountMasses(Ponderable *, int = 1);

    QString *prepareResultsTxtString();
  };

} // namespace massXpert


#endif // CROSS_LINK_HPP
