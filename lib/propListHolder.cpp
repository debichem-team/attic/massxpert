/* massXpert - the true massist's program.
	 --------------------------------------
	 Copyright(C) 2006,2007 Filippo Rusconi

http://www.massxpert.org

This file is part of the massXpert project.

The massxpert project is the successor to the "GNU polyxmass"
project that is an official GNU project package(see
www.gnu.org). The massXpert project is not endorsed by the GNU
project, although it is released ---in its entirety--- under the
GNU General Public License. A huge part of the code in massXpert
is actually a C++ rewrite of code in GNU polyxmass. As such
massXpert was started at the Centre National de la Recherche
Scientifique(FRANCE), that granted me the formal authorization to
publish it under this Free Software License.

This software is free software; you can redistribute it and/or
modify it under the terms of the GNU  General Public
License version 3, as published by the Free Software Foundation.


This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this software; if not, write to the

Free Software Foundation, Inc.,

51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Qt includes


/////////////////////// Local includes
#include "propListHolder.hpp"


namespace massXpert
{

	PropListHolder::PropListHolder()
	{
	}


	PropListHolder::PropListHolder(const PropListHolder &other)
	{
		for (int iter = 0; iter < other.m_propList.size(); ++iter)
		{
			Prop *prop = other.m_propList.at(iter);
			Q_ASSERT(prop);

			void *newProp = static_cast<void *>(prop->clone());
			Q_ASSERT(newProp);

			appendProp(static_cast<Prop *>(newProp));
		}
	}


	PropListHolder::~PropListHolder()
	{
		while(!m_propList.isEmpty())
			delete(m_propList.takeFirst());
	}


	PropListHolder *
		PropListHolder::clone() const
		{
			PropListHolder *other = new PropListHolder(*this);

			return other;
		}


	void 
		PropListHolder::clone(PropListHolder *other) const
		{
			Q_ASSERT(other);

			while(!other->m_propList.isEmpty())
				delete(other->m_propList.takeFirst());

			for (int iter = 0; iter < m_propList.size(); ++iter)
			{
				Prop *prop = m_propList.at(iter);
				Q_ASSERT(prop);

				void *newProp = static_cast<void *>(prop->clone());
				Q_ASSERT(newProp);

				other->appendProp(static_cast<Prop *>(newProp));
			}
		}


	void 
		PropListHolder::mold(const PropListHolder &other)
		{


			while(!m_propList.isEmpty())
				delete(m_propList.takeFirst());

			for (int iter = 0; iter < other.m_propList.size(); ++iter)
			{
				Prop *prop = other.m_propList.at(iter);
				Q_ASSERT(prop);

				void *newProp = static_cast<void *>(prop->clone());
				Q_ASSERT(newProp);

				appendProp(static_cast<Prop *>(newProp));
			}
		}


	PropListHolder & 
		PropListHolder::operator =(const PropListHolder &other)
		{
			if (&other != this)
				mold(other);

			return *this;
		}


	const QList<Prop *> & 
		PropListHolder::propList() const
		{
			return  m_propList;
		}


	QList<Prop *> & 
		PropListHolder::propList()
		{
			return m_propList;
		}


	Prop *
		PropListHolder::prop(const QString &name, int *index)
		{
			for (int iter = 0; iter < m_propList.size(); ++iter)
			{
				Prop *localProp = m_propList.at(iter);
				Q_ASSERT(localProp);

				if(localProp->name() == name)
				{
					if (index)
						*index = iter;

					return localProp;
				}
			}

			return 0;
		}


	int 
		PropListHolder::propIndex(const QString &name, Prop *prop)
		{
			for (int iter = 0; iter < m_propList.size(); ++iter)
			{
				Prop *localProp = m_propList.at(iter);
				Q_ASSERT(localProp);

				if(localProp->name() == name)
				{
					if (prop)
						prop = localProp;

					return iter;
				}
			}

			return -1;
		}


	bool
		PropListHolder::appendProp(Prop *prop)
		{
			m_propList.append(prop);

			return true;
		}


	bool
		PropListHolder::removeProp(Prop *prop)
		{
			if (m_propList.removeAll(prop))
				return true;

			return false;
		}


	bool
		PropListHolder::removeProp(const QString &name)
		{
			for (int iter = 0; iter < m_propList.size(); ++iter)
			{
				if(m_propList.at(iter)->name() == name)
				{
					m_propList.removeAt(iter);

					return true;
				}
			}

			return false;
		}

} // namespace massXpert
