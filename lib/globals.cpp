/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "globals.hpp"


#include <QRegExp>

int MXP_ATOM_DEC_PLACES = 10;
int MXP_OLIGOMER_DEC_PLACES = 5;
int MXP_POLYMER_DEC_PLACES = 3;
int MXP_PH_PKA_DEC_PLACES = 2;

// How many times the whole gaussian/lorentzian curve should span
// around the centroid mz ratio. 4 means that the peak will span
// [ mzRatio - (2 * FWHM) --> mzRatio + (2 * FWHM) ]

int MXP_FWHM_PEAK_SPAN_FACTOR = 4;

QString &
GlobUnspacifyQString(QString &src)
{
  if (src.isEmpty())
    return src;
  
  src.remove(QRegExp("\\s+"));
  
  return src;
}


QString
GlobBinaryRepresentation(int value)
{
  QString string;
  string = QString("%1")
    .arg(value, 32, 2);

  return string;
}


QString
GlobElideText(const QString &text, int charsLeft, int charsRight,
              const QString &delimitor)
{
  // We want to elide text. For example, imagine we have text = "that
  // is beautiful stuff", with charsLeft 4 and charsLeft 4 and
  // delimitor "...". Then the result would be "that...tuff"

  if(charsLeft < 1 || charsRight < 1)
    return text;
  
  int size = text.size();
  
  // If the text string is already too short, no need to elide
  // anything.
  if((charsLeft + charsRight + delimitor.size()) >= size)
    return text;
  
  QString result = text.left(charsLeft);
  result.append(delimitor);
  result.append(text.right(charsRight));
  
  return result;
}

