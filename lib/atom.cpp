/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Qt includes
#include <QtXml>


/////////////////////// Local includes
#include "atom.hpp"


namespace massXpert
{

  //! Constructs an atom.
  /*! 
  
    \param name name of the atom, ie "Hydrogen". Defaults to the null string.

    \param symbol symbol of the atom, ie 'H'. Defaults to the null string.
  */
  Atom::Atom(const QString &name, const QString &symbol)
  {
    if (!name.isNull())
      m_name = name;

    if (!symbol.isNull())
      m_symbol = symbol;
  }


  //! Constructs a copy of \p other.
  /*!  \param other atom to be used as a mold.
   */
  Atom::Atom(const Atom &other)
    : Ponderable(static_cast<Ponderable>(other)) 
  {
    m_name = other.m_name;
    m_symbol = other.m_symbol;
  
    for (int iter = 0 ; iter < other.m_isotopeList.size(); ++iter)
      {
	Isotope *isotope = new Isotope(*other.m_isotopeList.at(iter));
      
	m_isotopeList.append(isotope);
      }
  }


  //! Destroys the atom.
  Atom::~Atom()
  {
    while(!m_isotopeList.isEmpty())
      delete m_isotopeList.takeFirst();
  }



  //! Creates a new atom initialized with \c this.
  /*! The initialization of the new atom involves duplicating all the
    data of \c this, including all the isotopes of the isotope list.

    \return The newly created atom, which should be deleted when no
    longer in use.
  */
  Atom *
  Atom::clone() const
  {
    Atom *other = new Atom(*this);

    return other;
  }


  //! Modifies \p other to be identical to \p this.
  /*!  \param other atom.
   */
  void
  Atom::clone(Atom *other) const
  {
    Q_ASSERT(other);
  
    if (other == this)
      return;

    other->m_name = m_name;
    other->m_symbol = m_symbol;
  
    Ponderable::clone(other);
  
    while(!other->m_isotopeList.isEmpty())
      delete other->m_isotopeList.takeFirst();

    for (int iter = 0 ; iter < m_isotopeList.size(); ++iter)
      {
	Isotope *isotope = new Isotope(*(m_isotopeList.at(iter)));
      
	other->m_isotopeList.append(isotope);
      }
  }


  //! Modifies \p this  to be identical to \p other.
  /*!  \param other atom to be used as a mold.
   */
  void
  Atom::mold(const Atom &other)
  {
    if (&other == this)
      return;
  
    m_name = other.m_name ;   
    m_symbol = other.m_symbol;

    Ponderable::mold(other);

    while(!m_isotopeList.isEmpty())
      delete m_isotopeList.takeFirst();
  
    for (int iter = 0 ; iter < other.m_isotopeList.size(); ++iter)
      {
	Isotope *isotope = new Isotope(*other.m_isotopeList.at(iter));
      
	m_isotopeList.append(isotope);
      }
  }


  //! Assigns other to \p this atom.
  /*! \param other atom used as the mold to set values to \p this
    instance.
  
    \return a reference to \p this atom.
  */
  Atom & 
  Atom::operator =(const Atom &other)
  {
    if (&other != this)
      mold(other);
  
    return *this;
  } 


  //! Returns the isotope list.
  /*!  \return the isotope list.
   */
  const QList<Isotope *> &
  Atom::isotopeList() const
  {
    return m_isotopeList;
  }


  void 
  Atom::appendIsotope(Isotope *isotope)
  {
    Q_ASSERT(isotope);
  
    m_isotopeList.append(isotope);
  }


  void
  Atom::insertIsotopeAt(int index, Isotope *isotope)
  {
    Q_ASSERT(isotope);
    Q_ASSERT(index >= 0);
    
    if (index >= m_isotopeList.size())
      appendIsotope(isotope);
    else
      m_isotopeList.insert(index, isotope);
  }


  void
  Atom::removeIsotopeAt(int index)
  {
    Q_ASSERT(index >= 0 && index < m_isotopeList.size());

    m_isotopeList.removeAt(index);
  }


  //! Sets the name.
  /*!  \param str new name.
   */
  void
  Atom::setName(const QString &str)
  {
    m_name = str;
  }


  //! Returns the name.
  /*!  \return the name.
   */
  QString
  Atom::name() const
  {
    return m_name;
  }


  //! Sets the symbol.
  /*!  \param str new symbol
   */
  void
  Atom::setSymbol(const QString &str)
  {
    m_symbol = str;
  }


  //! Returns the symbol.
  /*!  \return the symbol.
   */
  QString
  Atom::symbol() const
  {
    return m_symbol;
  }


  //! Calculates the masses(mono and avg).
  /*! The calculation is actually performed by the two functions
    calculateMono() and calculateAvg().
  
    \return true if calculations were successful, false otherwise.

    \sa calculateMono() 

    \sa calculateAvg()
  */
  bool
  Atom::calculateMasses()
  {
    double mono =  calculateMono();
    double avg = calculateAvg();
  
    if (!mono || !avg)
      return false;
  
    return true;
  }


  //! Calculates the monoisotopic mass.
  /*! Calculation is done by iterating in the isotope list and looking
    for the most abundant isotope.
  
    \return the monoisotopic mass.
  */
  double
  Atom::calculateMono()
  {
    double temp = 0;
    double abund = 0;
    
    int idx = -1;

    // All we have to do is find what's the isotope that has the
    // highest abundance.

    for (int iter = 0; iter < m_isotopeList.size(); ++iter)
      {
	abund = m_isotopeList.at(iter)->abundance();

	if(temp == 0)
	  {
	    temp = abund;
	    idx = iter;
	  }
	else
	  {
	    if (abund > temp)
	      {
		temp = abund;
		idx = iter;
	      }
	  }
      }

    // At this point, we should have a idx variable greater than or
    // equal to 0. Note, however, that it might happen that the
    // monomer has no isotope in its list, for example, when the user
    // is editing the atom definitions in the atom definition window.
    if (idx == -1)
      m_mono = 0;
    else
      m_mono = m_isotopeList.at(idx)->mass();

    return m_mono;
  }


  //! Calculates the average mass.
  /*! Calculation is done by taking into account all the isotopes of the
    atom while compounding each isotope's mass with its corresponding
    abundance.
  
    \return the average mass.
  */
  double
  Atom::calculateAvg()
  {
    double total_abundance = 0;
    double mass = 0;
    double abundance = 0;

    // By using the isotopeList of allocated Isotope instances, we
    // can compute the average mass of the current atom instance.

    for (int iter = 0; iter < m_isotopeList.size(); ++iter)
      total_abundance += m_isotopeList.at(iter)->abundance();

    m_avg = 0;

    // Compute the average mass: sum of the product of each isotopic
    // mass per the ratio of the related abundance over the total
    // abundance.

    for (int iter = 0; iter < m_isotopeList.size(); ++iter)
      {
	mass = m_isotopeList.at(iter)->mass();
	abundance = m_isotopeList.at(iter)->abundance();

	m_avg += mass *(abundance / total_abundance);
      }

    return m_avg;
  }



  //! Increments the masses in the arguments.
  /*! The masses used for the accounting are are not found in \p this
    instance, actually. First a reference atom is searched in \p reflist
    using \p this atom's symbol as search criterion. The monoisotopic
    and average masses of the found atom are used for the
    computation. The values pointed to by the first two arguments are
    updated by incrementation using the masses(monoisotopic and
    average) compounded by the \p times argument.

    For example, if \p times is 2 ; \p *mono is 100 and \p *avg is 101 ;
    \p this atom's monoisotopic mass is 200 and average mass is 202,
    then the computation leads to \p *mono = 100 + 2 * 200 and \p *avg =
    101 + 2 * 202.
  
    \param refList list of reference atoms.

    \param mono monoisotopic mass to update. Defaults to 0, in which
    case this mass is not updated.

    \param avg average mass to update. Defaults to 0, in which case this
    mass is not updated.

    \param times times that the increment should be performed. Defaults
    to 1.

    \return true if successful, false otherwise(that is, if no
    reference atom could be found in \p refList).

    \sa accountMasses(double *mono, double *avg, int times)
  */
  bool 
  Atom::accountMasses(const QList<Atom *> &refList,
		       double *mono, double *avg, int times)
  {
    int idx = -1;
  
    // We have to find this atom in the reference List.
    // When we have found it we get an index to that item. We'll get
    // the masses out of the found item and multiply that mass by the
    // times parameter. We'll increment the mass values in massPair
    // accordingly.
  
    idx = isSymbolKnown(refList);
  
    if (idx == -1)
      return false;
  
    if (mono)
      *mono += refList.at(idx)->m_mono * times;
  
    if (avg)
      *avg += refList.at(idx)->m_avg * times;
  
    return true;
  }


  //! Increments the masses in the arguments.
  /*! The values pointed to by the first two arguments are updated by
    incrementation using the masses(monoisotopic and average)
    compounded by the \p times argument.

    For example, if \p times is 2 ; \p *mono is 100 and \p *avg is 101 ;
    \p this atom's monoisotopic mass is 200 and average mass is 202,
    then the computation leads to \p *mono = 100 + 2 * 200 and \p *avg =
    101 + 2 * 202.
  
    \param mono monoisotopic mass to update. Defaults to 0, in which
    case this mass is not updated.
  
    \param avg average mass to update. Defaults to 0, in which case this
    mass is not updated.
  
    \param times times that the increment should be performed. Defaults
    to 1.
  
    \return always true.

    \sa accountMasses(const QList<Atom *> &refList, double *mono,
    double *avg, int times)
  */
  bool 
  Atom::accountMasses(double *mono, double *avg, int times) const
  {
    if (mono)
      *mono += m_mono * times;
  
    if (avg)
      *avg += m_avg * times;
  
    return true;
  }


  //! Searches \p this atom in an atom list according to the symbol.
  /*! The list of reference atoms passed as argument is searched for an
    atom instance that has the same symbol as \p this atom.
  
    \param refList list of reference atoms.
  
    \return the index of the found atom or -1 if none is found or if the
    symbol is empty.
  */
  int
  Atom::isSymbolKnown(const QList<Atom *> &refList) const
  {
    if (m_symbol.isEmpty())
      return -1;
  
    for (int iter = 0; iter < refList.size(); ++iter)
      {
	if(refList.at(iter)->m_symbol == m_symbol)
	  return iter;
      }

    return -1;
  }


  //! Searches an atom in a list according to the \p str symbol.
  /*! Searches for an atom instance having a symbol identical to
    argument \p str in the atom list \p refList. If such atom is found,
    and if \p other is non-0, \p this atom's data are copied into \p
    other.
  
    \param str symbol.

    \param refList list of reference atoms.
  
    \param other atom in which to copy the data from the found
    atom. Defaults to 0, in which case no copying occurs.
  
    \return the int index of the found atom or -1 if no atom instance is
    found or if \p str is empty.
  */
  int
  Atom::isSymbolInList(const QString &str,
			const QList<Atom *> &refList,
			Atom *other)
  {
    if (str.isEmpty())
      return -1;

    for (int iter = 0; iter < refList.size(); ++iter)
      {
        Atom *atom = refList.at(iter);
	Q_ASSERT(atom);
      
	if(atom->m_symbol == str)
	  {
	    if (other != 0)
	      atom->clone(other);
	  
	    return iter;
	  }
      }
  
    return -1;
  }


  //! Searches \p this atom in an atom list according to the name.
  /*! The list of reference atoms passed as argument is searched for an
    atom instance that has the same name as \p this atom.
  
    \param refList list of reference atoms.
  
    \return the index of the found atom or -1 if no atom instance is
    found or if the name is empty.
  */
  int
  Atom::isNameKnown(const QList<Atom *> &refList) const
  {
    if (m_name.isEmpty())
      return -1;

    for (int iter = 0; iter < refList.size(); ++iter)
      {
	if(refList.at(iter)->m_name == m_name)
	  return iter;
      }
  
    return -1;
  }


  //! Searches an atom in a list according to the \p str name.
  /*! Searches for an atom instance having a name identical to
    argument \p str in the atom list \p refList. If such atom is found,
    and if \p other is non-0, \p this atom's data are copied into \p
    other.
  
    \param str name.

    \param refList list of reference atoms.
  
    \param other atom in which to copy the data from the found
    atom. Defaults to 0, in which case no copying occurs.
  
    \return the int index of the found atom or -1 if no atom instance is
    found or if \p str is empty.
  */
  int
  Atom::isNameInList(const QString &str, 
		      const QList<Atom *> &refList,
		      Atom *other)
  {
    Atom *atom = 0;

    if (str.isEmpty())
      return -1;

    for (int iter = 0; iter < refList.size(); ++iter)
      {
	atom = refList.at(iter);
	Q_ASSERT(atom);

	if(atom->m_name == str)
	  {
	    if (other != 0)
	      atom->clone(other);
	  
	    return iter;
	  }
      }
  
    return -1;
  }


  //! Validates \p this atom.
  /*! Validation is performed by ensuring that all member data have sane
    values. Note that the masses(monoisotopic and average) are not
    concerned by the validation process because the presence of at least
    one isotope in the isotope list essentially makes sure that masses
    are available(see below).
  
    \li the name of the atom cannot be empty;
  
    \li the symbol of the atom cannot be empty or longer than 3 Unicode
    \e letter characters. The first character has to be uppercase while
    the remaining ones(if any) have to be lowercase;
  
    \li the isotope list must at least have one isotope item.
  
    \return true if the atom was successfully validated, false otherwise.
  */
  bool 
  Atom::validate()
  {
    if (m_name.isEmpty())
      return false;
  
    if (m_symbol.isEmpty())
      return false;
  
    for (int iter = 0; iter < m_symbol.length(); ++iter)
      {
	QChar currentChar = m_symbol.at(iter);
      
	int category = currentChar.category();
      

	if(category != QChar::Letter_Uppercase && 
	    category != QChar::Letter_Lowercase)
	  return false;
      
	if(!iter)
	  {
	    if (category != QChar::Letter_Uppercase)
	      return false;
	  }
	else
	  {
	    if (category != QChar::Letter_Lowercase)
	      return false;
	  }
      }
  
    if (m_isotopeList.size()  < 1)
      return false;
  
    return true;
  }

  

  //! Parses an atom XML element.
  /*! Parses the atom XML element passed as argument and for each
    encountered data will set the data to \p this atom(this is called
    XML rendering). The masses are calculated by calling
    calculateMasses() and \p this atom instance is validated by calling
    validate().
  
    \param element XML element to be parsed and rendered.
  
    \return true if parsing and atom validation were successful, false
    otherwise.

    \sa formatXmlAtomElement(int offset, const QString &indent)
  */
  bool
  Atom::renderXmlAtomElement(const QDomElement &element, int version)
  {
    // For the time being the version is not necessary here. As of
    // version up to 2, the current function works ok.

    Isotope *isotope = 0;
    QDomElement child;
    QDomElement childIsotope;

    /* We are willing to create a new PxmAtom instance based on the 
     * following xml data:
     *
     *  <atom>
     *    <name>Hydrogen</name>
     *    <symbol>H</symbol>
     *    <isotope>
     *      <mass>1.0078250370</mass>
     *      <abundance>99.9885000000</abundance>
     *    </isotope>
     *    <isotope>
     *      <mass>2.0141017870</mass>
     *      <abundance>0.0115000000</abundance>
     *    </isotope>
     *  </atom>
     *
     * The element that is pointed to by element is the <atom> node:
     *
     * <atom> element tag:
     *  ^
     *  |
     *  +----- here we are right now.
     *
     * Which means that element.tagName() == "atom" and that
     * we'll have to go one step down to the first child of the 
     * current node in order to get to the <name> element.
     */

    if (element.tagName() != "atom")
      return false;

    child = element.firstChildElement("name");
  
    if (child.isNull())
      return false;
  
    m_name = child.text();
  
    child = child.nextSiblingElement("symbol");
  
    if (child.isNull())
      return false;
  
    m_symbol = child.text();
  
    // And now we have to deal with <isotope> elements, of which there
    // might be one or more(but no zero).
  
    childIsotope = child.nextSiblingElement("isotope");
  
    while(!childIsotope.isNull()) 
      {   
	// We have arrived to the first <isotope> element, for which we
	// delegate the parsing to the appropriate function:

	isotope = new(Isotope);
      
	if(!isotope->renderXmlIsotopeElement(childIsotope, version))
	  {
	    delete isotope;
	    return false;
	  }

	// Add the newly dynallocated isotope to the List.
	m_isotopeList.append(isotope);
      
	childIsotope = childIsotope.nextSiblingElement("isotope");
      }
  
    // Sort all the isotopes in mass-increasing order.
    qSort(m_isotopeList.begin(), m_isotopeList.end(), Isotope::lessThan);
  
    //   for (int iter = 0; iter < m_isotopeList.size(); ++iter)
    //     qDebug() << __FILE__ << __LINE__ 
    // 	      << "After Sort Atom:" << m_name 
    // 	      << m_isotopeList.at(iter)->mass();

    // We have to compute the mono/avg masses so that the atom is
    // immediately usable.
    calculateMasses();
    //  qDebug() << "mono:" << m_mono << "avg:" << m_avg;

    // Validate this atom.
    if (!validate())
      return false;

    return true;
  }

			
  //! Formats a string suitable to use as an XML element.
  /*! Formats a string suitable to be used as an XML element in a
    polymer chemistry definition file. The typical atom element that is
    generated in this function looks like this:

    \verbatim 
    <atom>
    <name>Hydrogen</name>
    <symbol>H</symbol>
    <isotope>
    <mass>1.0078250370</mass>
    <abundance>99.9885000000</abundance>
    </isotope>
    <isotope>
    <mass>2.0141017870</mass>
    <abundance>0.0115000000</abundance>
    </isotope>
    </atom>
    \endverbatim  
  
    \param offset times the \p indent string must be used as a lead in the
    formatting of elements.

    \param indent string used to create the leading space that is placed
    at the beginning of indented XML elements inside the XML
    element. Defaults to two spaces(QString(" ")).

    \return a dynamically allocated string that needs to be freed after
    use.

    \sa renderXmlAtomElement(const QDomElement &element)
  */
  QString *
  Atom::formatXmlAtomElement(int offset, const QString &indent)
  {
    int newOffset;
    int iter = 0;
  
    QString lead("");
    QString *string = new QString();
  
    Isotope *isotope = 0;
  

    // Prepare the lead.
    newOffset = offset;  
    while(iter < newOffset)
      {
	lead += indent;
	++iter;
      }

    *string += QString("%1<atom>\n")
      .arg(lead);
  
    // Prepare the lead.
    ++newOffset;
    lead.clear();
    iter = 0;
    while(iter < newOffset)
      {
	lead += indent;
	++iter;
      }
  
    // Continue with indented elements.

    *string += QString("%1<name>%2</name>\n")
      .arg(lead)
      .arg(m_name);

    *string += QString("%1<symbol>%2</symbol>\n")
      .arg(lead)
      .arg(m_symbol);
    
    // At this point we have the different isotope(s) of the atom. We
    // delegate that to the appropriate function of the isotope class.

    for (int jter = 0; jter < m_isotopeList.size(); ++jter)
      {
	isotope = m_isotopeList.at(jter);
      
	QString *isotopeString = isotope->formatXmlIsotopeElement(newOffset);
      
	*string += *isotopeString;
      
	delete(isotopeString);
      }
  
    // Prepare the lead for the closing element.
    --newOffset;
    lead.clear();
    iter = 0;
    while(iter < newOffset)
      {
	lead += indent;
	++iter;
      }

    *string += QString("%1</atom>\n")
      .arg(lead);
    
    return string;
  }

} // namespace massXpert
