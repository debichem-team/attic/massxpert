/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#ifndef PEAK_SHAPE_HPP
#define PEAK_SHAPE_HPP


/////////////////////// Qt includes
#include <QList>
#include <QPointF>


/////////////////////// Local includes
#include "globals.hpp"
#include "peakShapeConfig.hpp"
#include "peakCentroid.hpp"


namespace massXpert
{

  // The PeakShape class contains all required data to describe a
  // gaussian or lorentzian shape corresponding to a given centroid
  // (x,y) with z=m/z and y=relative intensity. The class contains all
  // the data rquired to computer either a gaussian shape or a
  // lorentzian shape. The points that make the shape are stored as
  // QPointF (double x, double y) instances in the
  // QList<QPointF*>m_pointList.

  class PeakShape : public PeakCentroid
  {
  private:
    QList<QPointF *>m_pointList;
  
    PeakShapeConfig m_config;
        
  public:
    PeakShape();
    PeakShape(double /* mz */,
              double /* intensity */, 
              double /* probability */, 
              PeakShapeConfig &/* config */);
    PeakShape(PeakCentroid &, PeakShapeConfig &);
    
    PeakShape(const PeakShape &);
    ~PeakShape();
    
    const QList<QPointF *> &pointList() const;
    
    void appendPoint(QPointF *);
    void emptyPointList();
    
    QPointF *firstPoint() const;
    QPointF *lastPoint() const;
    
    QList<QPointF *> *duplicatePointList() const;
    int transferPoints(QList<QPointF *> *);

    const PeakShapeConfig &config() const;
    void setPeakShapeConfig(const PeakShapeConfig &);
    
    void setMz(double);
    double mz();
    QString mzString();
    
    void setRelativeIntensity(double);
    double relativeIntensity();
    QString relativeIntensityString();
      
    int calculatePeakShape();
    int calculateGaussianPeakShape();
    int calculateLorentzianPeakShape();

    double intensityAt(double, double, bool *);

    QString dataAsString();
    bool dataToFile(QString);
  } ;

} // namespace massXpert

#endif // PEAK_SHAPE_HPP
