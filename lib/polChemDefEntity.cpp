/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Local includes
#include "polChemDefEntity.hpp"
#include "polChemDef.hpp"


namespace massXpert
{

  //! Constructs a polymer chemistry definition entity.
  /*! 
  
    \param polChemDef reference polymer chemistry definition. This pointer
    cannot be 0.

    \param name name of the entity.
  */
  PolChemDefEntity::PolChemDefEntity(const PolChemDef *polChemDef,
				      const QString &name)
  {
    Q_ASSERT(polChemDef);
    mp_polChemDef = polChemDef;
  
    Q_ASSERT(!name.isEmpty());
    m_name = name;
  }


  //! Constructs a copy of \p other.
  /*!  \param other entity to be used as a mold.
   */
  PolChemDefEntity::PolChemDefEntity(const PolChemDefEntity &other)
  {
    Q_ASSERT(other.mp_polChemDef);
    mp_polChemDef = other.mp_polChemDef;

    Q_ASSERT(!other.m_name.isEmpty());
    m_name = other.m_name;
  }


  PolChemDefEntity::~PolChemDefEntity()
  {
  }


  //! Creates a new polymer chemistry definition entity.
  /*! The newly created polymer chemistry definition entity is
    initialized with the data in \c this.

    \return the newly allocated polymer chemistry definition entity which
    will have to be deleted when no longer in use.
  */
  PolChemDefEntity *
  PolChemDefEntity::clone() const
  {
    PolChemDefEntity *other = new PolChemDefEntity(*this);
  
    return other;
  }


  //! Modifies \p other to be identical to \p this.
  /*!  \param other entity to modify.
   */
  void 
  PolChemDefEntity::clone(PolChemDefEntity *other) const
  {
    Q_ASSERT(other);
  
    Q_ASSERT(mp_polChemDef);
    other->mp_polChemDef = mp_polChemDef;
  
    Q_ASSERT(!m_name.isEmpty());
    other->m_name = m_name;
  }


  //! Modifies \p this  to be identical to \p other.
  /*!  \param other entity to be used as a mold.
   */
  void 
  PolChemDefEntity::mold(const PolChemDefEntity &other)
  {
    if (&other == this)
      return;

    Q_ASSERT(other.mp_polChemDef);
    mp_polChemDef = other.mp_polChemDef;

    Q_ASSERT(!other.m_name.isEmpty());
    m_name = other.m_name;
  }


  //! Assigns other to \p this entity.
  /*! \param other entity used as the mold to set values to \p this
    instance.
  
    \return a reference to \p this entity.
  */
  PolChemDefEntity & 
  PolChemDefEntity::operator =(const PolChemDefEntity &other)
  {
    if (&other != this)
      mold(other);
  
    return *this;
  }


  //! Sets the name.
  /*!  \param text new name.
   */
  void 
  PolChemDefEntity::setName(const QString &text)
  {
    m_name = text;
  }


  //! Returns the name.
  /*!  \return the name.
   */
  QString 
  PolChemDefEntity::name() const
  {
    Q_ASSERT(!m_name.isEmpty());
    return m_name;
  }


  void 
  PolChemDefEntity::setPolChemDef(PolChemDef *polChemDef)
  {
    mp_polChemDef = polChemDef;
  }
  

  //! Returns the polymer chemistry definition.
  /*!  \return the polymer chemistry definition. Assertion that it is
    non-0 is performed before the return. Non-0 return is thus
    guaranteed.
  */
  const PolChemDef *
  PolChemDefEntity::polChemDef() const
  {
    Q_ASSERT(mp_polChemDef);  
    return mp_polChemDef;
  }

  
  //! Tests equality.
  /*! The test is performed on:

    - The polymer chemistry definition pointer, not the contents of that
    polymer chemistry definition;

    - The name.
  
    \param other entity to be compared with \p this.

    \return true if the entities are identical, false otherwise.
  */
  bool 
  PolChemDefEntity::operator ==(const PolChemDefEntity &other) const
  {
    int tests = 0;

    Q_ASSERT(mp_polChemDef && other.mp_polChemDef);
    tests +=(mp_polChemDef == other.mp_polChemDef);

    Q_ASSERT(!m_name.isEmpty() && !other.m_name.isEmpty());
    tests +=(m_name == other.m_name);

    if (tests < 2)
      return false;
  
    return true;
  }


  //! Tests inequality.
  /*! The test is performed on:

    - The polymer chemistry definition pointer, not the contents of that
    polymer chemistry definition;

    - The name.
  
    \param other entity to be compared with \p this.

    \return true if the entities differ, false otherwise.
  */
  bool 
  PolChemDefEntity::operator !=(const PolChemDefEntity &other) const
  {
    int tests = 0;

    Q_ASSERT(mp_polChemDef && other.mp_polChemDef);
    tests +=(mp_polChemDef != other.mp_polChemDef);
  
    Q_ASSERT(!m_name.isEmpty() && !other.m_name.isEmpty());
    tests +=(m_name != other.m_name);
  
    if (tests > 0)
      return true;
  
    return false;
  }


  bool  
  PolChemDefEntity::validate() const
  {
    int tests = 0;
  
    tests +=(m_name.isNull() || m_name.isEmpty());
    tests +=(!mp_polChemDef);
 
    if (tests)
      return false;
 
    return true;
  }

} // namespace massXpert
