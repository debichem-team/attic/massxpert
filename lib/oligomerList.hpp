/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef OLIGOMER_LIST_HPP
#define OLIGOMER_LIST_HPP


/////////////////////// Qt includes
#include <QLocale>


/////////////////////// Local includes
#include "globals.hpp"
#include "oligomer.hpp"


namespace massXpert
{

  class OligomerList : public QObject, public QList<Oligomer *>
  {
    Q_OBJECT 

    private:
    QString m_name;
    QString m_comment;
    const QPointer<Polymer> mp_polymer;
    MassType m_massType;
  
  public:
    OligomerList(QString = QString(), 
		  Polymer * = 0,
		  MassType = MXT_MASS_BOTH);
    OligomerList(const OligomerList *);
    
    virtual ~OligomerList();
  
    void setName(QString);
    QString name();
  
    void setComment(QString);
    const QString &comment() const;

    const Polymer *polymer() const;

    void setMassType(MassType);
    const MassType massType() const;

    Oligomer *findOligomerEncompassing(int index, int * = 0);
    Oligomer *findOligomerEncompassing(const Monomer*, int * = 0);
      
    // In the methods below, MassType is by default MXT_MASS_NONE,
    // which means that the value of the variable is to be read in the
    // member m_massType.

    QString *makeMassText(MassType = MXT_MASS_NONE, QLocale = QLocale());

    static bool lessThanMono(const Oligomer *, const Oligomer *);
    static bool lessThanAvg(const Oligomer *, const Oligomer *);

    void sortAscending();
    void sortDescending();
  
    void applyMass(double, MassType = MXT_MASS_NONE);
    void removeLessThan(double, MassType = MXT_MASS_NONE);
  };

} // namespace massXpert


#endif // OLIGOMER_LIST_HPP
