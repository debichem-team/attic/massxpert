/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef CALC_OPTIONS_HPP
#define CALC_OPTIONS_HPP


/////////////////////// Qt includes
#include <QString>


/////////////////////// Local includes
#include "globals.hpp"
#include "coordinates.hpp"

namespace massXpert
{

  enum SelectionType
    {
      SELECTION_TYPE_RESIDUAL_CHAINS,
      SELECTION_TYPE_OLIGOMERS,
    };
  
  //! The CalcOptions class provides calculation options.
  /*! Calculations can be performed in two manners :

    - by taking into account the whole sequence;

    - by taking into account the selected region of the sequence;

    In the first case the computation is for the \em polymer, while in
    the second case the computation is for an \em oligomer.

    Options involve also the way the computations are taking into
    account the left/end caps of the sequence or the monomer
    modifications, or the polymer modifications...
  */
  class CalcOptions
  {
  private:
    //! Tells if the calculation should involve the recalculation of
    /*! all the masses of the monomers of the sequence.
     */
    bool m_deepCalculation;

    CoordinateList m_coordinateList;

    //! MXT_MASS_TYPE.
    int m_massType;

    //! MXT_CAP_TYPE.
    int m_capping;

    //! MXT_MONOMER_CHEMENT.
    int m_monomerEntities;

    //! MXT_POLYMER_CHEMENT.
    int m_polymerEntities;

    SelectionType m_selectionType;
    
  public:
    CalcOptions();
    CalcOptions(bool, int = MXT_MASS_BOTH, 
		 int = MXT_CAP_BOTH, 
		 int = MXT_MONOMER_CHEMENT_NONE, 
		 int = MXT_POLYMER_CHEMENT_NONE);
    CalcOptions(const CalcOptions &);

    ~CalcOptions();

    void clone(CalcOptions *) const;
    void mold(const CalcOptions &);
    CalcOptions & operator =(const CalcOptions &);
  
    void setDeepCalculation(bool);
    bool isDeepCalculation() const;
    
    void setStartIndex(int);
    int startIndex() const;
  
    void setEndIndex(int);
    int endIndex() const;

    void setCoordinateList(const Coordinates & = Coordinates());
    void setCoordinateList(const CoordinateList &);
    const CoordinateList & coordinateList() const;
    
    void setMassType(int);
    int massType() const;

    void setSelectionType(SelectionType);
    SelectionType selectionType() const;
    
    void setCapping(int);
    int capping() const;

    void setMonomerEntities(int);
    int monomerEntities() const;

    void setPolymerEntities(int);
    int polymerEntities() const;

    void debugPutStdErr() const;
  };

} // namespace massXpert


#endif // CALC_OPTIONS_HPP

