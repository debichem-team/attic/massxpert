/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef MONOMER_HPP
#define MONOMER_HPP
//#warning "Entering MONOMER_HPP"


/////////////////////// Qt includes
#include <QString>


/////////////////////// Local includes
#include "polChemDefEntity.hpp"
#include "formula.hpp"
#include "ponderable.hpp"
#include "propListHolder.hpp"
#include "calcOptions.hpp"
#include "modif.hpp"


namespace massXpert
{

  //! The Monomer class provides a monomer.
  /*! A monomer is the building block of a polymer sequence. It is
    mainly characterized by a name, a code and a formula.
  */
  class Monomer : public PolChemDefEntity, 
		  public Formula, 
		  public Ponderable, 
		  public PropListHolder
  {
    private:
    //! Code.
    QString m_code; 

    QList<Modif *> *mpa_modifList;
    
  public:
    Monomer(const PolChemDef *, QString,
	     QString = QString(), QString = QString());
  
    ~Monomer();
  
    Monomer *clone() const;
    void clone(Monomer *) const;
    void mold(const Monomer &);
  
    void setCode(const QString &);
    QString code() const;

    bool operator ==(const Monomer &) const;
    bool operator !=(const Monomer &) const;

    bool checkCodeSyntax() const;

    int isCodeKnown() const;
    static int isCodeInList(const QString &, 
			     const QList<Monomer*> &,
			     Monomer* = 0);
  
    int isNameKnown() const;
    static int isNameInList(const QString &, 
			     const QList<Monomer*> &,
			     Monomer* = 0);
  
    QString formula() const;
        
    QList<Modif *> *modifList() const;
  
    bool isModifTarget(const Modif &) const;
    bool modify(Modif *, bool , QStringList &);
    bool unmodify();
    bool unmodify(Modif *);
    bool isModified() const;

    int modifCount(const QString&);
      
    bool validate();
  
    bool calculateMasses(int = MXT_MONOMER_CHEMENT_NONE);
  
    bool accountMasses(double * = 0, double * = 0, int = 1) const;
    bool accountMasses(Ponderable *, int) const;
  
    bool renderXmlMnmElement(const QDomElement &, int);

    QString* formatXmlMnmElement(int, const QString & = QString("  "));

    bool renderXmlMonomerElement(const QDomElement &, int);
    bool renderXmlMonomerElementV2(const QDomElement &, int);
  
    QString* formatXmlMonomerElement(int, const QString & = QString("  ")) const;
  
    void debugPutStdErr() const;
  };

} // namespace massXpert


#endif // MONOMER_HPP

