/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Local includes
#include "oligomerList.hpp"
#include "polymer.hpp"

namespace massXpert
{

  OligomerList::OligomerList(QString name, 
			      Polymer *polymer,
			      MassType massType)
    : m_name(name), mp_polymer(polymer), m_massType(massType)
  {
  }


  OligomerList::OligomerList(const OligomerList *other)
    : QList<Oligomer*>(*other),
      m_name(other->m_name),
      m_comment(other->m_comment),
      mp_polymer(other->mp_polymer), 
      m_massType(other->m_massType)
  {
    // And now copy all the items from other in here:
    for (int iter = 0; iter < other->size(); ++iter)
      append(new Oligomer(*other->at(iter)));
  }


  OligomerList::~OligomerList()
  {
    //   qDebug() << __FILE__ << __LINE__
    // 	    << "Deleting all the oligomers in the list";
    qDeleteAll(begin(), end());
    clear();
  }


  //! Sets the name.
  /*!  \param name new name.
   */
  void
  OligomerList::setName(QString name)
  {
    if (!name.isEmpty())
      m_name = name;
  }


  QString
  OligomerList::name()
  {
    return m_name;
  }


  //! Sets the comment.
  /*!  \param comment new comment.
   */
  void
  OligomerList::setComment(QString comment)
  {
    if (!comment.isEmpty())
      m_comment = comment;
  }


  const QString &
  OligomerList::comment() const
  {
    return m_comment;
  }


  const Polymer *
  OligomerList::polymer() const
  {
    return mp_polymer;
  }


  void
  OligomerList::setMassType(MassType massType)
  {
    m_massType = massType;
  }


  const MassType
  OligomerList::massType() const
  {
    return m_massType;
  }


  Oligomer *
  OligomerList::findOligomerEncompassing(int index, int *listIndex)
  {
    // Is there any oligomer that emcompasses the index ? If so return
    // its pointer. Start searching in the list at index *listIndex if
    // parameter non-0.
  
    int iter = 0;
  
    if (listIndex)
      {
	if(*listIndex >= size())
	  return 0;
      
	if(*listIndex < 0)
	  return 0;
      
	iter = *listIndex;
      }
  
    for (; iter < size(); ++iter)
      {
	Oligomer *oligomer = at(iter);
      
	if(oligomer->encompasses(index))
	  {
	    if (listIndex)
	      *listIndex = iter;
	  
	    return oligomer;
	  }
      }
  
    return 0;
  }



  Oligomer *
  OligomerList::findOligomerEncompassing(const Monomer *monomer, int *listIndex)
  {
    // Is there any oligomer that emcompasses the monomer ? If so return
    // its pointer. Start searching in the list at index *listIndex if
    // parameter non-0.
  
    Q_ASSERT(monomer);
  
    int iter = 0;
  
    if (listIndex)
      {
	if(*listIndex >= size())
	  return 0;
      
	if(*listIndex < 0)
	  return 0;
      
	iter = *listIndex;
      }
  
    for (; iter < size(); ++iter)
      {
	Oligomer *oligomer = at(iter);
      
	if(oligomer->encompasses(monomer))
	  {
	    if (listIndex)
	      *listIndex = iter;
	  
	    return oligomer;
	  }
      }
  
    return 0;
  }



  QString *
  OligomerList::makeMassText(MassType massType, QLocale locale)
  {
    QString *text = new QString();
  
    MassType localMassType = MXT_MASS_NONE;
  
    if (massType == MXT_MASS_BOTH)
      {
	delete text;
	return 0;
      }
      
    if (massType & MXT_MASS_NONE)
      localMassType = m_massType;
    else
      localMassType = massType;
  
    for (int iter = 0; iter < size(); ++iter)
      {
	double value = 0;
      
	if(localMassType & MXT_MASS_MONO)
	  value = at(iter)->mono();
	else
	  value = at(iter)->avg();
      
	QString mass = locale.toString(value, 'f', MXP_OLIGOMER_DEC_PLACES);
	
	text->append(mass + "\n");
      }

    return text;
  }


  bool
  OligomerList::lessThanMono(const Oligomer *o1, 
			      const Oligomer *o2)
  {
    qDebug() << "o1 is:" << o1 << "o2 is:" << o2;
  
    return true;
  }


  bool
  OligomerList::lessThanAvg(const Oligomer *o1, 
			     const Oligomer *o2)
  {
    qDebug() << "o1 is:" << o1 << "o2 is:" << o2;
  
    return true;
  }


  void 
  OligomerList::sortAscending()
  {
    // We only can sort if this instance knows which mass type it should
    // handle, and that cannot be anything other than MXT_MASS_MONO or
    // MXT_MASS_AVG.

    if (m_massType == MXT_MASS_BOTH || m_massType & MXT_MASS_NONE)
      {
	qDebug() << "Could not sort the oligomer list: "
	  "m_massType is not correct";
      
	return;
      }
  
    if (m_massType == MXT_MASS_MONO)
      qSort(begin(), end(), OligomerList::lessThanMono);
    else
      qSort(begin(), end(), OligomerList::lessThanAvg);
  }

} // namespace massXpert
