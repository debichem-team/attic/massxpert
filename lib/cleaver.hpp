/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef CLEAVER_HPP
#define CLEAVER_HPP


/////////////////////// Local includes
#include "cleaveSpec.hpp"
#include "cleaveOptions.hpp"
#include "calcOptions.hpp"
#include "polymer.hpp"
#include "ionizeRule.hpp"
#include "cleaveOligomer.hpp"
#include "oligomerList.hpp"


namespace massXpert
{

  class Cleaver 
  {
  private:
    const QPointer<Polymer> mp_polymer;
    const PolChemDef *mp_polChemDef;
    CleaveOptions m_cleaveOptions;
    CalcOptions m_calcOptions;
    IonizeRule m_ionizeRule;
  
    QList<int> m_cleaveIndexList;
    QList<int> m_noCleaveIndexList;

    // Pointer to an oligomer list which WE DO NOT OWN.
    OligomerList *mp_oligomerList;
  
  public:
    Cleaver(Polymer *, const PolChemDef *, 
	     const CleaveOptions &,
	     const CalcOptions &,
	     const IonizeRule &);
  
    Cleaver(const Cleaver &);
    ~Cleaver();

    void setOligomerList(OligomerList *);
    OligomerList *oligomerList();
  
    bool cleave(bool = false);
    int cleavePartial(int);

    int analyzeCrossLinks(OligomerList *);
    int analyzeCrossLinkedOligomer(Oligomer *, OligomerList *);
  
    int fillIndexLists();
    int resolveCleavageNoCleavage();
    int removeDuplicatesCleavage();
    
    int findCleaveMotif(CleaveMotif &, int, int);
    bool accountCleaveRule(CleaveRule *, CleaveOligomer *);
  
    void emptyOligomerList();
  };

} // namespace massXpert


#endif // CLEAVER_HPP
