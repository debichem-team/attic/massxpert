/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef COORDINATES_HPP
#define COORDINATES_HPP


/////////////////////// Qt includes
#include <QString>
#include <QList>


namespace massXpert
{

  class Coordinates
  {
  protected:
    int m_start;
    int m_end;
  
  public:
    Coordinates(int = -1, int = -1);
    Coordinates(const Coordinates &);
    ~Coordinates();
  
    void setStart(int);
    int start() const;
  
    void setEnd(int);
    void incrementEnd();
    int end() const;
  
    int length() const;
    QString indicesAsText() const;
    QString positionsAsText() const;

    bool setFromText(QString);
      
    void reset();
  };

  

  class CoordinateList : public QList<Coordinates *>
  {
  protected:
    QString m_comment;
  
  public:
    CoordinateList(QString = QString(), QList<Coordinates *> * = 0);

    CoordinateList(const CoordinateList &);
  
    ~CoordinateList();
    
    CoordinateList & operator =(const CoordinateList &other);
    
    void setCoordinates(const Coordinates &);
    void setCoordinates(const CoordinateList &);
    void appendCoordinates(const Coordinates &);
    int setCoordinates(const QString &);
    
    void setComment(QString comment);
    QString comment() const;

    int leftMostCoordinates(QList<int> &) const;
    bool isLeftMostCoordinates(Coordinates *) const;

    int rightMostCoordinates(QList<int> &) const;
    bool isRightMostCoordinates(Coordinates *) const;
    
    bool encompassIndex(int) const;
    
    bool overlap() const;
    
    QString indicesAsText() const;
    QString positionsAsText() const;

    void empty();

    void debugPutStdErr();
  };

} // namespace massXpert


#endif // COORDINATES_HPP
