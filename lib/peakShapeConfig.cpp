/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// Local includes
#include "peakShapeConfig.hpp"

///////////////////////
/// For the M_PI Pi value
#include <math.h>


namespace massXpert
{

  PeakShapeConfig::PeakShapeConfig()
    : m_fwhm(0),
      m_pointNumber(0), 
      m_increment(0),
      m_normFactor(1), 
      m_peakShapeType(MXP_PEAK_SHAPE_TYPE_GAUSSIAN)
  {
    
  }


  PeakShapeConfig::PeakShapeConfig(double fwhm,
                                   int pointNumber,
                                   float increment,
                                   double normFactor,
                                   PeakShapeType peakShapeType)
    : m_fwhm(fwhm),
      m_pointNumber(pointNumber), 
      m_increment(increment),
      m_normFactor(normFactor), 
      m_peakShapeType(peakShapeType)
  {
  
  }


  PeakShapeConfig::PeakShapeConfig(const PeakShapeConfig &other)
    : m_fwhm(other.m_fwhm),
      m_pointNumber(other.m_pointNumber), 
      m_increment(other.m_increment),
      m_normFactor(other.m_normFactor), 
      m_peakShapeType(other.m_peakShapeType)
  {

  }


  PeakShapeConfig::~PeakShapeConfig()
  {
    
  }


  void
  PeakShapeConfig::setConfig(double fwhm,
                             int pointNumber,
                             float increment,
                             double normFactor,
                             PeakShapeType peakShapeType)
  {
    m_fwhm =fwhm;
    m_pointNumber = pointNumber;
    m_increment = increment;
    m_normFactor = normFactor;
    m_peakShapeType = peakShapeType;
  }
  

  void 
  PeakShapeConfig::setConfig(const PeakShapeConfig &other)
  {
    m_fwhm =other.m_fwhm;
    m_pointNumber = other.m_pointNumber; 
    m_increment = other.m_increment;
    m_normFactor = other.m_normFactor; 
    m_peakShapeType = other.m_peakShapeType;
  }
  
  
  void
  PeakShapeConfig::setFwhm(double value)
  {
    m_fwhm = value;
  }


  double  
  PeakShapeConfig::fwhm()
  {
    return m_fwhm;
  }


  QString  
  PeakShapeConfig::fwhmString()
  {
    QString string;
    string.setNum(m_fwhm);
  
    return string;
  }
  
  void
  PeakShapeConfig::setNormFactor(double value)
  {
    m_normFactor = value;
  }


  double  
  PeakShapeConfig::normFactor()
  {
    return m_normFactor;
  }


  QString  
  PeakShapeConfig::normFactorString()
  {
    QString string;
    string.setNum(m_normFactor);
    
    return string;
  }
  
  // For the lorentzian, that is half of the fwhm.
  double  
  PeakShapeConfig::hwhm()
  {
    return (m_fwhm / 2);
  }


  QString  
  PeakShapeConfig::hwhmString()
  {
    QString string;
    string.setNum(m_fwhm/2);
  
    return string;
  }


  void
  PeakShapeConfig::setPointNumber(int value)
  {
    m_pointNumber = value;
  }

  
  int
  PeakShapeConfig::pointNumber()
  {
    return m_pointNumber;
  }

  
  QString
  PeakShapeConfig::pointNumberString()
  {
    QString string;
    string.setNum(m_pointNumber);
    
    return string;
  }


  void 
  PeakShapeConfig::setPeakShapeType(PeakShapeType value)
  {
    m_peakShapeType = value;
  }
  

  PeakShapeType 
  PeakShapeConfig::peakShapeType()
  {
    return m_peakShapeType;
  }
  

  double  
  PeakShapeConfig::a()
  {
    //  double pi = 3.1415926535897932384626433832795029;
  
    double a = (1 / (c()*sqrt(M_PI)));
   
    // qDebug() << __FILE__ << __LINE__
    //          << "a:" << a;
    
    return a;
  }


  QString  
  PeakShapeConfig::aString()
  {
    double aValue = a();
  
    QString string;
    string.setNum(aValue);
  
    return string;
  }
  

  double  
  PeakShapeConfig::c()
  {
    double c = m_fwhm / (2 * sqrt(2*log(2)));

    // qDebug() << __FILE__ << __LINE__
    //          << "c:" << c;
    
    return c;
  }

  QString  
  PeakShapeConfig::cString()
  {

    double cValue = c();
  
    QString string;
    string.setNum(cValue);
  
    return string;
  }
  
  double  
  PeakShapeConfig::gamma()
  {
    double gamma = m_fwhm / 2;
  
    // qDebug() << __FILE__ << __LINE__
    //          << "gamma:" << gamma;

    return gamma;
  }

  QString  
  PeakShapeConfig::gammaString()
  {
    double gammaValue = gamma();
  
    QString string;
    string.setNum(gammaValue);
  
    return string;
  }


  void
  PeakShapeConfig::setIncrement(double value)
  {
    m_increment = value;
  }


  double
  PeakShapeConfig::increment()
  {
    // But what is m_increment ? We want the shape to be able to go
    // down to baseline. Thus we want that the shape to have a "basis"
    // corresponding to twice the FWHM on the left of the centroid and
    // to twice the FWHM on the right (that makes in total
    // MXP_FWHM_PEAK_SPAN_FACTOR * FWHM).
    if(!m_increment)
      {
        m_increment = (MXP_FWHM_PEAK_SPAN_FACTOR * m_fwhm) / m_pointNumber;
      }
    
    return m_increment;
  }


  QString  
  PeakShapeConfig::incrementString()
  {
    QString string;
    string.setNum(m_increment);
  
    return string;
  }


  QString  
  PeakShapeConfig::config()
  {
    QString string;
    QString value;
    
    if(m_peakShapeType == MXP_PEAK_SHAPE_TYPE_GAUSSIAN)
      {
        
        string += "Gauss:\n";
        
        value = QString().setNum(m_fwhm, 'f', 5);
        string += "FWHM:" ;
        string += value;
        string += "\n";
        
        double cValue = c();
        value = QString().setNum(cValue, 'f', 5);
        string += "c:" ;
        string += value;
        string += "\n";
              
        value = QString().setNum((cValue * cValue) , 'f', 5);
        string += "c^2:" ;
        string += value;
        string += "\n";
                
        value = QString().setNum(increment(), 'f', 5);
        string += "increment:" ;
        string += value;
        string += "\n\n";
      }
    
    if(m_peakShapeType == MXP_PEAK_SHAPE_TYPE_LORENTZIAN)
      {
        
        string += "Gauss:\n";
        
        value = QString().setNum(m_fwhm, 'f', 5);
        string += "FWHM:" ;
        string += value;
        string += "\n";
        
        double gammaValue = gamma();
        value = QString().setNum(gammaValue, 'f', 5);
        string += "gamma:" ;
        string += value;
        string += "\n";
        
        value = QString().setNum((gammaValue * gammaValue) , 'f', 5);
        string += "gamma^2:" ;
        string += value;
        string += "\n";
        
        value = QString().setNum(increment(), 'f', 5);
        string += "increment:" ;
        string += value;
        string += "\n\n";
      }
    
    return string;
  }

  
} // namespace massXpert
