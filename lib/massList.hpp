/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef MASS_LIST_HPP
#define MASS_LIST_HPP


/////////////////////// Qt includes
#include <QtGlobal>
#include <QLocale>
#include <QStringList>
#include <QDebug>


namespace massXpert
{

  class MassList
  {
  private:
    QString m_name;
    QString m_comment;
  
    QLocale m_locale;
    
    QString m_massText;
    QStringList m_massStringList;
    QList <double> m_massList;
  
  public:
    MassList(QString, 
	      const QLocale & = QLocale());
    MassList(QString, const QString &, 
	      const QLocale & = QLocale());
    MassList(QString, const QStringList &, 
	      const QLocale & = QLocale());
    MassList(QString, const QList <double> &, 
	      const QLocale & = QLocale());
    MassList(const MassList &);
    
    ~MassList();
  
    MassList * clone() const;
    void clone(MassList *) const;
    void mold(const MassList &);
    MassList & operator =(const MassList &);
  
    void setName(QString);
    QString name();
  
    void setComment(QString);
    const QString &comment() const;

    void setLocale(const QLocale &);
  
    int size() const;
  
    const QList <double> &massList() const;

    double at(int) const;
  
    const QString &massText() const;
  
    int makeMassList();
    int makeMassText();

    void sortAscending();
    void sortDescending();

    void applyMass(double);
    void removeLessThan(double);
  };

} // namespace massXpert


#endif // MASS_LIST_HPP
