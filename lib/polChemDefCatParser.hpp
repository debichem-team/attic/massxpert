/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef POL_CHEM_DEF_CAT_PARSER_HPP
#define POL_CHEM_DEF_CAT_PARSER_HPP


/////////////////////// Local includes
#include "polChemDefSpec.hpp"
#include "configSettings.hpp"


namespace massXpert
{

  enum 
    {
      POLCHEMDEF_CAT_PARSE_APPEND_CONFIG = 1 << 0,
      POLCHEMDEF_CAT_PARSE_PREPEND_CONFIG = 1 << 1,
      POLCHEMDEF_CAT_PARSE_SYSTEM_CONFIG =  1 << 2,
      POLCHEMDEF_CAT_PARSE_USER_CONFIG = 1 << 3,
      POLCHEMDEF_CAT_PARSE_BOTH_CONFIG = 
      POLCHEMDEF_CAT_PARSE_SYSTEM_CONFIG 
      | POLCHEMDEF_CAT_PARSE_USER_CONFIG,
    };


  class PolChemDefCatParser
  {
  private:
    QString m_filePath;
    
    int m_pendingMode;
    int m_configSysUser;

    const ConfigSettings *mp_configSettings;
    
  
  public:
    PolChemDefCatParser();
    ~PolChemDefCatParser();

    void setFilePath(const QString &);
    const QString &getFilePath();

    void setPendingMode(int);
    int getPendingMode();

    void setConfigSysUser(int);
    int getConfigSysUser();

    int parseFiles(const ConfigSettings &,
		    QList<PolChemDefSpec *> *);
  
    int parse(QList<PolChemDefSpec *> *, int);
  };

} // namespace massXpert


#endif // POL_CHEM_DEF_CAT_PARSER_HPP
