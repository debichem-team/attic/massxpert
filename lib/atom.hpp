/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef ATOM_HPP
#define ATOM_HPP


/////////////////////// Qt includes
#include <QString>
#include <QList>


/////////////////////// Local includes
#include "isotope.hpp"
#include "ponderable.hpp"


namespace massXpert
{

  //! The Atom class provides an atom.
  /*! An atom(also known as a chemical element) is a chemical entity
    that bears the following characteristics:

    \li a name(like Carbon);
  
    \li a symbol(like C);
  
    \li a monoisotopic mass that is the mass of the lightest isotope
   (see below);

    \li an average mass that is the average mass of all the isotopes
    weighed by their respective abundance;
  
    \li the list of all the isotopes that are comprised by the chemical
    element(for Carbon, there are two main isotopes
    \f$\mathrm{^{12}C}\f$ and \f$\mathrm{^{13}C}\f$.
  */
  class Atom : public Ponderable
  {
  protected:
    //! Name.
    QString m_name;

    //! Symbol
    /*! The symbol may not contain more than one uppercase
      character. The first character must be uppercase, and the remaining
      ones lowercase. Note that unlike other software, the number of
      characters allowed for a given symbol is not restricted to two
      letters. An atom symbol cannot be more than 3 letters-long.

      \sa validate()
    */
    QString m_symbol;
  
    //! List of the isotopes making up the atom.
    /*! This list is populated with dynamically allocated instances of
      the Isotope type.
    */
    QList<Isotope *> m_isotopeList;
  
  public:
    Atom(const QString & = QString(), const QString & = QString());
    Atom(const Atom &);  
    ~Atom();
  
    Atom *clone() const;
    void clone(Atom *) const;
    void mold(const Atom &);
    virtual Atom & operator =(const Atom &);
  
    const QList<Isotope *> &isotopeList() const;
  
    void appendIsotope(Isotope *);
    void removeIsotopeAt(int);
    void insertIsotopeAt(int, Isotope *);
  
    void setName(const QString &);
    QString name() const;

    void setSymbol(const QString &);
    QString symbol() const;

    bool calculateMasses();
    double calculateMono();
    double calculateAvg(); 

    virtual bool accountMasses(const QList<Atom *> &,
				double * = 0, double * = 0, int = 1);
  
    virtual bool accountMasses(double * = 0, double * = 0, int = 1) const;
  
    int isSymbolKnown(const QList<Atom *> &) const;
    static int isSymbolInList(const QString &,
			       const QList<Atom *> &,
			       Atom * = 0);
  
    int isNameKnown(const QList<Atom*> &) const;
    static int isNameInList(const QString &, 
			     const QList<Atom *> &,
			     Atom * = 0);
  
    bool validate();
  
    bool renderXmlAtomElement(const QDomElement &, int);
  
    QString *formatXmlAtomElement(int , const QString & = QString("  "));
  };

} // namespace massXpert


#endif // ATOM_HPP

