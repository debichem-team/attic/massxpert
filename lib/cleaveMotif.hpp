/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef CLEAVE_MOTIF_HPP
#define CLEAVE_MOTIF_HPP


/////////////////////// Qt includes
#include <QString>
#include <QStringList>
#include <QList>


/////////////////////// Local includes
#include "polChemDefEntity.hpp"
#include "monomer.hpp"


namespace massXpert
{

  //! The CleaveMotif class provides cleavage motifs.
  /*! When a polymer sequence cleavage occurs, using for example the
    specification "Lys/;Arg/;-Lys/Pro", a number of actions need be
    performed prior to listing the oligomers obtained following
    cleavage.

    The "Lys/;Arg/;-Lys/Pro" specification gets crunched in a number of
    steps and motifs are generated for it. In this specific case we'll
    have three motifs with the following data:

    - First motif:
    - "Lys/" 
    - code list [0] = "Lys"
    - offset = 1('/' indicates that cut is right of monomer)
    - is for cleavage ? = true

    - Second motif:
    - "Arg/" 
    - code list [0] = "Arg"
    - offset = 1('/' indicates that cut is right of monomer)
    - is for cleavage ? = true

    - Third motif:
    - "-Lys/Pro" 
    - code list [0] = "Lys", [1] = "Pro"
    - offset = 1('/' indicates that cut is right of monomer)
    - is for cleavage ? = false

    Thanks to this deconstruction(from "Lys/;Arg/;-Lys/Pro" to the 3
    motifs above) is the polymer sequence cleaved according to the
    specification.
  */
  class CleaveMotif : public PolChemDefEntity
  {
  private:
    //! Motif.
    QString m_motif;

    //! List of codes in motif.
    QStringList m_codeList;

    //! Offset of the cleavage.
    int m_offset;
  
    //! Tells if motif is for cleavage.
    bool m_isForCleave;
  
  public:
    CleaveMotif(const PolChemDef *, 
		 QString, const QString & = QString(),
		 int = 0, bool = false);

    CleaveMotif(const CleaveMotif &);
  
  
    ~CleaveMotif();

    void clone(CleaveMotif *) const;
    void mold(const CleaveMotif &);  
    CleaveMotif & operator =(const CleaveMotif &);
  
    void setMotif(const QString &);
    const QString &motif();

    const QStringList &codeList() const;
  
    void setOffset(int);
    int offset();

    void setForCleave(bool);
    bool isForCleave();

    int parse(const QString &);

    bool validate();
  };

} // namespace massXpert


#endif // CLEAVE_MOTIF_HPP
