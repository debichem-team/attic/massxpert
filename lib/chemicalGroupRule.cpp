/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/

/////////////////////// Local includes
#include "chemicalGroupRule.hpp"


namespace massXpert
{

  ChemicalGroupRule::ChemicalGroupRule(QString entity, QString name,
					int outcome)
    : m_entity(entity), m_name(name), m_outcome(outcome)
  {
    Q_ASSERT(m_outcome == MXP_CHEMGROUP_RULE_LOST ||
	      m_outcome == MXP_CHEMGROUP_RULE_PRESERVED);
  }


  void 
  ChemicalGroupRule::setEntity(QString entity)
  {
    m_entity = entity;
  }


  QString 
  ChemicalGroupRule::entity()
  {
    return m_entity;
  }

  
  void 
  ChemicalGroupRule::setName(QString name)
  {
    m_name = name;
  }


  QString 
  ChemicalGroupRule::name()
  {
    return m_name;
  }

  
  void 
  ChemicalGroupRule::setOutcome(int outcome)
  {
    m_outcome = outcome;
  }


  int
  ChemicalGroupRule::outcome()
  {
    return m_outcome;
  }


  bool
  ChemicalGroupRule::renderXmlElement(const QDomElement &element)
  {
    QDomElement child;

    // In an acidobasic definition file, the following xml structure
    // is encountered:

    // 
    //   
    //       <mnmchemgroup>			    
    //         <name>N-term NH2</name>		    
    //	<pka>9.6</pka>			    
    // 	<acidcharged>TRUE</acidcharged>	    
    // 	<polrule>left_trapped</polrule>	    
    // 	<chemgrouprule>			    
    // 	  <entity>LE_PLM_MODIF</entity>	    
    // 	  <name>Acetylation</name>	    
    // 	  <outcome>LOST</outcome>	    
    // 	</chemgrouprule>		    
    // 

    // The relevant DTD line is:
    // <!ELEMENT chemgrouprule(entity,name,outcome)>

    // And the element the parameter points to is:

    //  <chemgrouprule>				  

    // Which means that element.tagName() == "chemgrouprule" and that we'll
    // have to go one step down to the first child of the current node
    // in order to get to the <entity> element.

    if (element.tagName() != "chemgrouprule")
      return false;
    
    child = element.firstChildElement("entity");
  
    if (child.isNull())
      return false;
  
    m_entity = child.text();
  
    child = child.nextSiblingElement();
  
    if (child.isNull() || child.tagName() != "name")
      return false;
  
    m_name = child.text();
  
    child = child.nextSiblingElement();
  
    if (child.isNull() || child.tagName() != "outcome")
      return false;
  
    if (child.text() == "LOST")
      m_outcome = MXP_CHEMGROUP_RULE_LOST;
    else if (child.text() == "PRESERVED")
      m_outcome = MXP_CHEMGROUP_RULE_PRESERVED;
    else
      return false;
    
    return true;
  }

} // namespace massXpert
