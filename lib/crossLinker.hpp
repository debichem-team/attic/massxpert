/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef CROSS_LINKER_HPP
#define CROSS_LINKER_HPP
//#warning "Entering CROSS_LINKER_HPP"


/////////////////////// Local includes
#include "polChemDefEntity.hpp"
#include "formula.hpp"
#include "modif.hpp"


namespace massXpert
{

  class CrossLinker : public PolChemDefEntity, 
		      public Formula, 
		      public Ponderable
  {
  protected:
    // We do not own the modifications below, the pointers refer to
    // modifications in the polymer chemistry definition.
    QList<Modif *> m_modifList;
  
  public:
    CrossLinker(const PolChemDef *,
		 const QString &,
		 const QString &);
    
    CrossLinker(const CrossLinker &);

    ~CrossLinker();
  
    CrossLinker *clone() const;
    void clone(CrossLinker *) const;
    
    bool setModifAt(Modif *, int);
    bool appendModif(Modif *);
    const Modif *modifAt(int) const;
    bool removeModifAt(int);

    QString formula() const;
    
    QList<Modif *> &modifList();

    int hasModif (const QString &);
      
    virtual bool operator ==(const CrossLinker &) const;
  
    int isNameKnown();
    static int isNameInList(const QString &, 
			     const QList<CrossLinker*> &,
			     CrossLinker * = 0);
    bool validate();

    bool calculateMasses();
  
    virtual bool accountMasses(double * = 0, double * = 0, int = 1);

    virtual bool accountMasses(Ponderable *, int = 1);

    bool renderXmlClkElement(const QDomElement &, int);
    bool renderXmlClkElement(const QDomElement &);
    bool renderXmlClkElementV3(const QDomElement &);

    QString *formatXmlClkElement(int , const QString & = QString("  "));
  };
  
} // namespace massXpert


#endif // CROSS_LINKER_HPP
