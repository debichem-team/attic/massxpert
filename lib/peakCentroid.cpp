/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Local includes
#include "peakCentroid.hpp"


namespace massXpert
{


  PeakCentroid::PeakCentroid(double mz, 
                             double relIntensity,
                             double probability)
  {
    Q_ASSERT(mz >= 0);
    Q_ASSERT(relIntensity >= 0);
    Q_ASSERT(probability >= 0);

    m_mz = mz;
    m_relativeIntensity = relIntensity;
    m_probability = probability;
  }


  PeakCentroid::PeakCentroid(const PeakCentroid &other)
    : m_mz(other.m_mz),
      m_relativeIntensity(other.m_relativeIntensity),
      m_probability(other.m_probability)
  {
  }


  PeakCentroid::~PeakCentroid()
  {
    
  }
  
  
  PeakCentroid * 
  PeakCentroid::clone() const
  {
    PeakCentroid *other = new PeakCentroid(*this);
    
    return other;
  }


  void 
  PeakCentroid::clone(PeakCentroid *other) const
  {
    Q_ASSERT(other);
  
    if (other == this)
      return;
    
    other->m_mz = m_mz;
    other->m_relativeIntensity = m_relativeIntensity;
    other->m_probability = m_probability;
  }


  void 
  PeakCentroid::mold(const PeakCentroid &other)
  { 
    if (&other == this)
      return;
  
    m_mz = other.m_mz;
    m_relativeIntensity = other.m_relativeIntensity;
    m_probability = other.m_probability;
  }


  PeakCentroid &
  PeakCentroid::operator =(const PeakCentroid &other)
  {
    if (&other != this)
      mold(other);
  
    return *this;
  }

  void
  PeakCentroid::setMz(double value)
  {
    m_mz = value;
  }
  
  
  double
  PeakCentroid::mz() const
  {
    return m_mz;
  }
  

  void
  PeakCentroid::setProbability(double value)
  {
    Q_ASSERT(value >= 0);

    m_probability = value;
  }


  void 
  PeakCentroid::incrementProbability(double value)
  {
    Q_ASSERT(value >= 0);
  
    m_probability += value;
  }


  void
  PeakCentroid::setRelativeIntensity(double value)
  {
    Q_ASSERT(value >= 0);

    m_relativeIntensity = value;
  }


  double
  PeakCentroid::relativeIntensity() const
  {
    return m_relativeIntensity;
  }


  double
  PeakCentroid::probability() const
  {
    return m_probability;
  }


  QString 
  PeakCentroid::probability(const QLocale &locale, int decimalPlaces) const
  {
    if (decimalPlaces < 0)
      decimalPlaces = 0;
    
    return locale.toString(m_probability, 'f', decimalPlaces);
  }


  QString 
  PeakCentroid::relativeIntensity(const QLocale &locale, 
				   int decimalPlaces) const
  {
    if (decimalPlaces < 0)
      decimalPlaces = 0;
    
    return locale.toString(m_relativeIntensity, 'f', decimalPlaces);
  }

} // namespace massXpert
