/* massXpert - the true massist's program.

   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/

/////////////////////// Local includes
#include "fragOptions.hpp"
#include "polChemDef.hpp"

namespace massXpert
{

  FragOptions::FragOptions(PolChemDef *polChemDef, 
			    QString name, QString formula,
			    MxtFragEnd fragEnd,
			    const QString &comment, 
			    bool sequenceEmbedded)
    : FragSpec(polChemDef, name, formula, fragEnd, comment), 
      m_sequenceEmbedded(sequenceEmbedded)
  {
    // Have to be 1 and not 0, otherwise the setting functions ensuring
    // that m_startIonizeLevel>=m_endIonizeLevel will bug.
    m_startIonizeLevel = 1;
    m_endIonizeLevel = 1;
  }


  FragOptions::FragOptions(const FragSpec &fragSpec,
			    int startIndex, int endIndex,
			    bool sequenceEmbedded)
    : FragSpec(fragSpec), 
      m_startIndex(startIndex), m_endIndex(endIndex), 
      m_sequenceEmbedded(sequenceEmbedded)
  {
    // Have to be 1 and not 0, otherwise the setting functions ensuring
    // that m_startIonizeLevel>=m_endIonizeLevel will bug.
    m_startIonizeLevel = 1;
    m_endIonizeLevel = 1;
  }

 
  FragOptions::FragOptions(const FragOptions &other)
    : FragSpec(other), 
      m_startIndex(other.m_startIndex), m_endIndex(other.m_endIndex), 
      m_startIonizeLevel(other.m_startIonizeLevel),
      m_endIonizeLevel(other.m_endIonizeLevel),
      m_sequenceEmbedded(other.m_sequenceEmbedded)
  {
    for(int iter = 0; iter < other.m_formulaList.size(); ++iter)
      {
        Formula *formula = new Formula(*other.m_formulaList.at(iter));
        m_formulaList.append(formula);
      }
  }



  FragOptions::~FragOptions()
  {
    // There might be some allocated Formula instances in the
    // m_formulaList. Free them.

    while(!m_formulaList.isEmpty())
      delete m_formulaList.takeFirst();
  }


  FragOptions *
  FragOptions::clone() const
  {
    FragOptions *other = new FragOptions(*this);
  
    return other;
  }


  void 
  FragOptions::clone(FragOptions *other) const
  {
    Q_ASSERT(other);
  
    FragSpec::clone(other);

    other->m_sequenceEmbedded = m_sequenceEmbedded;
  }


  void 
  FragOptions::mold(const FragOptions &other)
  {
    if (&other == this)
      return;

    FragSpec::mold(other);
  
    m_sequenceEmbedded = other.m_sequenceEmbedded;
  }


  void 
  FragOptions::setStartIndex(int value)
  {
    m_startIndex = value;
  }


  int 
  FragOptions::startIndex() const
  {
    return m_startIndex;
  }


  void 
  FragOptions::setEndIndex(int value)
  {
    m_endIndex = value;
  }


  int 
  FragOptions::endIndex() const
  {
    return m_endIndex;
  }


  void 
  FragOptions::setStartIonizeLevel(int value)
  {
    int local =(value < 0 ) ? abs(value) : value;
  
    if (local <= m_endIonizeLevel)
      {
	m_startIonizeLevel = local;
      }
    else
      {
	m_startIonizeLevel = m_endIonizeLevel; 
	m_endIonizeLevel = local;
      }
  }


  int 
  FragOptions::startIonizeLevel() const
  {
    return m_startIonizeLevel;
  }


  void 
  FragOptions::setEndIonizeLevel(int value)
  {
    int local =(value < 0) ? abs(value) : value;
  
    if (local > m_startIonizeLevel)
      {
	m_endIonizeLevel = local;
      }
    else
      {
	m_startIonizeLevel = m_endIonizeLevel; 
	m_endIonizeLevel = local;
      }
  }


  int 
  FragOptions::endIonizeLevel() const
  {
    return m_endIonizeLevel;
  }

  bool
  FragOptions::addFormula(const Formula &formula)
  {
    // First check that the formula is not already in the list.

    for(int iter = 0, size = m_formulaList.size();
        iter < size; ++iter)
      {
        if (*(m_formulaList.at(iter)) == formula)
          return true;
      }

    // At this point we can say that the formula is OK and can be
    // added.

    Formula *newFormula = new Formula(formula);

    // At this point, we should check if the formula is valid.
    const QList<Atom *> &atomRefList = mp_polChemDef->atomList();

    if(!newFormula->validate(atomRefList))
      {
        qDebug() << "Formula" << newFormula->text()
                 << "failed to validate.";
        
        delete newFormula;
        
        return false;
      }
    
    m_formulaList.append(newFormula);
    
    return true;
  }
  
  bool
  FragOptions::addFormula(const QString &text)
  {
    // With the string make a true Formula instance.

    Formula formula = Formula(text);

    return addFormula(formula);
  }
  
  void 
  FragOptions::removeFormula(const QString &text)
  {
  }
  

  const QList<Formula *>&  
  FragOptions::formulaList()
  {
    return m_formulaList;
  }
  
  void 
  FragOptions::setSequenceEmbedded(bool value)
  {
    m_sequenceEmbedded = value;
  }

  
  bool 
  FragOptions::isSequenceEmbedded() const
  {
    return m_sequenceEmbedded;
  }

} // namespace massXpert
