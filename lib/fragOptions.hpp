/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef FRAG_OPTIONS_HPP
#define FRAG_OPTIONS_HPP


/////////////////////// Local includes
#include "fragSpec.hpp"
#include "globals.hpp"


namespace massXpert
{

  class FragOptions : public FragSpec
  {
  private:
    int m_startIndex;
    int m_endIndex;
    
    // These two values have to be both positive and in increasing order
    // or equal. That is m_endIonizeLevel >= m_startIonizeLevel. Only
    // use access functions to set their values.
    int m_startIonizeLevel;
    int m_endIonizeLevel;

    bool m_sequenceEmbedded;

    QList<Formula *>m_formulaList;
    
  public:
    FragOptions(PolChemDef *, 
		 QString, QString,
		 MxtFragEnd = MXT_FRAG_END_NONE,
		 const QString & = QString(), 
		 bool = false);
  
    FragOptions(const FragSpec &, int = 0, int = 0, bool = false);
  
    FragOptions(const FragOptions &);
  
    ~FragOptions();

    FragOptions *clone() const;
    void clone(FragOptions *) const;
    void mold(const FragOptions &);

    void setStartIonizeLevel(int);
    int startIonizeLevel() const;
  
    void setEndIonizeLevel(int);
    int endIonizeLevel() const;

    void setStartIndex(int);
    int startIndex() const;
  
    void setEndIndex(int);
    int endIndex() const;

    bool addFormula(const Formula &);
    bool addFormula(const QString &);
    void removeFormula(const QString &);
    const QList<Formula *>& formulaList();
        
    void setSequenceEmbedded(bool);
    bool isSequenceEmbedded() const;
  
  };

} // namespace massXpert


#endif // FRAG_OPTIONS_HPP
