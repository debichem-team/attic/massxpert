/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef CLEAVE_OLIGOMER_HPP
#define CLEAVE_OLIGOMER_HPP


/////////////////////// Local includes
#include "oligomer.hpp"


namespace massXpert
{

  class CleaveOligomer : public Oligomer
  {
  protected:
    int m_partialCleavage;
      
  public:
    CleaveOligomer(Polymer *,
                   const QString &,
                   const QString &, 
                   const Ponderable & = Ponderable(),
                   int = -1, int = -1, int = 0,
                   const CalcOptions & = CalcOptions());
    
    CleaveOligomer(Polymer *,
                   const QString &, 
                   const QString &, 
                   double = 0, double = 0, int = -1, int = -1, int = 0,
                   const CalcOptions & = CalcOptions());
    
    CleaveOligomer(const CleaveOligomer &);
    
    void setPartialCleavage(int);
    int partialCleavage() const;
  };

} // namespace massXpert


#endif // CLEAVE_OLIGOMER_HPP
