/* massXpert - the true massist's program.

   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Qt includes
#include <QChar>
#include <QString>


/////////////////////// Local includes
#include "formula.hpp"


namespace massXpert
{

  //! Constructs a formula initialized with formula and ...
  /*! Initialization of the formula is done by \p formula.  Upon
    construction of the formula, no parsing occurs.

    \p formula gets simply copied into the member formula.
  
    \param formula formula in the form of a string. Defaults to a null
    string.
  */
  Formula::Formula(const QString &formula)
  {
    if (!formula.isEmpty())
      m_formula = formula;
  }


  //! Constructs a copy of \p other.
  /*! 
  
    \param other formula to be used as a mold.
  */
  Formula::Formula(const Formula &other)
    : m_formula(other.m_formula), 
      m_plusFormula(other.m_plusFormula),
      m_minusFormula(other.m_minusFormula)
  {
    AtomCount *atomCount = 0;
  
    for (int iter = 0 ; iter < other.m_atomCountList.size(); ++iter)
      {
	atomCount = new AtomCount(*other.m_atomCountList.at(iter));
      
	m_atomCountList.append(atomCount);
      }
  }


  //! Destroys the formula.
  Formula::~Formula()
  {
    // Remove all the items in the atomcountList of dynamically
    // allocated Atomcount instances.

    while(!m_atomCountList.isEmpty())
      delete m_atomCountList.takeFirst();
  }


  //! Returns the list of atom count objects.
  /*! 
    \return The list of atom count objects.
  */
  const QList<AtomCount *> &
  Formula::atomCountList() const
  {
    return m_atomCountList;
  }


  //! Creates a new formula initialized using \p this.
  /*! The initialization involved copying all the data from \p this,
    including the atom count lists.

    \return The new formula, which should be deleted when no more in
    use.
  */
  Formula * 
  Formula::clone() const
  {
    Formula *other = new Formula(*this);
  
    return other;
  }

  //! Modifies \p other to be identical to \p this.
  /*!
  
    \param other formula.
  */
  void 
  Formula::clone(Formula *other) const
  {
    if (other == this)
      return;
  
    AtomCount *atomCount = 0;
  
    Q_ASSERT(other);
  
    other->m_formula = m_formula;
    other->m_plusFormula = m_plusFormula;
    other->m_minusFormula = m_minusFormula;
  
    while(!other->m_atomCountList.isEmpty())
      delete other->m_atomCountList.takeFirst();

    for (int iter = 0 ; iter < m_atomCountList.size(); ++iter)
      {
	atomCount = new AtomCount();
      
	m_atomCountList.at(iter)->clone(atomCount);
      
	other->m_atomCountList.append(atomCount);
      }
  }


  //! Modifies \p this  to be identical to \p other.
  /*! 
  
    \param other formula to be used as a mold.
  */
  void 
  Formula::mold(const Formula &other)
  {
    if (&other == this)
      return;

    AtomCount *atomCount = 0;
  
    m_formula = other.m_formula;
    m_plusFormula = other.m_plusFormula;
    m_minusFormula = other.m_minusFormula;
    
    while(!m_atomCountList.isEmpty())
      delete m_atomCountList.takeFirst();
  
    for (int iter = 0 ; iter < other.m_atomCountList.size(); ++iter)
      {
	atomCount = new AtomCount();
      
	atomCount->mold(*other.m_atomCountList.at(iter));
      
	m_atomCountList.append(atomCount);
      }
  }


  //! Assigns \p other to \p this formula.
  /*!   
    \param other formula.
  
    \return true if the formulas are identical, false otherwise.
  */
  Formula &
  Formula::operator =(const Formula &other)
  {
    if (&other != this)
      mold(other);
  
    return *this;
  }


  //! Sets the formula.
  /*! 
  
    \param formula formula initializer.
  */
  void
  Formula::setFormula(const QString &formula)
  {
    m_formula = formula;
  }


  //! Sets the formula.
  /*! 
  
    \param formula formula initializer.
  */
  void
  Formula::setFormula(const Formula &formula)
  {
    m_formula = formula.m_formula;
  }


  //! Returns the formula.
  /*!
  
    \return the formula as a string.
  */
  QString
  Formula::text() const
  {
    return m_formula;
  }


  void
  Formula::clear()
  {
    m_formula.clear();
    m_plusFormula.clear();
    m_minusFormula.clear();
    
    while(!m_atomCountList.isEmpty())
      delete m_atomCountList.takeFirst();
  }
  
    
  //! Sets the plus-formula.
  /*! 
  
    \param formula formula initializer.
  */
  void
  Formula::setPlusFormula(const QString &formula)
  {
    m_plusFormula = formula;
  }


  //! Returns the plus-formula.
  /*!
  
    \return the plus-formula as a string.
  */
  const QString &
  Formula::plusFormula() const
  {
    return m_plusFormula;
  }


  //! Sets the minus-formula.
  /*! 
  
    \param formula formula initializer.
  */
  void
  Formula::setMinusFormula(const QString &formula)
  {
    m_minusFormula = formula;
  }


  //! Returns the minus-formula.
  /*!
  
    \return the formula as a string.
  */
  const QString &
  Formula::minusFormula() const
  {
    return m_minusFormula;
  }


  //! Tests equality.
  /*! The test only pertains to the formula(not the minus-/plus-
    formulas).
  
    \param other formula to be compared with \p this.

    \return true if the formulas are identical, false otherwise.
  */
  bool 
  Formula::operator ==(const Formula &other) const
  {
    return(m_formula == other.m_formula);
  }


  //! Tests inequality.
  /*! The test only pertains to the formula(not the minus-/plus-
    formulas).
  
    \param other formula to be compared with \p this.

    \return true if the formulas differ, false otherwise.
  */
  bool 
  Formula::operator !=(const Formula &other) const
  {
    return(m_formula != other .m_formula);
  }


  //! Tells the actions found in the formula.
  /*! Following analysis of the \p formula argument, this function will
    be able to tell if the formula contains only '+'-associated elements
    or also '-'-associated elements. 

    If a formula contains no sign at all, then it is considered to
    contain only '+'-associated member. As soon as one member is
    associated with a '-' action, the minus actions prevails.
  
    This function is used to quickly have an indication if the
    splitParts() function is to be run or if it is not necessary.

    \param formula formula to report the actions about.

    \return '+' if no '-' action was found, '-' otherwise.

    \sa splitParts(int times, bool store, bool reset)
  */
  QChar 
  Formula::actions(const QString &formula) const
  {
    int minusCount = formula.count('-', Qt::CaseInsensitive);
  
    return(minusCount == 0 ? '+' : '-');
  }


  //! Tells the actions found in the formula.
  /*! Following analysis of the \p formula argument, this function will
    be able to tell if the formula contains only '+'-associated elements
    or also '-'-associated elements. 

    If a formula contains no sign at all, then it is considered to
    contain only '+'-associated member. As soon as one member is
    associated with a '-' action, the minus actions prevails.
  
    This function is used to quickly have an indication if the
    splitParts() function is to be run or if it is not necessary.

    \return '+' if no '-' action was found, '-' otherwise.

    \sa splitParts(int times, bool store, bool reset)
  */
  QChar
  Formula::actions() const
  {
    return actions(m_formula);
  }

  // Returns the number of removed characters.
  int
  Formula::removeTitle()
  {
    int length = m_formula.length();
    
    // We want to remove the possibly-existing title for the formula,
    // which is enclosed between `"' at the beginning of the line.

    // One formula can be like this:

    // "Decomposed adenine" C5H4N5 +H 

    // The "Decomposed adenine" is the title
    // The C5H4N5 +H is the formula.
    
    m_formula.remove(QRegExp("\".*\""));

    // Return the number of removed characters.
    return (length - m_formula.length());
  }
  

  // Returns the number of removed characters.
  int
  Formula::removeSpaces()
  {
    int length = m_formula.length();
    
    // We want to remove all the possibly-existing spaces.
    
    m_formula.remove(QRegExp("\\s+"));

    // Return the number of removed characters.
    return (length - m_formula.length());
  }
  

  //! Splits the formula according to its plus-/minus- actions.
  /*! Analyzes the formula and separates all the minus components of
    that formula from all the plus components. The different components
    are set to their corresponding formula(minus formula and plus
    formula).

    At the end of the split work, each sub-formula(plus- and/or minus-)
    is actually parsed for validity, using the reference atom list.
  
    \param refList List of reference atoms.

    \param times Number of times the formula has to be accounted
    for. Defaults to 1.
  
    \param store Indicates if AtomCount objects created during the
    parsing of the sub-formulas generated by the split of the formula
    have to be stored, or not. Defaults to false.
  
    \param reset Indicates if the list of AtomCount objects has to
    be reset before the splitParts work. This parameter may be useful in
    case the caller needs to "accumulate" the accounting of the
    formula. Defaults to false.
  
    \return MXT_FORMULA_SPLIT_FAIL if the splitting failed,
    MXT_FORMULA_SPLIT_PLUS if the components of the formula are all of
    type plus, MXT_FORMULA_SPLIT_MINUS if all the components of the
    formula are of type minus. The result value can be an OR'ing of
    MXT_FORMULA_SPLIT_PLUS and MXT_FORMULA_SPLIT_MINUS.
  */
  int
  Formula::splitParts(const QList<Atom *> &refList,
		       int times, 
		       bool store, 
		       bool reset)
  {
    QChar curChar;
    QString tempFormula;

    int result = 0;

    bool wasParsingFormula = false;
    bool shouldBeFormula = false;
    bool wasMinusSign = false;
	
    if(refList.isEmpty())
      qFatal("Fatal error at %s@%d. Aborting.",__FILE__, __LINE__);
  
    // We are asked to put all the '+' components of the formula
    // into corresponding formula and the same for the '-' components.
	
    m_plusFormula.clear();
    m_minusFormula.clear();

    // Because the formula that we are analyzing might contain a title
    // and spaces , we first remove these. But make a local copy of
    // the member datum.

    QString formula = m_formula;

    // qDebug() << __FILE__ << __LINE__
    //          << "splitParts before working:"
    //          << "m_formula:" << m_formula
    //          << "text" << Formula::text();

    // One formula can be like this:

    // "Decomposed adenine" C5H4N5 +H 

    // The "Decomposed adenine" is the title
    // The C5H4N5 +H is the formula.
    
    formula.remove(QRegExp("\".*\""));

    // We want to remove all the possibly-existing spaces.
    
    formula.remove(QRegExp("\\s+"));
    
    // If the formula does not contain any '-' character, then we
    // can approximate that all the formula is a '+' formula, that is a
    // plusFormula:
	
    if (actions() == '+')
      {
	m_plusFormula.append(formula);
		
	// At this point we want to make sure that we have a correct
	// formula. Remove all the occurrences of the '+' sign.
	m_plusFormula.replace(QString("+"), QString(""));

	if(m_plusFormula.length() > 0)
	  {
            // qDebug() << __FILE__ << __LINE__
            //          << "splitParts: with m_plusFormula:"
            //          << m_plusFormula;
            
	    if (!parse(refList, m_plusFormula, times, store, reset))
	      return MXT_FORMULA_SPLIT_FAIL;
	    else
	      return MXT_FORMULA_SPLIT_PLUS;
	  }
      }
  
    // At this point, we truly have to iterate in the formula...
  
    for (int iter = 0 ; iter < formula.length() ; ++iter)
      {
	curChar = formula.at(iter);
	//       qDebug() << "curChar:" << curChar;
      
	if(curChar == '+' || curChar == '-')
	  {
	    if (shouldBeFormula)
	      return MXT_FORMULA_SPLIT_FAIL;

	    if (wasParsingFormula)
	      {
		// We were parsing a formula, wich means that we are
		// ending that formula now, by starting another one. For
		// example, if we had "-CH3+COOH" we would typically be
		// at the '+' after having parsed -CH3. So we now have
		// to account for that latter formula.

		if(wasMinusSign)
		  m_minusFormula.append(tempFormula);
		else
		  m_plusFormula.append(tempFormula);

		// Reinit the tempFormula for next round.
		tempFormula.clear();

		// Now set proper bool values for next round.
		shouldBeFormula = true;
		wasMinusSign =(curChar == '-' ? true : false);

		continue;
	      }
	    else
	      {
		wasMinusSign =(curChar == '-' ? true : false);
		shouldBeFormula = true;

		continue;
	      }
	  }
	else
	  {
	    // We are parsing either a digit or an alphabetical
	    // character : we just append it to the tempFormula:
	    tempFormula.append(curChar);

	    wasParsingFormula = true;

	    // We do not necessarily have to expect another formula
	    // component at next round, admitting we were on the
	    // nitrogen atom of CH3CN:
	    shouldBeFormula = false;

	    continue;
	  }
      } // End for (int iter = 0 ; iter < formula.length() ; ++iter)

    // At this point the loop was finished so we might have something
    // interesting cooking:

    if (wasParsingFormula && tempFormula.length() > 0)
      {
	if(wasMinusSign)
	  m_minusFormula.append(tempFormula);
	else
	  m_plusFormula.append(tempFormula);
      }
		
    // At this point we want to make sure that we have a correct
    // formula. First reset the atomcount stuff if required.

    if (reset)
      {
	while(!m_atomCountList.isEmpty())
	  delete m_atomCountList.takeFirst();
      }


    // qDebug() << __FILE__ << __LINE__
    //          << "splitParts:"
    //          << "right after splitting:"
    //          << "m_formula:" << m_formula
    //          << "text" << text();

  	
    // Now that we have reset if required the atomCountList, we need not
    // and we must not reset during the parsing below, otherwise if we
    // have -H+H3PO4, then we'll compute +H3PO4 first, then we compute
    // -H with reset to true : the +H3PO4 component is destroyed!

    if (m_plusFormula.length() > 0)
      {
	bool res = parse(refList, m_plusFormula, times, 
                         store, false /* reset */);
        
        // qDebug() << __FILE__ << __LINE__
        //          << "splitParts:"
        //          << "right after parse of m_plusFormula:"
        //          << m_plusFormula
        //          << "m_formula:" << m_formula
        //          << "text" << text();
        
  	if(!res)
          return MXT_FORMULA_SPLIT_FAIL;
	else
	  result = MXT_FORMULA_SPLIT_PLUS;
      }
    
    
    if (m_minusFormula.length() > 0)
      {
        bool res = parse(refList, m_minusFormula, -times, 
                         store, false /* reset */);
        
        // qDebug() << __FILE__ << __LINE__
        //          << "splitParts:"
        //          << "right after parse of m_minusFormula:"
        //          << m_minusFormula
        //          << "m_formula:" << m_formula
        //          << "text" << text();
        
        if(!res)
          return MXT_FORMULA_SPLIT_FAIL;
	else
	  result |= MXT_FORMULA_SPLIT_MINUS;
      }

    
    //   qDebug() << __FILE__ << __LINE__
    // 	    << formula.toAscii() << "-->" 
    // 	    << "(+)" << m_plusFormula.toAscii() 
    // 	    << "(-)" << m_minusFormula.toAscii();
  
    return result;
  }


  //! Parses the \p formula using the reference atom list.
  /*! Upon parsing of the formula, a list of AtomCount objects are
    created in order to be able to account for the mass of the formula.
  
    \param refList List of reference atoms.

    \param formula Formula to parse.
  
    \param times Number of times that the formula should be accounted
    for. Default value is 1.
  
    \param store Indicates if AtomCount objects created during the
    parsing of the formula have to be stored, or not. Default value is
    false.
  
    \param reset Indicates if AtomCount objects created during the
    parsing of the formula have to be destroyed before doing another
    parsing. This parameter is interesting if the caller needs to
    "accumulate" the accounting of the formula. Default value is false.
  
    \return true if parsing succeeded, false otherwise.
  */
  bool
  Formula::parse(const QList<Atom *> &refList,
		  const QString &formula,
		  int times, 
		  bool store, 
		  bool reset)
  {
    QChar curChar;
    QString parsedCount;
    QString parsedSymbol;
    AtomCount atomCount;

    bool wasDigit = false;
    bool gotUpper = false;

    Q_ASSERT(refList.size());

    // The formula member is a QString that should hold the formula
    // according to this typical schema: "H2O"(water). That means we
    // only want letters(Upper and lower case and number).

    // The member atomCountList might be reset before starting, or if
    // !reset, then the new atom counts are added to the ones
    // preexisting.

    // The formula should thus not be empty, otherwise there is nothing
    // to do. But it is not an error that the formula be empty.
    if (formula.length() == 0)
      return true;

    if (!checkSyntax(formula))
      return false;

    // Also, the first character of the formula should be an Uppercase
    // letter. If not, logically, the formula is incorrect.
    if (formula.at(0).category() != QChar::Letter_Uppercase)
      return false;

    if (reset)
      {
	// We first want to iterate in the atomCountList and make sure
	// we remove all items from it.

	while(!m_atomCountList.isEmpty())
	  delete m_atomCountList.takeFirst();
      }

    // And now finally start the real parsing stuff.

    for (int iter = 0 ; iter < formula.length() ; ++iter)
      {
	curChar = formula.at(iter);

	if(curChar.category() == QChar::Number_DecimalDigit)
	  {
	    // We are parsing a digit.

	    parsedCount.append(curChar);

	    wasDigit = true;

	    continue;
	  }
	else if (curChar.category() == QChar::Letter_Lowercase)
	  {
	    // Current character is lowercase, which means we are inside
	    // of an atom symbol, such as Ca(the 'a') or Nob(either
	    // 'o' or 'b'). Thus, gotUpper should be true !

	    if (!gotUpper)
	      return false;

	    // Make use of the parsed numerical character.
	    parsedSymbol.append(curChar);

	    // Let the people know that we have parsed a lowercase char
	    // and not a digit.
	    wasDigit = false;
	  }
	else if (curChar.category() == QChar::Letter_Uppercase)
	  {
	    // Current character is uppercase, which means that we are
	    // at the beginning of an atom symbol. Check if there was a
	    // symbol being parsed before this one.

	    if (parsedSymbol.isEmpty())
	      {
		// Start new parsing round.
		parsedSymbol.append(curChar);

		gotUpper = true;
		wasDigit = false;
		continue;
	      }
	  
	    // There was a symbol being parsed. Fill-in the atomCount
	    // object.
	    atomCount.setSymbol(parsedSymbol);
	  
	    // Now we can prepare the field for the next one.
	    parsedSymbol.clear();
	    parsedSymbol.append(curChar);

	    // Before going on, check if the symbol is correct.
	    if (atomCount.isSymbolKnown(refList) == -1)
              return false;
	  
	    // If there was a count being parsed, we have to take it
	    // into account.
	    if (wasDigit)
	      {
		// And now we have to convert the string representation
		// of the atom count for that atom to int. In fact, we
		// have to be able to know that water H2O has TWO
		// hydrogen atoms in it.
		bool isok = true;
		atomCount.setCount(parsedCount.toInt(&isok, 10));

		if(atomCount.count() == 0 && !isok)
		  // The atom counts for nothing ! Or was there
                  // an error in the conversion ?
                  return false;
                
                // But we remember that we have to take into account the
		// times parameter.
	      
		atomCount.setCount(atomCount.count() * times);

		// Clear parsedCount for next count parsing round.
		parsedCount.clear();
	      }
	    else
	      atomCount.setCount(1 * times);
            
	    // We can now make sure that the atom gets represented
	    // in the formula.atomCountList list of
	    // AtomCount*. But for this we use a function that
	    // will make sure there is not already the same atom
	    // symbol in that List, so as not to duplicate the items
	    // accounting for a single atom symbol.

	    if (store)
              {
                accountInList(atomCount);

                // qDebug() << __FILE__ << __LINE__
                //          << "accountInList:"
                //          << atomCount.symbol() << atomCount.count();
              }
            
	    // Let the people know what we got:
            
	    wasDigit = false;
	    gotUpper = true;
	  }
	// end(curChar.category() == QChar::Letter_Uppercase)
      }
    // end for (int iter = 0 ; iter < formula.length() ; ++iter)

    // At this point we are at then end of the string, and we thus might
    // still have something cooking:

    // Thus we have to check that the last parsed atom
    // symbol is correct.

    atomCount.setSymbol(parsedSymbol);

    if (atomCount.isSymbolKnown(refList) == -1)
      return false;
    
    // And now we have to convert the string representation
    // of the atom count for that atom to int. In fact, we
    // have to be able to know that water H2O has TWO
    // hydrogen atoms in it.

    // If there was a count being parsed, we have to take it
    // into account.
    if (wasDigit)
      {
	// And now we have to convert the string representation
	// of the atom count for that atom to int. In fact, we
	// have to be able to know that water H2O has TWO
	// hydrogen atoms in it.
	bool isok = true;
	atomCount.setCount(parsedCount.toInt(&isok, 10));

	if(atomCount.count() == 0 && !isok)
          // The atom counts for nothing ! Or was there
          // an error in the conversion ?
          return false;
	  
        // But we remember that we have to take into account the
	// times parameter.
	      
	atomCount.setCount(atomCount.count() * times);
      }
    else
      atomCount.setCount(1 * times);
	  
    // Finally, if asked by the caller, we can account for
    // this atom symbol/count also !

    if (store)
      {
        accountInList(atomCount);
    
        // qDebug() << __FILE__ << __LINE__
        //          << "accountInList:"
        //          << atomCount.symbol() << atomCount.count();
      }
    
    return true;
  }


  int
  Formula::accountInList(const AtomCount &atomCount, int times)
  {
    int count = atomCount.count() * times;
    int newCount = 0;
    bool found = false;
    
    for(int iter = 0; iter < m_atomCountList.size(); ++iter)
      {
        if(m_atomCountList.at(iter)->symbol() == atomCount.symbol())
          {
            // qDebug() << __FILE__ << __LINE__
            //          << "accountInList:"
            //          << "same symbol:" << atomCount.symbol();
           
            found = true;
            
            // Same atom found. If the result of accounting is that
            // the count is 0, then remove the atomCount instance
            // alltogether. For example, admitting that there was 1
            // Carbon atom in the list, and we account -1 Carbon, the
            // result is that there is no Carbon atom remaining in the
            // list. The call below would return 0. In that case
            // remove an atomCount item of which the count is 0.
            
            newCount = m_atomCountList.at(iter)->account(count);
            
            if(!newCount)
              {
                delete m_atomCountList.takeAt(iter);
                            
                // qDebug() << __FILE__ << __LINE__
                //          << "accountInList:"
                //   "deleted atomCount instance "
                //   "because count reached 0";
              }
            else
              {
                // qDebug() << __FILE__ << __LINE__
                //          << "accountInList:"
                //          << "new count:" 
                //          << m_atomCountList.at(iter)->count();
              }
      
            // We have found an atomCount instance by the same symbol,
            // there should not be twice the same symbol in a formula,
            // thus we can break the loop.
            break;
          }

        continue;
      }
    // End of
    // for(int iter = 0; iter < m_atomCountList.size(); ++iter)

    if(!found)
      {
        // AtomCount not found locally. Thus make a copy and append to
        // *this m_atomCountList..
        
        AtomCount *newAtomCount = new AtomCount(atomCount);
        newAtomCount->setCount(count);
        
        m_atomCountList.append(newAtomCount);

        newCount = count;
      }
    else
      {
        // One AtomCount was found locally, newCount contains the new
        // count for that found AtomCount.
      }
    
    // Update what's the text of the formula to represent what is in
    // atomCount list.
    m_formula = elementalComposition();

    return newCount;
  }


  int 
  Formula::accountInList(const QString &text, 
                         const QList<Atom *> &atomRefList, 
                         int times)
  {
    // We get a formula as an elemental composition and we want to
    // account for that formula in *this formula.

    // First off, validate the text.

    Formula formula(text);
    
    if(!formula.validate(atomRefList, true, true))
      return -1;
    
    // Now, for each AtomCount search one in the current formula.

    const QList<AtomCount *> &otherAtomCountList = formula.atomCountList();
    
    for(int iter = 0; iter < otherAtomCountList.size(); ++iter)
      accountInList(*otherAtomCountList.at(iter), times);

    // Update what's the text of the formula to represent what is in
    // atomCount list.
    m_formula = elementalComposition();
    
    return 1;
  }
  


//! Checks the syntax of the \p formula.
  /*! The syntax of the \p formula is checked by verifying that the
    letters and ciphers in the formula are correctly placed. That is, we
    want that the ciphers appear after an atom symbol and not before
    it. We want that the atom symbol be made of one uppercase letter and
    that the following letters be lowercase.
  
    \attention This is a syntax check and not a true validation, as the
    formula can contain symbols that are syntactically valid but
    corresponding to atom definitions not available on the system.

    \param formula the formula.
  
    \return true upon successful check, false otherwise.

    \sa validate().
  */
  bool
  Formula::checkSyntax(const QString &formula)
  {
    QChar curChar;

    bool gotUpper = false;
    bool wasSign = false;

    // Because the formula that we are analyzing might contain a title
    // and spaces , we first remove these. But make a local copy of
    // the member datum.

    QString localFormula = formula;
    
    // One formula can be like this:

    // "Decomposed adenine" C5H4N5 +H 

    // The "Decomposed adenine" is the title
    // The C5H4N5 +H is the formula.
    
    localFormula.remove(QRegExp("\".*\""));

    // We want to remove all the possibly-existing spaces.
    
    localFormula.remove(QRegExp("\\s+"));
    
  
    for (int iter = 0 ; iter < localFormula.length() ; ++iter)
      {
	curChar = localFormula.at(iter);

	if(curChar.category() == QChar::Number_DecimalDigit)
	  {
	    // We are parsing a digit.

	    // We may not have a digit after a +/- sign.
	    if (wasSign)
	      return false;
	  
	    wasSign = false;

	    continue;
	  }
	else if (curChar.category() == QChar::Letter_Lowercase)
	  {
	    // Current character is lowercase, which means we are inside
	    // of an atom symbol, such as Ca(the 'a') or Nob(either
	    // 'o' or 'b'). Thus, gotUpper should be true !

	    if (!gotUpper)
	      return false;


	    // We may not have a lowercase character after a +/- sign.
	    if (wasSign)
	      return false;
	  
	    // Let the people know that we have parsed a lowercase char
	    // and not a digit.
	    wasSign = false;
	  }
	else if (curChar.category() == QChar::Letter_Uppercase)
	  {
	    // Current character is uppercase, which means that we are
	    // at the beginning of an atom symbol.
	  
	    // Let the people know what we got:
	  
	    wasSign = false;
	    gotUpper = true;
	  }
	else 
	  {
	    if (curChar != '+' && curChar != '-')
	      return false;
	    else
	      {
		// We may not have 2 +/- signs in a raw.
		if(wasSign)
		  return false;
	      }
	  
	    wasSign = true;
	    gotUpper = false;
	  }
      }
    // end for (int iter = 0 ; iter < localFormula.length() ; ++iter)

    // At this point we found no error condition.
    return true;
  }


  //! Checks the syntax of the formula.
  /*! The syntax of the formula is checked by verifying that the letters
    and ciphers in the formula are correctly placed. That is, we want
    that the ciphers appear after an atom symbol and not before it. We
    want that the atom symbol be made of one uppercase letter and that
    the following letters be lowercase.

    Note that the checking only concerns the formula, and not the
    minus-/plus- formulas.
  
    \attention This is a syntax check and not a true validation, as the
    formula can contain symbols that are syntactically valid but
    corresponding to atom definitions not available on the system.
  
    \return true upon successful check, false otherwise.

    \sa validate().
  */
  bool
  Formula::checkSyntax()
  {
    // The default formula is always m_formula.

    return checkSyntax(m_formula);
  }


  //! Validates the formula.
  /*! The validation of the formula involves:

    \li Checking that the formula is not empty;

    \li Splitting that formula into its plus-/minus- parts and parse
    the obtained plus-/minus- formulas. During parsing of the
    minus-/plus- formulas, each atom symbol encountered in the
    formulas is validated against the reference atom list;

    \li Checking that at least the plus- or the minus- part contains
    something(same idea that the formula cannot be empty).
  

    \param refList List of reference atoms.

    \param store Indicates if AtomCount objects created during the
    parsing of the sub-formulas generated by the split of the formula
    have to be stored, or not. Defaults to false.
  
    \param reset Indicates if the list of AtomCount objects has to be
    reset before the splitParts work. This parameter may be useful in
    case the caller needs to "accumulate" the accounting of the
    formula. Defaults to false.
  

    \return true if the validation succeeded, false otherwise.
  */
  bool
  Formula::validate(const QList<Atom *> &refList, 
		     bool store, bool reset)
  {
    if (!m_formula.size())
      return false;
    
    int result = splitParts(refList, 1, store, reset);
  
    if (result == MXT_FORMULA_SPLIT_FAIL)
      return false;

    // The sum of m_plusFormula and m_minusFormula cannot be empty.
    if (m_plusFormula.size() && ! m_plusFormula.size())
      return false;
    
    return true;
  }


  //! Accounts \p this formula's mono/avg masses.
  /*! The masses are calculated first and then the \p mono and \p avg
    parameters are updated using the calculated values. The accounting
    can be compounded \p times times.
  
    \param refList List of atoms to be used as reference.

    \param mono Pointer to the monoisotopic mass to be updated. Defaults
    to 0, in which case the value is not updated.

    \param avg Pointer to the average mass to be updated.  Defaults to 0,
    in which case the value is not updated.

    \param times Times that the masses should be compounded prior to be
    accounted for.

    \return true upon success, false otherwise.
  */
  bool 
  Formula::accountMasses(const QList<Atom *> &refList,
			  double *mono, double *avg, int times)
  {
    // Note the 'times' param below.
    if (splitParts(refList, times, 
                   true /* store */,
                   true /* reset */) == MXT_FORMULA_SPLIT_FAIL)
      return false;
  
    // qDebug() << __FILE__ << __LINE__
    //          << "accountMasses:"
    //          << "after splitParts:"
    //          << "store: true ; reset: true"
    //          << "m_formula:" << m_formula
    //          << "text" << Formula::text();
    
    for (int iter = 0; iter < m_atomCountList.size(); ++iter)
      {
	AtomCount *atomCount = 0;
      
	atomCount = m_atomCountList.at(iter);
      
	// note the '1' times below because we already accounted
	// for the 'times' parameter in the splitParts() call.
	if(!atomCount->accountMasses(refList, mono, avg, 1))
	  return false;
      }

    return true;
  }


  //! Accounts \p this formula's mono/avg masses.
  /*! The masses are calculated first and then the \p ponderable is
    updated using the calculated values. The accounting can be
    compounded \p times times.
  
    \param refList List of atoms to be used as reference.

    \param ponderable Pointer to the ponderable to be updated. Cannot be 0.

    \param times Times that the masses should be compounded prior to be
    accounted for.

    \return true upon success, false otherwise.
  */
  bool 
  Formula::accountMasses(const QList<Atom *> &refList,
			  Ponderable *ponderable, int times)
  {
    Q_ASSERT(ponderable);
  
    // Note the 'times' param below.
    if (splitParts(refList, times, true, true) == MXT_FORMULA_SPLIT_FAIL)
      return false;
  
    for (int iter = 0; iter < m_atomCountList.size(); ++iter)
      {
	AtomCount *atomCount = m_atomCountList.at(iter);
      
	// note the '1' times below because we already accounted
	// for the 'times' parameter in the splitParts() call.
	if(!atomCount->accountMasses(refList, 
				       &ponderable->rmono(), 
				       &ponderable->ravg(), 1))
	  return false;
      }

    return true;
  }


  //! Accounts the atoms in the formula \p times times.
  /*! Calls splitParts(\p refList, \p times, true, true).
  
    \param refList List of atoms to be used as reference.

    \param times Times that the atom counts should be compounded prior to
    be accounted for.

    \return true upon success, false otherwise.

    \sa splitParts().
  */
  bool 
  Formula::accountAtoms(const QList<Atom *> &refList, int times)
  {
    // Note the 'times' param below.
    if (splitParts(refList, times, true, false) == MXT_FORMULA_SPLIT_FAIL)
      return false;

    return true;
  }


  //! Computes a formula string.
  /*! Computes a formula string by iterating in the list of atom count
    objects.
  
    \return A string containing the formula.
  */
  QString
  Formula::elementalComposition() const
  {
    // We have a list of AtomCount objects which might have either a
    // positive or a negative count.

    // We want to provide a formula that lists the positive component
    // first and the negative component last.

    // Each positive/negative component will list the atoms in the
    // conventional order : CxxHxxNxxOxx and all the rest in
    // alphabetical order.

    // For each atomCount in the m_atomCountList list, prepare a
    // string like "C12" and "H13", for example, in two distinct
    // QStringList's, one for positive count values and one for
    // negative count values.

    QStringList negativeStringList;
    QStringList positiveStringList;
    
    for (int iter = 0; iter < m_atomCountList.size(); ++iter)
      {
	AtomCount *atomCount = m_atomCountList.at(iter);
        
        if(atomCount->count() < 0)
          {
            negativeStringList.append(QString("%1%2")
                                      .arg(atomCount->symbol())
                                      .arg(-1 * atomCount->count()));

            // qDebug() << __FILE__ << __LINE__
            //          << "negative atomCount:" 
            //          << atomCount->symbol() 
            //          << "/" 
            //          << (-1 * atomCount->count());
          }
        else
          {  
            positiveStringList.append(QString("%1%2")
                                      .arg(atomCount->symbol())
                                      .arg(atomCount->count()));

            // qDebug() << __FILE__ << __LINE__
            //          << "positive atomCount:" 
            //          << atomCount->symbol() 
            //          << "/" 
            //          << atomCount->count();
          }
      }
    
    // Sort the lists.

    negativeStringList.sort();
    positiveStringList.sort();

    // We want to provide for each positive and negative components of
    // the initial formula object, an elemental formula that complies
    // with the convention : first the C atom, next the H, N, O atoms
    // and all the subsequent ones in alphabetical order. 

    // Thus we look for the four C, H, N, O atoms, and we create the
    // initial part of the elemental formula. Each time we find one
    // such atom we remove it from the list, so that we can later just
    // append all the remaining atoms, since we have sorted the lists
    // above.

    // The positive component
    // ======================

    int atomIndex = 0;
    QString positiveComponentString;
    
    // Carbon
    atomIndex = positiveStringList.indexOf(QRegExp("C\\d+"));
    if(atomIndex != -1)
      {
        positiveComponentString += positiveStringList.at(atomIndex);
        positiveStringList.removeAt(atomIndex);
      }
  
    // Hydrogen
    atomIndex = positiveStringList.indexOf(QRegExp("H\\d+"));
    if(atomIndex != -1)
      {
        positiveComponentString += positiveStringList.at(atomIndex);
        positiveStringList.removeAt(atomIndex);
      }
  
    // Nitrogen
    atomIndex = positiveStringList.indexOf(QRegExp("N\\d+"));
    if(atomIndex != -1)
      {
        positiveComponentString += positiveStringList.at(atomIndex);
        positiveStringList.removeAt(atomIndex);
      }
  
    // Oxygen
    atomIndex = positiveStringList.indexOf(QRegExp("O\\d+"));
    if(atomIndex != -1)
      {
        positiveComponentString += positiveStringList.at(atomIndex);
        positiveStringList.removeAt(atomIndex);
      }

    // All the other items are already sorted and can be appended.

    positiveComponentString += positiveStringList.join("");

    // qDebug() << __FILE__ << __LINE__
    //          <<"positiveComponentString:" << positiveComponentString;
    

    // The negative component
    // ======================

    atomIndex = 0;
    QString negativeComponentString;
    
    // Carbon
    atomIndex = negativeStringList.indexOf(QRegExp("C\\d+"));
    if(atomIndex != -1)
      {
        negativeComponentString += negativeStringList.at(atomIndex);
        negativeStringList.removeAt(atomIndex);
      }
  
    // Hydrogen
    atomIndex = negativeStringList.indexOf(QRegExp("H\\d+"));
    if(atomIndex != -1)
      {
        negativeComponentString += negativeStringList.at(atomIndex);
        negativeStringList.removeAt(atomIndex);
      }
  
    // Nitrogen
    atomIndex = negativeStringList.indexOf(QRegExp("N\\d+"));
    if(atomIndex != -1)
      {
        negativeComponentString += negativeStringList.at(atomIndex);
        negativeStringList.removeAt(atomIndex);
      }
  
    // Oxygen
    atomIndex = negativeStringList.indexOf(QRegExp("O\\d+"));
    if(atomIndex != -1)
      {
        negativeComponentString += negativeStringList.at(atomIndex);
        negativeStringList.removeAt(atomIndex);
      }

    // All the other items are already sorted and can be appended.

    negativeComponentString += negativeStringList.join("");

    // qDebug() << __FILE__ << __LINE__
    //          <<"negativeComponentString:" << negativeComponentString;

    // Create the final elemental formula that comprises both the
    // positive and negative element. First the positive element and
    // then the negative one. Only append the negative one, prepended
    // with '-' if the string is non-empty.

    QString elementalComposition = positiveComponentString;

    if (!negativeComponentString.isEmpty())
      elementalComposition += QString("-%1")
      .arg(negativeComponentString);

    // qDebug() << __FILE__ << __LINE__
    //          <<"elementalComposition:" << elementalComposition;
    
    return elementalComposition;
  }


  //! Computes the total number of atoms.
  /*!   

    \return The number of atoms.
  */
  int 
  Formula::totalAtoms() const
  {
    int totalAtomCount = 0;
  
    for (int iter = 0; iter < m_atomCountList.size(); ++iter)
      {
	AtomCount *atomCount = m_atomCountList.at(iter);
      
	totalAtomCount += atomCount->count();
      }
  
    return totalAtomCount;
  }


  //! Computes the total number of isotopes.
  /*! 
  
    \param refList List of atoms to be used as reference.

    \return The number of isotopes.
  */
  int 
  Formula::totalIsotopes(const QList<Atom *> &refList) const
  {
    int totalIsotopeCount = 0;
  
    for (int iter = 0; iter < m_atomCountList.size(); ++iter)
      {
	AtomCount *atomCount = m_atomCountList.at(iter);
      
	Atom listAtom;
      
	if(Atom::isSymbolInList(atomCount->symbol(), 
				  refList, &listAtom) == -1)
	  return -1;
      
	// The number of isotopes for current atomCount is the number of
	// atoms compounded per the number of isotopes in the isotope
	// list.
	totalIsotopeCount += 
	 (listAtom.isotopeList().size() * atomCount->count());
      }
  
    return totalIsotopeCount;
  }


  //! Computes the number of entities(atoms and isotopes).
  /*! 
  
    \param refList List of atoms to be used as reference. 

    \param totalAtoms Pointer to a integer in which to store the number of
    atoms. Defaults to 0, in which case the value is not updated.

    \param totalIsotopes Pointer to a integer in which to store the number
    of isotopes. Defaults to 0, in which case the value is not updated.

    \return true upon a successfull computation, false otherwise.
  */
  bool 
  Formula::totalEntities(const QList<Atom *> &refList,
			  int *totalAtoms, int *totalIsotopes) const
  {
    for (int iter = 0; iter < m_atomCountList.size(); ++iter)
      {
	AtomCount *atomCount = m_atomCountList.at(iter);

	if(totalAtoms)
	  *totalAtoms += atomCount->count();
      
	if(totalIsotopes)
	  {
	    Atom listAtom;
	  
	    if (Atom::isSymbolInList(atomCount->symbol(), 
				      refList, &listAtom) == -1)
	      return false;
	  
	    // The number of isotopes for current atomCount is the number of
	    // atoms compounded per the number of isotopes in the isotope
	    // list.
	    *totalIsotopes += 
	     (listAtom.isotopeList().size() * atomCount->count());
	  }
      }
  
    return true;
  }


  //! Performs a deep copy of the atom count objects.
  /*! Each atom count object in the list of such objects is updated
    deeply with the values obtained from the corresponding atom in the
    \p refList list of reference atoms. This ensures that each atom
    count object in the formula has a deep knowledge of its isotopic
    composition. Such kind of process is used when isotopic pattern
    calculations are to be performed for a given formula.
  
    \param refList List of reference atoms.

    \return true upon success, false otherwise.
  */
  bool
  Formula::deepAtomCopy(const QList<Atom *> &refList)
  {
    // When the formula is parsed, the atomCount objects(derived form
    // Atom) are created by only shallow-copying(only the atom
    // symbol is actually copied to identify the atom).

    // Here, we are asked that the Atom component of the AtomCount
    // objects in the list of such instances be deep-copied from the
    // corresponding Atom found in the reference atom list
    // 'refList'. This way, the updated objects have their actual list
    // of isotopes(this is useful for the isotopic pattern calculation,
    // for example).

    for (int iter = 0; iter < m_atomCountList.size(); ++iter)
      {
	AtomCount *atomCount = m_atomCountList.at(iter);

	if(Atom::isSymbolInList(atomCount->symbol(), 
                                refList, atomCount) == -1)
	  return false;
      }

    return true;
  }



  //! Parses a formula XML element and sets the data to the formula.
  /*! Parses the formula XML element passed as argument and sets the
    data of that element to \p this formula instance(this is called XML
    rendering). The syntax of the parsed formula is checked and the
    result of that check is returned.
  
    \param element XML element to be parsed and rendered.
  
    \return true if parsing and syntax checking were successful, false
    otherwise.
  */
  bool
  Formula::renderXmlFormulaElement(const QDomElement &element)
  {
    if (element.tagName() != "formula")
      return false;
  
    m_formula = element.text();
  
    // Do not forget that we might have a title associated with the
    // formula and spaces. checkSyntax() should care of removing these
    // title and spaces before checking for chemical syntax
    // correctness.

    return checkSyntax();
  }

} // namespace massXpert
