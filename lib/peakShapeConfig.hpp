/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#ifndef PEAK_SHAPE_CONFIG_HPP
#define PEAK_SHAPE_CONFIG_HPP


/////////////////////// Qt includes
#include <QList>
#include <QPointF>


/////////////////////// Local includes
#include "globals.hpp"


namespace massXpert
{

  // The PeakShape class contains all required data to describe a
  // gaussian or lorentzian shape corresponding to a given centroid
  // (x,y) with z=m/z and y=relative intensity. The class contains all
  // the data rquired to computer either a gaussian shape or a
  // lorentzian shape. The points that make the shape are stored as
  // QPointF (double x, double y) instances in the
  // QList<QPointF*>m_pointList.

  class PeakShapeConfig
  {
  private:
    double m_fwhm;
    double m_resolution;
    
    int m_pointNumber;
    double m_increment;
    double m_normFactor;
    PeakShapeType m_peakShapeType ;
    
  public:
    PeakShapeConfig();
    PeakShapeConfig(double /* fwhm */,
                    int /* number of points */, 
                    float /* increment between two consecutive points */,
                    double /* normFactor */, 
                    PeakShapeType /* peakShapeType */);
    PeakShapeConfig(const PeakShapeConfig &);
    ~PeakShapeConfig();

    void setConfig(double /* fwhm */,
                   int /* pointNumber */,
                   float /* increment */,
                   double /*normFactor */,
                   PeakShapeType /* peakShapeType */);

    void setConfig(const PeakShapeConfig &);
        
    void setFwhm(double);
    double fwhm();
    QString fwhmString();

    void setPointNumber(int);
    int pointNumber();
    QString pointNumberString();

    void setNormFactor(double);
    double normFactor();
    QString normFactorString();

    PeakShapeType peakShapeType();
    void setPeakShapeType (PeakShapeType);
        
    // For the lorentzian, that is half of the fwhm.
    double hwhm();
    QString hwhmString();
  
    double a();
    QString aString();
  
    double c();
    QString cString();

    double gamma();
    QString gammaString();

    void setIncrement(double);
    double increment();
    QString incrementString();

    QString config();
  } ;
  
} // namespace massXpert

#endif // PEAK_SHAPE_CONFIG_HPP
