/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef FRAGMENTER_HPP
#define FRAGMENTER_HPP


/////////////////////// Local includes
#include "fragSpec.hpp"
#include "fragOptions.hpp"
#include "calcOptions.hpp"
#include "polymer.hpp"
#include "ionizeRule.hpp"
#include "oligomerList.hpp"
#include "crossLinkedRegion.hpp"

namespace massXpert
{

  class Fragmenter 
  {
  private:
    const QPointer<Polymer> mp_polymer;
    const PolChemDef *mp_polChemDef;
    QList<FragOptions *> m_fragOptionList;
    CalcOptions m_calcOptions;
    IonizeRule m_ionizeRule;
  
    // Pointer to an oligomer list which WE DO NOT OWN.
    OligomerList *mp_oligomerList;

    // A list of CrossLinkedRegion instances that we compute in case
    // there are cross-links in the fragmented sequence.
    QList<CrossLinkedRegion *> m_crossLinkedRegionList;
        

  public:
    Fragmenter(Polymer *, const PolChemDef *, 
		const QList<FragOptions *> &fragOptionList,
		const CalcOptions &, const IonizeRule &);
  
    Fragmenter(const Fragmenter &);
    ~Fragmenter();

    void addFragOptions(FragOptions *);
  
    void setOligomerList(OligomerList *);
    OligomerList * oligomerList();
  
    bool fragment();
    int fragmentEndNone(FragOptions &);
    int fragmentEndLeft(FragOptions &);
    int fragmentEndRight(FragOptions &);

    bool accountFragRule(FragRule *, bool, int, int, Ponderable *);
    int accountFormulas(OligomerList *, FragOptions &, QString, int);
    OligomerList *accountIonizationLevels(OligomerList *, FragOptions &);

    void emptyOligomerList();
  };

} // namespace massXpert


#endif // FRAGMENTER_HPP
