/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.


   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef PROP_LIST_HOLDER_HPP
#define PROP_LIST_HOLDER_HPP


/////////////////////// Qt includes


/////////////////////// Local includes
#include "prop.hpp"


namespace massXpert
{

  //! The PropList class provides a list of allocated Prop instances.
  /*! PropList provides a list of allocated Prop instances that it
    owns. This class is used to derive classes needing to store
    dynamically allocated Prop objects without knowing with
    anticipation how many of such objects are going to be used.
  */
  class PropListHolder
  {
  protected:
    QList<Prop *> m_propList;

  public:
    PropListHolder();
    PropListHolder(const PropListHolder &);
    virtual ~PropListHolder();

    virtual PropListHolder *clone() const;
    virtual void clone(PropListHolder *) const;
    virtual void mold(const PropListHolder &);
    virtual PropListHolder & operator =(const PropListHolder &);

    const QList<Prop *> &propList() const;
    QList<Prop *> &propList();

    Prop *prop(const QString &, int * = 0);
    int propIndex(const QString &, Prop * = 0);

    int propListSize() const;

    bool appendProp(Prop *);
    bool removeProp(Prop *);
    bool removeProp(const QString &);
  };

} // namespace massXpert

#endif // PROP_LIST_HOLDER_HPP
