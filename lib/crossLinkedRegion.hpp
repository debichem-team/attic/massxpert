/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007,2008,2009,2010,2011 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef CROSS_LINKED_REGION_HPP
#define CROSS_LINKED_REGION_HPP
//#warning "Entering CROSS_LINKED_REGION_HPP"


/////////////////////// Local includes
#include "crossLink.hpp"
#include "crossLinkList.hpp"


namespace massXpert
{
  

  class CrossLinkedRegion
  {
  private:
    int m_startIndex;
    int m_endIndex;
    // We do not own the cross-links, we cannot destroy them because
    // that would destroy the cross-links in the polymer sequence,
    // which we do not want!!
    QList<CrossLink *> m_crossLinkList;

    

  public:
    CrossLinkedRegion();
    CrossLinkedRegion(int, int);
    CrossLinkedRegion(const CrossLinkedRegion &);
    
    ~CrossLinkedRegion();
  
    void setStartIndex(int);
    int startIndex();
    
    void setEndIndex(int);
    int endIndex();
    
    const QList<CrossLink *> & crossLinkList() const;
    
    int appendCrossLink(CrossLink *);
    int appendCrossLinks(const QList<CrossLink *> &);

    int removeCrossLink(CrossLink *);
    int removeCrossLinkAt(int);
  };

} // namespace massXpert


#endif // CROSS_LINKED_REGION_HPP
