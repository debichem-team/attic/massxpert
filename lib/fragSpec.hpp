/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef FRAG_SPEC_HPP
#define FRAG_SPEC_HPP


/////////////////////// Qt includes
#include<QString>
#include<QList>


/////////////////////// Local includes
#include "fragRule.hpp"


namespace massXpert
{

  //! Fragmentation end.
  enum MxtFragEnd
    {
      MXT_FRAG_END_NONE = 1 << 0,
      MXT_FRAG_END_LEFT = 1 << 1,
      MXT_FRAG_END_RIGHT = 1 << 2,
      MXT_FRAG_END_BOTH =(MXT_FRAG_END_LEFT | MXT_FRAG_END_RIGHT),
    };



  //! The FragSpec class provides a fragmentation specification.
  /*! Fragmentation specifications determine the chemical reaction that
    governs the fragmentation of the polymer in the gas-phase. The
    chemical reaction is embodied by a formula. The side of the polymer
   (left or right) that makes the fragment after the fragmentation has
    occurred is described by a fragmentation-end enumeration.
  
    A fragmentation specification might not be enough information to
    determine the manner in which a polymer fragments in the
    gas-phase. Fragmentation rules might be required to refine the
    specification.  A fragmentation specification might hold as many
    fragmentation rules as required.
  */
  class FragSpec : public PolChemDefEntity, public Formula
  {
  private:
    //! Fragmentation end.
    MxtFragEnd m_fragEnd;
    //! Fragmented monomer's mass contribution. See fragSpecDefDlg.cpp
    //! for a detailed explanation of what this member is for.
    int m_monomerContribution;

    //! Comment.
    QString m_comment;

    //! List of fragmentation rules.
    QList<FragRule*> m_ruleList;
  
  public:
    FragSpec(const PolChemDef *, QString, 
	      QString = QString(),
	      MxtFragEnd = MXT_FRAG_END_NONE,
	      const QString & = QString());

    FragSpec(const PolChemDef *, QString, QString);

    FragSpec(const FragSpec &);
    
    ~FragSpec();

    FragSpec *clone() const;
    void clone(FragSpec *) const;
    void mold(const FragSpec &);
    FragSpec & operator =(const FragSpec &);
  
    QList<FragRule *> &ruleList();
    void appendRule(FragRule *);
    void insertRuleAt(int, FragRule *);
    void removeRuleAt(int);
    
    void setFragEnd(MxtFragEnd);
    MxtFragEnd fragEnd() const;

    void setMonomerContribution(int);
    int monomerContribution();
  
    QString formula() const;
    
    void setComment(const QString &);
    QString comment() const;
  
    static int isNameInList(const QString &, 
			     const QList<FragSpec*> &,
			     FragSpec * = 0);  
  
    bool validate();
  
    bool renderXmlFgsElement(const QDomElement &element, int);
    bool renderXmlFgsElementV2(const QDomElement &);
  
    QString *formatXmlFgsElement(int , const QString & = QString("  "));
  };

} // namespace massXpert


#endif // FRAG_SPEC_HPP
