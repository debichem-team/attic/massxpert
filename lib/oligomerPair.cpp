/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Local includes
#include "oligomerPair.hpp"
#include "oligomer.hpp"


namespace massXpert
{


  OligomerPair::OligomerPair(const Oligomer *oligomer1, 
                             const Oligomer *oligomer2,
                             MassType massType,
                             double error,
                             bool isMatching,
                             const QString &name)
    : mp_oligomer1(oligomer1), 
      mp_oligomer2(oligomer2),
      m_massType(massType), 
      m_error(error), 
      m_isMatching(isMatching), 
      m_name(name)
  {
    Q_ASSERT(mp_oligomer1);
    Q_ASSERT(mp_oligomer2);
  }
  
  
  OligomerPair::OligomerPair(const OligomerPair &other)
    : mp_oligomer1(other.mp_oligomer1), 
      mp_oligomer2(other.mp_oligomer2),
      m_massType(other.m_massType), 
      m_error(other.m_error), 
      m_isMatching(other.m_isMatching),
      m_name(other.m_name)
  {
    Q_ASSERT(mp_oligomer1);
    Q_ASSERT(mp_oligomer2);
  }
  

  OligomerPair::~OligomerPair()
  {
  }


  QString 
  OligomerPair::name()
  {
    return m_name;
  }
  
    
  const Oligomer *
  OligomerPair::oligomer1() const
  {
    return mp_oligomer1;
  }
  
  
  const Oligomer *
  OligomerPair::oligomer2() const
  {
    return mp_oligomer2;
  }

  
  void 
  OligomerPair::setError(double error)
  {
    m_error = error;
  }
  
  
  MassType
  OligomerPair::massType() const
  {
    return m_massType;
  }
  

  void
  OligomerPair::setMassType(MassType massType)
  {
    m_massType = massType;
  }
  

  double 
  OligomerPair::error() const
  {
    return m_error;
  }
    

  void
  OligomerPair::setMatching(bool isMatching)
  {
    m_isMatching = isMatching;
  }
    

  bool
  OligomerPair::isMatching() const
  {
    return m_isMatching;
  }
    

  double 
  OligomerPair::mass1()
  {
    return mp_oligomer1->mass(m_massType);
  }
  

  int
  OligomerPair::charge1()
  {
    return mp_oligomer1->charge();
  }
  

  double 
  OligomerPair::mass2()
  {
    return mp_oligomer2->mass(m_massType);
  }

  
  int
  OligomerPair::charge2()
  {
    return mp_oligomer2->charge();
  }  
  
} // namespace massXpert
