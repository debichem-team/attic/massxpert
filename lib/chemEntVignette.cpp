/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// Local includes
#include "chemEntVignette.hpp"
#include "chemEntVignetteRenderer.hpp"


namespace massXpert
{

  ChemEntVignette::ChemEntVignette(QGraphicsSvgItem *parent)
    : QGraphicsSvgItem(parent)
  {
    mp_owner = 0;
    mp_renderer = 0;
    
    //   qDebug() << __FILE__ << __LINE__ 
    // 	    << "ChemEntVignette()" <<(const void*) this;
  }


  ChemEntVignette::ChemEntVignette(const QString & 
				    filePath, 
				    QGraphicsSvgItem *parent)
    : QGraphicsSvgItem(filePath, parent)
  {
    mp_owner = 0;
    mp_renderer = 0;
    
    //   qDebug() << __FILE__ << __LINE__ 
    // 	    << "ChemEntVignette()" <<(const void*) this;
  }


  ChemEntVignette::~ChemEntVignette()
  {
    // We do not own these:
    // mp_owner
    //mp_renderer
    
    //   qDebug() << __FILE__ << __LINE__
    // 	    << "~ChemEntVignette()" <<(const void*) this;
    
    mp_renderer->decrementRefCount();
  }


  QString 
  ChemEntVignette::name()
  {
    return m_name;
  }


  void 
  ChemEntVignette::setName(QString name)
  {
    m_name = name;
  }
  

  PolChemDefEntity * 
  ChemEntVignette::owner()
  {
    return mp_owner;
  }


  void 
  ChemEntVignette::setOwner(PolChemDefEntity *owner)
  {
    Q_ASSERT(owner);
  
    mp_owner = owner;
  }


  void 
  ChemEntVignette::setSharedRenderer(ChemEntVignetteRenderer *renderer)
  {
    Q_ASSERT(renderer);
  
    mp_renderer = renderer;
    mp_renderer->incrementRefCount();
  
    QGraphicsSvgItem::setSharedRenderer(static_cast<QSvgRenderer *>(renderer));
  }


  ChemEntVignetteRenderer *
  ChemEntVignette::renderer()
  {
    return mp_renderer;
  }

} // namespace massXpert
