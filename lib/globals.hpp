/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#ifndef GLOBALS_HPP
#define GLOBALS_HPP


#include <QString>



//!Type of mass.
/*! Can be monoisotopic or average or both.
*/
enum MassType
{
  MXT_MASS_NONE = 0 << 1, //!< No mass
  MXT_MASS_MONO = 1 << 1, //!< Monoisotopic mass
  MXT_MASS_AVG = 2 << 1, //!< Average mass
  MXT_MASS_BOTH =(MXT_MASS_MONO | MXT_MASS_AVG) //!< Both masses
};


//!Type of mass tolerance.
/*! Can be atomic mass units, percentage or part per million.
*/
enum MassToleranceType
{
  MXT_MASS_TOLERANCE_AMU = 1 << 1, //!< atomic mass units
  MXT_MASS_TOLERANCE_PCT = 2 << 1, //!< percentage
  MXT_MASS_TOLERANCE_PPM = 3 << 1, //!< parts per million
};


//! End of the polymer sequence.
enum PolymerEnd
  {
    MXT_END_NONE = 0 << 1,
    MXT_END_LEFT = 1 << 1,
    MXT_END_RIGHT = 2 << 1,
    MXT_END_BOTH =(MXT_END_LEFT | MXT_END_RIGHT),
  };


//! Type of capping.
/*! The sequence may be capped at left end , at right end, at both
  ends or not capped at all.
 */
 enum CapType
  {
    MXT_CAP_NONE = 0 << 1,
    MXT_CAP_LEFT = 1 << 1,
    MXT_CAP_RIGHT = 2 << 1,
    MXT_CAP_BOTH =(MXT_CAP_LEFT | MXT_CAP_RIGHT),
  };


//! Monomer chemical entities.
/*! The monomer chemical entities can be nothing or modifications.
*/
 enum MonomerChemEnt
  {
    MXT_MONOMER_CHEMENT_NONE = 0 << 1,
    MXT_MONOMER_CHEMENT_MODIF = 1 << 1,
    MXT_MONOMER_CHEMENT_CROSS_LINK = 1 << 2,
  };

//! Polymer chemical entities.
/*! The polymer chemical entities can be nothing or left end
  modification or right end modification or both ends modifications.
*/
enum PolymerChemEnt
  {
    MXT_POLYMER_CHEMENT_NONE = 1 << 0,
    MXT_POLYMER_CHEMENT_LEFT_END_MODIF = 1 << 1,
    MXT_POLYMER_CHEMENT_FORCE_LEFT_END_MODIF = 1 << 2,
    MXT_POLYMER_CHEMENT_RIGHT_END_MODIF = 1 << 3,
    MXT_POLYMER_CHEMENT_FORCE_RIGHT_END_MODIF = 1 << 4,
    MXT_POLYMER_CHEMENT_BOTH_END_MODIF = 
   (MXT_POLYMER_CHEMENT_LEFT_END_MODIF | 
     MXT_POLYMER_CHEMENT_RIGHT_END_MODIF),
    MXT_POLYMER_CHEMENT_VORCE_BOTH_END_MODIF = 
   (MXT_POLYMER_CHEMENT_FORCE_LEFT_END_MODIF |
     MXT_POLYMER_CHEMENT_FORCE_RIGHT_END_MODIF)
  };

enum PeakShapeType
  {
    MXP_PEAK_SHAPE_TYPE_GAUSSIAN = 1 << 0,
    MXP_PEAK_SHAPE_TYPE_LORENTZIAN = 1 << 1,
  };

extern int MXP_ATOM_DEC_PLACES;
extern int MXP_OLIGOMER_DEC_PLACES;
extern int MXP_POLYMER_DEC_PLACES;
extern int MXP_PH_PKA_DEC_PLACES;

// How many times the whole gaussian/lorentzian curve should span
// around the centroid mz ratio. 4 means that the peak will span
// [ mzRatio - (2 * FWHM) --> mzRatio + (2 * FWHM) ]

extern int MXP_FWHM_PEAK_SPAN_FACTOR;


						     
QString &
GlobUnspacifyQString(QString &);

QString
GlobBinaryRepresentation(int);

QString
GlobElideText(const QString &, int = 4, int = 4, const QString & = "...");


#endif // GLOBALS_HPP
