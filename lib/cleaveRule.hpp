/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef CLEAVE_RULE_HPP
#define CLEAVE_RULE_HPP


/////////////////////// Qt includes
#include <QString>


/////////////////////// Local includes
#include "polChemDefEntity.hpp"
#include "formula.hpp"
#include "monomer.hpp"


namespace massXpert
{

  //! The CleaveRule class provides a cleavage rule.
  /*! Cleavage rules help refine the description of the chemical
    reaction that is the basis of a cleavage(either enzymatic or
    chemical). 

    While a number of cleavage agent(like a number of enzymes) do not
    make unexpected reactions upon cleavage(enzyme usually hydrolyze
    their substrates), chemical agents sometimes make a cleavage, and in
    the process of the cleavage reaction modify chemically the ends of
    the generaed oligomers. One notorious example is the case of cyanogen
    bromide, that cleaves proteins right of methionyl residues. Upon such
    cleavage, the monomer at the right side of the generated oligomer
   (methionyl residue) get modified according to this formula:
    "-CH2S+O".

    Cleavage rules are designed to be able to model such complex reactions.
  */
  class CleaveRule: public PolChemDefEntity
  {
  private:
    //! Left code.
    QString m_leftCode;
  
    //! Left formula.
    Formula m_leftFormula;
  
    //! Right code.
    QString m_rightCode;

    //! Right formula.
    Formula m_rightFormula;
  
  public:
    CleaveRule(const PolChemDef *, QString, 
		QString = QString(), QString = QString(), 
		QString = QString(), QString = QString());
  
    CleaveRule(const CleaveRule &);
  
    ~CleaveRule();
  
    void clone(CleaveRule *);
    void mold(const CleaveRule &);
    CleaveRule & operator =(const CleaveRule &);
  
    void setLeftCode(const QString &);
    const QString &leftCode();

    void setRightCode(const QString &);
    const QString &rightCode();

    void setLeftFormula(const Formula &);
    const Formula &leftFormula();

    void setRightFormula(const Formula &);
    const Formula &rightFormula();

    void setAtomRefList(const QList<Atom *> *);
    void setMonomerRefList(const QList<Monomer *> *);
  
    static int isNameInList(const QString &, 
			     const QList<CleaveRule*> &,
			     CleaveRule *other = 0);

    bool validate();

    bool renderXmlClrElement(const QDomElement &, int);

    QString *formatXmlClrElement(int , const QString & = QString("  "));
  };

} // namespace massXpert


#endif // CLEAVE_RULE_HPP
