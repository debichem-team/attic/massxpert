/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


#ifndef ATOM_COUNT_HPP
#define ATOM_COUNT_HPP


/////////////////////// Qt includes
#include <QObject>
#include <QString>


/////////////////////// Local includes
#include "atom.hpp"


namespace massXpert
{

  //! The AtomCount class provides an atom and a count of that atom.
  /*! The AtomCount class derives from Atom, and add a member
    datum representing the number of times that the atom occurs(an
    occurrence count). This class is used during the parsing of
    formulas.
  */
  class AtomCount : public Atom
  {
  private:
    //! Number of times that the atom occurs(its count).
    int m_count;
  
  public:
    AtomCount();
    AtomCount(const AtomCount &);
  
    AtomCount *clone() const;
    void clone(AtomCount *) const;
    void mold(const AtomCount &);
    AtomCount & operator =(const AtomCount &);
  
    void setCount(int);
    int count() const;
  
    int account(int = 1);
      
    bool accountMasses(const QList<Atom *> &,
			double * = 0, double * = 0, int = 1) const;
  };

} // namespace massXpert


#endif // ATOM_COUNT_HPP

