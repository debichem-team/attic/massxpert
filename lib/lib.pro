TEMPLATE = lib
LANGUAGE = C++

# for i in $(ls -1 *.hpp | grep -v ^moc.* | grep -v ^qrc.*) ; do echo "    $i" \\;done
HEADERS += \
atom.hpp \
    atom.hpp \
    atomCount.hpp \
    calcOptions.hpp \
    chemEntVignette.hpp \
    chemEntVignetteRenderer.hpp \
    chemicalGroup.hpp \
    chemicalGroupRule.hpp \
    cleaveMotif.hpp \
    cleaveOligomer.hpp \
    cleaveOptions.hpp \
    cleaveRule.hpp \
    cleaveSpec.hpp \
    cleaver.hpp \
    configSettings.hpp \
    coordinates.hpp \
    crossLink.hpp \
    crossLinkList.hpp \
    crossLinker.hpp \
    crossLinkerSpec.hpp \
    cross_link_default_vignette.hpp \
    formula.hpp \
    fragOptions.hpp \
    fragRule.hpp \
    fragSpec.hpp \
    fragmenter.hpp \
    globals.hpp \
    ionizable.hpp \
    ionizeRule.hpp \
    isotope.hpp \
    isotopicPeak.hpp \
    massList.hpp \
    modif.hpp \
    modifSpec.hpp \
    modification_default_vignette.hpp \
    monomer.hpp \
    monomerSpec.hpp \
    oligomer.hpp \
    oligomerList.hpp \
    pkaPhPi.hpp \
    pkaPhPiDataParser.hpp \
    polChemDef.hpp \
    polChemDefCatParser.hpp \
    polChemDefEntity.hpp \
    polChemDefSpec.hpp \
    polymer.hpp \
    ponderable.hpp \
    prop.hpp \
    propListHolder.hpp \
    sequence.hpp \
    userSpec.hpp

# for i in $(ls -1 *.cpp | grep -v ^moc.* | grep -v ^qrc.*) ; do echo "    $i" \\;done
SOURCES += \
    atom.cpp \
    atomCount.cpp \
    calcOptions.cpp \
    chemEntVignette.cpp \
    chemEntVignetteRenderer.cpp \
    chemicalGroup.cpp \
    chemicalGroupRule.cpp \
    cleaveMotif.cpp \
    cleaveOligomer.cpp \
    cleaveOptions.cpp \
    cleaveRule.cpp \
    cleaveSpec.cpp \
    cleaver.cpp \
    configSettings.cpp \
    coordinates.cpp \
    crossLink.cpp \
    crossLinkList.cpp \
    crossLinker.cpp \
    crossLinkerSpec.cpp \
    formula.cpp \
    fragOptions.cpp \
    fragRule.cpp \
    fragSpec.cpp \
    fragmenter.cpp \
    globals.cpp \
    ionizable.cpp \
    ionizeRule.cpp \
    isotope.cpp \
    isotopicPeak.cpp \
    massList.cpp \
    modif.cpp \
    modifSpec.cpp \
    monomer.cpp \
    monomerSpec.cpp \
    oligomer.cpp \
    oligomerList.cpp \
    pkaPhPi.cpp \
    pkaPhPiDataParser.cpp \
    polChemDef.cpp \
    polChemDefCatParser.cpp \
    polChemDefEntity.cpp \
    polChemDefSpec.cpp \
    polymer.cpp \
    ponderable.cpp \
    prop.cpp \
    propListHolder.cpp \
    sequence.cpp \
    userSpec.cpp


CONFIG = staticlib qt warn_on release

QT += xml
QT += svg

TARGET = masslib

INCLUDEPATH += -I ..

MOC_DIR = ../../build-work/$(TARGET)-moc
OBJECTS_DIR = ../../build-work/$(TARGET)-obj
##universal tiger
CONFIG += link_prl x86 ppc
QMAKE_MAC_SDK=/Developer/SDKs/MacOSX10.4u.sdk
QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.3

