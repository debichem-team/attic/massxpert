/* massXpert - the true massist's program.
   --------------------------------------
   Copyright(C) 2006,2007,2008,2009,2010,2011 Filippo Rusconi

   http://www.massxpert.org/massXpert

   This file is part of the massXpert project.

   The massxpert project is the successor to the "GNU polyxmass"
   project that is an official GNU project package(see
   www.gnu.org). The massXpert project is not endorsed by the GNU
   project, although it is released ---in its entirety--- under the
   GNU General Public License. A huge part of the code in massXpert
   is actually a C++ rewrite of code in GNU polyxmass. As such
   massXpert was started at the Centre National de la Recherche
   Scientifique(FRANCE), that granted me the formal authorization to
   publish it under this Free Software License.

   This software is free software; you can redistribute it and/or
   modify it under the terms of the GNU  General Public
   License version 3, as published by the Free Software Foundation.
   

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this software; if not, write to the

   Free Software Foundation, Inc.,

   51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
*/


/////////////////////// Local includes
#include "crossLinkedRegion.hpp"


namespace massXpert
{
  
  //            [3] [4] [5] [6]        [9]    [11]
  // o---o---o---o---o--o---o---o---o---o---o---o---o---o---o
  //             |   |__|   |       |   |       |       |
  //             |          +-----------+       +-------+
  //             |                  |          
  //             +------------------+          
  //                                           
  //                                           
  // In the example above, there are two cross-linked regions: [3--9]
  // and [11--13].
  

  CrossLinkedRegion::CrossLinkedRegion()
  {
    m_startIndex = 0;
    m_endIndex = 0;
  }
  

  CrossLinkedRegion::CrossLinkedRegion(int startIndex, int endIndex)
  {
    m_startIndex = startIndex;
    m_endIndex = endIndex;
  }


  CrossLinkedRegion::CrossLinkedRegion(const CrossLinkedRegion &other)
  {
    m_startIndex = other.m_startIndex;
    m_endIndex = other.m_endIndex;
    
    for(int iter = 0; iter < other.m_crossLinkList.size(); ++iter)
      {
        m_crossLinkList.append(other.m_crossLinkList.at(iter));
      }
  }
  
  
  CrossLinkedRegion::~CrossLinkedRegion()
  {
    // Do not destroy the CrossLink instances that we do not own. If
    // we destroyed them, then their destructor would trigger the
    // removal of the cross-link from the polymer sequence, which we
    // must not do.
  }
  
  void 
  CrossLinkedRegion::setStartIndex(int index)
  {
    m_startIndex = index;
  }
  

  int 
  CrossLinkedRegion::startIndex()
  {
    return m_startIndex;
  }
  
    
  void 
  CrossLinkedRegion::setEndIndex(int index)
  {
    m_endIndex = index;
  }
  

  int 
  CrossLinkedRegion::endIndex()
  {
    return m_endIndex;
  }
  

  const QList<CrossLink *> & 
  CrossLinkedRegion::crossLinkList() const
  {
    return m_crossLinkList;
  }
  
  
  int 
  CrossLinkedRegion::appendCrossLink(CrossLink *crossLink)
  {
    if(!crossLink)
      qFatal("Fatal error at %s@%d. Aborting.",__FILE__, __LINE__);

    // Only append the cross-link if it is not alreaday in the list.
    if (!m_crossLinkList.contains(crossLink))
      m_crossLinkList.append(crossLink);

    return m_crossLinkList.size();
  }
  

  int 
  CrossLinkedRegion::appendCrossLinks(const QList<CrossLink *> &list)
  {
    for(int iter = 0; iter < list.size(); ++iter)
      {
        m_crossLinkList.append(list.at(iter));
      }

    return m_crossLinkList.size();
  }
  

  
  int
  CrossLinkedRegion::removeCrossLink(CrossLink *crossLink)
  {
    if(!crossLink)
      qFatal("Fatal error at %s@%d. Aborting.",__FILE__, __LINE__);

    m_crossLinkList.removeOne(crossLink);
  
    return m_crossLinkList.size();
  }
  

  int
  CrossLinkedRegion::removeCrossLinkAt(int index)
  {
    if(index >= m_crossLinkList.size() || index < 0)
      qFatal("Fatal error at %s@%d. Aborting.",__FILE__, __LINE__);

    m_crossLinkList.removeAt(index);

    return m_crossLinkList.size();
  }
  

  
} // namespace massXpert



