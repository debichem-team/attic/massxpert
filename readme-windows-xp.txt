Wellcome to the massXpert program readme.txt file
=================================================

The Program and the Data were installed in C:\Program Files\massxpert.


DOCUMENTATION
+++++++++++++

Documentation in PDF format is available at the http://www.massxpert.org website.


USER FILES
++++++++++

The .massxpert directory was created in your directory: C:\Documents and Settings\{your username}.


CUSTOMIZATION
+++++++++++++

The massXpert software program is customizable by modifying/adding atom/polymer chemistry definitions.

The simplest approach is to copy the stuff in the "C:\Program Files\massxpert\data" 
directory to C:\Documents and Settings\{your username}\.massxpert. 

Then you are free to add/modify any polymer chemistry definition. Once you have finished making modifications or adding 
brand new polymer chemistries, make sure you register them to the system by editing the following file:

C:\Documents and Settings\{your username}\.massxpert\pol-chem-defs\massxpert-pol-chem-defs-cat

You may create as many catalogue files as you wish, with the requirement that these have to 
have their name ending with "pol-chem-defs-cat".

For example, if you have created a new polymer chemistry definition named myNewDef, then you would
add a line like the following telling massXpert where to look for its data:

myNewDef=C:\Documents and Settings\massxpert\data\pol-chem-defs\myNewDef\myNewDef.xml

either in the massxpert-pol-chem-defs-cat file, or  in a new file named something like :

all-my-definitions-pol-chem-defs-cat

Restart the software and the new polymer chemistry definition should be made available.

To explore the features of massXpert, the best is to run the program and explore the data
located in C:\Program Files\massxpert\data (either polymer chemistry definitions or polymer sequences).

Of course you can open polymer chemistry definitions with the XpertDef menu and polymer sequences with the XpertEdit menu !


Happy massXpert'ing !

And do not forget to report bugs !

Filippo Rusconi,
Author developer of massXpert 
