TEMPLATE      = subdirs

SUBDIRS       = lib \
                plugins-src \
                gui 


license.path = $${DOCDIR}
license.files = COPYING
INSTALLS += license

usermanpdf.path = $${USERMANPDFDIR}
usermanpdf.files = userman/pdf/massxpert.pdf
INSTALLS += usermanpdf

usermanhtml.path = $${USERMANHTMLDIR}
usermanhtml.files = userman/html/*.html
usermanhtml.files += userman/html/*.png
INSTALLS += usermanhtml

usermanfig.path = $${USERMANFIGDIR}
usermanfig.files = userman/figures/*
INSTALLS += usermanfig

data.path = $${DATADIR}
data.files = data/*
INSTALLS += data
