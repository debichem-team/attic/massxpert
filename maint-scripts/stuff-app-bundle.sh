#!/bin/sh

set -e

if [ "$1" != "Release" ] && [ "$1" != "Debug" ]
then
echo "Please tell if *Release* or *Debug* mode"
exit 1
fi

cd ~/devel/build-work/$1/massXpert.app/Contents

mkdir -p Resources
cp -vrpf ~/devel/massxpert/data Resources/

mkdir -p Locales
cp -v ~/devel/massxpert/gui/massxpert_fr.qm  Locales/

mkdir -p Plugins
cp -v ~/devel/build-work/$1/lib*.dylib Plugins

