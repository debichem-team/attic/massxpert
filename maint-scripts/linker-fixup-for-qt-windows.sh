#!/bin/sh

cd /c/devel/b/gui/CMakeFiles/massxpert.dir

# In emacss open the file and do the following regext search/replace:
# XP
# qt\\lib\\lib\(Qt[A-Za-z]*4\).a--->>>qt\\bin\\\1.dll
# Win7
# lib\\lib\(Qt[A-Za-z]*4\).a--->>>bin\\\1.dll


cp link.txt link-bkp.txt

 sed 's/\(C:\\Qt\\2009.05\\qt\)\\lib\\lib\(Qt[A-Za-z]*4\).a /\1\\bin\\\2.dll /g' link.txt > link-sed.txt

if [ "$?" == 0 ]
    then
    mv link-sed.txt link.txt
else
    rm link-sed.txt
    echo "Failed to sed the file."
fi

cat link.txt

cd -
