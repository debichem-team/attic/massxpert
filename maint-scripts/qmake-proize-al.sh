#!/bin/sh

cd ~/devel/massxpert

cd lib ; qmake -spec macx-xcode lib.pro
cd ../gui ; qmake -spec macx-xcode gui.pro

cd ../plugins-src/massListSorterPlugin ; qmake -spec macx-xcode massListSorterPlugin.pro
cd ../numeralsLocaleConverterPlugin ; qmake -spec macx-xcode numeralsLocaleConverterPlugin.pro
cd ../seqToolsPlugin ; qmake -spec macx-xcode seqToolsPlugin.pro

cd ../ ; qmake -spec macx-xcode plugins-src.pro

cd ../ ; qmake -spec macx-xcode massxpert.pro
