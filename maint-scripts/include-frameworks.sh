#!/bin/sh 

set -e

usage ()
{
    echo "" 
    echo "Usage:"
    echo "$0 <Absolute path to the bundle>"
    echo ""
    echo "If the bundle.app full path is  /the/dir/bundle.app,"
    echo "then the parameter is \"/the/dir/bundle.app\""
    echo "" 
    exit 1
}

if [ "$#" != "1" ]
    then
    usage
fi

if [ ! -d "$1" ]
    then
    echo ""
    echo "The directory does not exist."
    usage
fi

# At this point we have a bundle directory. Check that it
# has a Contents/MacOS subdirectory.

if [ ! -d "$1/Contents/MacOS" ]
    then
    echo ""
    echo "The directory does not seem to have"
    echo "a canonical bundle structure"
    usage
fi

echo ${1} | grep ^/ > /dev/null 2>&1

if [ "$?" = "1" ]
then 
    echo "Path is not absolute"
    usage
fi

absolutePath=$1;
echo "Parameter = ${absolutePath}"

bundleDir=$(basename ${absolutePath});

echo "bundleDir = ${bundleDir}";

pwd=$(pwd);

cd ${absolutePath}/../

workingDir=$(pwd)
echo "workingDir = ${workingDir}"


echo "Running command otool -L ${bundleDir}/Contents/MacOS/massXpert"
otool -L ${bundleDir}/Contents/MacOS/massXpert

mkdir -p ${bundleDir}/Contents/Frameworks;
rm -rf ${bundleDir}/Contents/Frameworks/Qt*

printf "Now copying the Frameworks... "

for lib in Core Gui Svg Network Xml 
do 
#    cp -R /Library/Frameworks/Qt${lib}.framework ${bundleDir}/Contents/Frameworks
    cp  /Library/Frameworks/Qt${lib}.framework/Versions/4/Qt${lib} ${bundleDir}/Contents/Frameworks
done

printf "Done copying the Frameworks into the bundle\n"

sleep 2

Printf "Performing the install_name_tool -id for the frameworks"

for lib in Core Gui Svg Network Xml
do
    install_name_tool -id @executable_path/../Frameworks/Qt${lib} \
         ${bundleDir}/Contents/Frameworks/Qt${lib}
done

printf "Done install_name_tool -id\n"

sleep 2


printf "Performing the install_name_tool -change for the frameworks"

for lib in Core Gui Svg Network Xml
do
    install_name_tool -change Qt${lib}.framework/Versions/4/Qt${lib} \
         @executable_path/../Frameworks/Qt${lib} \
         ${bundleDir}/Contents/MacOS/massXpert
done

printf "Done install_name_tool -change\n"

sleep 2


printf "Dealing with the frameworks' inter-dependencies\n"

printf "QtGui <--> QtCore..."
install_name_tool -change QtCore.framework/Versions/4/QtCore \
@executable_path/../Frameworks/QtCore \
${bundleDir}/Contents/Frameworks/QtGui
printf "Done\n"

printf "QtSvg <--> QtCore && QtSvg <--> QtGui..."
install_name_tool -change QtCore.framework/Versions/4/QtCore \
@executable_path/../Frameworks/QtCore \
${bundleDir}/Contents/Frameworks/QtSvg

install_name_tool -change QtGui.framework/Versions/4/QtGui \
@executable_path/../Frameworks/QtGui \
${bundleDir}/Contents/Frameworks/QtSvg
printf "Done\n"

printf "QtXml <--> QtCore..."
install_name_tool -change QtCore.framework/Versions/4/QtCore \
@executable_path/../Frameworks/QtCore \
${bundleDir}/Contents/Frameworks/QtXml
printf "Done\n"

printf "QtNetwork <--> QtCore..."
install_name_tool -change QtCore.framework/Versions/4/QtCore \
@executable_path/../Frameworks/QtCore \
${bundleDir}/Contents/Frameworks/QtNetwork
printf "Done\n"

printf "Done dealing with the frameworks' inter-dependencies\n"

printf "Treating the plugins in the save way as above\n"

for lib in Core Gui Svg Xml Network
do
    for plugin in massListSorter numeralsLocaleConverter seqTools
    do
	install_name_tool -change Qt${lib}.framework/Versions/4/Qt${lib} \
            @executable_path/../Frameworks/Qt${lib} \
            ${bundleDir}/Contents/Plugins/lib${plugin}Plugin.so
    done
done

printf "Done treating the plugins\n"

sleep 2

echo "Running command otool -L ${bundleDir}/Contents/MacOS/massXpert"
otool -L ${bundleDir}/Contents/MacOS/massXpert

echo "Running command otool -L for each inter-dependent Qt framework:"

for lib in Core Gui Svg Xml Network
do 
    otool -L ${bundleDir}/Contents/Frameworks/Qt${lib} | grep Qt
done


echo "Running command otool -L on each plugin"
for plugin in massListSorter numeralsLocaleConverter seqTools
    do
    otool -L ${bundleDir}/Contents/Plugins/lib${plugin}Plugin.so
done

printf "Copying bundle to Desktop..."
dateString=$(date "+%Y%m%d-%H%M")
cp -rpf ${bundleDir} /Users/rusconi/Desktop/massXpert-${dateString}.app
printf "as /Users/rusconi/Desktop/massXpert-${dateString}.app"


exit

Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. 
Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. 
Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. Notes. 

Build the program, it goes in 

~/devel/build-work/Release/${bundleDir}

Then go to ~/devel/build-work/Release/${bundleDir}/Contents/MacOS

and run

otool -L massXpert 
massXpert:
	QtSvg.framework/Versions/4/QtSvg (compatibility version 4.4.0, current version 4.4.0)
	QtXml.framework/Versions/4/QtXml (compatibility version 4.4.0, current version 4.4.0)
	QtGui.framework/Versions/4/QtGui (compatibility version 4.4.0, current version 4.4.0)
	/System/Library/Frameworks/Carbon.framework/Versions/A/Carbon (compatibility version 2.0.0, current version 128.0.0)
	/System/Library/Frameworks/AppKit.framework/Versions/C/AppKit (compatibility version 45.0.0, current version 824.42.0)
	QtNetwork.framework/Versions/4/QtNetwork (compatibility version 4.4.0, current version 4.4.0)
	QtCore.framework/Versions/4/QtCore (compatibility version 4.4.0, current version 4.4.0)
	/usr/lib/libz.1.dylib (compatibility version 1.0.0, current version 1.2.3)
	/usr/lib/libSystem.B.dylib (compatibility version 1.0.0, current version 88.3.6)
	/usr/lib/libstdc++.6.dylib (compatibility version 7.0.0, current version 7.4.0)
	/usr/lib/libgcc_s.1.dylib (compatibility version 1.0.0, current version 1.0.0)
	/System/Library/Frameworks/CoreFoundation.framework/Versions/A/CoreFoundation (compatibility version 150.0.0, current version 368.28.0)


We see that the Qt frameworks are known in the form

QtSvg.framework/Versions/4/QtSvg *and not*

QtSvg.framework/Versions/4.0/QtSvg.


OK, now get to know the internal dependencies:

* QtCore does not depend on any Qt framework.

* QtGui depends on QtCore

* QtSvg depends on both QtCore and QtGui

*QtXml depends on QtCore

*QtNetwork depends on QtCore.

This means that we'll have to perform a number of steps on the bundled-incorporated frameworks...


Further, the plugins need the same kind of treatment.

libmassListSorterPlugin.dylib depends on QtCore and QtGui
libnumeralsLocaleConverterPlugin.dylib depends on QtCore and QtGui
libseqToolsPlugin.dylib depends on QtCore and QtGui

