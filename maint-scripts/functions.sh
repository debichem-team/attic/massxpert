#!/bin/sh


sourceVersion ()
{
    VERSION=$(grep 'SET (VERSION ' CMakeLists.txt | sed 's/^.*VERSION *"*\([0-9.]*\)"*)/\1/')

    echo ${VERSION}
}

checkUsermanualIntegrity ()
{
    cd $1 || { echo "Failed to change directory to $1"; exit 1; }

    # Invert match with -v so that not finding the string is success
    # to keep bash's set -e happy.

    grep -v '^ *% *\\input' massxpert.tex > /dev/null 2>&1
    if [ ! -f "massxpert-version.tex" ]
    then
	echo -n "Please rerun the CMake configuration "
	echo "massxpert-version.tex is not present"
	exit 1
    fi
    
    cd -
   
    result="$?"
    
    echo "${result}"
    return "${result}"
}

usermanualVersion ()
{
    cd $1 || { echo "Failed to change directory to $1"; exit 1; }

    echo $(grep ver massxpert-version.tex | sed 's/^.*}{//' | sed 's/}//')
}

makeUsermanualPdf ()
{
    cd $1 || { echo "Failed to change directory to $1"; exit 1; }
    
    make 
    
    result="$?"
    
    echo "${result}"
    return "${result}"
}


# Creates a directory ~/tmp/dirName and copies in there all the files
# from ~/devel/massxpert that belong to the source tree. Once the
# directory is filled-up, a tarball is made with it, with name
# ~/tmp/dirName.tar.gz.
makeSourceTarball ()
{
    # Argument is the tarball name.
    if [ -z "$1" ] 
    then
	return 1
    fi
    
    thisDir=$(pwd)

    dirName=$(basename $1 .tar.gz)

    if [ -d "~/tmp/${dirName}" ]
    then
	echo "Should \"~/tmp/${dirName}\" be removed before y/n?"
		
	read char
	
	if [ "x$char" == "xy" ]
	then
	    rm -rf "~/tmp/${dirName}"
	fi
    fi

    cd ~/devel/massxpert || { echo "Failed to change directory to massxpert/"; return 1; }
 
    ##########################################################################
    # Make sure we are not going to package backup files.
    
    fileList=$(find -name "*~")
    
    if [ ! -z "${fileList}" ]
    then
	echo "There are backup files (*~) still present:"
	echo "${fileList}"
	echo "Please remove them first."
	exit 1
    fi
    fileList=$(find -name "*\#*")
    
    if [ ! -z "${fileList}" ]
    then
	echo "There are backup files (#*) still present:"
	echo "${fileList}"
	echo "Please remove them first."
	exit 1
    fi
      fileList=$(find -name "\.#*")
    
    if [ ! -z "${fileList}" ]
    then
	echo "There are backup files (.#*) still present:"
	echo "${fileList}"
	echo "Please remove them first."
	exit 1
    fi
    fileList=$(find -regex ".*-bkp*")
    
    if [ ! -z "${fileList}" ]
    then
	for item in ${fileList}
	do
	    echo $item | grep "\.git"
	    if [ "$?" !=  "0" ]
	    then 
		echo "There are backup files (*-bkp*) still present:"
		echo "${fileList}"
		echo "Please remove them first."
		exit 1
	    fi
	done
    fi
    
    cd ~/devel || { echo "Failed to change directory to devel/"; return 1; }

    cp -rpf massxpert ~/tmp/${dirName}

    cd ~/tmp/${dirName} || { echo "Failed to change directory to ~/tmp/${dirName}"; return 1; } 
    
    # Remember that we cannot put the .git directory in the source
    # tarball.
    rm -rf .git
    
    cd ~/tmp

    tar cvzf $1 ${dirName}

    cd ${thisDir}

    return $?
}


makeBinaryTarball ()
{
    # Argument $1 is the tarball name.
    if [ -z "$1" ]
	then
	echo "Tarball name '$1' is empty."
	return 1
    fi
    
    # Argument $2 is the prefix where the files are to be collected.
    if [ ! -d "$2" ]
	then 
	echo "Prefix $2 does not exist."
	return 1
    fi

    tar cvzf $1 \
	$2/bin/massxpert \
	$2/lib/massxpert \
	$2/share/massxpert \
	$2/share/doc/massxpert \
	--exclude=*~ --exclude=*-bkp ||  { echo "Failed to create tar file."; return 1; }
    
    return 0
}
    


# $1 is the full pathname to source directory 

# $2 is the full pathname to the binary directory where cmake should
# operate, by reading in $1.

makeBinary ()
{    
    thisDir=$(pwd)

    cd ~/tmp || { echo "Failed to change directory to ~/tmp."; return 1; }

    mkdir -p $2 || { echo "Failed to create build directory in tmp."; return 1; }
    cd $2 || { echo "Failed to change to build directory in tmp."; return 1; }

    # We do not care of the potential errors with the emptying of the builddir.
    set +e
    rm -rf $2/*
    set -e
    
    cmake $1
    make || { echo "Failed to build software"; return 1; }

    cd ~/devel/massxpert/usermanual || { echo "Failed to change directory to usermanual."; return 1; }

    checkUsermanualIntegrity || { echo "The User Manual does not include all subdocuments"; return 1; }
    
    if [ $(usermanualVersion) != "${VERSION}" ]
	then
	echo "User Manual has version different than Source package"
	return 1
    fi

    makeUsermanualPdf  || { echo "Failed to compile the User Manual PDF."; return 1; }    
#    makeUsermanualHtml || { echo "Failed to compile the User Manual HTML."; return 1; }
    
    cd ${thisDir}

    return 0
}

# $1 is the directory from which to make the install as sudo.
makeInstall ()
{   
    thisDir=$(pwd)

    cd $1 || { echo "Failed to change directory to $1"; return 1; }

    sudo make install || { echo "Failed to make install massxpert."; return 1; }

    cd {thisDir}

    return 0
}

