#!/bin/sh

set -e

prefix=/usr/local

echo "Should anything massxpert removed from ${prefix}? (y/n)"
read char

if [ "x$char" = "xy" ]
then
    # Remove anything massxpert in ${prefix}.
    for removedFile in $(find ${prefix} -name "*massxpert*")
    do 
	sudo rm -vrf ${removedFile}
    done
fi
