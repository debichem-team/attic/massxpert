#!/bin/sh

if [ "$#" == "0" ]
    then
    echo "Pass <src|pdf|all> as parameter to this script."
    exit 1
fi

cd ~/devel  || { echo "Failed to change dir to ~/devel."; exit 1; }

if [ "$1" == "src" ]
    then 
    rsync -av --delete massxpert rusconi@rddm503fr1:devel/
elif [ "$1" == "pdf" ]
    then 
   rsync -av --delete b/usermanual/massxpert.pdf \
        rusconi@rddm503fr1:devel/build-work
else
    rsync -av --delete massxpert rusconi@rddm503fr1:devel/
    
    rsync -av --delete b/usermanual/massxpert.pdf \
        rusconi@rddm503fr1:devel/build-work
fi