#!/bin/sh

usage()
{
	echo "	Usage:"
	echo "	This program runs git archive to craft a debian"
	echo "	<project>-<version>_orig.tar.bz2 tarball that can be used for the"
	echo "  packaging of <project>."
	echo ""
	echo "	Simply answer the questions."

	exit
}

if [ "$1" = "-h" ] || [ "$1" = "--help" ] || [ "$1" = "?" ]
then
	usage
fi


mainDir="$HOME/devel/massxpert"

tarballDir="${mainDir}/tarballs"
buildDir="${mainDir}/build-area"

devDir="${mainDir}/development"
debianDir="${devDir}/debian"

cd ${devDir}

# Check that we are in the master branch or alert the user.

gitBranch=""

gitBranch=$(git status | head -n1 | awk '{print $3}')
if [ "${gitBranch}" != "master" ]
then
	echo "Checkout the master branch first, or is this branch fine (y/n)."
	read answer
	if [ "x${answer}" != "xy" ]
	then
		exit
	fi
fi

# Check that we have nothing to commit.

gitOutput=$(git status | \
	grep "nothing to commit" | awk '{printf $1 $2 $3}')

if [ "${gitOutput}" != "nothingtocommit," ]
then
	echo "Branch ${gitBranch} is not clean." 
	exit
fi

DEBVER=$(head -n1 ${debianDir}/changelog | \
	awk '{printf $2}' | sed 's|(\([0-9\.-]*\))|\1|')
UPVER=$(echo ${DEBVER} | sed 's/-[0-9]\+//')

echo "Making .orig.tar.{gz,bz2} for"
echo "    UPVER=${UPVER}"
echo "    DEBVER=${DEBVER}"

echo ""
echo "Is this correct? Ctrl-C to stop process. Return to accept"
read answer
echo "Going on, then."

# Now that we have the Upstream version, we can craft the name of the
# orig.tar.gz tarball.

origTarball=massxpert_${UPVER}.orig.tar.gz

# We have to make that archive:

printf "Making ${tarballDir}/${origTarball}... "

git archive --format=tar --verbose \
	--prefix=massxpert-${UPVER}/ ${gitBranch} -- | \
	gzip -c - > ${tarballDir}/${origTarball}

printf "exit status: $?\n"

ln -vsf ${tarballDir}/${origTarball} ${mainDir}/${origTarball}

