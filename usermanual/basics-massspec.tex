\chapter[Basics in Mass Spectrometry]{Basics in Mass Spectrometry}
\label{chap:basics-mass-spectrometry}
\index{mass~spectrometry}

Mass spectrometry has become a ``buzz word'' in the field of
structural biology. While it has been used for long to measure the
molecular mass of little molecules, its recent developments have
brought it to the center of the analytical arsenal in the field of
structural biology (also of ``general'' polymer science). It is now
current procedure to use mass spectrometry to measure the mass of
polypeptides, oligonucleotides (even complete transfer RNAs!) and
saccharides, amongst other complex biomolecules.

A mass spectrometer is usually described by giving to its three main
different ``regions'' a name suggestive of their function:
\begin{itemize}
\item the source, where production of ionized analytes takes place;
\item the analyzer, where the ions are electrically/magnetically
  ``tortured'';
\item the detector, where the ions arrive, are detected and counted.
\end{itemize}


\renewcommand{\sectitle}{Ion Production: The Source}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}
\index{mass~spectrometer!ion~source}

Mass spectrometry can do nothing if the molecule to analyze (the
\emph{analyte}) is not in an electrically charged state. The process
of creating an ion from an un-charged analyte is called
\emph{ionization}. Most of the times the ionization is favored by
adapting the sample's pH to a value lower than the isoelectric pH of
the analyte, which will elicit the appearance of charge(s) onto it. In
cases where the analyte cannot be charged by simple pH variations
(small molecule that does not bear any ionizable chemical group), the
ionization step might require---on the massist's part---use of starker
ionization techniques, like electronic impact ionization or chemical
ionization.  In biopolymer mass spectrometry, the pH strategy is
usually considered the right way to proceed. The ionization process
might involve complex charge transfer mechanisms (not fully understood
yet, at least for certain ionization/desorption methods) which tend to
ionize the analyte in a way not predictable by looking at the
analyte's chemical structure.

Ion production should not be uncoupled from one important feature of
mass spectrometry: solvent
evaporation--\emph{desolvation}\index{desolvation}, in case of liquid
sample delivery to the mass spectrometer--and sample
\emph{desorption}\index{desorption}---in case of solid state sample
introduction. The general idea is that mass spectrometry works on gas
phase ions. This is because it is of crucial importance, for a correct
mass measurement to take place, that the analyte be \emph{totally}
freed of its chemical immediate environment.  That is, it should be
``naked'' in the gas phase. Equally important is the fact that ions
must be capable of travelling long distances without ever encountering
any other molecule in their way.  This is achieved by pumping very
hard in the two regions called ``analyzer'' and ``detector''. In this
respect, the source is a special region because, depending on the
design of the mass spectrometer, it might be partially at the
atmospheric pressure during mass spectrometer operation. It is not the
aim of this manual to provide insights into mass spectrometer design
topics, but the general principle is that mass spectrometry involves
working on gas phase ions. This is why a mass spectrometer is usually
built on extremely reliable pumping technology aimed at maintaining
for long periods of time (with no sudden interruption, otherwise the
detector might suffer seriously) a good vacuum in the conduit in which
ions must flow during operation.

\renewcommand{\sectitle}{The Analyzer}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}
\index{mass~spectrometer!analyzer}

Once an ion has been generated in the gas phase, its mass should be
measured. This is a complex physical process. Depending on the mass
spectrometer design, the mass measurement is based on more or less
complex physical events. Magnetic mass spectrometers are usually
thought of as pretty complex devices; this is also the case for the
Fourier transform ion cyclotronic resonance devices. An analyzer like
the \emph{time of flight} analyzer is much simpler. I will refrain
from trying to explain the physics of the mass measurement, just limit
myself to saying that---at some stage of the mass measurement
process---forces are exerted on the ions by electric/magnetic fields
(incidentally, this explains why it is so important that an analyte be
ionized, otherwise it would not be subject to these fields). The
ionized analytes submitted to these forces have their trajectory
modified in such a way that the detector should be able to quantify
this modification. Roughly, this is the measurement process.

\renewcommand{\sectitle}{What Is Really Measured?}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}

Prior to entering into some detail, it seems necessary to make a few
definitions\footnote{Interesting posting signed by Ken I. Mitchelhill
  in the ABRF mailing list at \url{http://www.abrf.org/archives}, and
  a document published by the California Institute of Technology.}:
\begin{itemize}
\item unified mass scale\index{units!unified~mass~scale} (u): IUPAC \&
  IUPAP (1959-1960) agreed upon scale with 1 u equal to 1/12 the mass
  of the most abundant form of carbon; the dalton is taken as
  identical to u (but not accepted as standard nomenclature by IUPAC
  or IUPAP), it is abbreviaed in Da.
\item a former unit was ``a.m.u.'' (\textit{i.e.} ``atomic mass
  unit'')\index{units!atomic~mass~unit (amu)}. It should be considered
  obsolete, since based on an old 1/16 of $\mathrm{^{16}O}$ standard;
\item the mass of a molecule (also ``molecular mass'') is expressed in
  daltons\index{units!dalton~(Da)}. The symbol commonly used is ``M'' (not
  ``m''), as in ``M+H'' or ``M+Na''\dots\ Symbol ``m'' is already
  employed for ion mass (as in ``m/z'');
\item the mass-to-charge ratio\index{units!mass-to-charge~ratio~(m/z)}
  (``m/z'') of an ion is the ion's mass (in daltons) divided by the
  number (z) of elementary charges. Hence ``m/z'' is ``mass per
  charge'' and units of ``m/z'' are ``daltons per charge'';
\item nominal mass: the integral sum of the nucleons in an atom (it is
  also the atomic mass number);
\item exact (also known as accurate) mass: the sum of the masses of
  the protons and neutrons plus the nuclear binding energy;
\end{itemize}

In the previous sections I used to say that a mass spectrometer's task
is to measure masses. Well, this is not 100~\% exact. A mass
spectrometer actually allows to measure something else: it measures
the \emph{$m$ to $z$ ratio} of the analyte, which is denoted
\emph{$m/z$}. What is this ``\emph{$m$ to $z$ ratio}'' all about?
Well, we said above that a mass spectrometer has to exert forces on
the ions in order to determine their $m/z$. Now, let us say that we
have an electric field of constant value, $E$. We also have two ions
of identical masses, one bearing one charge ($q$) and the other one
bearing two charges (2$q$)---positive or negative, no matter in this
discussion. These two ions, when put in the same electric field $E$,
will ``feel'' two different forces exerted on them: $F_1$ and $F_2$.
It is possible to calculate these forces ($F_1=qE$ and $F_2=2qE$).
Evidently, the ion that bears two charges is submitted to a force that
is twice as intense as the one exerted on the singly charged ion.

What does this mean? It means simply that the numeric result provided
by the mass spectrometer is not going to be the same for both ions,
since the physics of the mass spectrometer takes into account the
charge level of each different analyte. Our two ions weigh exactly the
same, but the mass spectrometer simply can not know that; all it knows
is how a given ion reacts to the electric field it is put in. And our
two ions, evidently, will react differently.

When we say that a mass spectrometer measures a $m/z$ ratio, the $z$
in this ratio represents the sum of all the charges (this is a net
charge) that sit onto the analyte. But what does the $m$ stand for?
The molecular mass? No! The $m$ stands for the mass of the whole
analyte ion, which is---in a word---the \emph{measured mass}. This is
not the molecular mass (which would be $M$), it is the molecular mass
\emph{plus/less} the mass of the chemical entity that brings the
charge to the analyte. When ionizing a molecule, what happens is that
something brings (or removes) a charge. In biopolymer chemistry, for
example, often the ionization is a simple protonation/deprotonation.
If it is a protonation, that means that an electronic doublet (on some
basic group of the analyte) captures a proton. This brings the mass of
a proton to the biopolymer ($\simeq$ 1~Da). Conversely, if it is a
deprotonation (loss of a proton by some acidic group, say a carboxylic
that becomes a carboxylate) the polymer looses the mass of a proton.
Of course, if the ionization involves a single electron transfer the
mass difference is going to be so feeble as to be un-measurable on a
variety of mass spectrometers. Let us try to formalize this in a less
verbose manner by using a sweet amino acid as an example: \smallskip

\begin{itemize}
\item the non-ionized analyte (Glycine) has the following formula:
  $\mathrm{C_2H_5O_2N_1}$; \\
  the molecular mass is thus $\mathrm{M = 75.033}$ Da;
\item the analyte gets protonated in the mass spectrometer: 
  \[\mathrm{C_2H_5O_2N_1 + H \rightharpoonup\ C_2H_6O_2N_1}\]
  the measured mass of the ion is thus $\mathrm{m = 75.033 + 1.00782}$
  Da and the charge beared by the ion is thus $z = +1$.
\item the peak value read on the mass spectrum for this analyte will
  thus be (with $\mathrm{z = +1}$):\\
  \[\mathrm{value = \frac{m}{z} = \frac{M + 1.00782}{z} = 76.04}\]
  
\end{itemize}

\noindent We see here that the label on the mass spectrum does not
correspond to the nominal molecular mass of the analyte: the ionizing
proton is ``weighed'' along with the Glycine molecule. Imagine now
that, by some magic, this same Glycine molecule just gets protonated a
second time. Let's do exactly the same type of calculation as above,
and try to predict what value will be printed onto the mass spectrum:
\smallskip

\begin{itemize}
\item the un-ionized analyte (Glycine) has the following formula:
  $\mathrm{C_2H_5O_2N_1}$; \\
  the molecular mass is thus $\mathrm{M = 75.033}$ Da;
\item the analyte gets protonated in the mass spectrometer \emph{two
    times}: \[\mathrm{C_2H_5O_2N_1 + 2H \rightharpoonup\
    C_2H_7O_2N_1}\] the measured mass of the ion is thus $\mathrm{m =
    75.033 + 2.01564}$ Da and the charge beared by the ion is thus
  $\mathrm{z = +2}$.
\item the peak value read on the mass spectrum for this analyte will
  thus be (with $\mathrm{z = +2}$):\\
  \[\mathrm{value = \frac{m}{z} = \frac{M + 2.01564}{z} = 38.52}\]
  
\end{itemize}
%
At this point it is absolutely clear that a \mz is not a molecular
mass. By the way, if the Glycine happened to be ionized
\emph{negatively} the calculation would have been analogous to the one
above, but instead of \emph{adding} the mass of the proton(s) we would
have \emph{removed} it. Summing up all this in a few words: an
ionization involves one or more charge transfer(s) and in most cases
(at least in biopolymer mass spectrometry) also involves matter
transfer(s). It is crucial \emph{not} to forget the matter transfer(s)
when ionizing an analyte.  This means that when an ionization process
is described, its description ought to be complete, clearly stating
three different pieces of information:

\begin{itemize}
\item the matter transfer (optional; usually a formula like ``+H1'');

\item the charge transfer (net charge that is brought by the
  ionization agent);

\item the ionization level (the number of ionization event; 0 means
  ``no ionization''; usually this would be 1 for a single ionization,
  but might be as large as 30 if, for example, a protein was ionized
  by electrospray. In this case the \mz value would be computed this
  way (with $\mathrm {z = +30}$):
  \[\mathrm{value = \frac{m}{z} = \frac{M + 30\cdot 1.00782}{30} =
    \frac{16959 + 30.2346}{30} = 566.30}\]
\end{itemize}
%  
In the next chapters of this manual \mXp\ will be described so as to
let the user take advantage of its powerful capabilities.  In a first
chapter some general concepts around the way the program behaves will
be presented.  Next, in the remaining part of this manual, a chapter
will be dedicated to each important \mXp\ function or characteristic.

\cleardoublepage



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "massxpert"
%%% End: 
