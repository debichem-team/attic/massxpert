\chapter[\mXp\ Generalities]{\mXp\
Generalities}

\label{chap:massxpert-generalities}

In this chapter, I wish to introduce some general concepts around the
\mXp\ program. 

\renewcommand{\sectitle}{General \mXp\ Concepts}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}

The \mXp\ mass spectrometry software suite has been designed to be
able to ``work'' with every linear polymer. Well, in a certain way
this is true\dots\ A more faithful account of the \mXp's capabilities
would be: ``\emph{The \mXp\ software suite works with whatever polymer
  chemistry the user cares to define; the more accurate the polymer
  chemistry definition, the more \mXp\ will be accurate}''.

For the program to be able to cope with a variety of possibly very
different polymers, it had to be written using some \emph{abstraction
  layer} in between the mass calculations engine and the mere
description of the polymer sequence. This abstraction layer is
implemented with the help of ``polymer chemistry definitions'', which
are files describing precisely how a given polymer type should behave
in the program and what its constitutive entities are. The way polymer
chemistry definitions are detailed by the user is the subject of a
chapter of this book (see menu \guimenu{\xpd} of the program).
However, in order to give a quick overview, here is a simple
situation: a user is working on two polymer sequences, one of
chemistry type ``protein'' and another one of chemistry type ``dna''.
The protein sequence reads ``ATGC'', and the dna sequence reads
``CGTA''. Now imagine that the user wants to compute the mass of these
sequences. How will \mXp\ know what formula (hence mass) each monomer
code corresponds to? There must be a way to inform \mXp\ that one of
the sequences is a protein while the other is a DNA oligonucleotide:
this is done upon creation of a polymer sequence; the programs asks of
what chemistry type the sequence to be created is.  Once this
``chemical parentage'' has been defined for each sequence, \mXp\ will
know how to handle both the graphical display of each sequence and the
calculations for each sequence.


\renewcommand{\sectitle}{On Formul{\ae} And Chemical Reactions}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}

\label{sect:formulae-and-chemical-reactions}

Any user of \mXp\ will inevitably have to perform two kinds of
chemical simulations:
\smallskip

\begin{itemize}

  \item Define the formula of some chemical entity;

  \item Define a given chemical reaction, like a protein monomer
    modification, for example.

\end{itemize}
%
While the definition of a formula poses no special difficulty, the
definition of a chemical reaction is less trivial, as detailed in the
following example. The lysyl residue has the following formula:
$\mathrm{C_6H_{12}N_2O}$. If that lysyl residue gets acetylated, the
acetylation reaction will read this way:---\textsl{``An acetic acid
  molecule will condense onto the $\epsilon$~amine of the lysyl side
  chain''.}  This can also read:---\textsl{``An acetyl group enters
  the lysyl side chain while a hydrogen atom leaves the lysyl side
  chain; water is lost in the process''.} The representation of that
reaction is:

\[\mathrm{R-NH_2 + CH_3COOH \rightleftharpoons R-NH-CO-CH_3 + H_2O}\]

\noindent When the user wants to define that chemical reaction, she
can use that representation: ``$\mathrm {-H_2O+CH_3COOH}$'', or even
the more brief but chemically equivalent one: ``$\mathrm
{-H+CH_3CO}$''. In \mXp, the chemical reaction representation is
considered a valid formula.


\renewcommand{\sectitle}{The \mXp\ Framework Data Format}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}
\index{format}

All the data dealt with in \mXp\ are stored on disk as
\fileformat{XML}-formatted
files. \fileformat{XML}\index{format!file!xml} is the \emph{eXtensible
  Markup Language}. This ``language'' allows to describe the structure
of a document. The structure of the data is first described in a
section of the document that is called the \textit{Document Type
  Definition, DTD}, and the data follow in the same file. One of the
big advantages of using such \fileformat{XML} format in \mXp\ is that
it is a text format, and not a binary one.  This means that any data
in the \mXp\ package is human-readable (even if the \fileformat{XML}
syntax makes it a bit difficult to read data, it is actually
possible). Try to read one of the polymer chemistry definition
\fileformat{XML} files that are shipped with this software package,
and you'll see that these files are pure text files (the same applies
for the \filename{*.mxp}\index{format!file!mxp} \fileformat{XML}
polymer sequence files.  The advantages of using text file formats,
with respect to binary file formats are:

\begin{itemize}

\item The data in the files are readable even without the program that
  created them. Data extraction is possible, even if it costs work;

\item Whenever a text document gets corrupted, it remains possible to
  extract some valid data bits from its uncorrupted parts. With a
  binary format, data are chained from bit to bit; loosing one bit
  lead to automatic corruption of all the remaining bits in the file;

\item Text data files are searchable with standard console tools
  (\progname{sed, grep\dots}, which make it possible to search easily
  text patterns in any text file or thousands of these files in one
  single command line. This is not possible with binary format, simply
  because reading them require the program that knows how to decode
  the data and the powerful console-based tools would prove useless.

\end{itemize} 


\renewcommand{\sectitle}{General Chemical Entity Naming Policy}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}

\label{sect:chemical_entity_naming_policy}

Unless otherwise specified, the user is \emph{strongly} advised
\emph{not} to insert any non-alphanumeric-non-ASCII characters (space,
\%, \#, \$\dots) in the strings that identify polymer chemistry
definition entities. This means that, for example, users must refrain
from using non-alphanumeric-non-ASCII characters for the atom names
and symbols, the names, the codes or the formul{\ae} of the monomers
or of the modifications, or of the cleavage specifications, or of the
fragmentation specifications\dots\ Usually, the accepted delimiting
characters are `-' and `\_'. It is important not to cripple these
polymer data for two main reasons:
\smallskip

\begin{itemize}

\item So that the program performs smoothly (some file-parsing
  processes rely on specific characters (like `\#' or `\%', for
  example) to isolate sub-strings from larger strings);

\item So that the results can be easily and clearly displayed when
  time comes to print all the data.

\end{itemize}


\cleardoublepage


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "polyxmass"
%%% End: 
