\chapter[\xpd] {\xpd: Definition Of Polymer Chemistries}
\label{chap:xpertdef}
\index{\xpd|(}

After having completed this chapter the reader will be able to
accomplish the very first steps needed to use \mXp's features at best:
the normal workflow, indeed, is to first make a polymer chemistry
definition, in order to be able to edit polymer sequences of that
specific definition. The \xpd\ module is made available in \mXp\ by
pulling down the \guimenu{\xpd} menu item from the program's menu. It
is possible to start a new polymer chemistry definition from scratch,
but it is certainly usually easier to first duplicate a polymer
chemistry definition shipped with \mXp\ and then open that copy and
edit it.  Please, refer to chapter~\ref{chap:data-customization},
page~\pageref{chap:data-customization} for an explanation of how this
is safely done.

\begin{figure}
  \begin{center}
    \includegraphics [width=0.65\textwidth]
    {figures/select-pol-chem-def.png}
  \end{center}
  \caption[Select one polymer chemistry definition
  file]{\textbf{Select one polymer chemistry definition file.} It is
    possible to immediately select a polymer chemistry definition
    already registered with the system, or open an arbitrary file by
    browsing the filesystem (click the \guilabel{Cancel} button,
    hidden in this figure, if so desired).}
  \label{fig:select-pol-chem-def}
\end{figure}

To open a polymer chemistry definition, the user may either select one
that is already registered with the system, and that appears listed in
the drop-down list widget shown in
Figure~\vref{fig:select-pol-chem-def} or click the \guilabel{Cancel}
button so as to open one definition file by browsing the
filesystem. In the polymer chemistry definition window that shows up,
the user accomplishes two different tasks:

\begin{itemize}

\item Define the name of the polymer chemistry definition;\index{\xpd!name}

\item Define ``singular'' data like the left cap and the right cap of
  the polymer, the ionization rule governing the default ionization of
  the polymer
  sequence\index{\xpd!singular~data};

\item Define the atoms needed to operate the different polymer
  chemistry entities (these are ``plural''
  data)\index{\xpd!atoms} ;

\item Define all the polymer chemistry entities needed to work on
  polymer sequences (all these are also ``plural
  data'')\index{\xpd!plural~data} .

\end{itemize}


\noindent The definition of the atoms and of all the chemical entities
belonging to a given polymer chemistry are collectively called a
\textit{polymer chemistry definition}. The polymer chemistry
definition window that shows up is shown in
Figure~\vref{fig:xpertdef-pol-chem-def}.

\begin{figure}[h!]
  \begin{center}
    \includegraphics [width=0.75\textwidth]
    {figures/xpertdef-pol-chem-def.png}
  \end{center}
  \caption[\xpd\ polymer chemistry definition window]{\textbf{\xpd\
      polymer chemistry definition window.} All the polymer chemistry
    entities are defined in this window. The different buttons dealing
    with atoms, monomers, modifications, cross-linkers, cleavage and
    fragmentation specifications open up specific dialogs (see
    below).}
  \label{fig:xpertdef-pol-chem-def}
\end{figure}


\renewcommand{\sectitle}{The Atoms}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}
\index{\xpd!atoms}


The definition of the atoms is performed through the user interface
shown in Figure~\vref{fig:xpertdef-atoms} (\guilabel{Atoms} button in
the polymer chemistry definition window). In this dialog, the user
defines chemical elements (atoms) as entities made of isotopes (at
least one isotope per atom, logically).

\begin{figure}
  \begin{center}
    \includegraphics [width=0.75\textwidth]
    {figures/xpertdef-atoms.png}
  \end{center}
  \caption[\xpd\ atom definition]{\textbf{\xpd\ atom definition.}
    Each chemical element must contain at least one isotope, otherwise
    it does not have any \textit{``raison d'\^etre''}.}
  \label{fig:xpertdef-atoms}
\end{figure}


The design of this dialog window follows the general design for all
the dialog windows related to the definition of plural data in the
polymer chemistry definition. The leftmost list widget
(\guilabel{Atoms}) lists the final object as defined and available in
the polymer chemistry definition (in this case the atoms), while the
second list widget (\guilabel{Isotopes}) lists the objects that are
defined in order to actually make the selected object in the first
list widget (thus, atoms are made of isotopes). We see that two
isotopes were defined in order to create the \guivalue{Carbon} atom.

To add a new atom\index{\xpd!atoms!new~atom}, the user clicks the
\guilabel{Add} button below \guilabel{Atoms} list widget, which
triggers the insertion of a new row in the list widget. The
\guilabel{Details} groupbox on the right side of the dialog window now
shows \guivalue{Type name} as the name of the atom and \guivalue{Type
  symbol} as its symbol. The list of isotopes is empty, because we
still did not define any. First thing to do is to actually give the
atom a name and a symbol. The are no length limitations to any of the
new data, but a reasonable limit is 3 characters for the symbol, the
first being uppercase and all the remaining ones lowercase. Use only
alphabetic characters (that is [a-zA-Z]). Once these two data are set,
click on to the \guilabel{Apply} button; the list widget item will be
updated to reflect the new atom name.

To add a new isotope\index{\xpd!atoms!new~isotope}, first select the
atom to which it should be added. Click on the \guilabel{Add} button
below the \guilabel{Isotopes} list widget. A new item will be added to
the list widget with text \guivalue{0.0000000000}. Enter the
mass/abundance data in the \guilabel{Isotope} groupbox and click
\guilabel{Apply}. The corresponding item in the list widget will be
updated (the mass of the isotope is displayed in the list
widget). Each time a modification is performed in the list of isotopes
of a given atom, the monoisotopic and average
masses\index{\xpd!atoms!average~mass} are updated in the
\guilabel{Atom} groupbox. Recalculation of the average mass is
automatic as soon as something is modified in the list of isotopes.

Other buttons, like \guilabel{Move up} or \guilabel{Move down}, are
self-explanatory. Before moving on, please, validate the atom
definitions by clicking onto the \guilabel{Validate} button.


\renewcommand{\sectitle}{The Polymer Chemical Entities}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}

Once the atoms have been properly defined (note that such atoms are
already available in the distributed package), it is possible to start
entering data for the other polymer chemical entities. These are often
defined using chemical formulas, which explain why it is necessary to
first define the atoms.

The following are the data that need to be entered so as to obtain a
usable polymer chemistry definition: \medskip

\begin{itemize}

\item The polymer chemistry definition \guilabel{Name}\index{\xpd!name}
  \guivalue{protein-1-letter} Name of the polymer chemistry
  definition;

\item \guilabel {Caps}\index{\xpd!caps} Chemical capping reactions
  that should happen on the left end (\guilabel{Left}) and on the
  right end (\guilabel{Right}) of the polymer sequence:

  \begin{itemize}

  \item \guilabel{Left}\index{\xpd!caps!left} \guivalue{+H} Left
    capping of the polymer sequence;

  \item \guilabel{Right}\index{\xpd!caps!right} \guivalue{+OH} Right
    capping of the polymer sequence;

  \end{itemize}

\item Polymer \guilabel{Ionization rule}\index{\xpd!ionization~rule}
  This rule describes the manner in which the polymer sequence should
  be ionized by default, when the mass is calculated. This rule
  actually holds two elements:

  \begin{itemize} 

  \item \guilabel {Formula}\index{\xpd!ionization~rule!formula}
    \guivalue{+H} Chemical reaction that ionizes the polymer sequence.
    In the example, all the polymer sequences of polymer chemistry
    definition ``protein-1-letter'' are protonated by default;

  \item \guilabel {Charge}\index{\xpd!ionization~rule!charge}
    \guivalue{1} Charge that is brought by the chemical agent ionizing
    the polymer (the formula above). In the example, a protonation
    reaction brings a single positive charge.

  \item \guilabel {Level}\index{\xpd!ionization~rule!level}
    \guivalue{1} Number of times that the ionization must be performed
    by default on any polymer sequence of this chemistry
    definition. In this example, monoprotonation is set as the default
    ionization rule.

  \end{itemize}

\end{itemize}

At this point, time has come to deal with ``plural'' data.  The first
chemical entities to deal with are monomers.


\renewcommand{\sectitle}{The Monomers}
\subsection*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}
\label{sect:monomers}
\index{\xpd!monomers}

The monomers are the constitutive blocks of the polymer sequence.  In
the \mXp's jargon, ``monomer'' stands \emph{not} for the molecule that
may be used to perform a polymer synthesis; it stands for this
molecule \emph{less} the chemical group(s) that were eliminated upon
polymerization. If this concept is not familiar to the reader, it
might be useful to read chapter~\vref{chap:basics-polymer-chemistry}
for an overview of polymer chemistry.

Click onto the \guilabel{Monomers} button, which triggers the opening
of the dialog window shown in Figure~\ref{fig:xpertdef-monomers}.

\begin{figure}
  \begin{center}
    \includegraphics [width=0.75\textwidth]
    {figures/xpertdef-monomers.png}
  \end{center}
  \caption[\xpd\ monomers definition]{\textbf{\xpd\ monomers
      definition.}  Each monomer is defined using a name, a code and
    a chemical formula.}
  \label{fig:xpertdef-monomers}
\end{figure}

The way this dialog is operated is similar to what was described for
the atom, unless it is simpler, because monomers are non-deep objects:
there are no contained objects. One data element is critical: the
number of characters that might be used to define the code of the
element cannot be greater than the value entered in \guilabel{Code
  length} spinbox widget\footnote{Allowing more than one letter to
  craft monomer codes might seem trivial at first. But that design
  decision triggered the requirement for non-trivial algorithms
  throughout all the code of the of program. This is easily
  understandable at least in the polymer sequence editor: how are
  monomer codes keyed-in if `A' and ``Ala'' are valid monomer codes in
  a polymer chemistry definition? The magic is described in the
  chapter about \xpe\ (see chapter~\vref{chap:xpertedit}).}. The
fundamental rule is the following:\medskip

\begin{center}
  \fbox{\parbox{0.9\textwidth}{\textsl {``The first character of a
        monomer code must be uppercase, while the remaining characters
        (if any) must be lowercase.''} That means that---if
      \guilabel{Code length} is \guivalue{3}---`A', ``Al'', ``Ala''
      would be perfectly fine, while ``Alan'', ``AL'', `a', ``AlA''
      would be wrong.}}
\end{center} 

\noindent Each time a formula is either displayed by selecting a new
monomer in the list or modified by editing it in its line edit widget,
the monoisotopic and average masses are recalculated.

As of version 2.3.5, it is possible to calculate the mass difference
between any two monomers in the definition. This is useful, for
example, to grasp the resolution and mass accuracy requirements for a
given polymer definition. The user sets a threshold to filter the
results. The results of such a calculation are displayed in
Figure~\ref{fig:xpertdef-monomers-mass-differences}.

\begin{figure}
  \begin{center}
    \includegraphics [width=0.6\textwidth]
    {figures/xpertdef-monomers-mass-differences.png}
  \end{center}
  \caption[\xpd\ monomer mass differences]{\textbf{\xpd\ monomer mass
      differences.}  The mass difference between any two monomers in
    the definition is computed and displayed only if it is less or
    equal to a threshold (see Figure~\ref{fig:xpertdef-monomers}).}
  \label{fig:xpertdef-monomers-mass-differences}
\end{figure}


After addition of the monomers it is always a good idea to validate
them by clicking onto the \guilabel{Validate} button.


\renewcommand{\sectitle}{The Modifications}
\subsection*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}
\label{sect:modifications}
\index{\xpd!modifications}

Polymer are often either chemically or biochemically modified. In
nature, biopolymers are modified more often than not. One of the more
common modifications in the protein reign are phosphorylation or
acetylation. Nucleic acids are modified with a sheer number of
chemical modifications, saccharides also. The \mXp\ software provides
entire freedom to define any number of intelligent modifications, that
is modifications with any chemical formula but also that are
knowledgeable of what monomers they can modifiy. Indeed, it would make
no sense to phosphorylate a glycyl residue in a protein, for example.

\begin{figure}
  \begin{center}
    \includegraphics [scale=0.8]
    {figures/xpertdef-modifications.png}
  \end{center}
  \caption[\xpd\ modifications definition]{\textbf{\xpd\ modifications
      definition.}  Each modification is defined using a name, a
    targets specification and a chemical formula.}
  \label{fig:xpertdef-modifications}
\end{figure}

Click onto the \guilabel{Modifications} button, which triggers the
opening of the dialog window shown in
Figure~\ref{fig:xpertdef-modifications}. In the example shown, the
\emph{Phosphorylation} modification is being defined. A modification
is defined by a \guilabel{Name}\index{\xpd!modifications!name}, a list
of monomer codes that might be modified by this modification:
\guilabel{Targets}\index{\xpd!modifications!targets}\footnote{A
  \guilabel{Targets} datum is made of monomer codes separated by `;'
  separators.}, a \guilabel{Max. count} describing the maximum number
of times that modification can be applied to the target
monomers\footnote{This feature is essential when working on
  methylation of proteins, for example, with arginyl and lysyl
  residues being multi-methylated.} , and finally a
\guilabel{Formula}\index{\xpd!modifications!formula}. The formula is
actually a chemical reaction, as explained in
section~\ref{sect:formulae-and-chemical-reactions},
chapter~\ref{chap:massxpert-generalities},
page~\pageref{chap:massxpert-generalities}. The \emph{Phosphorylation}
reaction can thus be read like this: ---\textsl{``The polymer looses a
  proton and gains H2PO3''}.  The \emph{Phosphorylation} is being
defined as having \guivalue{S;T;Y} targets only, that means that when
the user will try to modify non-seryl or non-threonyl or non-tyrosinyl
monomers, the program will complain that these monomers are not
targets of \emph{Phosphorylation}.  There is, however, and for maximum
flexibility, the possibility to override these target-limiting data
when modifying monomers. When the polymer is modified with this
modification, its masses will change according to the net mass of this
\emph{Phosphorylation} ``reaction''.


\renewcommand{\sectitle}{The Cross-linkers}
\subsection*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}
\label{sect:cross-linkers}
\index{\xpd!cross-linkers}

Polymers are often either chemically or biochemically modified by
interconnecting monomers from the same polymer sequence. In the
protein reign, one classical example of intra-sequence cross-linking
is the formation of disulfide bonds\index{disulfide~bond}. Another
wonderful example is the formation of the fluorophore in the
fluorescent proteins\index{fluorescent~protein}: there is a chemical
reaction involving the side chains of three consecutive residues going
on, resulting in the formation of a complex intra-sequence
cross-link. Each side chain of the three monomers involved are
chemically modified.

Cross-linkers are defined\index{\xpd!cross-linkers!definition} in the
dialog window shown in Figure~\ref{fig:xpertdef-crosslinkers}. This
dialog window is opened by clicking onto the \guilabel{CrossLinkers}
button.

\begin{figure}
  \begin{center}
    \includegraphics [scale=0.8]
    {figures/xpertdef-crosslinkers.png}
  \end{center}
  \caption[\xpd\ cross-linkers definition]{\textbf{\xpd\ cross-linkers
      definition.}  Each cross-linker is defined using a name, a
    formula and either no modification or as many modifications as
    there are monomers involved in the formation of the cross-link.}
  \label{fig:xpertdef-crosslinkers}
\end{figure}

The formation of cross-link between one or more monomers often
involves chemical reactions to occur at the level of the engaged
monomers. Cross-linkers defined in \mXp should refer to these
modifications as modification objects already available in the polymer
chemistry definition. Note that, in some cases, it is not necessary to
define modifications to occur at the level of the cross-linked
monomers. The example described in
Figure~\vref{fig:xpertdef-crosslinkers}, corresponds to the
cross-linking reaction involved in the formation of the chromophore of
the cyan fluorescent protein. That reaction involves the three
following monomers: $\rm ^{65}$\,Threonyl, $\rm ^{66}$\,Tyrosinyl,
$\rm ^{67}$\,Glycyl. Each monomer undergoes a distinct chemical
modification: ``-0'', ``-H3'' and ``-H'', respectively. Three
modifications were thus defined: \guivalue{Chromo-0},
\guivalue{Chromo-H3} and \guivalue{Chromo-H}, in that specific order,
as these modifications are going to be sequentially applied to their
corresponding monomer in the cross-linking reaction.

Note that the formula of the \guivalue{CFP-Chromophore} cross-linker
is \guivalue{+Nul}, that is there is no chemical reaction defined for
the cross-linker \textit{per se}. When modifications are defined,
their number must match the number of monomers involved, and their
order must match the order with which the monomers are
cross-linked. If no modification is defined, then, the chemical
reaction that occurs upon cross-linking might be defined in the
formula of the cross-linker.


\renewcommand{\sectitle}{The Cleavage Specifications}
\subsection*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}
\label{sect:cleavespecif}
\index{\xpd!cleavage~specifications}

It is common practice---in biopolymer chemistry, at least---to cut a
polymer into pieces using molecular scissors like the following:
\smallskip

\begin{figure}
  \begin{center}
    \includegraphics [width=1\textwidth]
    {figures/xpertdef-cleavages.png}
  \end{center}
  \caption[\xpd\ cleavage specifications definition]{\textbf{\xpd\
      cleavage specifications definition.}  Each cleavage
    specification is defined using a name, a cleavage pattern and any
    number of cleavage rules.}
  \label{fig:xpertdef-cleavages}
\end{figure}


\begin{itemize}

\item proteases, for proteins;

\item nucleases, for nucleic acids;

\item glycosidases, for saccharides\dots

\end{itemize}

\noindent For each different polymer type, the molecular scissors are
specific.  Indeed, a protease will not cleave a polysaccharide. This
is why cleavage specifications belong to polymer chemistry
definitions. In the example of Figure~\vref{fig:xpertdef-cleavages},
the definition\index{\xpd!cleavage~specifications!definition} of the
\guivalue{CyanogenBromide} cleavage specification is detailed (this
organic reagent cleaves right of methionyl residues). The
\guivalue{CyanogenBromide} cleavage specification is qualified as so:
\smallskip

\begin{itemize}

\item \guilabel{Name}\index{\xpd!cleavage~specifications!name}
  \guivalue{CyanogenBromide} Name of the cleavage agent;

\item \guilabel{Pattern}\index{\xpd!cleavage~specifications!pattern}
  \guivalue{M/} Sequence specificity of the cleavage agent. In this
  case, the cleavage agent cleaves the protein right after
  \guivalue{M}ethionyl residues;

\item \guilabel{Cleavage
    rule}\index{\xpd!cleavage~specifications!cleavage~rule} This
  groupbox allows the definition of the cleavage rules that might be
  added to the cleavage specification: \smallskip

  \begin{itemize}
  \item \guilabel{Left
      Code}\index{\xpd!cleavage~specifications!left~code}
    and \guilabel{Left Formula} (Empty) This is a special case for
    those cleavage agents that not only cut a polymer sequence
    (usually it is a hydrolysis) but that also modify the substrate in
    such a way that must be taken into account by \mXp\ so that it
    computes correct molecular masses for the resulting
    oligomers. These rules are optional. However, if \guilabel{Left
      Code} is filled with something, then it is compulsory that
    \guilabel {Left Formula} be filled with something valid also, and
    conversely;
    
  \item \guilabel{Right
      Code}\index{\xpd!cleavage~specifications!right~code}
    and \guilabel {Right Formula} \guivalue{M} and
    \guivalue{-CH2S+O3}, respectively. Same explanation as above.
    This cleavage rule stipulates that upon cleavage of a protein
    using cyanogen bromide, the methionyl residue that gets
    effectively cleaved must be converted to a homoseryl residue. See
    below for a detailed explanation.
    
  \end{itemize}

\end{itemize}

\noindent Here are some examples of more complex cleavage patterns:
\smallskip

\begin{itemize}

\item \textbf{Trypsin = K/;R/;-K/P} \textsl {``Trypsin cuts right of a
    `K' and right of a `R'. But it does not cut right of a `K' if this K
    is immediately followed by a P''};

\item \textbf{EndoAspN = /D} \textsl {``EndoAspN cuts left of a D''};

\item \textbf{Hypothetical = T/YS; PGT/HYT; /MNOP; -K/MNOP} \textsl
  {``Hypothetical cuts after `T' if it is followed by YS and also cuts
    after `T' if preceded by PG and followed by HYT. Also, Hypothetical
    cuts prior to `M' if `M' is followed by NOP and if `M' is not preceded
    by K''}.

\end{itemize}
%
Please, \emph{do} note that the letters in the examples
above correspond to monomer codes and \emph{not} to monomer names. If,
for example, we were defining a ``Trypsin'' cleavage specification
pattern---in a protein polymer chemistry definition with the standard
3-character monomer codes---we would have defined it this way:
``Trypsin = Lys/;Arg/;-Lys/Pro''.

Now comes the time to explain in more detail what the \guilabel{Left
  Code} and \guilabel {Left Formula} (along with the \guilabel {Right}
siblings) are for. For this, we shall consider that we have the
following polymer sequence (1-character monomer codes):
\textsc{thismwillmbecutmandthatmalso}. If that sequence had been
cleaved using ``CyanogenBromide'' and if the cleavage had been
total,\footnote{Cleavage occurs at every possible position, right of
  each monomer `M'.} that would have generated the following
oligomers: \textsc{thism willm becutm andthatm also}. But if there had
been partial cleavages, one or more of the following oligomers would
have been generated: \textsc{thismwillm becutmandthatm also
  willmbecutm andthatmalso} and so on\dots

Now, the biochemist knows that when a protein is cleaved with cyanogen
bromide\index{cyanogen~bromide}, the cleavage occurs effectively right
of monomer `M' (this we also know already) \emph{and} the `M' monomer
that underwent the cleavage is changed from a methionyl residue to an
homoseryl residue (this chemical change involves this formula:
``-CH2S+O''). Amongst all the oligomers generated above, there are two
oligomers that should not undergo the cleavage rule ``-CH2S+O'':
\textsc{also} and \textsc{andthatmalso}. Indeed, these two oligomers
were generated by the ``CyanogenBromide'' cleavage, but were not
actually cleaved at the right side of a methionyl residue, because
they correspond the the right end terminal part of the protein
sequence (even if one them does contain a `M' residue; the cleavage
did not \emph{occur} at that residue).

This example should clarify why the definition clearly stipulates---in
the cleavage specification for ``CyanogenBromide''---that the
oligomers resulting from this cleavage should \textsl{``undergo the
  `-CH2S+O' formula only if they have a `M' as their right end monomer
  code''}. These \emph{cleavage rule}s need to be defined in a very
careful way: imagine that---in some cyanogen bromide
experiments---that reagent would cleave right of `C' (cysteine)
residues, but with no chemical modification of the `C'
monomer.\footnote {This is a purely hypothetical situation that I
  never observed personally.} In this case, it would be suitable to
put the flexibility of \mXp\ at work by specifying that the generated
oligomers should ``undergo the `-CH2S+O' formula'' only if they have a
`M' as their right end monomer, so that `C'-terminated oligomers are
not chemically modified. Thus the cleavage pattern might be safely
defined: ``M/;C/''\dots\


\renewcommand{\sectitle}{The Fragmentation Specifications}
\subsection*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}
\label{sect:fragspecif}
\index{\xpd!fragmentation~specifications}

As previously discussed (chapter~\ref{chap:basics-polymer-chemistry},
section~\vref{sect:polymer-fragmentation}), specifying the
fragmentation specifications of a polymer chemistry definition is not
a trivial task. In this section three different cases will be
described, from simple to more complex. One major rule is that the
fragmentation specification should be crafted in such a manner that
the resulting fragment is neutral. The ionization of the fragments
will be then automatically performed by \mXp upon calculation of the
ion products according to the current ionization rule. This is a major
improvement over previous versions, that forced the user to define
fragmentation specifications by assuming a product ion of a given
ionization ([M+H]$^+$ for proteins or ([M-H]$^-$ for nucleic acids,
for example).

\subsubsection*{Simple fragmentation patterns}
\index{\xpd!fragmentation~specifications!simple patterns}

One simple example of polymer chain fragmentation is the formation of
\emph{a} fragments with a nucleic acid (DNA, in this example). The
fragments obtained by \emph{a}-type fragmentation are described in
Figure~\vref{fig:dna-fragmentation}. Bond cleavage occurs right before
the sugar-carbon-linked oxygen of the phosphoester bond linking one
deoxyribonucleotide to the next. Thus, the molecular weight of the
fragment corresponds, as illustrated, to the sum of the monomer masses
from the left end of the polymer up to and including the monomer being
decomposed \emph{less} one oxygen. Note that this specification yields
a nucleic acid ion product that is protonated.  We thus need to remove
a proton to change its charge to 0, thus the formula of the \emph{a}
fragmentation pattern is ``-OH''. Therefore, the definition of the
\emph{a} DNA fragmentation pattern is as described in
Figure~\vref{fig:xpertdef-fragmentations-simplest}, were wee see that
the \guilabel{Name} of the fragmentation specification for \emph{a}
fragments is \guivalue{a}, that the \guilabel{Formula} is
\guivalue{-OH}, that the fragments encompass the \guivalue{LE} (for
``left end'') \guilabel{End} of the polymer chain. The \guilabel{Side
  chain} value is set to \guivalue{0}, which will be explained later.

\begin{figure}
  \begin{center}
    \includegraphics [width=0.8\textwidth]
    {figures/xpertdef-fragmentations-simplest.png}
  \end{center}
  \caption[\xpd\ fragmentation rules definition]{\textbf{\xpd\
      fragmentation rules definition.}  Each fragmentation rule is
    defined using a name, a formula and a local logic, that is a set
    of logial conditions which must be verified for the fragmentation
    rule to be applied to the fragment.}
  \label{fig:xpertdef-fragmentations-simplest}
\end{figure}

\subsubsection*{More complex fragmentation patterns}
\index{\xpd!fragmentation~specifications!complex patterns}

In nucleic acids gas-phase chemistry, it often happens that not only
fragmentation occurs at the level of the phospho-ribose skeleton, but
also at the level of the nucleic base. These fragmentation patterns
are called abasic patterns. The decomposition of the base occurs at
the monomer position where the fragmentation occurs. For example, if a
``ATGC'' oligonucleotide is fragmented according to pattern \emph{a}
but with nucleic base decomposition, and that fragmentation occurs at
position 1, then the computation of the mass should occur like
represented in
Figure~\vref{fig:xpertdef-fragmentations-special-dna-a-base-case}. This
figure illustrates a number of things, amongst which some known
basics. The panel on the top right hand side shows the constituents of
the DNA polymer chemistry definition: the caps are OH on the left end
and H on the right end; the circled formula is the skeleton (also
called backbone) and the base attached to the deoxyribose ring
singularizes the nucleotide. That base might be adenine, guanine,
cytosine, thymine. In the ``dna'' polymer chemistry definition, the
monomers are made of the skeleton (formula C5H8O5P) plus the formula
of the base, which is understandable.

\paragraph*{Using a generalizable specification}

Now, if we want to compute the mass of the a-B\#1 fragment, that is
fragmentation occurs according to pattern \emph{a} right after the `A'
monomer \emph{plus} decomposition of the base (in our case this is an
Adenine). Note that the decomposition of the base is accompanied by
the formation of an insaturation on the sugar moiety of which the net
formula is -H. We thus have to: \\

\begin{itemize}

\item Apply an adapted specification for \emph{a} fragments: removal
  of the H due to the insaturation on the sugar and also removal of
  the OH related to the formation of an \emph{a} fragment ; see the
  \guivalue{-HOH} component of the formula);
\item Remove one \emph{full monomer} with \guilabel{Side chain} set to
  \guivalue{-1} (this equals to the removal of both the skeleton and
  the side chain---the adenine, here);
\item Add back the skeleton (the \guivalue{+C5H8O5P} component of the
  formula);
\item As for \emph{a} fragments, the end of the polymer sequence that
  gets included in the fragment is the \guivalue{LE} (``left end''),
  which will be transformed into a +OH formula, since this is the
  formula of the left end cap for nucleic acids.

\end{itemize}

\begin{figure}
  \begin{center}
    \includegraphics [width=1\textwidth]
    {figures/xpertdef-fragmentations-special-dna-a-base-case.png}
  \end{center}
  \caption[\xpd\ fragmentation specifications
  definition]{\textbf{\xpd\ fragmentation specifications definition.}
    Each fragmentation specification is defined using a name, a
    formula, the fragmented monomer side chain contribution, the end
    of the polymer that is contained in the fragment and any number of
    fragmentation rules. Note that the examples yiel neutral fragment
    ions, the ionization is performed by the \mXp{} program by
    applying the ionization rule currently in use.
}
  \label{fig:xpertdef-fragmentations-special-dna-a-base-case}
\end{figure}

\noindent The advantage of working this way is that we need not
specify a fragmentation rule for each different monomer in the
sequence (see below, for how this might be done). Indeed, by
specifying \guilabel{Side chain} to be \guivalue{-1}, we
indicate---without knowing the monomer identity---to the mass
calculation engine that once the fragmentation has occurred in the
polymer chain, the mass of the monomer that got fragmented should be
subtracted from the fragment mass. That subtraction removes, however
too much material, as we do not want to loose the skeleton, we only
want to loose the base (adenine, in our example). This is why we ask
in the fragmentation specification formula that the skeleton be added
(the \guivalue{+C5H8O5P} component of the formula). Because the
skeleton does not change along the polymer chain, even if the base
itself changes, this computation method is generalizable, and because
of this the polymer chemistry definition works.

This whole process of defining a fragmentation pattern that needs to
``know'' what monomer is being fragmented so as to compute the
fragment masses correctly, can be performed by using fragmentation
rules. This is described below.

\paragraph*{Using a monomer-specific specification}

Another way of achieving what was described above is by using
fragmentation rules, whereby the fragment's mass computation is made
conditional to one or more conditions that should be
verified. Figure~\vref{fig:xpertdef-fragmentations-frag-rules} shows
how the \emph{a-B} fragmentation pattern might be defined using
fragmentation rules.

\begin{figure}
  \begin{center}
    \includegraphics [width=0.8\textwidth]
    {figures/xpertdef-fragmentations-frag-rules.png}
  \end{center}
  \caption[\xpd\ fragmentation rules definition]{\textbf{\xpd\
      fragmentation rules definition.}  Each fragmentation rule is
    defined using a name, a formula and a local logic, that is a set
    of logical conditions which must be verified for the fragmentation
    rule to be applied to the fragment.}
  \label{fig:xpertdef-fragmentations-frag-rules}
\end{figure}

\noindent The \emph{a-B} fragmentation specification comprises 4
rules, one rule for each available monomer in the ``dna'' polymer
chemistry definition: `A', `T', `G' and `C'. The figures illustrates
the definition of the fragmentation specification \emph{a-B} which
stipulates that the mass of the fragment should be computed this
way: \par

\begin{itemize}

\item For the fragmentation specification part, everything is like for
  fragments of type \emph{a}, that is, the formula is merely
  \guivalue{-OHH} and the end is \guivalue{LE} (see above, for
  explanations);
  
\item But there is one rule (\emph{a-B-c}) which adds some
  \guilabel{Local logic} for the fragmentation specification: the
  formula \guivalue{-C4H4O3N} should be applied upon calculation of
  the fragment's masses if the monomer at which the fragmentation
  actually occurs is of \guilabel{Curr code} \guivalue{C}, that is if
  it is a Cytosine. The \guivalue{-C4H4O3N} formula is the formula of
  Cytosine (the base, not the monomer).

\item The other rules (for \guilabel{Curr code} \guivalue{A},
  \guivalue{T} and \guivalue{G} are identical to the \emph{a-B-c} one
  unless the \guilabel{Curr code} is `A', `T' or `G' and the formula
  to be removed is the formula of the corresponding DNA base.

\end{itemize}


\noindent The fragmentation rule-based definition of fragmentation
pattern \emph{a-B} yields identical results as for the more
generalizable method described earlier.


\subsubsection*{Even more complex fragmentation patterns}

Note that in saccharide chemistry, the fragmentation patterns are
extremely complex, and often totally depend on the nature of the
monomers local to the fragmentation site. For example, the
fragmentation behaviour at position `E' in a sequence ``DEAR'' might
be different than in a sequence ``DERA''. \mXp\ had to be able to
model these complex situations, and this is done using fragmentation
rules where the local logic involves defining the \guilabel{Prev code}
and/or the \guilabel{Next code} for a given \guilabel{Curr code} at
which the fragmentation occurs. For example, one specific
fragmentation pattern for fragmentation at `E' in sequence ``DEAR''
might be defined this way: \\

\begin{itemize}

  \item \guilabel{Prev code}: \guivalue{D};

  \item \guilabel{Curr code}: \guivalue{E};

  \item \guilabel{Next code}: \guivalue{A}.

\end{itemize}

\noindent In stead of that fragmentation rule, one would have for
fragmentation at `E' in sequence ``DERA'' the following rule: \par 

\begin{itemize}

  \item \guilabel{Prev code}: \guivalue{D};

  \item \guilabel{Curr code}: \guivalue{E};

  \item \guilabel{Next code}: \guivalue{R}.

\end{itemize}

\noindent Note the change for \guilabel{Next code}, from \guivalue{A}
to \guivalue{R}. Also, be aware that the ``Prev'', ``Curr'' and
``Next'' notions are polar, that is, they depend on the value of
\guilabel{End} (that is \guivalue{LE} or \guivalue{RE}). For example,
if we wanted to model the fragmentation pattern at `E' for a fragment
of \guilabel{End} \guivalue{RE}, similar to what was done above with
sequences ``DEAR'' and ``DERA'', we would have set the local logical
like this: \\

\noindent \textbf{For sequence ``DEAR'':}\par

\begin{itemize}

  \item \guilabel{Prev code}: \guivalue{A};

  \item \guilabel{Curr code}: \guivalue{E};

  \item \guilabel{Next code}: \guivalue{D}.

\end{itemize}

\noindent \textbf{For sequence ``DERA'':}\par

\begin{itemize}
  
  \item \guilabel{Prev code}: \guivalue{R};

  \item \guilabel{Curr code}: \guivalue{E};

  \item \guilabel{Next code}: \guivalue{D}.

\end{itemize}


\noindent This highly flexible fragmentation specification allows for
definition of highly complex fragmentation behaviours of biopolymers.



\renewcommand{\sectitle}{Saving The Definition}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}
\label{sect:save-polymer-chemistry-definition}
\index{\xpd!saving the definition}

Once the polymer chemistry definition is completed, the user can save
it to an \fileformat {XML} file. Prior to actually writing to the
file, the program checks the validity of all the chemical entities in
the definition.  This check can be triggered manually by clicking onto
the \guilabel{Validate} button. If an error is found, it is reported
so that the user may identify the problem and fix it.

The location where the file should be saved, and the manner that it
may be made available to \mXp\ is to be described in a later chapter.
It is, in fact, very important that \mXp\ knows where to find newly
defined polymer chemistries so as to be able to use them when
sequences of that polymer chemistry are created or used.
\index{\xpd|)}

\cleardoublepage

