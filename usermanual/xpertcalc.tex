\chapter[\xpc] {\xpc: A Powerful Mass Calculator}
\label{chap:xpertcalc}
\index{\xpc|(}

After having completed this chapter you will be able to perform
sophisticated polymer chemistry-aware mass calculations.

\renewcommand{\sectitle}{\xpc\ Invocation}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle} 
\index{\xpc!module-invocation}

The \xpc\ module is easily called by pulling down the \guimenu{\xpc}
menu item from the \mXp\ program's menu. The user is presented with a
window to select the polymer chemistry definition that should be used
for the calculations (Figure~\ref{fig:xpertcalc-select-pol-chem-def}).

\begin{figure}
  \begin{center}
    \includegraphics [width=0.75\textwidth]
    {figures/xpertcalc-select-pol-chem-def.png}
  \end{center}
  \caption[Selecting a polymer chemistry definition for use with
  \xpc]{\textbf{Selecting a polymer chemistry definition for use with
      \xpc.} This figure shows that the user can either select one
    already registered polymer chemistry definition (listed in the
    drop-down widget) or browse the filesystem to select one polymer
    chemistry definition file.  Choosing a polymer chemistry
    definition allows to take advantage of all the chemical entities
    defined therein during the mass calculations.}
  \label{fig:xpertcalc-select-pol-chem-def}
\end{figure}



\renewcommand{\sectitle}{An Easy Operation}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}

Once the polymer chemistry definition has been correctly selected, it
is parsed by the \xpc\ module and its entities are automatically made
available in the calculator window, as shown in
Figure~\ref{fig:xpertcalc-protein-1-letter}. The way \xpc\ is operated
is very easy. This is partly due to the very self-explanatory
graphical user interface of the module, which is illustrated in
Figure~\ref{fig:xpertcalc-protein-1-letter}.  \xpc\ can handle a
number of items that are reviewed below:

\begin{itemize}

\item \guilabel {Seed Masses}\index{\xpc!seed-masses} The user may (is not obliged to) seed
  the calculation by setting masses manually in these line~edit
  widgets (the left line~edit is for \guilabel{mono} and the right one
  for \guilabel{avg}; both monoisotopic and average \mz values need to
  be entered).  For example, imagine that a mass spectrum analysis
  session ends up like this: ---\textsl{``There is a peak with \mz
    1000.55, z=1 and another one roughly 80~Da more. Is it possible
    that the analyte showing up at \mz 1000.55 is phopshorylated?''}
  The massist would seed the calculator with mass~\guivalue{1000.55}
  and ask that one \guivalue{Phosphorylation} modification be added to
  it by setting \guivalue{1} in front of the corresponding drop-down
  widget.  Clicking onto \guilabel{Apply} triggers the calculation,
  with the resulting masses being displayed in the \guilabel{Result
    Masses} line~edit widgets. We can see that the phosphorylation of
  our analyte shifts its \mz value from \guivalue{1000.55} to
  \guivalue{1080.5163}.

  Note that each time a calculation is triggered by clicking onto
  \guilabel{Apply}, the values already present in the \guilabel{Result
    Masses} line~edit widgets are transferred to the \guilabel{Seed
    Masses} line~edit widgets. This provides a 1-level undo;

\item \guilabel{Result Masses}\index{\xpc!result-masses} Each time a
  calculation is triggered by clicking the \guilabel{Apply} button (or
  the chemical pad's buttons; see below), the newly obtained masses
  are displayed in these line~edit widgets. The values that were
  displayed there previously are transferred to the \guilabel{Seed
    Masses} line~edit widgets, thus providing a 1-level undo.

\item \guilabel {Formula} This group~box widget contains two widgets:
  a line~edit widget where the formula is typed and a count spin~box
  widget where the user sets the number of times that the formula
  should be applied. Setting the formula to \guivalue{H2O} and the
  count to \guivalue{2} would hydrate the analyte twice.

\item \guilabel {Polymer Chemistry Definition
    Entities}\index{\xpc!chemical-entities} This group~box widget
  contains two drop-down widgets and a line~edit widget. The drop-down
  widget on the left lists all the monomers defined in the
  \guivalue{protein-1-letter} polymer chemistry definition; the
  drop-down widget on the right lists all the modifications defined in
  the \guivalue{protein-1-letter} polymer chemistry definition.  Each
  drop-down widget has its corresponding count spin~box widget.  In
  the example, the user asked that one (\guivalue{1})
  \guivalue{Phosphorylation} modification be applied during the
  calculation. The line~edit widget below the first row of widgets is
  the polymer sequence widget where the user might enter a sequence of
  monomers. It is possible to apply many times the sequence by setting
  the count spin~box widget value to something greater than 1 (either
  positive or negative);

\end {itemize}
%
It is possible to perform a set of calculations in one go,
that is, the user may ask for a formula, a monomer, a modification, a
sequence to be accounted in one single calculation operation. Once all
the chemical entities to be taken into account have been set, the user
clicks onto the \guilabel{Apply} button: all the entities are
parsed in sequence and their mass equivalent are added to the result
masses. Other
prominent features of \xpc\ are described in the following sections.

\begin{figure}
  \begin{center}
    \includegraphics [width=0.8\textwidth]
    {figures/xpertcalc-protein-1-letter.png}
  \end{center}
  \caption[Interface of the \xpc\ module]{\textbf{Interface of the
      \xpc\ module.} This figure shows that the \xpc\ polymer chemistry
    definition-aware module can handle atoms, formul{\ae}, monomers,
    modifications and even polymer sequences for computing masses.}
  \label{fig:xpertcalc-protein-1-letter}
\end{figure}


\renewcommand{\sectitle}{The Programmable Calculator}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle} 
\index{\xpc!programming}

For the scientists who work on molecules that are often modified in
the same usual ways, \xpc\ features a built-in mechanism by which they
can easily program their calculator. This programming involves the
definition of how a \emph{chemical pad} (or \emph{chempad}) may be
arranged, exactly the same way as a desktop calculator would display
its numerical keypad.

\begin{figure}
  \begin{center}
    \includegraphics [width=0.66\textwidth]
    {figures/xpertcalc-protein-1-letter-chempad.png}
  \end{center}
  \caption[Interface of the chemical pad]{\textbf{Interface of the
      chemical pad.} This figure shows that the chemical pad is very
    similar to what a numerical calculator would display. Here, the
    user has programmed a number of chemical reactions.} 
  \label{fig:xpertcalc-protein-1-letter-chempad}
\end{figure}

The chemical pad can be shown/hidden by using the \guilabel{Show
  Chemical Pad} check~box widget. An example of such a chemical pad is
shown in Figure~\ref{fig:xpertcalc-protein-1-letter-chempad}, where a
``protein-1-letter'' polymer chemistry definition-associated chempad
is featured. As shown, the user has programmed a number of chemical
reactions that may be applied to the masses in the \xpc\ calculator
window by simply clicking on their respective button (see
Figure~\ref{fig:xpertcalc-protein-1-letter-chempad}). The
configuration of the chemical pad is very easy, as shown in the code
below:

{\small
\begin{alltt}
chempad_columns\$3

color\%aliceblue\%240,248,255
color\%antiquewhite\%250,235,215
color\%aqua\%0,255,255

chempadgroup\%Generic

chempadkey=protonate\%+H1\%adds a proton
chempadkey=hydrate\%+H2O1\%adds a water molecule
chempadkey=0H-ylate\%+O1H1\%adds an hydroxyl group
chempadkey=acetylate\%-H1+C2H3O1\%adds an acetyl group
chempadkey=phosphorylate\%-H+H2PO3\%add a phosphate group
chempadkey=sulfide bond\%-H2\%oxydizes with loss of hydrogen

chempadgroup\%Hexoses \&\& Fucose\%[midnightblue]

chempadkey\%Res-1bRE-Hexose\%C6H11O6\%residue Hexose (1bRE)\%[lawngreen,black]
chempadkey\%Res-1bRE-Hexalditol\%C6H12O6\%residue Hexalditol (1bRE-ol)\%[lawngreen,black]
\end{alltt}
}
%
What this text file says is very simple:
\begin {itemize}
\item That the buttons should be arranged in rows of three columns;
\item That colors might be defined using the RGB paradigm (3 numerical
  values in the range [0--255] to represent the intensity of the three
  red, green and blue primary colors. The colors might be later used
  to colorize some widgets;
\item That buttons following the line \texttt{chempadgroup\%Generic}
  (and until another such line) will be grouped into a groupbox widget
  entitled ``Generic''. Note that a coloring specification might be
  optionally appended like the following:
  \texttt{chempadgroup\%Hexoses \%\&\& Fucose\%[midnightblue]}, in which
  case the background of the section groupbox will be colored;
\item That buttons are simply defined according to the following
  '\%'-delimited line syntax:
  \texttt{chempadkey\%Res-1bRE-Hexose\%C6H11O6\%residue Hexose
    (1bRE)\%[lawngreen,black]}. The first part is the simple delimitor
  \texttt{chempadkey}. The second part (\texttt{Res-1bRE-Hexose}) is
  the text string that will label the button in the chemical pad. The
  third part (\texttt{C6H11O6}) is the formula that will be applied in
  the calculator whenever that button is clicked. The fourth part
  (\texttt{residue Hexose (1bRE)}) is the text string that will be
  displayed in a tooltip whenever the cursor remains over the
  button. Finally the color specification (\texttt{[lawngreen,black]})
  instruct that the background of the button should be of the
  lawngreen color, while the text label should be of the black color.
\end {itemize}
%
These buttons might be used in two distinct ways:

\begin{itemize}

\item Upon clicking the button, its formula is evaluated
  and the corresponding masses are added to (or subtracted from) the
  \guilabel{Result Masses};

\item Upon simultaneous clicking the button and keeping the
  \kbdKey{Ctrl} key pressed, its formula is inserted into the
  \guilabel{Formula} line edit widget. In this case, the formula is
  not evaluated and the \guilabel{Result Masses} are not modified.

\end{itemize}
%
The last feature (insertion only of the formula in the
\guilabel{Formula} line edit widget) is of particular use when
computing masses of complex polymers for which one might desire to
have the full chemical formula stored before clicking the
\guilabel{Apply} button. In the author's experience this feature is
most convenient when calculating masses of complex branched glycans.

 

\renewcommand{\sectitle}{The LogBook Recorder}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle} 
\index{\xpc!recorder}

Each time an action that is chemically relevant---from a molecular
mass point of view---is performed, the program dumps the calculations
to the \xpc\ recorder window
(Figure~\ref{fig:xpertcalc-protein-1-letter-recorder}). The recorder
can be shown/hidden by using the \guilabel{Show Recorder} check~box
widget. The text in the recorder window is editable for the user to
edit the \xpc\ output, and selectable also, so that pasting to text
editors or word processors is easy \textit{via} the clipboard.

\begin{figure}
  \begin{center}
    \includegraphics [width=0.66\textwidth]
    {figures/xpertcalc-protein-1-letter-recorder.png}
  \end{center}
  \caption[The \xpc\ recorder window]{\textbf{The \xpc\ recorder
      window.} This figure shows that the recorder window is a simple
    text~edit widget that records all the mass-significant operations
    in the \xpc\ calculator. The text in the recorder may be selected
    and later used in an electronic logbook or printed.}
  \label{fig:xpertcalc-protein-1-letter-recorder}
\end{figure}



\renewcommand{\sectitle}{The \mz Ratio Calculator}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle} 
\label{sect:xpertcalc-mz-ratio-calculator}
\index{\xpc!m/z calculator}
\index{m/z calculator}

It very often happens that the massist doing electrospray analyzes is
faced with a challenging task: to compute by mind all the \mz ratios
for a given family of charge peaks. To ease that daunting task, \xpc\
contains a \mz ratio calculator that is called by clicking onto the
\guilabel{m/z Calculation} button. This action pops up a window that
is shown in Figure~\ref{fig:xpertcalc-mz-calculator}.


\begin{figure}
  \begin{center}
    \includegraphics [width=0.75\textwidth]
    {figures/xpertcalc-mz-calculator.png}
  \end{center}
  \caption[The \mz ratio calculator]{\textbf{The \mz ratio
      calculator.} The \mz calculator is rather straight forward to
    use. Given some initial parameters, the results are displayed in
    the \guilabel{Ion Charge Family} treeview widget.}
  \label{fig:xpertcalc-mz-calculator}
\end{figure}


In order to compute the \mz ratios requested by the user, the program
needs to have some seeding data, which have to be entered in the
\guilabel{Initial Status} frame widget. If the calculation is to be
started from a formula, enter the formula in the \guilabel{Formula}
line edit widget after having checked the check box widget (if not
already checked).

When not starting from a formula, simply uncheck the corresponding
check box and enter the initial \mz values (both monoisotopic and
average \mz values need to be entered).

The user must inform the calculator about how the \mz values (either
computed starting from the formula or entered directly as numerical
values) were calculated, that is, what was the ionization status of
the analyte when these \mz values were obtained.  These ionization
data are entered in the \guilabel{Ionization Rule} frame, which
contains one line~edit widget and two spin~box widgets. The
\guilabel{Formula} line~edit widget lets the user indicate the
ionization agent (for us it is a protonation).  The \guilabel{Charge}
and \guilabel{Level} widgets let the user indicate what is the charge
brought by the \guilabel{Formula} and the number of such ionization
event. In the example, the protonation brings one (\guivalue{1})
positive charge, and the \mz value corresponds to a mono-protonation
of the analyte.

With all these data, the \mz ratio calculator can ``reverse-compute''
the \emph{molecular} mass of the analyte (not the ion mass). That
molecular mass will then be used to perform the requested \mz ratio
calculations (\guilabel{Target Ionization Status} frame, which behaves
identically to the one described above). The computed \mz ratios are
displayed in a treeview widget (\guilabel{Ion Charge Family}).

It is possible to use the results to create a full spectrum out of the
different \mz values calculated. Select \guilabel{Calculate spectrum}
from the \guilabel{Actions} menu drop-down list. See next section for
details. Note that in this case, the \guilabel{m/z} group box in the
\guilabel{Input data} group box are make inactive as the data are made
available directly in the dialog window using the data computed here.


\renewcommand{\sectitle}{The Isotopic Peaks Calculator}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle} 
\label{sect:xpertcalc-isotopic-pattern-calculator}
\index{\xpc!isotopic peak}
\index{isotopic peak}

It is sometimes useful to predict (or calculate \textit{a posteriori})
the isotopic peaks pattern of a given analyte (also called an isotopic
cluster). This calculation takes a number of parameters, as shown in
Figure~\vref{fig:xpertcalc-isotopic-pattern-calculator}: \smallskip


\begin{figure}
  \begin{center}
    \includegraphics [width=0.85\textwidth]
    {figures/xpertcalc-isotopic-pattern-calculator.png}
  \end{center}
  \caption[The isotopic pattern calculator]{\textbf{The isotopic
      pattern calculator.} The isotopic pattern calculator is rather
    straight forward to use. Given some initial parameters, the
    results are displayed in the \guilabel{Results} tab page
    widget. the \guilabel{Log} tab page widget will display all the
    details of the ongoing calculation.}
  \label{fig:xpertcalc-isotopic-pattern-calculator}
\end{figure}


\begin{itemize}
  
\item \guilabel{Formula} Formula of which the isotopic pattern
  calculation must be performed. This formula might correspond to a
  protein or a peptide, for example;

\item \guilabel{z} The charge of the analyte;

\item \guilabel{m/z} The mass-to-charge ratio that is calculated on
  the basis of the formula and the charge above. It is considered that
  the formula already accounts for the ionization chemical agent if z
  is greater than 0;

\item \guilabel{Min. Probability} The minimum probability value to
  find a given \mz peak in the isotopic pattern.  This allows a degree
  of optimization when calculations are too long to perform, by
  removing all isotopic peaks for which the probability of occurrence
  is lower than the set value;

\item \guilabel{Resolution} Resolution of the mass
  spectrometer. Should be of a compatible value with respect to the
  \mz of the analyte;

\item \guilabel{FWHM} Full width at half-maximum of each peak. This is
  calculated from the m/z ratio and the value in the
  \guilabel{Resolution} line edit widget. It is possible to set the
  FWHM directly;

\item \guilabel{gaussian} or \guilabel{lorentzian} Kind of curve that
  is calculated for each peak in the cluster. The gaussian curves have
  a steeper ascending and descending segments than the lorentzian
  curves. Experiment with both to find the best one;

\item \guilabel{Points} Set the number of points desired to make the
  curve of a single isotopic peak. Entering \guivalue{100} means that
  there will be 50 points on the left of the centroid of the isotopic
  peak and 49 on its right;

\item \guilabel{Increment} Interval between any two points of the
  curve making the isotopic peak. This value is calculated on the
  basis of m/z, Points and Resolution;

\item \guilabel{Max. Peaks} Maximum number of peaks in the isotopic
  pattern.  This allows a degree of optimization when calculations are
  too long to perform by limiting the number of isotopic peaks in the
  pattern to the set value (the number of peaks in the isotopic peaks
  pattern increases exponentially with the numer of atoms);

\item \guilabel{Output File...} Button to click so as to choose a file
  in which all the data are to be stored for later plotting of the
  isotopic peaks pattern spectrum;

\item \guilabel{Locale} If checked, the results should be displayed
  (or written to file) using the current locale. It might be useful
  \emph{not} to check this check~box widget in case the plotting
  program does not understand numerical values as produced by the
  currrent locale.  For example, some plotting programs do not
  understand values like \guivalue{140,000.00} (that is one hundred
  and fourty thousands with a comma separating thousands and dot as
  the decimal separator).

\end{itemize}
%
During the calculation, the details of that calculation are displayed
in the \guilabel{Log} tab page widget. Upon clicking onto the
\guilabel{Execute} button, the tab widget will automatically switch to
that page.  The \guilabel{Results} tab page widget is updated at the
end of the calculation and will contain both the input data (as a
record) and the results data if no output file was first selected.  If
an \guilabel{Output File} name was set (see above), the (x,y)
coordinates of the isotopic peaks pattern graph are not displayed in
the \guilabel{Results} tab page widget.  The results for the given
example are graphed using \progname{mMass}\footnote{\progname{mMass}
  is an excellent mass spectrum viewer written by Martin
  Strohalm. This is Free Software available at
  \url{http://mmass.org}.} and shown in
Figure~\vref{fig:xpertcalc-isotopic-pattern-calculator-example-graph}.


\begin{figure}
  \begin{center}
    \includegraphics [width=0.66\textwidth]
    {figures/xpertcalc-isotopic-pattern-calculator-example-graph.png}
  \end{center}
  \caption[An isotopic pattern calculator output example]{\textbf{An
      isotopic pattern calculator output example.} The graph shows the
    isotopic pattern that should be expected to be obtained by
    performing a mass spectrometric analysis of a protein (formula
    C737H1148N188O272S11) protonated ten times.}
  \label{fig:xpertcalc-isotopic-pattern-calculator-example-graph}
\end{figure}
\index{\xpc|)}


\cleardoublepage


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "polyxmass"
%%% End: 
