\chapter[\xpm] {\xpm: \\A Data Miner} 
\label{chap:xpertminer}
\index{\xpm|(}

\xpm\ is a module that has been conceived as a repository of
functionalities aimed at analyzing mass data. The data to be subjected
to mining can originate from:

\begin{itemize}

\item \mXp-based simulations, like polymer cleavage, oligomer
  fragmentation or arbitrary mass searches;

\item an export of a mass list from the mass spectrometer
  software;

\item any mass data that might have been processed outside of \mXp\ and
  that need to be reimported in \xpm.

\end{itemize}

As of version 3.3.0, \xpm\ only contains one ``mining'' functionality:
\mzl.


\renewcommand{\sectitle}{\xpm\ Invocation}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}
\index{\xpm!module~invocation}

The \xpm\ module is easily called by pulling down the \guimenu{\xpm}
menu item from the \mXp\ program's menu. Clicking on
\guimenu{\xpm}\guimenuitem{mzLab} will open the \mzl\ window, as
represented in Figure~\vref{fig:xpertminer-mzlab-wnd}.


\renewcommand{\sectitle}{\mzl: Mining m/z ratios}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}
\index{\xpm!mining~m/z~ratios}

The kind of data on which the features available in this laboratory
will operate is lists of m/z values in the form of \mzz\ pairs. The
mass of the ion is represented by m, while z is the charge of the
ion. With the two data in the pair, the m/z ratio and the z charge,
and knowing the ionization rule that ionized the analyte in the first
place, it is possible to perform any mass calculation on the \mzz\
pair.

The \mzl\ window is represented in
Figure~\vref{fig:xpertminer-mzlab-wnd}. This window is divided into
three distinct parts:

\begin{figure}
  \begin{center}
    \includegraphics[width=0.9\textwidth]
    {figures/xpertminer-mzlab-wnd.png}
  \end{center}
  \caption[\mzl\ window]{\textbf{\mzl\ window.} The \mzl\ window is
    the central location of the laboratory. From there it is possible
    to open any number of m/z list dialog windows. See text for details.}
  \label{fig:xpertminer-mzlab-wnd}
\end{figure}

\begin{itemize}

\item The left part (\guilabel{Working lists}) contains two list
  widgets which will hold the names of the different working m/z
  lists;

\item The central part contains:

  \begin{itemize}

  \item A group box widget (\guilabel{Default ionization}) where the
    ionization rule for the current polymer chemistry definition is
    detailed;

  \item A group box widget (\guilabel{Actions on a single list}) with
    a number of actions that might be performed on one list of m/z
    ratios.

  \end{itemize}
  
\item The right part contains a group box widget (\guilabel{Perform
    matches between two input lists}) in which the user may perform
  matches between lists of m/z ratios.

\end{itemize}

\renewcommand{\sectitle}{Creating A New Input m/z List}
\subsection*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{subsection}{\numberline{}\sectitle}
\index{\xpm!new~input~list~creation}

In order to be able to use the \mzl, it is necessary to create at
least one list of \mzz\ pairs, which is referred to by ``input m/z
list'', for short. To create a new input m/z list, the user clicks
onto the button labelled \guilabel{New list}. This will trigger the
opening of an input dialog window where the user enters an unambiguous
name for the new input m/z list. The new input m/z list dialog window
shows up empty like in
Figure~\vref{fig:xpertminer-empty-input-mz-list}.  That kind of list
is actually a tree view widget that is embedded in a dialog
window. The first column of the tree view widget holds the m/z value,
and the second column, the z value. Optionally, the name of the
corresponding oligomer and its coordinates in the polymer sequence can
be shown, depending on how data have been imported into the m/z list
(see below).

\begin{figure}
  \begin{center}
    \includegraphics[width=0.33\textwidth]
    {figures/xpertminer-empty-input-mz-list.png}
  \end{center}
  \caption[\mzl's empty input m/z list dialog window]{\textbf{\mzl's
      empty input m/z list dialog window.} The \mzl's input m/z list
    dialog window that shows up when the user creates a new input m/z
    list is empty. Filling that list is performed by drag-and-drop or
    clipboard operations .}
  \label{fig:xpertminer-empty-input-mz-list}
\end{figure}

Note that upon creation of a new input m/z list, its name is used to
refer to it in the two list widgets on the left of the \mzl\
window. This way, it will be possible later to refer to the various
input m/z lists by their name. Therefore, it might make sense to use a
meaninfgul name for the lists.


\subsubsection*{Filling Input m/z Lists With Data}

Once a new input m/z list has been named and created, it is necessary
to fill it with \mzz\  pairs. This is performed \textit{via}
drag-and-drop or clipboard operations. There might be a number of
different data sources to be used for filling the input m/z list:

\begin{itemize}

\item Data from the various simulations available in \mXp. These
  simulations are cleavages, fragmentations and mass searches, which
  all produce oligomers that are displayed in tree view widgets, as
  shown in Figure~\vref{fig:xpertedit-cleavages} or
  Figure~\vref{fig:xpertedit-fragmentation} or
  Figure~\vref{fig:xpertedit-mass-search}. There are two ways to copy
  oligomer data from these tree view widgets into a m/z list of \xpm,
  after having selected the oligomers of interest in the tree view
  widget:

  \begin{itemize}

  \item The selected oligomers might be dragged and dropped into the
    list widget;

  \item The selected oligomers might be exported to the clipboard. It
    is thus necessary to specify which masses are to be exported
    (select either the \guilabel{Mono} or the \guilabel{Avg} radio
    button). Also, the export format will be correct for pasting in
    the MzLab m/z list window if the \guilabel{For XpertMiner} check
    box is actually checked. Once the data have been exported in the
    proper format, go in the \mzl\ m/z list window and click the
    \guilabel{Get data from clipboard} button.

  \end{itemize}

  Both the ways above produce identical results, as described in
  Figure~\vref{fig:xpertminer-filled-input-mz-list}. One can see that
  the mass of the oligomers is set in the list, along with the charge,
  the oligomer name and, finally, the coordinates of the oligomer in
  the corresponding polymer.

\item Data in the form of textual lines. This is typically the case
  when data originate from outside of \mXp. For example, data could
  originate in the mass data acquisition software (typically a peak
  list, either in the form of a simple list of m/z values or a list of
  \mzz\ pairs. Whatever the origin of the data, the format of the data
  must be like this:

  \verb! mass <delim> charge <delim> name <delim> coordinates <delim>!

  For example, data can be formatted like this:

\begin{verbatim}

    3818.05262$1$0#2#z=1$[3-39]
    3834.05262$1$0#2#z=1$[3-39]

\end{verbatim}
  
  Note that, in the example above, the <delim> (field delimiter) is
  the \verb!$! character. Any character might be used (including
  spaces). The delimiter character (or string) that is set in the
  \mzl's input m/z list window must be the same as the one defined in
  the window from where the data originate (when using the option to
  export the selected oligomers data to the clipboard).

  The compulsory datum (that is, the imported datum, either dragged
  and dropped or pasted from the clipboard, is the \emph{mass}. The
  charge, name, coordinates fields are optional. If the charge is
  present, it will be taken into account while preparing the data for
  further use by the \mzl. If the charge is absent, it will be defined
  according to user-defined specifications in the \mzl\ main window
  (see below). If there is no charge value, then the other name and
  coordinates fields cannot be filled (or an error will result). The
  presence of the name and coordinates fields is optional. Note,
  however that the coordinates field is fundamental to be able to
  highlight the corresponding region in the sequence editor upon
  double-clicking of any given item in the m/z list [for this to be
  possible, the data must have been originated by drag and drop from a
  \mXp\ simulation window or the \mzl\ window must have been connected
  to a polymer sequence editor window (see below)].

  If the data dropped or pasted from the clipboard are in the form of
  a list of m/z data, without the z value (there is only one m/z ratio
  value per line), then the z value will be considered to be the
  charge that would result from the ionization of the analyte using
  the ionization rule detailed in the \guilabel{Default ionization}
  group box widget. This case is represented in
  Figure~\vref{fig:xpertminer-mz-only-filled-input-mz-list}. %\FIXME

  If the data dropped or clipboard-pasted are a list of \mzz\ pairs
  (one pair per line, like ``\texttt{1234.567 2}''), then the z value
  will be read from the dropped data (\texttt{2}, in this
  example). This case is represented in
  Figure~\vref{fig:xpertminer-mz-z-filled-input-mz-list}. %\FIXME

\end{itemize}


\begin{figure}
  \begin{center}
    \includegraphics[width=0.33\textwidth]
    {figures/xpertminer-filled-input-mz-list.png}
  \end{center}
  \caption[\mzl's data-filled input m/z list dialog
  window]{\textbf{\mzl's data-filled input m/z list dialog window.}
    The \mzl's input m/z list dialog window fills with data when
    result items are dragged and dropped onto it.}
  \label{fig:xpertminer-filled-input-mz-list}
\end{figure}

\begin{figure}
  \begin{center}
    \includegraphics[width=0.5\textwidth]
    {figures/xpertminer-mz-only-filled-input-mz-list.png}
  \end{center}
  \caption[\mzl's m-only textual data-filled input m/z list dialog
  window]{\textbf{\mzl's (m/z) textual data-filled input m/z list
      dialog window.}  See text for details.}
  \label{fig:xpertminer-mz-only-filled-input-mz-list}
\end{figure}

\begin{figure}
  \begin{center}
    \includegraphics[width=0.5\textwidth]
    {figures/xpertminer-mz-z-filled-input-mz-list.png}
  \end{center}
  \caption[\mzl's m-z textual data-filled input m/z list dialog
  window]{\textbf{\mzl's \mzz\ textual data-filled input m/z list
      dialog window.}  See text for details.}
  \label{fig:xpertminer-mz-z-filled-input-mz-list}
\end{figure}

\subsubsection*{Imposing The Mass Type: Mono Or Avg}

When dropping data---either from \mXp-driven simulations (cleavage,
fragmentation or mass search) or from textual data originating from
outside of \mXp---it is necessary to inform the input m/z list of what
kind of mass is dealt with. That is, when dropping a line like
\texttt{1234.56 1}, the question is: --- \textsl{``The m/z 1234.56
  value is a monoisotopic m/z or an average m/z?''} The type of the
masses dropped in an input m/z list is governed by the two radio
buttons labelled \guilabel{Mono} and \guilabel{Avg}. The one of the
two radiobuttons that is checked at the moment the drop or the
clipboard-paste occurs determines the type of the masses that are
dealt with. It will be possible to check the other radio button widget
once a first data drop occurred, but then the user will be alerted
about doing so, as this has huge implications for the calculations to
be performed later.


\subsubsection*{Imposing The Oligomer Kind}

When dropping data, it is required that the \mzl\ know if the
oligomers are cleavage-, mass search- or fragmentation-based
oligomers. Indeed, the way the calculations are performed in the
laboratory is dependent on the kind of the oligomers used:
fragmentation oligomers are not equivalent to cleavage oligomers,
because cleavage oligomers are not charged by themselves, while
fragmentation oligomers \emph{are} charged by themselves (the charge
that the fragmentation oligomer gets upon its creation is intrinsic to
it thanks to the fragmentation mechanism that gave rise to it). It is
thus of crucial importance for the faithfulness of the further
computations that the laboratory be fed with identified
oligomers. This is the reason why the input m/z list dialog windows
have a \guilabel{Fragments} check box widget that the user must check
if dealing with fragmentation data in the form of textual data. It is
not necessary to check this box when dropping in the input m/z list
data obtained by fragmenting a sequence in \mXp, because in that case
the program knows that the oligomers are actually fragmentation
oligomers and the check box gets checked automatically.


\renewcommand{\sectitle}{Working On One Input m/z List}
\subsection*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{subsection}{\numberline{}\sectitle}
\index{\xpm!one~input~list}

Once an input m/z list has been filled with data, it becomes possible
to perform simulations on these data. Because there might be any
number of input m/z lists open at any given time, it is necessary to
identify the input m/z list onto which to perform these
simulations. The selection of the input m/z list is performed in two
steps: first, the user indicates which list (that is, the catalogue)
of input m/z lists will contain the input m/z list of interest (select
either \guilabel{Input 1} or \guilabel{Input 2} in the
\guilabel{Substrate list and modality} group box widget) ; second, in
that list of input m/z lists, select the input m/z list by its
name. If no list is selected, then no simulation might be performed.


\subsubsection*{Available Calculations}
\index{\xpm!available~calculations}

There are a number of operations that might be performed, all of which
are selectable in the \guilabel{Actions on a single list} group box
widget. The simulations are organized into two groups: 

\begin{itemize}

\item \guilabel{Formula-based} actions which involve processing the
  input m/z lists with formulas (that is, chemical entities
  represented using formulas):
  
  \begin{itemize}
    
  \item \guilabel{Apply formula} will perform the same as above but
    starting from a formula. This is where it is crucial that the mass
    type be set correctly, because the type of the mass calculated for
    the formula will be of the same type as the type of the data;

  \item \guilabel{Increment charge by} will iterate in all the items
    present in the list and apply the charge increment to them. One
    item in the list that is charged 1 will be deionized and reionized
    to 2 (this calculation involves the ionization rule of the
    oligomer, and thus its ionization formula);

  \item \guilabel{Reionization} will iterate in all the items present
    in the list and apply the new ionization rule, defined in this
    group box widget.

  \end{itemize}


\item \guilabel{Mass-based} actions which involve processing the input
  m/z lists with numerical data representing masses:

  \begin{itemize}

  \item \guilabel{Apply mass} will iterate in all the items present in
    the list and apply the entered mass to them;

  \item \guilabel{Apply threshold} will remove all data items in the
    list for which m/z or M is less than the value set.

  \end{itemize}

\end{itemize}



\subsubsection*{Output Of The Calculations}

Simulations performed on a single input m/z list produce a m/z list
that is identical to the input list, unless for the m and/or z values,
which might have changed. This means that it is perfectly possible to:

\begin{itemize}

\item Overwrite the initial data with the newly obtained ones (this is
  performed by checking the \guilabel{Perform computation in place}
  check button widget);

\item Create a new list with the newly obtained data. As a convenience
  for the user, the new list will be an input m/z list in which it
  will be possible to perform ulterior simulations. This is useful
  when the simulations that need to be performed are sequential in
  kind. To have a new list created uncheck \guilabel{Perform
    computation in place} check button widget.

\end{itemize}


\subsubsection*{Internal Workings}

When an operation is performed on the items of an input m/z list, say
we want to make sodium adducts (that would be a formula ``$\rm
-H+Na$'') of all the items in the list, the process involves the
following steps, as detailed below for one single item of the list
(which has data pair (334.341$\,,\,$3) and \guivalue{protonation} as
ionization agent).

\begin{itemize}
  
\item Convert the tri-protonated analyte into a non-ionized analyte,
  thus getting M=1000; \footnote{Note that if the oligomer is a
    fragmentation oligomer, the tri-protonated analyte is converted to
    the canonical oligomer bearing a single charge.}

\item Compute the mass of the ``$\rm -H+Na$'' formula: 21.98 Da;

\item Add 1000+21.98;

\item Reionize to the initial charge state: (341.67$\,,\,$3).

\end{itemize}


\renewcommand{\sectitle}{Working On Two Input m/z Lists}
\subsection*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{subsection}{\numberline{}\sectitle}
\index{\xpm!two~input~lists}

It is possible to perform calculations on two input m/z lists. These
calculations are called matches. The \mzz\ pairs of two different
input m/z lists might be matched. Typically, a match operation would
involve data from the mass spectrometer and data from a \mXp-based
simulation (cleavage or fragmentation, for example). In order to
perform a match operation, the first input m/z list (the data from the
mass spectrometer) should be selected by its name in the
\guilabel{Input 1 List} list and the second input m/z list (the data
from the simulation) should be selected by its name in the
\guilabel{Input 2 List} list. Note that if the two input m/z lists are
not of the same type (one is mono and the other is avg), the user will
be alerted about this point.

\subsubsection*{Output Of The Calculations}

Calculations involving matches between two input lists produce an
output that is displayed in an output m/z list, which is different
from an input m/z
list. Figure~\vref{fig:xpertminer-mz-z-filled-output-mz-list} shows
the results after having performed a match operation between an input
m/z list obtained from the mass spectrometer (\guilabel{Input 1 list})
and an input m/z list obtained by simulating a cleavage with trypsin
(\guilabel{Input 2 list}). The output m/z list dialog window holds all
the matches along with the original data and the error.

\begin{figure}
  \begin{center}
    \includegraphics[width=1\textwidth]
    {figures/xpertminer-mz-z-filled-output-mz-list.png}
  \end{center}
  \caption[\mzl's match operation output list dialog
  window]{\textbf{\mzl's match operation output list dialog window.}
    See text for details.}
  \label{fig:xpertminer-mz-z-filled-output-mz-list}
\end{figure}

\


\renewcommand{\sectitle}{Tracing The Data}
\subsection*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{subsection}{\numberline{}\sectitle}
\index{\xpm!tracing~the~data}

When the data used for filling an input m/z list come from a
\mXp-based simulation it is possible to trace back the \mzz\ pair
items to the corresponding sequence in the polymer sequence editor
that gave rise to these oligomers in the first place. This is only
possible if:

\begin{itemize}

\item The way the data were fed into the input m/z list was by
  dragging oligomers from the tree view widgets, as described earlier;
  
\item The polymer sequence window is still opened when the tracing
  back is tried.

\end{itemize}

If the data do not originate immediately from the \mXp-based
simulations (that is, the data do not originate directly from results
tree view of cleavages or fragmentations or mass searches), it is
still possible to perform the highlighting of corresponding oligomers
in the sequence editor window, provided that the following
requirements are met:

\begin{itemize}

\item The data imported in the m/z list of \mzl\ are rich, that is
  they comprise the coordinates data;

\item The proper polymer sequence is opened in a sequence editor window;

\item The identifier in the \guilabel{This window identifier} line
  edit widget of the sequence editor window above is copied into the
  \guilabel{Sequence editor identifier} line edit widget.

\end{itemize}

In the case the above conditions are met, double-clicking onto a item
of the m/z list will highlight the corresponding sequence region in
the sequence editor window.

\noindent In order to trace back any given item in an input or in an
output m/z list to its corresponding polyemr sequence, just activate
the item while having a look at the polymer sequence whence the
oligomers initially originated. Each time an item is activated by
double-clicking it, its corresponding sequence region will be
highlighted (selected, actually) in the polymer sequence.
\index{\xpm|)}

\enlargethispage{5mm}

\cleardoublepage


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "polyxmass"
%%% End: 
