\chapter{Preface}

\label{chap:preface} 

%\enlargethispage*{\baselineskip}

This manual is about the \mXp\ mass spectrometric software suite, a
software program that aims at letting users predict/analyze mass
spectrometric data on (bio)polymers. As such, this manual is intended
for people willing to learn how to install and use this software
package.

Mass spectrometry has gained popularity across the past ten years or
so. Indeed, developments in polymer mass spectrometry have made this
technique appropriate to accurately measure masses of polymers as
heavy as many hundreds of kDa, and of any chemical type.

There are a number of utilities---sold by mass spectrometer
constructors with their machines, usually as a marketing
``plus''---that allow predicting/analyzing mass spectrometric data
obtained on polymers.  These programs are usually different from a
constructor to another.  Also, there are as many mass spectrometric
data prediction/analysis computer programs as there are different
polymer types. You will get a program for oligonucleotides, another
one for proteins, maybe there is one program for saccharides, and so
on. Thus, the biochemist/massist, for example, who happens to work on
different biopolymer types will have to learn to use several different
software packages.  Also, if the software user does not own a mass
spectrometer, chances are he will need to buy all these software
packages.

The \mXp\ mass spectrometric software is designed to provide
\emph{free} solutions to all these problems by:

\begin{itemize} 
\item Allowing \textit{ex nihilo} polymer chemistry definitions (in
  the \xpd\ module that is part of the \mXp\ program);
\item Allowing simple yet powerful mass computations to be made in a
  mass desktop calculator that is both polymer chemistry
  definition-aware and fully programmable (that's the \xpc\ module
  also part of the \mXp\ program);
\item Allowing highly sophisticated editing of polymer sequences on a
  polymer chemistry definition-specific basis, along with chemical
  reaction simulations, finely configured mass spectrometric
  computations\dots (all taking place in the \xpe\ module that is the
  main module of the \mXp\ program);
\item Allowing customization of the way each monomer will show up
  graphically during the program operation (in the \xpe\ module);
\item Allowing polymer sequence editing with immediate visualization
  of the mass changes elicited by the editing activity (in the \xpe\ 
  module);
\item Unlimited number of polymer sequences opened at any given time
  and of any given polymer chemistry definition type (in the \xpe\ 
  module).
\end{itemize} 

\noindent This manual will progressively introduce all these
functionalities in a timely and clear manner.


\renewcommand{\sectitle}{Project History}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}

This is a brief history of \mXp.

\begin{itemize}

\item \textbf{1998--2000} The name \mXp\ comes from a project I
  started while I was a post-doctoral fellow at the \'Ecole
  Polytechnique\index{\'Ecole Polytechnique} (Institut Europ�en de
  Chimie et Biologie, Universit�~Bordeaux~1, Pessac, France).

  The \mXp\ program was published in \textit{Bioinformatics} (Rusconi,
  F.\index{Rusconi, F.} and Belghazi, M.\index{Belghazi, M.}
  \textsl{Desktop prediction/analysis of mass spectrometric data in
    proteomic projects by using \mXp\.}
  \textit{Bioinformatics}\index{Bioinformatics}, 2002, 644--655).

  At that time, \OSname{MS-Windows} was at the \OSname{Windows NT~4.0}
  version and the next big release was going to be ``you'll see what
  you'll see'' : \OSname{MS-Windows 2000}.

  When I tried \mXp\ on that new version (one colleague had it with a
  new machine), I discovered that my software would not run normally
  (the editor was broken). The Microsoft technical staff' would advise
  to "buy a new version of the compiler environment and rebuild". This
  was a no-go: I did not want to continue paying for using something I
  had produced.

\item \textbf{2001--2006}

  During fall 1999, I decided that I would stop using Microsoft
  products for my development. At the beginning of 2000 I started as a
  CNRS\index{CNRS} research staff in a new laboratory and decided to
  start fresh: I switched to GNU/Linux (I never looked back). After
  some months of learning, I felt mature to start a new development
  project that would eventually become an official GNU package: GNU
  polyxmass\index{GNU polyxmass}.

  The GNU polyxmass software, much more powerful than what the initial
  \mXp\ software used to be, was published in \textit{BMC
    Bioinformatics} in 2006 (Rusconi, F., \textsl{GNU polyxmass: a
    software framework for mass spectrometric simulations of linear
    (bio-)polymeric analytes.}  \textit{BMC Bioinformatics}\index{BMC
    Bioinformatics}, 2006,226).

  Following that publication I got a lot of feedback (very positive,
  in a way) along the lines: ---\textsl{``Hey, your software looks
    very interesting; only it's a pity we cannot use it because it
    runs on GNU/Linux, and we only use \OSname{MS-Windows} and
    \OSname{MacOSX}!''}

\item \textbf{2007--}
  
  In december~2006, I decided to make a full rewrite of GNU polyxmass.
  The software of which you are reading the user manual is the result
  of that rewrite. I decided to ``recycle'' the \mXp\ name because
  this software is written in C++, as was the first \mXp\ software.
  Also, because the first \OSname{MS-Windows}-based \mXp\ project is
  not developped anymore, taking that name was kind of a ``revival''
  which I enjoyed.  However, the toolkit I used this time is not the
  Microsoft Foundation Classes (first \mXp\ version) but the
  Trolltech\index{Trolltech} Qt\index{Qt libraries} framework (see the
  ``About Qt'' help menu in \mXp).

  Coding with Qt libraries has one big advantage: it allows the
  developer to code once and to compile on the three main platforms
  available today: \OSname{GNU/Linux}, \OSname{MacOSX},
  \OSname{MS-Windows}.  Another advantage is that Qt libraries are
  wonderful software, technically and philosophically (Free
  Software\index{Free Software}).

\end{itemize}



\renewcommand{\sectitle}{Typographical Conventions}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}


Throughout the book the following typographical
conventions are used:
\begin{itemize} 
\item \emph{emphasized text} {\footnotesize is used each time a new
    term or concept is introduced} 
\item \prompt\ {\footnotesize shows the prompt at which a command
    should be entered as non-root} 
\item \promptsu\ {\footnotesize shows the prompt at which a command
    should be entered as root} 
\item \command{this typography} {\footnotesize applies to commands
    that the user enters at the shell prompt along with eventual options} 
\item \kbdEnterKey\ symbolizes pressing the \kbdKey{Enter} key
\item \verb#this typography# {\footnotesize applies to an output
    resulting from entering a command at the shell prompt} 
\item \progname{emacs} or \filename{libQtCore} {\footnotesize names of
    a program or of a library}
\item \progname{KDE}, \progname{The Gimp} {\footnotesize is the name
    of a generic software (not a specific executable file)}
\item \filename{/usr/local/share/massxpert}, \filename{/usr/bin/massxpert}
  {\footnotesize are names of a directory or of a file} 
\item \url{http://www.gnu.org} {\footnotesize is an URL (Uniform
    Resource Locator)}
\end{itemize}

\renewcommand{\sectitle}{Program Availability, Technicalities}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}

\label{sect:prog-availability-technicalities}

The ancestor of \mXp, GNU polyxmass\index{GNU polyxmass}, was
initially developed on a \OSname{GNU/Linux} system (RedHat
distribution versions successively 6.0, 7.0, 7.2, 7.3, 8.0, 9.0) using
software from the Free Software Foundation\index{Free Software
  Foundation} (FSF\footnote{For an in-depth coverage of the philosophy
  behind the FSF, specifically creating a \emph{free operating
    system}, you might desire to visit \url{http://www.gnu.org}.}).
The main libraries used were \filename{libglib},
\filename{libgobject}, \filename{libxml2} and \filename{libgtk+}.
Since mid-2002, the development was performed on a \OSname{Debian
  GNU/Linux} system (\url{http://www.debian.org}), which I find to be
the ultimate highly-configurable easy-to-use distribution on earth.
\mXp\ is still developed using the \OSname{Debian GNU/Linux} system,
using Free Software libraries that allow cross-platform computer
program development with unprecedented ease (Qt libraries from the
Trolltech company; \url{http://www.trolltech.com}).  Developing for
\OSname{GNU/Linux} has been utterly exciting and extremely efficient.


\renewcommand{\sectitle}{Organization Of This Manual}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}

After having quickly described the installation of \mXp, this manual
aims at providing the required conceptual toolset for understanding
what to expect from a computer program like \mXp\ and how to use it.
Thus, the general
organization of this book is: \\
 
\begin{itemize} 

\item Installation of the \mXp\ software program;
 
\item The basics of polymer chemistry;

\item The basics of mass spectrometry;

\item Generalities about \mXp;

\item The \xpd\ module (definition of atoms and of new polymer
  chemistries);

\item The \xpc\ module (polymer chemistry-aware programmable
  calculator);

\item The \xpe\ module (sequence editor, biochemical/mass spectrometric
  simulations);

\item The \xpm\ module (data mining calculations);

\item The data customization that \mxp\ is designed to not only make
  possible but also to foster;

\item Appendices.

\end{itemize}

\renewcommand{\sectitle}{\mXp's Licensing}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}

The front matter of this manual contains a Copyright statement. I
retain the copyright to \mXp\ and all related writings (source and
configuration files, programmer's documentation, user manual\dots) I
encourage others to make copies of the work, to distribute it freely,
to modify the work and redistribute that derivative work according to
the GNU General Public License version~3\index{General Public
  License}. The aim of this licensing is to favor spread of knowledge
to the widest public possible. Also, it encourages interested
hackers\footnote{\emph{Hacker}\index{hacker} is a specialized term to
  design the programmer who codes programs; this term should
  \emph{not} be mistaken with \emph{cracker}\index{cracker} who is a
  person who uses computer science knowledge to break information
  systems' security barriers.}  to change the code, to improve it and
to send patches to the author so that their improvements get into the
program to the benefit of the widest public possible. For an in-depth
study of the \textsc{free software}\index{free software} philosphy I
kindly urge the reader to visit \url{http://www.gnu.org/philosophy}.
 
\newpage 

\renewcommand{\sectitle}{Contacting The Author}\index{author}
\section*{\textcolor{sectioningcolor}{\sectitle}}
\addcontentsline{toc}{section}{\numberline{}\sectitle}

\mXp\ is the fruit of years of work on my part.\footnote{As said
  earlier, \mXp\ is the successor to the GNU polyxmass\index{GNU
    polyxmass} project of which it inherits all the original features,
  while still integrating new interesting developments.} While I've
put a lot of energy into making this program as stable and reliable a
piece of software as possible, \mXp\ comes with no warranty of any
kind.

The general policy for directing questions, comments, feature
requests, \mXp\ program and/or \mXp\ documentation bug
reports\index{bug reports} should be self-explanatory by looking at
the addresses below:

\begin {center}
  \includegraphics [scale=1] {figures/addresses.png}
\end {center}

\noindent To direct any comment(s) to the author through snail mail,
use the following address\index{author address}:\\

\begin{center}
\noindent D$\mathrm{^r}$~Filippo \textsc{Rusconi}\\
\vspace{2mm}%
Charg� de recherches au CNRS\index{CNRS}\\
\textsc{Centre national de} \\
\textsc{la Recherche scientifique} \vspace{2mm} \\
UMR CNRS 5153 - UR INSERM 565 - USM MNHN 0503\\
Mus�um national d'Histoire naturelle\\
43, rue Cuvier \\
F-75231 Paris \textsc{Cedex} 05 \\
France
\end{center}

\cleardoublepage

